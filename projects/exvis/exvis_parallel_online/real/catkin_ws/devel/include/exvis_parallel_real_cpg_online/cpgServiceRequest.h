// Generated by gencpp from file exvis_parallel_real_cpg_online/cpgServiceRequest.msg
// DO NOT EDIT!


#ifndef EXVIS_PARALLEL_REAL_CPG_ONLINE_MESSAGE_CPGSERVICEREQUEST_H
#define EXVIS_PARALLEL_REAL_CPG_ONLINE_MESSAGE_CPGSERVICEREQUEST_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace exvis_parallel_real_cpg_online
{
template <class ContainerAllocator>
struct cpgServiceRequest_
{
  typedef cpgServiceRequest_<ContainerAllocator> Type;

  cpgServiceRequest_()
    : cpgType()
    , cpgFunc()
    , initFreqR(0.0)
    , initFreqL(0.0)
    , updateFreqR(0.0)
    , updateFreqL(0.0)  {
    }
  cpgServiceRequest_(const ContainerAllocator& _alloc)
    : cpgType(_alloc)
    , cpgFunc(_alloc)
    , initFreqR(0.0)
    , initFreqL(0.0)
    , updateFreqR(0.0)
    , updateFreqL(0.0)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _cpgType_type;
  _cpgType_type cpgType;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _cpgFunc_type;
  _cpgFunc_type cpgFunc;

   typedef double _initFreqR_type;
  _initFreqR_type initFreqR;

   typedef double _initFreqL_type;
  _initFreqL_type initFreqL;

   typedef double _updateFreqR_type;
  _updateFreqR_type updateFreqR;

   typedef double _updateFreqL_type;
  _updateFreqL_type updateFreqL;





  typedef boost::shared_ptr< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> const> ConstPtr;

}; // struct cpgServiceRequest_

typedef ::exvis_parallel_real_cpg_online::cpgServiceRequest_<std::allocator<void> > cpgServiceRequest;

typedef boost::shared_ptr< ::exvis_parallel_real_cpg_online::cpgServiceRequest > cpgServiceRequestPtr;
typedef boost::shared_ptr< ::exvis_parallel_real_cpg_online::cpgServiceRequest const> cpgServiceRequestConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator1> & lhs, const ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator2> & rhs)
{
  return lhs.cpgType == rhs.cpgType &&
    lhs.cpgFunc == rhs.cpgFunc &&
    lhs.initFreqR == rhs.initFreqR &&
    lhs.initFreqL == rhs.initFreqL &&
    lhs.updateFreqR == rhs.updateFreqR &&
    lhs.updateFreqL == rhs.updateFreqL;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator1> & lhs, const ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace exvis_parallel_real_cpg_online

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "ff3e74b1920ea8bff8137459b706875e";
  }

  static const char* value(const ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xff3e74b1920ea8bfULL;
  static const uint64_t static_value2 = 0xf8137459b706875eULL;
};

template<class ContainerAllocator>
struct DataType< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "exvis_parallel_real_cpg_online/cpgServiceRequest";
  }

  static const char* value(const ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "string cpgType\n"
"string cpgFunc\n"
"float64 initFreqR\n"
"float64 initFreqL\n"
"float64 updateFreqR\n"
"float64 updateFreqL\n"
;
  }

  static const char* value(const ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.cpgType);
      stream.next(m.cpgFunc);
      stream.next(m.initFreqR);
      stream.next(m.initFreqL);
      stream.next(m.updateFreqR);
      stream.next(m.updateFreqL);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct cpgServiceRequest_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::exvis_parallel_real_cpg_online::cpgServiceRequest_<ContainerAllocator>& v)
  {
    s << indent << "cpgType: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.cpgType);
    s << indent << "cpgFunc: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.cpgFunc);
    s << indent << "initFreqR: ";
    Printer<double>::stream(s, indent + "  ", v.initFreqR);
    s << indent << "initFreqL: ";
    Printer<double>::stream(s, indent + "  ", v.initFreqL);
    s << indent << "updateFreqR: ";
    Printer<double>::stream(s, indent + "  ", v.updateFreqR);
    s << indent << "updateFreqL: ";
    Printer<double>::stream(s, indent + "  ", v.updateFreqL);
  }
};

} // namespace message_operations
} // namespace ros

#endif // EXVIS_PARALLEL_REAL_CPG_ONLINE_MESSAGE_CPGSERVICEREQUEST_H
