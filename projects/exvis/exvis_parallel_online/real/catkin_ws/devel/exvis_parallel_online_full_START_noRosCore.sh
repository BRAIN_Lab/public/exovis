#!/bin/sh

tmux start-server

# ROSCORE -----
# tmux new-session -d -s ExvisCon -n Win1 -d "/usr/bin/env zsh -c \"echo '1st shell'\"; roscore; /usr/bin/env zsh -i"
tmux new-session -d -s ExvisCon
# CONTROL NODES -----
tmux new-window -n Win2 
tmux split-window -v -t ExvisCon:Win2
tmux split-window -h -t ExvisCon:Win2

tmux send-keys -t ExvisCon:Win2.1 "echo '1st shell'; $PWD/lib/exvis_commucan_online/exvis_commucan_online_node" C-m
tmux send-keys -t ExvisCon:Win2.2 "echo '2nd shell'; $PWD/lib/exvis_parallel_real_online/exvis_parallel_real_online_node" C-m
tmux send-keys -t ExvisCon:Win2.3 "echo '3rd shell'; $PWD/lib/exvis_parallel_real_cpg_online/exvis_parallel_real_cpg_online_node" C-m
# RECORD NODE -----
tmux new-window -n Win3
tmux send-keys -t ExvisCon:Win3 "$PWD/lib/exvis_parallel_real_rw_online/exvis_parallel_real_rw_online_node" C-m
#tmux select-layout -t ExvisCon:Win2 tiled

# UI NODES -----
tmux new-session -d -s ExvisUI -n Win1 -d 
tmux split-window -v -t ExvisUI:Win1
tmux split-window -v -t ExvisUI:Win1
tmux select-pane -t 1
tmux split-window -h -t ExvisUI:Win1
tmux select-pane -t 3
tmux split-window -h -t ExvisUI:Win1

tmux send-keys -t ExvisUI:Win1.1 "echo '1st shell'; $PWD/lib/exvis_parallel_real_mode_online/exvis_parallel_real_mode_online_node" C-m
tmux send-keys -t ExvisUI:Win1.2 "echo '2nd shell'; $PWD/lib/exvis_parallel_real_speed_online/exvis_parallel_real_speed_online_node" C-m
tmux send-keys -t ExvisUI:Win1.3 "echo '3rd shell'; $PWD/lib/exvis_parallel_real_assist_online/exvis_parallel_real_assist_online_node" C-m
tmux send-keys -t ExvisUI:Win1.4 "echo '4th shell'; $PWD/lib/exvis_parallel_real_freq_online/exvis_parallel_real_freq_online_node" C-m
tmux send-keys -t ExvisUI:Win1.5 "echo '5th shell'; $PWD/lib/exvis_parallel_real_noti_online/exvis_parallel_real_noti_online_node" C-m

# Attach ------
gnome-terminal \
	--title="Exvis Controller" \
	--geometry=120x50 \
	-- zsh -c "tmux attach -t ExvisCon; exec zsh"

gnome-terminal \
	--title="Exvis UI" \
	--geometry=70x150 \
	-- zsh -c "tmux attach -t ExvisUI; exec zsh"


# ----------------------------------------------
# Running screen
# ----------------------------------------------
#gnome-terminal -- zsh -c 'echo test; echo test2; exec zsh'
#gnome-terminal --title="1st Terminal" \
#	-- zsh -c "tmux new-session -d -s Exvis -n Win1"

# ----------------------------------------------
# Control screen
# ----------------------------------------------
#gnome-terminal
#tmux select-pane -t 3
#tmux split-window -h -t Exvis:Win2 "/usr/bin/env zsh -c \"echo ' shell'\"; /usr/bin/env zsh $PWD/lib/exvis_parallel_real_mode_online/exvis_parallel_real_mode_online_node"
#tmux split-window -h -t Exvis:Win2 "/usr/bin/env zsh -c \"echo ' shell'\"; /usr/bin/env zsh $PWD/lib/exvis_parallel_real_speed_online/exvis_parallel_real_speed_online_node"
#tmux split-window -h -t Exvis:Win2 "/usr/bin/env zsh -c \"echo ' shell'\"; /usr/bin/env zsh $PWD/lib/exvis_parallel_real_assist_online/exvis_parallel_real_assist_online_node"
#tmux split-window -h -t Exvis:Win2 "/usr/bin/env zsh -c \"echo ' shell'\"; /usr/bin/env zsh $PWD/lib/exvis_parallel_real_freq_online/exvis_parallel_real_freq_online_node"
#tmux split-window -h -t Exvis:Win2 "/usr/bin/env zsh -c \"echo ' shell'\"; /usr/bin/env zsh $PWD/lib/exvis_parallel_real_noti_online/exvis_parallel_real_noti_onine_node"

# Change layout to tiled
#tmux select-layout -t Exvis:Win2 tiled
# Attach
#tmux attach -t Exvis


