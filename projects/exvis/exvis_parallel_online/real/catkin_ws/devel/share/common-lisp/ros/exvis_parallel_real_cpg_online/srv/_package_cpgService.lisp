(cl:in-package exvis_parallel_real_cpg_online-srv)
(cl:export '(CPGTYPE-VAL
          CPGTYPE
          CPGFUNC-VAL
          CPGFUNC
          INITFREQR-VAL
          INITFREQR
          INITFREQL-VAL
          INITFREQL
          UPDATEFREQR-VAL
          UPDATEFREQR
          UPDATEFREQL-VAL
          UPDATEFREQL
          SUCCESS-VAL
          SUCCESS
))