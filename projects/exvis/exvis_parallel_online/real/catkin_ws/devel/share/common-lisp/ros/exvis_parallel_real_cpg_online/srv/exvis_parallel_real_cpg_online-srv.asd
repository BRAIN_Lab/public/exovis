
(cl:in-package :asdf)

(defsystem "exvis_parallel_real_cpg_online-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "cpgService" :depends-on ("_package_cpgService"))
    (:file "_package_cpgService" :depends-on ("_package"))
  ))