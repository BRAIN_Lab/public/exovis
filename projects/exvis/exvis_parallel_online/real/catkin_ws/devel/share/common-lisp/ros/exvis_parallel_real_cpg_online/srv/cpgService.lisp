; Auto-generated. Do not edit!


(cl:in-package exvis_parallel_real_cpg_online-srv)


;//! \htmlinclude cpgService-request.msg.html

(cl:defclass <cpgService-request> (roslisp-msg-protocol:ros-message)
  ((cpgType
    :reader cpgType
    :initarg :cpgType
    :type cl:string
    :initform "")
   (cpgFunc
    :reader cpgFunc
    :initarg :cpgFunc
    :type cl:string
    :initform "")
   (initFreqR
    :reader initFreqR
    :initarg :initFreqR
    :type cl:float
    :initform 0.0)
   (initFreqL
    :reader initFreqL
    :initarg :initFreqL
    :type cl:float
    :initform 0.0)
   (updateFreqR
    :reader updateFreqR
    :initarg :updateFreqR
    :type cl:float
    :initform 0.0)
   (updateFreqL
    :reader updateFreqL
    :initarg :updateFreqL
    :type cl:float
    :initform 0.0))
)

(cl:defclass cpgService-request (<cpgService-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <cpgService-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'cpgService-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name exvis_parallel_real_cpg_online-srv:<cpgService-request> is deprecated: use exvis_parallel_real_cpg_online-srv:cpgService-request instead.")))

(cl:ensure-generic-function 'cpgType-val :lambda-list '(m))
(cl:defmethod cpgType-val ((m <cpgService-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader exvis_parallel_real_cpg_online-srv:cpgType-val is deprecated.  Use exvis_parallel_real_cpg_online-srv:cpgType instead.")
  (cpgType m))

(cl:ensure-generic-function 'cpgFunc-val :lambda-list '(m))
(cl:defmethod cpgFunc-val ((m <cpgService-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader exvis_parallel_real_cpg_online-srv:cpgFunc-val is deprecated.  Use exvis_parallel_real_cpg_online-srv:cpgFunc instead.")
  (cpgFunc m))

(cl:ensure-generic-function 'initFreqR-val :lambda-list '(m))
(cl:defmethod initFreqR-val ((m <cpgService-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader exvis_parallel_real_cpg_online-srv:initFreqR-val is deprecated.  Use exvis_parallel_real_cpg_online-srv:initFreqR instead.")
  (initFreqR m))

(cl:ensure-generic-function 'initFreqL-val :lambda-list '(m))
(cl:defmethod initFreqL-val ((m <cpgService-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader exvis_parallel_real_cpg_online-srv:initFreqL-val is deprecated.  Use exvis_parallel_real_cpg_online-srv:initFreqL instead.")
  (initFreqL m))

(cl:ensure-generic-function 'updateFreqR-val :lambda-list '(m))
(cl:defmethod updateFreqR-val ((m <cpgService-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader exvis_parallel_real_cpg_online-srv:updateFreqR-val is deprecated.  Use exvis_parallel_real_cpg_online-srv:updateFreqR instead.")
  (updateFreqR m))

(cl:ensure-generic-function 'updateFreqL-val :lambda-list '(m))
(cl:defmethod updateFreqL-val ((m <cpgService-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader exvis_parallel_real_cpg_online-srv:updateFreqL-val is deprecated.  Use exvis_parallel_real_cpg_online-srv:updateFreqL instead.")
  (updateFreqL m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <cpgService-request>) ostream)
  "Serializes a message object of type '<cpgService-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'cpgType))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'cpgType))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'cpgFunc))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'cpgFunc))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'initFreqR))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'initFreqL))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'updateFreqR))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'updateFreqL))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <cpgService-request>) istream)
  "Deserializes a message object of type '<cpgService-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'cpgType) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'cpgType) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'cpgFunc) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'cpgFunc) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'initFreqR) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'initFreqL) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'updateFreqR) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'updateFreqL) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<cpgService-request>)))
  "Returns string type for a service object of type '<cpgService-request>"
  "exvis_parallel_real_cpg_online/cpgServiceRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'cpgService-request)))
  "Returns string type for a service object of type 'cpgService-request"
  "exvis_parallel_real_cpg_online/cpgServiceRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<cpgService-request>)))
  "Returns md5sum for a message object of type '<cpgService-request>"
  "f76f149eb78f5f171475e3975b4978cf")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'cpgService-request)))
  "Returns md5sum for a message object of type 'cpgService-request"
  "f76f149eb78f5f171475e3975b4978cf")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<cpgService-request>)))
  "Returns full string definition for message of type '<cpgService-request>"
  (cl:format cl:nil "string cpgType~%string cpgFunc~%float64 initFreqR~%float64 initFreqL~%float64 updateFreqR~%float64 updateFreqL~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'cpgService-request)))
  "Returns full string definition for message of type 'cpgService-request"
  (cl:format cl:nil "string cpgType~%string cpgFunc~%float64 initFreqR~%float64 initFreqL~%float64 updateFreqR~%float64 updateFreqL~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <cpgService-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'cpgType))
     4 (cl:length (cl:slot-value msg 'cpgFunc))
     8
     8
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <cpgService-request>))
  "Converts a ROS message object to a list"
  (cl:list 'cpgService-request
    (cl:cons ':cpgType (cpgType msg))
    (cl:cons ':cpgFunc (cpgFunc msg))
    (cl:cons ':initFreqR (initFreqR msg))
    (cl:cons ':initFreqL (initFreqL msg))
    (cl:cons ':updateFreqR (updateFreqR msg))
    (cl:cons ':updateFreqL (updateFreqL msg))
))
;//! \htmlinclude cpgService-response.msg.html

(cl:defclass <cpgService-response> (roslisp-msg-protocol:ros-message)
  ((success
    :reader success
    :initarg :success
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass cpgService-response (<cpgService-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <cpgService-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'cpgService-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name exvis_parallel_real_cpg_online-srv:<cpgService-response> is deprecated: use exvis_parallel_real_cpg_online-srv:cpgService-response instead.")))

(cl:ensure-generic-function 'success-val :lambda-list '(m))
(cl:defmethod success-val ((m <cpgService-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader exvis_parallel_real_cpg_online-srv:success-val is deprecated.  Use exvis_parallel_real_cpg_online-srv:success instead.")
  (success m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <cpgService-response>) ostream)
  "Serializes a message object of type '<cpgService-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'success) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <cpgService-response>) istream)
  "Deserializes a message object of type '<cpgService-response>"
    (cl:setf (cl:slot-value msg 'success) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<cpgService-response>)))
  "Returns string type for a service object of type '<cpgService-response>"
  "exvis_parallel_real_cpg_online/cpgServiceResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'cpgService-response)))
  "Returns string type for a service object of type 'cpgService-response"
  "exvis_parallel_real_cpg_online/cpgServiceResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<cpgService-response>)))
  "Returns md5sum for a message object of type '<cpgService-response>"
  "f76f149eb78f5f171475e3975b4978cf")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'cpgService-response)))
  "Returns md5sum for a message object of type 'cpgService-response"
  "f76f149eb78f5f171475e3975b4978cf")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<cpgService-response>)))
  "Returns full string definition for message of type '<cpgService-response>"
  (cl:format cl:nil "bool success~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'cpgService-response)))
  "Returns full string definition for message of type 'cpgService-response"
  (cl:format cl:nil "bool success~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <cpgService-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <cpgService-response>))
  "Converts a ROS message object to a list"
  (cl:list 'cpgService-response
    (cl:cons ':success (success msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'cpgService)))
  'cpgService-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'cpgService)))
  'cpgService-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'cpgService)))
  "Returns string type for a service object of type '<cpgService>"
  "exvis_parallel_real_cpg_online/cpgService")