;; Auto-generated. Do not edit!


(when (boundp 'exvis_parallel_real_cpg_online::cpgService)
  (if (not (find-package "EXVIS_PARALLEL_REAL_CPG_ONLINE"))
    (make-package "EXVIS_PARALLEL_REAL_CPG_ONLINE"))
  (shadow 'cpgService (find-package "EXVIS_PARALLEL_REAL_CPG_ONLINE")))
(unless (find-package "EXVIS_PARALLEL_REAL_CPG_ONLINE::CPGSERVICE")
  (make-package "EXVIS_PARALLEL_REAL_CPG_ONLINE::CPGSERVICE"))
(unless (find-package "EXVIS_PARALLEL_REAL_CPG_ONLINE::CPGSERVICEREQUEST")
  (make-package "EXVIS_PARALLEL_REAL_CPG_ONLINE::CPGSERVICEREQUEST"))
(unless (find-package "EXVIS_PARALLEL_REAL_CPG_ONLINE::CPGSERVICERESPONSE")
  (make-package "EXVIS_PARALLEL_REAL_CPG_ONLINE::CPGSERVICERESPONSE"))

(in-package "ROS")





(defclass exvis_parallel_real_cpg_online::cpgServiceRequest
  :super ros::object
  :slots (_cpgType _cpgFunc _initFreqR _initFreqL _updateFreqR _updateFreqL ))

(defmethod exvis_parallel_real_cpg_online::cpgServiceRequest
  (:init
   (&key
    ((:cpgType __cpgType) "")
    ((:cpgFunc __cpgFunc) "")
    ((:initFreqR __initFreqR) 0.0)
    ((:initFreqL __initFreqL) 0.0)
    ((:updateFreqR __updateFreqR) 0.0)
    ((:updateFreqL __updateFreqL) 0.0)
    )
   (send-super :init)
   (setq _cpgType (string __cpgType))
   (setq _cpgFunc (string __cpgFunc))
   (setq _initFreqR (float __initFreqR))
   (setq _initFreqL (float __initFreqL))
   (setq _updateFreqR (float __updateFreqR))
   (setq _updateFreqL (float __updateFreqL))
   self)
  (:cpgType
   (&optional __cpgType)
   (if __cpgType (setq _cpgType __cpgType)) _cpgType)
  (:cpgFunc
   (&optional __cpgFunc)
   (if __cpgFunc (setq _cpgFunc __cpgFunc)) _cpgFunc)
  (:initFreqR
   (&optional __initFreqR)
   (if __initFreqR (setq _initFreqR __initFreqR)) _initFreqR)
  (:initFreqL
   (&optional __initFreqL)
   (if __initFreqL (setq _initFreqL __initFreqL)) _initFreqL)
  (:updateFreqR
   (&optional __updateFreqR)
   (if __updateFreqR (setq _updateFreqR __updateFreqR)) _updateFreqR)
  (:updateFreqL
   (&optional __updateFreqL)
   (if __updateFreqL (setq _updateFreqL __updateFreqL)) _updateFreqL)
  (:serialization-length
   ()
   (+
    ;; string _cpgType
    4 (length _cpgType)
    ;; string _cpgFunc
    4 (length _cpgFunc)
    ;; float64 _initFreqR
    8
    ;; float64 _initFreqL
    8
    ;; float64 _updateFreqR
    8
    ;; float64 _updateFreqL
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _cpgType
       (write-long (length _cpgType) s) (princ _cpgType s)
     ;; string _cpgFunc
       (write-long (length _cpgFunc) s) (princ _cpgFunc s)
     ;; float64 _initFreqR
       (sys::poke _initFreqR (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _initFreqL
       (sys::poke _initFreqL (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _updateFreqR
       (sys::poke _updateFreqR (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _updateFreqL
       (sys::poke _updateFreqL (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _cpgType
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _cpgType (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _cpgFunc
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _cpgFunc (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; float64 _initFreqR
     (setq _initFreqR (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _initFreqL
     (setq _initFreqL (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _updateFreqR
     (setq _updateFreqR (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _updateFreqL
     (setq _updateFreqL (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass exvis_parallel_real_cpg_online::cpgServiceResponse
  :super ros::object
  :slots (_success ))

(defmethod exvis_parallel_real_cpg_online::cpgServiceResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional (__success :null))
   (if (not (eq __success :null)) (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass exvis_parallel_real_cpg_online::cpgService
  :super ros::object
  :slots ())

(setf (get exvis_parallel_real_cpg_online::cpgService :md5sum-) "f76f149eb78f5f171475e3975b4978cf")
(setf (get exvis_parallel_real_cpg_online::cpgService :datatype-) "exvis_parallel_real_cpg_online/cpgService")
(setf (get exvis_parallel_real_cpg_online::cpgService :request) exvis_parallel_real_cpg_online::cpgServiceRequest)
(setf (get exvis_parallel_real_cpg_online::cpgService :response) exvis_parallel_real_cpg_online::cpgServiceResponse)

(defmethod exvis_parallel_real_cpg_online::cpgServiceRequest
  (:response () (instance exvis_parallel_real_cpg_online::cpgServiceResponse :init)))

(setf (get exvis_parallel_real_cpg_online::cpgServiceRequest :md5sum-) "f76f149eb78f5f171475e3975b4978cf")
(setf (get exvis_parallel_real_cpg_online::cpgServiceRequest :datatype-) "exvis_parallel_real_cpg_online/cpgServiceRequest")
(setf (get exvis_parallel_real_cpg_online::cpgServiceRequest :definition-)
      "string cpgType
string cpgFunc
float64 initFreqR
float64 initFreqL
float64 updateFreqR
float64 updateFreqL
---
bool success


")

(setf (get exvis_parallel_real_cpg_online::cpgServiceResponse :md5sum-) "f76f149eb78f5f171475e3975b4978cf")
(setf (get exvis_parallel_real_cpg_online::cpgServiceResponse :datatype-) "exvis_parallel_real_cpg_online/cpgServiceResponse")
(setf (get exvis_parallel_real_cpg_online::cpgServiceResponse :definition-)
      "string cpgType
string cpgFunc
float64 initFreqR
float64 initFreqL
float64 updateFreqR
float64 updateFreqL
---
bool success


")



(provide :exvis_parallel_real_cpg_online/cpgService "f76f149eb78f5f171475e3975b4978cf")


