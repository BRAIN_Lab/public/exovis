// Auto-generated. Do not edit!

// (in-package exvis_parallel_real_cpg_online.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class cpgServiceRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.cpgType = null;
      this.cpgFunc = null;
      this.initFreqR = null;
      this.initFreqL = null;
      this.updateFreqR = null;
      this.updateFreqL = null;
    }
    else {
      if (initObj.hasOwnProperty('cpgType')) {
        this.cpgType = initObj.cpgType
      }
      else {
        this.cpgType = '';
      }
      if (initObj.hasOwnProperty('cpgFunc')) {
        this.cpgFunc = initObj.cpgFunc
      }
      else {
        this.cpgFunc = '';
      }
      if (initObj.hasOwnProperty('initFreqR')) {
        this.initFreqR = initObj.initFreqR
      }
      else {
        this.initFreqR = 0.0;
      }
      if (initObj.hasOwnProperty('initFreqL')) {
        this.initFreqL = initObj.initFreqL
      }
      else {
        this.initFreqL = 0.0;
      }
      if (initObj.hasOwnProperty('updateFreqR')) {
        this.updateFreqR = initObj.updateFreqR
      }
      else {
        this.updateFreqR = 0.0;
      }
      if (initObj.hasOwnProperty('updateFreqL')) {
        this.updateFreqL = initObj.updateFreqL
      }
      else {
        this.updateFreqL = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type cpgServiceRequest
    // Serialize message field [cpgType]
    bufferOffset = _serializer.string(obj.cpgType, buffer, bufferOffset);
    // Serialize message field [cpgFunc]
    bufferOffset = _serializer.string(obj.cpgFunc, buffer, bufferOffset);
    // Serialize message field [initFreqR]
    bufferOffset = _serializer.float64(obj.initFreqR, buffer, bufferOffset);
    // Serialize message field [initFreqL]
    bufferOffset = _serializer.float64(obj.initFreqL, buffer, bufferOffset);
    // Serialize message field [updateFreqR]
    bufferOffset = _serializer.float64(obj.updateFreqR, buffer, bufferOffset);
    // Serialize message field [updateFreqL]
    bufferOffset = _serializer.float64(obj.updateFreqL, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type cpgServiceRequest
    let len;
    let data = new cpgServiceRequest(null);
    // Deserialize message field [cpgType]
    data.cpgType = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [cpgFunc]
    data.cpgFunc = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [initFreqR]
    data.initFreqR = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [initFreqL]
    data.initFreqL = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [updateFreqR]
    data.updateFreqR = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [updateFreqL]
    data.updateFreqL = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += _getByteLength(object.cpgType);
    length += _getByteLength(object.cpgFunc);
    return length + 40;
  }

  static datatype() {
    // Returns string type for a service object
    return 'exvis_parallel_real_cpg_online/cpgServiceRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ff3e74b1920ea8bff8137459b706875e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string cpgType
    string cpgFunc
    float64 initFreqR
    float64 initFreqL
    float64 updateFreqR
    float64 updateFreqL
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new cpgServiceRequest(null);
    if (msg.cpgType !== undefined) {
      resolved.cpgType = msg.cpgType;
    }
    else {
      resolved.cpgType = ''
    }

    if (msg.cpgFunc !== undefined) {
      resolved.cpgFunc = msg.cpgFunc;
    }
    else {
      resolved.cpgFunc = ''
    }

    if (msg.initFreqR !== undefined) {
      resolved.initFreqR = msg.initFreqR;
    }
    else {
      resolved.initFreqR = 0.0
    }

    if (msg.initFreqL !== undefined) {
      resolved.initFreqL = msg.initFreqL;
    }
    else {
      resolved.initFreqL = 0.0
    }

    if (msg.updateFreqR !== undefined) {
      resolved.updateFreqR = msg.updateFreqR;
    }
    else {
      resolved.updateFreqR = 0.0
    }

    if (msg.updateFreqL !== undefined) {
      resolved.updateFreqL = msg.updateFreqL;
    }
    else {
      resolved.updateFreqL = 0.0
    }

    return resolved;
    }
};

class cpgServiceResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.success = null;
    }
    else {
      if (initObj.hasOwnProperty('success')) {
        this.success = initObj.success
      }
      else {
        this.success = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type cpgServiceResponse
    // Serialize message field [success]
    bufferOffset = _serializer.bool(obj.success, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type cpgServiceResponse
    let len;
    let data = new cpgServiceResponse(null);
    // Deserialize message field [success]
    data.success = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a service object
    return 'exvis_parallel_real_cpg_online/cpgServiceResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '358e233cde0c8a8bcfea4ce193f8fc15';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool success
    
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new cpgServiceResponse(null);
    if (msg.success !== undefined) {
      resolved.success = msg.success;
    }
    else {
      resolved.success = false
    }

    return resolved;
    }
};

module.exports = {
  Request: cpgServiceRequest,
  Response: cpgServiceResponse,
  md5sum() { return 'f76f149eb78f5f171475e3975b4978cf'; },
  datatype() { return 'exvis_parallel_real_cpg_online/cpgService'; }
};
