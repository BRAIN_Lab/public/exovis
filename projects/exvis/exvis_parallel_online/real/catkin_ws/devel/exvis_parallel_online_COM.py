#!/usr/bin/env python

import rospy
import subprocess
import signal
import os

child = subprocess.Popen(["roslaunch","exvis_parallel_online_COM.launch"])
#child.wait() #You can use this line to block the parent process untill the child process finished.
#print("parent process")
#print(child.poll())

#rospy.loginfo('The PID of child: %d', child.pid)
#print ("The PID of child:", child.pid)

rospy.sleep(15)

# qSignal = raw_input("Pree q to kill all nodes: ")
qSignal = input("Pree q to kill all nodes: ")
if (qSignal == "q"):
	child.send_signal(signal.SIGINT) #You may also use .terminate() method
	#child.terminate()

	os.system("rosnode list | grep -v rosout | xargs rosnode kill")
