#!/bin/sh

# ROSCORE -----

# CONTROL NODES -----
tmux send-keys -t ExvisCon:Win2.1 C-c
tmux send-keys -t ExvisCon:Win2.2 C-c
tmux send-keys -t ExvisCon:Win2.3 C-c
# RECORD NODE -----
tmux send-keys -t ExvisCon:Win3 C-c
#tmux select-layout -t ExvisCon:Win2 tiled

# UI NODES -----
tmux send-keys -t ExvisUI:Win1.1 C-c
tmux send-keys -t ExvisUI:Win1.2 C-c
tmux send-keys -t ExvisUI:Win1.3 C-c
tmux send-keys -t ExvisUI:Win1.4 C-c
tmux send-keys -t ExvisUI:Win1.5 C-c
tmux send-keys -t ExvisUI:Win1.6 C-c

# Attach ------

#######################
# Do not forget to 
# $ tmux kill-server

