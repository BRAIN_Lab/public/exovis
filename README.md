# EXOVIS

The proposed algorithm is part of a research which is submitted to 

C. Akkawutvanich and P. Manoonpong, "Personalized Symmetrical and Asymmetrical Gait Generation of a Lower Limb Exoskeleton," in *IEEE Transactions on Industrial Informatics*, doi: 10.1109/TII.2023.3234619.

***
<br>
<div align="center">
<img src="./pic/ControlSystem_light.png" width="700">
</div>
<br>

## OVERVIEW
This project called EXOVIS (**EXO**skeleton framework for **V**ersatile personalized ga**I**t generation with a **S**eamless user-exo interface) is to develop an adaptive neural controller which can enable physical human-robot interaction (pHRI) with seamless human-robot interfaces for a lower limb exoskeleton. The framework combines the advantages of central pattern generator (CPG) -based and learning from demonstration (LfD) strategy.

***
## PREREQUISITE
The program is based on C++ and developed based on the following platforms:
1. [Ubuntu 20.04.5 LTS](https://releases.ubuntu.com/focal/) - It is used as an operating system.
2. [ROS Noetic (ROS 1 LTS)](http://wiki.ros.org/noetic/Installation) - It is used as a framework and communication protocol for a robot.
3. [tmux](https://en.wikipedia.org/wiki/Tmux) - It is used to manage running terminal for the program.
4. [PCAN-USB](https://www.peak-system.com/PCAN-USB.199.0.html?&L=1) - It is a driver for a pcan-to-usb cable.

***
## HARDWARE
### Exo-H3
<br>
<div align="center">
<img src="./pic/Exo-H3.png" width="200">
</div>
<br>

The bilateral exoskeleton hardware is an open-research platform called "Exo-H3" from [Technaid company](https://www.technaid.com/products/robotic-exoskeleton-exo-exoesqueleto-h3/). It is 6-DOF with hips, knees, and ankles motors which move mainly in sagittal plane.


### Communication
<br>
<div align="center">
<img src="./pic/EXOVIS_Commu.png" width="700">
</div>
<br>

The communication scheme is based on robot operating system (ROS). We can connect a wire directly between a low-level controller and a remote computer or we can wirelessly connect through an extra on-board computer (in this case NUC) installed in a backpack which connects to the low-level controller.


***
## FILE STRUCTURE

There are 3 main folders:  
1. **projects**  
   This folder contains a ROS workspace i.e., castkin_ws in this case. It compraises 3 subfolders which are:  
   *src* -  This folder stores *CMakeList.txt* files which are used to config each ROS node (or package).  
   *devel* - This folder stores executable files in */lib*, and *.launch* or *.sh* files to run the program.    
   *build* - This folder stores built files which is overwritten after each compilation.
   
2. **controllers**  
   This folder contains *.cpp* and *.h* files where all algorithms are implemented here.
   
3. **utils**  
   This folder contains other realted *.cpp* and *.h* files that are used as utility files such as the driver for pcan-usb. Another ROS node is created here as an interface files to transform CAN data into ROS layer. 


***
## SUPPLEMENTARY MATERIALS
## General comparision to previous works

In comparison to a previous work <a href="#wang_matsuokas_2020">[1]</a> combining a neural rhythmic oscillator with a post-processing network to generate control signals for bipedal locomotion, our minimal structure with decoupled CPGs and Hebbian synaptic plasticity does not require
<ol type="I">
   <li> feature processing for generating uniformly distributed CPG outputs,
   <li> signal filtering for generating continuous signals during the transition between different CPG frequencies, and
   <li> coordinate transformation as well as system dynamic model for transforming desired trajectories into joint angle commands as needed in their control approach <a href="#wang_matsuokas_2020">[1]</a>.
</ol>

This is because all these features are already embedded in our adaptive CPG-RBF control network which operates in joint space. Furthermore, according to our distributed decoupled CPG structure, we can independently or flexibly generate right and left leg frequencies and patterns for asymmetrical walking conditions which have not been shown in <a href="#wang_matsuokas_2020">[1]</a>.


## Example of frequency adaptaion mechanism
<br>
<div align="center">
<img src="./pic/CPGSim_text.png" width="500">
</div>
<br>

Example of frequency adaptation of a neural oscillator network with Hebbian-based synaptic plasticity with respect to feedback ($F_{CPG}$). This shows the frequency adaptation and memorization of the CPG. It can adapt its frequency to match the sensory feedback frequency (learning) and maintain the learned or adapted frequency even the feedback disappears (no input).


## An experimental protocol
<br>
<div align="center">
<img src="./pic/Exp_Protocol.png" width="500">
</div>
<br>

The protocol for automatic personalized gait generation during walking on a level floor and a split-belt treadmill. The free mode is activated for learning right/left leg frequencies online (i.e., frequency adaptation) and recording hip, knee, and ankle joint angle patterns to imitate the joint angle patterns of the user (i.e., pattern adaptation) (periods 1-3). The assistive mode is then activated using the learned frequencies and patterns (periods 5-6). The total duration of the seamless process without additional human interference is less than 5 min. In the assistive mode, we start our adaptive neural control when the user is ready in a standing posture (approx. zero degree for all joint angles). The control takes the learned frequency from the user as the initial walking speed where the joint patterns are generated in each time step according to the learned data. Note that the control can be started and stopped as needed during each experimental session. The typical procedure used here is as follows:  

Step 1 (standby), the user starts from a standing posture with the device in a standby mode (all motors are not controlled and can be passively moved with mechanical constraints, e.g., gearbox friction and mechanical stoppers).  

Step 2 (start walking), the neural control is activated where all motors are driven by position control.  

Step 3 (stop walking), we send a stopping command to deactivate the neural control where all motors will be stopped and maintained at the last position. Then, we send another command to deactivate all the motors allowing the user to freely return to the standing posture (Step 1) and wait for the next command. We can start to reactivate the device and repeat the whole process again (Step 3).


## Experimental results : Level floor
In our experiment, 5 subjects were experienced our algorithm according to the protocol. The following results are example results from subject 2 during walking on the level floor.

<br>
<div align="center">
   <img src="./pic/LevelFloor_OL_Free_CPGFreq_Sub2.png" width="500">
</div>
<br>

The rhythmic pattern generation and frequency adaptation of the exoskeleton demonstrating subject 2 walking on a level floor. The graphs show right and left hip joint angles and CPG frequencies during the free mode. The frequencies start from 1 Hz in both legs and then continuously adapt and converge to the user's walking frequency which is around 0.38 Hz in this case (grey). The shaded area along the line shows 95\% CI of mean. The average convergence time is approximately 22.68 s (orange). The frequency value almost corresponds to the walking cycle time (swing-stance) of 2.21 s as can be observed from the snapshots.

<br>
<div align="center">
   <img src="./pic/LevelFloor_OL_Act_Pat_Sub2.png" width="500">
</div>
<br>

The pattern adaptation of the exoskeleton demonstrating subject 2 walking on a level floor. The signals are from the left hip, knee, and ankle. Each target signal (blue) is derived from the average sensory signal during the free mode (i.e., zero-torque control). The output signals (green) are calculated from the adaptive CPG post-processing network (pattern adaptation module). The joint angle feedback of the exoskeleton (yellow) are monitored when the device is controlled by the output signals. The differences in the output and the target signals are less than 5 degrees in all positions shown in the error graphs (red). A dashed line separates the stance (ST) and swing (SW) phases at around 63\%. The shaded area along the line shows 95\% CI of mean.


## Experimental results : Treadmill
### The constant speed

The example results of 2 subjects are shown below.
<br>
<div align="center">
   <img src="./pic/Treadmill_OL_044_CPGFreq_Sub1.png" width="398">
   <img src="./pic/Treadmill_OL_044_CPGFreq_Sub2.png" width="400">
</div>
<br>

**(Left)** The rhythmic pattern generation and frequency adaptation of the exoskeleton demonstrated by subject 1 walking on a split-belt treadmill at a constant speed. The linear speed of both belts is constant at 0.44 m/s. **(A)** The right and left hip signals and their convergence frequencies are around 0.49 Hz (grey). The average convergence time is about 15.44 s (orange). **(B)** The right and left joint angle patterns of the free mode during the frequency adaptation period. Heel strike (HS) and toe-off (TO) can be seen clearly in the snapshots. **(C)** During the assistive period, the personalized joint angle patterns and walking frequencies similar to those observed in the free mode period. ST and SW indicate stance and swing phases, respectively. A video of this experiment can be seen [SupplementaryVideo2](www.manoonpong.com/EXOVIS/SupplementaryVideo2.mp4})

**(Right)** The rhythmic pattern generation and frequency adaptation of the exoskeleton demonstrating subject 2 walking on a split-belt treadmill at a constant speed. The linear speed of both belts is constant at 0.44 m/s. **(A)** The right and left hip signals and their convergence frequencies are almost the same around 0.49 Hz (grey). The average convergence time is about 23.68 s (orange). **(B)** The right and left joint angle patterns of the free mode during the frequency adaptation period. Heel strike (HS) and toe-off (TO) can be clearly observed from the snapshots. **(C)** During the assistive period, similar personalized joint angle patterns and walking frequency are observed as in the free mode period. ST and SW indicate stance and swing phases, respectively.

<br>
<div align="center">
   <img src="./pic/Treadmill_OL_044_Pat_Sub1_30D.png" width="400">
   <img src="./pic/Treadmill_OL_044_Pat_Sub2_30D.png" width="392">
</div>
<br>

**(Left)** The pattern adaptation of the exoskeleton for subject 1 walking on a split-belt treadmill at a constant speed of 0.44 m/s. The statistical average signals are from the left hip, knee, and ankle. The output signals (green) calculated from the proposed method differ from the target signals (blue) by less than 5 degrees as shown in the error graphs (red). As expected, the feedback (yellow) measured during device assistance shows a similar pattern. A dashed line separates between the stance (ST) and swing (SW) phases at around 65\%. The shaded area along the line shows 95\% CI of mean.

**(Right)** The pattern adaptation of the exoskeleton demonstrating subject 2 walking on a treadmill at a constant speed of 0.44 m/s. The statistical average signals are from the left hip, knee, and ankle. The output signals (green) calculated using the proposed method differ from the target signals (blue) by less than 5 degrees as shown in the error graphs (red). As expected, the feedback (yellow) measured during the device assistance show similar pattern. A dashed line separates stance (ST) and swing (SW) phases at around 66\%. The shaded area along the line shows 95\% CI of mean.

<br>
<div align="center">
   <img src="./pic/Treadmill_Free_EachSpeeds_CPGFreq_Sub1_revised.png" width="388">
   <img src="./pic/Treadmill_Free_EachSpeeds_CPGFreq_Sub2.png" width="400">
</div>
<br>

**(Left)** The CPG frequencies of the exoskeleton demonstrating subject 1 walking at various split-belt treadmill speeds. **(A)** The frequency adaptation of right and left CPGs at different speeds: 0.25 m/s ~ 0.31 Hz, 0.50 m/s ~ 0.74 Hz, 0.75 m/s ~ 0.80 Hz, 1.00 m/s ~ 0.95 Hz, and 1.25 m/s ~ 0.98 Hz. The average convergence time is about 30 s, with the data used about 10 s later, to find a relationship graph (grey). **(B)** The relationship between the split-belt treadmill speed in m/s and CPG frequency in Hz is plotted. The graph shows that values fluctuate at higher speeds. The split-belt treadmill speed is almost linearly proportional to the CPG frequency.

**(Right)** The CPG frequencies of the exoskeleton demonstrating subject 2 walking at various split-belt treadmill speeds. **(A)** The frequency adaptation of right and left CPGs at different speeds: 0.25 m/s ~ 0.63 Hz, 0.50 m/s ~ 0.79 Hz, 0.75 m/s ~ 0.84 Hz, 1.00 m/s ~ 0.87 Hz, and 1.25 m/s ~ 0.99 Hz. The average convergence time is about 30 s, with the data used about 10 s later, to find a relationship graph (grey). **(B)** The relationship between the split-belt treadmill speed in m/s and CPG frequency in Hz is plotted.

<br>
<div align="center">
   <img src="./pic/FreqUpDown_Treadmill.png" width="1000">
</div>
<br>

The continuous frequency adaptation at different walking frequencies of the subject 1 on a treadmill. Here, the subject 1 walked with the exoskeleton under the free mode and we changed the treadmill speed continuously to regulate the walking speed/frequency during the experiment.


### The different speeds
The example results of 2 subjects are shown below.
<br>
<div align="center">
   <img src="./pic/Treadmill_OL_Split_CPGFreq_Sub1.png" width="390">
   <img src="./pic/Treadmill_OL_Split_CPGFreq_Sub2.png" width="400">
</div>
<br>

**(Left)** The rhythmic pattern generation and frequency adaptation of the exoskeleton demonstrating subject 1 walking on a split-belt treadmill with the two legs at different speeds. The speeds of each belt are set separately to 0.25 m/s (right) and 0.75 m/s (left), respectively. **(A)** The right and left hip signals and their convergence frequencies. Both legs swing at almost the same average frequency of around 0.59 Hz (grey). The average convergence time is approximately 26.90 s (orange). **(B)** Both right and left joint angle signals of the free mode during the frequency adaptation period. **(C)** All joint angle feedback during the assistive period. A video of this experiment can be seen at [SupplementaryVideo3](www.manoonpong.com/EXOVIS/SupplementaryVideo3.mp4).

<br>
<div align="center">
   <img src="./pic/Treadmill_OL_Split_Pat_Sub1_30D.png" width="400">
   <img src="./pic/Treadmill_OL_Split_Pat_Sub2_30D.png" width="400">
</div>
<br>

**(Left)** The asymmetrical pattern adaptation of the exoskeleton demonstrating subject 1 walking on a split-belt treadmill with the two legs at different speeds (right 0.25 m/s, left 0.75 m/s). The signals of hip, knee, and ankle in both legs are compared. All outputs (green) can imitate targets (blue) with errors (red) of less than 2 degrees. As expected, all feedback follow the target patterns. A dashed line separates the stance (ST) and swing (SW) phases at around 73\% (R-ST) and 27\% (R-SW) on the right leg and 57\% (L-ST) and 43\% (L-SW) on the left leg. The output signals (green) generated from the proposed algorithm differ from the target signals (blue) by less than 2 degrees as shown by the error signals (red) in each joint. As expected, the feedback received during the assistive period also follow the target shapes.

**(Right)** The asymmetric pattern adaptation of the exoskeleton demonstrating subject 2 walking on a split-belt treadmill with the two legs at different speeds (right 0.25 m/s, left 0.75 m/s). The signals of hip, knee, and ankle in both legs are compared. All outputs (green) can imitate targets (blue) with errors (red) of less than 6 degrees. As expected, all feedback also follow the target pattern. A dashed line separates the stance (ST) and swing (SW) phases at around 74\% (R-ST) and 36\% (R-SW) on the right leg and 54\% (L-ST) and 46\% (L-SW) on the left leg. The output signals (green) generated from the proposed algorithm differ from the target signals (blue) by less than 2 degrees as shown by the error signals (red) in each joint. As expected, the feedback received during the assistive period also follow the target shapes.


## The subprocess of the frequency and pattern adaptation time
<br>
<div align="center">
   <img src="./pic/PipelineTime.png" width="1000">
</div>
<br>

The total learning period of the gait learning pipeline and the approximate processing time in each part. During the learning period, there are two consecutive processes in the pipeline: one for frequency adaptation and the other one for pattern adaptation. The former requires a convergence time of less than 30 s (i.e., finding the working frequency). The latter process takes around 90 s for its three sub parts (data collection, target finding, RBF training). Among these, the longest time is the data collection time which takes around 60 – 80 s (approximately 40 walking steps). In this study, we empirically set the data collection time to ensure that we can cover all changing patterns. The target finding time which is for approximating the target joint signals (i.e., finding the average joint patterns) takes only about 10 s for all joints. Finally, the RBF training time takes only about 0.2 s to estimate each target signal (i.e., error converges to nearly zero), thus six joints require less than 2 s in total.

## The parameters analysis
<br>
<div align="center">
   <img src="./pic/RBF_Error_MAEKernelTotal.png" width="1000">
</div>
<br>

Parameters analysis for the pattern adaptation. The number of kernels (k), the number of calculation steps during learning (step), and the learning rate ($\zeta$)  are the key system parameters that affect the generated output signal controlling a joint of the exoskeleton. The pattern adaptation mechanism consists of two main parts: **(A)** kernel creation (part 1) and **(B)** error-based learning (part 2). Based on the analysis of learning one joint pattern, during the learning process (part 2) increasing the number of calculation steps, for example from 10 to 400 steps, can lower error (MAE) (see **(B)**). Increasing k can also reduce the MAE. For example, if k is equal to or greater than 30, we can obtain small error (error $\le$ 0.3). While a large k (e.g., k = 45) can achieve the same MAE with fewer computation steps compared to a smaller k (e.g., k = 30), it requires more time for kernel creation (part 1, see **(A)** and **(C)**). The learning rate ($\zeta$) also plays an important role. For a certain $\zeta$ value, it allows for using different maximum numbers of kernels without divergence. For example, with $\zeta$ = 0.01 we can use a large number of kernels ($\ge$ 45), while a larger value ($\zeta$ = 0.1) can result in a lower MAE at the same number of kernels and steps (see **(D)**). In this study, we set $\zeta$ = 0.1, step = 400, and k = 30. Although 200 steps (or 0.2 s) to train each joint pattern are enough (MAE ~ 0.37) and quite satisfied for our work (i.e., when testing the control in the assistive mode, the user can perform proper walking with clear swing, stance, and double support phases), increasing to 400 steps (or 0.4 s) could gain around 11\% MAE reduction (see **(C)**). We set k = 30 because it balances a tradeoff between the MAE and computational effort (time) for kernel creation (i.e., we can save 40\% of computational effort comparing to k = 45, (see **(A)**)). The time calculation is based on timestamp of the python code implementation of the RBF network running on Ubuntu 20.04 64 bit; Intel core i7-1065G7 @ 1.3 GHz.

## A comparison between adaptive frequency oscillator (AFO) and our adaptive oscillator with Hebbian-based synaptic plasticity
<br>
<div align="center">
   <img src="./pic/Graph_AFO_AFDC.png" width="1000">
</div>
<br>

A comparison between our adaptive oscillator with Hebbian-based synaptic plasticity and the state-of-the-art AFO <a href="#righetti_dynamic_2006">[2]</a>. **(A)** A standard cosine signal with a frequency of 0.5 Hz is used as a perturbation ($F$) to both oscillators ($per_{AFO}$ and $per$) during 2000 to 4000 time steps. Both oscillators can adapt to generate their output ($x$, $y$, = AFO outputs, and $H_0$, $H_1$ = our oscillator outputs) to follow the frequency of the external signal. However, the frequency from our oscillator ($f_{CPG}$) can converge to the target or perturbation frequency more quickly and precisely than the AFO and with less overshoot frequency value (see $f_{AFO}$ vs. $f_{CPG}$). This is because the adaptive synaptic weight ($\omega_{2F}$) can handle the input perturbation more effectively than the constant weight of the AFO ($\epsilon$). $\omega_{2F}$ can be also considered as online perturbation strength adaptation. **(B)** A real hip signal of the exoskeleton system with a frequency 0.4 Hz is used as the input perturbation to both oscillators. It is also applied during 2000 to 4000 time steps. To handle the more complex signal, both oscillators require more time than in the previous case (cosine signal). Nevertheless, our oscillator can converge to the hip frequency in a shorter period of time than the AFO. In both tests, the oscillators start at the same internal frequency of 1 Hz. Note that the AFO parameter values ($\mu$ and $\epsilon$) are set based on a theoretical study <a href="#righetti_dynamic_2006">[2-6]</a>

## A framework comparison between dynamic movement primitive (DMP) and our neural control
<br>
<div align="center">
   <img src="./pic/DMPvsOurs.png" width="1000">
</div>
<br>

Our neural control framework compares to a DMP framework. Both are quite similar, but different in details. Our framework utilizes an adaptive neural oscillator (CPG with Hebbian-based synaptic plasticity) as a rhythmic source which is comparable to a canonical system mimicking a phase function. A position signal ($y$) is required to create a target signal, while all physical values, i.e., position ($y$), velocity ($\dot{y}$), and acceleration ($\ddot{y}$) are needed for DMPs. A simple error-based learning rule is applied to our framework to find the optimal weight ($\omega^{Opt}$) comparable to a function approximation algorithm in the DMPs. The Gaussian is a basis function in our case when the DMPs uses von Mises. During assistance, our position control signal ($y$) is a direct result signal ($r(t)^{Opt}$) rather than derived from the variable $\ddot{y}$ as done in the DMPs. In summary, our framework can be considered as a neural version of DMPs with position control output signals. However, the benefits of our adaptive CPG-RBF control networks with a distributed decoupled structure are that they allow us to use only neurons and synapses with flexibility to:

<ol type="I">
   <li> implement neural plasticity (i.e., Hebbian-based synaptic plasticity of the CPG for online frequency adaptation as shown here),
   <li> explore and exploit not only limit-cycle (as shown here) but also fixed-point and hysteresis regimes of the neural CPG network by modulating its synaptic weights <a href="#pasemann_complex_2002">[7]</a> for other motor control tasks or activities,
   <li> shape neural CPG dynamics by introducing so-called dynamical state forcing (DSF) based on sensory feedback to spontaneously adapt the geometry/amplitude of the CPG signals for preventing motor damage from unexpected perturbations <a href="#phodapol_grab_2022">[8]</a>,
   <li> introduce more neural connections (e.g., recurrent connections) into the RBF network (i.e., RRBFN <a href="#zemouri_recurrent_2003">[9]</a>, <a href="#shafiabady_proposal_2014">[10]</a>) to obtain dynamic neural memory for learning and memorizing temporal motor sequences of complex motor control tasks or complex activities.
</ol>


## Extended EXOVIS concept for unhealthy subjects
To extend the proposed EXOVIS control scheme for unhealthy subjects, we introduce firstly different scenarios of our framework involving different types of subjects (I, II, and III in the figure below).
<br>
<div align="center">
   <img src="./pic/ExtendedUnhealthy.png" width="500">
</div>
<br>

Different scenarios when applying our framework to subjects with different physical conditions. **(Scenario I)** A healthy subject (H<sub>1</sub>) uses our system. The movement data of the subject can be recorded (H<sub>1</sub> $\rightarrow$ Record#1). The same subject later turns to be an unhealthy subject with a partially walking condition (P<sub>1</sub>; Muscle weakness grade 3 or 4) where we can still obtain walking data (P<sub>1</sub> $\rightarrow$ Record#2). Then, the subject becomes a paraplegic (U<sub>1</sub>). **(Scenario II)**} There is no information of H<sub>1</sub>. Our system has only P<sub>1</sub>. P<sub>1</sub> turns to be U<sub>1</sub>. **(Scenario III)** U<sub>1</sub> starts using our system directly with no prior data related to the subject. Data pool is one concept to provide the best fit pattern where no prior data is available. If there is prior data of the healthy subject, it can be used as a basis for the exoskeleton to assist the same subject when he/she is in less healthy conditions. Regarding the paraplegic whose condition provides us low movement data by default, the prior data is quite beneficial in this case (Opt. 1). Another way, a healthy person (or a physiotherapist (H<sub>2</sub>)) is required to help the paraplegic in demonstrating movement which will be applied during assistance (Opt. 2). This LfD approach, which is a part of our framework, can be used to achieve many complex tasks (if one's physical condition allows).

There are three scenarios:
<ul style="list-style-type:none;">
   <li> <b>Scenario I</b> is where we begin with a healthy subject and the physical condition of the same subject deteriorates to be a partially walk (muscle weakness grade 3 or 4) or paraplegic state,
   <li> <b>Scenario II</b> is where we begin with a partially walking subject, then the same subject becomes a paraplegic,
   <li> <b>Scenario III</b> is where we begin with a paraplegic with no movement data available.
</ul>

In general, we can obtain more subject walking data (especially, when they are in a good condition) in Scenario I > II > III.

Since our framework is based on learning from demonstration (LfD), a recorded data is by-product of our operating process, i.e., after zero-torque control (free mode). Different scenarios provide us different levels of data availability, i.e., data of healthy (H<sub>1</sub> data or Record#1), data of partially healthy (P<sub>1</sub> data or Record#2), or no data of a paraplegic (U<sub>1</sub>). This individual recorded data can be used directly as a base line to assist the subject when his/her condition is worse than before (H<sub>1</sub> data can be later used for either P<sub>1</sub> or U<sub>1</sub>, or P<sub>1</sub> data for U<sub>1</sub>). Another way, data pool can be created from several healthy movement/walking data. Then, the most appropriate pattern can be chosen and applied to the case that lacks healthy information.

In this way, the usage of the prior data (record-replay with possible modification) can be treated as an option (Opt. 1) to handle the paraplegic case where the condition by nature does not allow us to acquire more movement information to be used in the assistive period. On the other hand, without prior data, an additional healthy person (physiotherapist) is required to help the paraplegic in demonstrating the movement that will be applied during assistance (Opt. 2). 

Moreover, the LfD approach allows us to achieve complex tasks simply such as walking on a treadmill, stairs climbing (as shown here), sit-stand, or crossing obstacles

More details on the data pool concept are described below.

<br>
<div align="center">
   <img src="./pic/Datapool.png" width="500">
</div>
<br>

A concept of an extended EXOVIS control framework with a data pool scheme for unhealthy subjects.

First, without the support from external equipment such as motion capture, we expect to collect information from the healthy subjects (H) wearing the exoskeleton with the existing sensors and they perform our experimental protocol, i.e., the free and assistive modes. The resulting data can be collected to form the data pool. The variables as walking frequency from CPGs ($f$), weights from RBF networks ($\omega$), or other anthropological data can be used as a feature to create a clustering landscape capable of identifying different subject groups. This is based on the assumption that we cannot easily collect data from the unhealthy subject (UH) wearing the exoskeleton and that the appropriate frequency and pattern to drive the coupling system (human-exo) for an unknown group are derived from previous testing results with the same situation (human wearing the exoskeleton) rather than from data of pure natural walking (human walking without the exoskeleton). Besides, if we have information on various types of gait disorders, we can also simulate them with the healthy subject wearing the device to create the simulated unhealthy walking data pool for further use <a href="#santos_predictive_2021">[11]</a>.

When we use this control framework on an unseen unhealthy subject, we can divide it into two categories: partially walk and cannot walk. We can still collect the subject's walking data if he or she is still able to walk. The frequency and pattern of data collected during natural walking over a specific time period are extracted and used to identify walking characteristics in the clustering landscape. In our algorithm, the chosen frequency and pattern serve as initial values. If, on the other hand, a subject is unable to walk, we may be able to identify the initial values using other anthropological information.

After coupling with the device, the unhealthy subject may not be compatible with the chosen initial parameters. As a result, to adapt the device to the desired dynamics, an online tuning step is required. Our CPG-RBF framework allows for online tuning. Our CPG module with frequency adaptation can learn feedback online in real time, in this case the hip joint angle. In terms of pattern, our RBF module typically generates the joint trajectory point-by-point at each iteration. So, it is possible to adapt the weight set or directly introduce a scaling factor to regulate each joint angle profile online <a href="#manoonpong_neural_2013">[12]</a> to obtain a new profile, such as a smaller or larger hip angle for a shorter or longer stride.

***
## REFERENCES
<a id="wang_matsuokas_2020">[1]</a> Y. Wang, X. Xue, and B. Chen, “Matsuoka’s CPG With Desired Rhythmic Signals for Adaptive Walking of Humanoid Robots,” IEEE Transactions on Cybernetics, vol. 50, no. 2, pp. 613–626, Feb. 2020.

<a id="righetti_dynamic_2006">[2]</a> L. Righetti, J. Buchli, and A. J. Ijspeert, “Dynamic Hebbian learning in
adaptive frequency oscillators,” Physica D: Nonlinear Phenomena, vol.
216, no. 2, pp. 269–281, Apr. 2006.

<a id="ronsse_oscillator-based_2011">[3]</a> R. Ronsse <i>et al.</i>, “Oscillator-based assistance of cyclical movements: model-based and model-free approaches,” Med Biol Eng Comput, vol. 49, no. 10, pp. 1173–1185, Oct. 2011.

<a id="aguirre-ollinger_learning_2013">[4]</a> G. Aguirre-Ollinger, “Learning muscle activation patterns via nonlinear oscillators: Application to lower-limb assistance,” in 2013 IEEE/RSJ International Conference on Intelligent Robots and Systems, Tokyo, Nov. 2013, pp. 1182–1189.


<a id="delia_physical_2017">[5]</a> N. d’Elia <i>et al.</i>, “Physical human-robot interaction of an active pelvis orthosis: toward ergonomic assessment of wearable robots,” J NeuroEngineering Rehabil, vol. 14, no. 1, p. 29, Dec. 2017.

<a id="ruiz_garate_experimental_2017">[6]</a> V. Ruiz Garate <i>et al.</i>, “Experimental Validation of Motor Primitive-Based Control for Leg Exoskeletons during Continuous Multi-Locomotion Tasks,” Front. Neurorobot., vol. 11, Mar. 2017.

<a id="pasemann_complex_2002">[7]</a> F. Pasemann, “Complex dynamics and the structure of small neural networks,” Network, vol. 13, no. 2, pp. 195–216, Jan. 2002.

<a id="phodapol_grab_2022">[8]</a> S. Phodapol, T. Chuthong, B. Leung, A. Srisuchinnawong, P. Manoonpong, and N. Dilokthanakul, “GRAB: GRAdient-Based Shape-Adaptive Locomotion Control,” IEEE Robotics and Automation Letters, vol. 7, no. 2, pp. 1087–1094, Apr. 2022.

<a id="zemouri_recurrent_2003">[9]</a> R. Zemouri, D. Racoceanu, and N. Zerhouni, “Recurrent radial basis function network for time-series prediction,” Engineering Applications of Artificial Intelligence, vol. 16, no. 5, pp. 453–463, Aug. 2003.

<a id="shafiabady_proposal_2014">[10]</a> N. Shafiabady, D. Isa, and M. Vakilian, “The Proposal of Two New Recurrent Radial Basis Function Neural Networks,” International Journal of Computer Applications, vol. 92, Mar. 2014.

<a id="santos_predictive_2021">[11]</a> G. F. Santos, E. Jakubowitz, N. Pronost, T. Bonis, and C. Hurschler, “Predictive simulation of post-stroke gait with functional electrical stimulation,” Sci Rep, vol. 11, no. 1, p. 21351, Dec. 2021.

<a id="manoonpong_neural_2013">[12]</a> P. Manoonpong, U. Parlitz, and F. Wörgötter, “Neural control and adaptive neural forward models for insect-like, energy-efficient, and adaptable locomotion of walking machines,” Frontiers in Neural Circuits, vol. 7, p. 12, 2013.
