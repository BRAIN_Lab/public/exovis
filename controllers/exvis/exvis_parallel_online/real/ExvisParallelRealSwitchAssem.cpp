/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealSwitchAssem.h"

//***************************************************************************

//--------------------------------------------------------------------------
// initializer
//--------------------------------------------------------------------------
ExvisParallelRealSwitchAssem::ExvisParallelRealSwitchAssem(int argc,char* argv[]){

	// ROS related
    realRos = new ExvisParallelRealSwitchROS(argc, argv);



	if(ros::ok()) {
    	cout << "Initialize ExvisParallelRealSwitchAssem" << endl;
    }

}

//--------------------------------------------------------------------------
// runAssem
//--------------------------------------------------------------------------
bool ExvisParallelRealSwitchAssem::runAssem() {

	if(ros::ok()) {
		// cout << "********************************************************" << endl;
		// cout << "ExvisParallelRealSwitchAssem is running" << endl;
		// cout << "********************************************************" << endl;
		cout << nodeCounter << endl;


		// + Pub/Sub area --------------------------------
		// cout << "--------------- Published ---------------" << endl;
		// clock_gettime(CLOCK_REALTIME, &tNode);
		// cout << "tPublished: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		
		cout << "Input 1 to switch pattern between legs" << endl;
		while (true){
			// The flag to tell to switch pattern between R and L
			cin >> switchCode;
			if ((!cin.fail()) && (switchCode == 1)) {
				switchFlag = true;
				break;
			} else {
				cin.clear();
				cin.ignore();
			}
		}

  
		realRos->sendSwitch(switchFlag);



		// - Pub/Sub area --------------------------------
		
		

		// + Node spin to have sub value --------------------------------
		// cout << "--------------- Subscribed ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		// cout << "tSubscribed: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->rosSpinOnce();
		// - Node spin to have sub value --------------------------------

		nodeCounter++;
		cout << "********************************************************" << endl;
		return true;

	} else {
		cout << "Shutting down the node" << endl;
		return false;
	}
}





ExvisParallelRealSwitchAssem::~ExvisParallelRealSwitchAssem(){

}
