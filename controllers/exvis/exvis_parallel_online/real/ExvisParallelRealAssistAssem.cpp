/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealAssistAssem.h"

//***************************************************************************

//--------------------------------------------------------------------------
// initializer
//--------------------------------------------------------------------------
ExvisParallelRealAssistAssem::ExvisParallelRealAssistAssem(int argc,char* argv[]) {

	// ROS related
    realRos = new ExvisParallelRealAssistROS(argc, argv);

    if(ros::ok()) {
    	cout << "Initialize ExvisParallelRealAssistAssem" << endl;
    }

}

//--------------------------------------------------------------------------
// runAssem
//--------------------------------------------------------------------------
bool ExvisParallelRealAssistAssem::runAssem() {
	if(ros::ok()) {
		cout << "********************************************************" << endl;
		cout << "ExvisParallelRealAssistAssem is running" << endl;
		cout << "********************************************************" << endl;
		cout << nodeCounter << endl;
		
		printf("Please select PERCENTAGE OF ASSISTANCE: \n");
		printf("Integer number between 0-100 \n");
		printf("**WARNING** For safety reason, \n");
		printf("Speed 7 will limit %%Assist up to 80%% \n");
		printf("Speed 8 will limit %%Assist up to 60%% \n");
		printf("Speed 9 will limit %%Assist up to 50%% \n");
		printf("Speed 10 will limit %%Assist up to 40%% \n");
		
		while (true){
			cin >> assist;
			if ((!cin.fail()) && (assist >= 0 && assist <= 100)) {
				break;
			} else {
				cin.clear();
				cin.ignore();
			}
		}

		cout << "--------------- Published ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tPublished: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->sendAssist(assist);
		


		// + Node spin to have pub/sub value --------------------------------
		cout << "--------------- Subscribed ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tSubscribed: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->rosSpinOnce();
		// - Node spin to have pub/sub value --------------------------------

		nodeCounter++;
		cout << "********************************************************" << endl;
		return true;
		
	} else {
		cout << "Shutting down the node" << endl;
		return false;
	}
}

ExvisParallelRealAssistAssem::~ExvisParallelRealAssistAssem() {
	
}
