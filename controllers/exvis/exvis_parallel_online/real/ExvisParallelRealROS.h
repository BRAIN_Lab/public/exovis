/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#ifndef EXVISPARALLELREALROS_H
#define EXVISPARALLELREALROS_H

#include <stdio.h> // standard input / output functions
#include <unistd.h> // standard function definitions
#include <stdlib.h> // variable types, macros, functions
#include <string.h> // string function definitions
#include <iostream> // C++ to use cout
#include <math.h>
#include <vector>

using namespace std;

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Int32MultiArray.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <rosgraph_msgs/Clock.h>
#include <sensor_msgs/JointState.h>


#include "exvis_parallel_real_cpg_online/cpgService.h" // catkin_ws/devel/include
//***************************************************************************

class ExvisParallelRealROS {
	public:

		// ------------------------
		// Subscribers related variables
		// ------------------------
		double timeStep;

		// TIME:
		string timeStampCAN;
		// UI:
		bool notiNo_1_Done = false;
		bool switchFlag = false;
		// EXO:
			// Feedback signal
		vector<int> jointAngle = {0, 0, 0, 0, 0, 0}; // int = 4 bytes
		vector<int> jointTorque = {0, 0, 0, 0, 0, 0};
		vector<int> motorTorque = {0, 0, 0, 0, 0, 0};
		vector<int> footSwitch = {0, 0, 0, 0, 0, 0};
		vector<int> exoStatus = {0, 0, 0, 0, 0, 0};
		// CPG:
			// AFDC - Right
		double rightLegFreqAFDC; // Internal freq unit
		double rightCpgO0AFDC;
		double rightCpgO1AFDC;
		double rightCpgO2AFDC;
			// AFDC - Left
		double leftLegFreqAFDC; // Internal freq unit
		double leftCpgO0AFDC;
		double leftCpgO1AFDC;
		double leftCpgO2AFDC;
			// SO2 Learn - Right
		double rightLegFreqSO2Learn; // Internal freq unit
		double rightCpgO0SO2Learn;
		double rightCpgO1SO2Learn;
		double rightCpgO2SO2Learn;
			// SO2 Learn - Left
		double leftLegFreqSO2Learn; // Internal freq unit
		double leftCpgO0SO2Learn;
		double leftCpgO1SO2Learn;
		double leftCpgO2SO2Learn;
			// SO2 Action - Right
		double rightLegFreqSO2Action; // Internal freq unit
		double rightCpgO0SO2Action;
		double rightCpgO1SO2Action;
		double rightCpgO2SO2Action;
			// SO2 Action - Left
		double leftLegFreqSO2Action; // Internal freq unit
		double leftCpgO0SO2Action;
		double leftCpgO1SO2Action;
		double leftCpgO2SO2Action;

		// ------------------------
		// Publishers related variables
		// ------------------------
		// UI:
		// mode
		// 1. Basic control (Position, Stiffness, Torque)
		// 2. Adaptive control
		// 3. Motor disable
		// 4. Motor stop
		string mode = "3"; // 3 Default is Motor disable
		int speed = 6; // Default speed
		int dataPts = 50; // Default for number of data/walking cycle
		int assist = 100; // Default percentage of assistance
		int onlineVsManualFreq = -1; // 1: Online freq, 2: Manual input freq, 3: Manual inc/dec freq 
		double freqManual; // e.g., 0.5 Hz
		double upDownFreqVal; // Inc = +0.01, Dec = -0.01
		vector<int> setpointValueIn; // {x, x, x, x, x, x} position/stiffness/torque input value from user
		// EXO:
			// Control signal
		vector<int> jointAngleCon = {0, 0, 0, 0, 0, 0};
		vector<int> stiffnessCon = {0, 0, 0, 0, 0, 0};
		vector<int> torqueCon = {0, 0, 0, 0, 0, 0};


		// ------------------------
		// Public Methods
		// ------------------------
		ExvisParallelRealROS(int argc, char *argv[]);
		~ExvisParallelRealROS();
		
		// UI:
		void sendTypeCon(int typeCon);
		void sendSpeedCon(int speedCon);
		void sendAssistCon(int assistCon);
		void sendDataPtsCon(int dataPtsCon);
		void sendNotiNo(int notiNumber);
		// EXO:
			// Control signal
		void sendJointAngleCon(vector<int> jointAngleCon);
		void sendTorqueCon(vector<int> torqueCon);
		void sendStiffnessCon(vector<int> stiffnessCon);
		// CPG:
			// AFDC
		void sendFlagStrAFDC(bool flagStrAFDCV);
			// SO2 Action
		void sendFlagStrSO2Action(bool flagStrSO2ActionV);
		void sendFlagStrSO2ActionPhaseZero(bool flagStrSO2ActionPhaseZeroV);
		// How to use ----------------
		// cpgType: "AFDC", "SO2"
		// cpgFunc: 
		// 		"initOnline" = Create AFDC with initFreqXV (Internal freq unit)
		//		"initLearn"	= Create SO2 during Learn with initFreqXV (Internal freq unit)
		//		"initAction" = Create SO2 during Action with initFreqXV (Internal freq unit)
		//		"updateOnline" = update AFDC with updateFreqXV (Internal freq unit)
		//		"updateLearn" = update SO2 Learn with updateFreqXV (Internal freq unit)
		//		"updateLearn" = update SO2 Action with updateFreqXV (Internal freq unit)
		// Either initFreqXV or updateFreqXV is used at a time, so can input any value if one does not use.
		bool sendCpgService(const string& cpgTypeV, const string& cpgFuncV, double initFreqRV, double initFreqLV, double updateFreqRV, double updateFreqLV);

		void rosSpinOnce();



	private:
		ros::Rate* rate;

		// ------------------------
		// Subscribers 
		// ------------------------
		// TIME:
		ros::Subscriber timeStampCANSub;
		// UI:
		ros::Subscriber modeSub;
		ros::Subscriber speedSub;
		ros::Subscriber assistSub;
		ros::Subscriber onlineVsManualFreqSub;
		ros::Subscriber freqManualSub;
		ros::Subscriber upDownFreqValSub;
		ros::Subscriber notiNo_1_DoneSub;
		ros::Subscriber setpointValueInSub;
		ros::Subscriber switchFlagSub;
		// EXO:
			// Feedback signal
		ros::Subscriber jointAngleSub;
		ros::Subscriber jointTorqueSub;
		ros::Subscriber motorTorqueSub;
		ros::Subscriber footSwitchSub;
		ros::Subscriber exoStatusSub;
		// CPG:
			// AFDC - Right
		ros::Subscriber rightLegFreqAFDCSub;
		ros::Subscriber rightCpgO0AFDCSub;
		ros::Subscriber rightCpgO1AFDCSub;
		ros::Subscriber rightCpgO2AFDCSub;
			// AFDC - Left
		ros::Subscriber leftLegFreqAFDCSub;
		ros::Subscriber leftCpgO0AFDCSub;
		ros::Subscriber leftCpgO1AFDCSub;
		ros::Subscriber leftCpgO2AFDCSub;
			// SO2 Learn - Right
		ros::Subscriber rightLegFreqSO2LearnSub;
		ros::Subscriber rightCpgO0SO2LearnSub;
		ros::Subscriber rightCpgO1SO2LearnSub;
		ros::Subscriber rightCpgO2SO2LearnSub;
			// SO2 Learn - Left
		ros::Subscriber leftLegFreqSO2LearnSub;
		ros::Subscriber leftCpgO0SO2LearnSub;
		ros::Subscriber leftCpgO1SO2LearnSub;
		ros::Subscriber leftCpgO2SO2LearnSub;
			// SO2 Action - Right
		ros::Subscriber rightLegFreqSO2ActionSub;
		ros::Subscriber rightCpgO0SO2ActionSub;
		ros::Subscriber rightCpgO1SO2ActionSub;
		ros::Subscriber rightCpgO2SO2ActionSub;
			// SO2 Action - Left
		ros::Subscriber leftLegFreqSO2ActionSub;
		ros::Subscriber leftCpgO0SO2ActionSub;
		ros::Subscriber leftCpgO1SO2ActionSub;
		ros::Subscriber leftCpgO2SO2ActionSub;

		// ------------------------
		// Publishers
		// ------------------------
		// UI:
		ros::Publisher typeConPub;
		ros::Publisher speedConPub;
		ros::Publisher dataPtsConPub;
		ros::Publisher assistConPub;
		ros::Publisher notiNoPub;
		// EXO:
			// Control signal
		ros::Publisher jointAngleConPub;
		ros::Publisher torqueConPub;
		ros::Publisher stiffnessConPub;
		// CPG:
			// AFDC
		ros::Publisher flagStrAFDCPub;
			// SO2 Action
		ros::Publisher flagStrSO2ActionPub;
		ros::Publisher flagStrSO2ActionPhaseZeroPub;

		// ------------------------
		// Client 
		// ------------------------
		ros::ServiceClient cpgServiceCli;

		// ------------------------
		// Private Methods
		// ------------------------
		// Data from CAN is only 1 byte which equals to unsigned char (0 to 255) or signed char (-128 to 127), but
		// we will convert it to int (-247483648 to 247483647) 4 bytes and send back to this subscriber.
		// TIME:
		void timeStampCAN_CB(const std_msgs::String&);
		// UI:
		void mode_CB(const std_msgs::String&);
		void speed_CB(const std_msgs::Int32&);
		void assist_CB(const std_msgs::Int32&);
		void onlineVsManualFreq_CB(const std_msgs::Int32&);
		void freqManual_CB(const std_msgs::Float64&);
		void upDownFreqVal_CB(const std_msgs::Float64&);
		void notiNo_1_Done_CB(const std_msgs::Bool&);
		void setpointValueIn_CB(const std_msgs::Int32MultiArray&);
		void switchFlag_CB(const std_msgs::Bool&);
		// EXO:
			// Feedback signal
		void jointAngle_CB(const std_msgs::Int32MultiArray&);
		void jointTorque_CB(const std_msgs::Int32MultiArray&);
		void motorTorque_CB(const std_msgs::Int32MultiArray&);
		void footSwitch_CB(const std_msgs::Int32MultiArray&);
		void exoStatus_CB(const std_msgs::Int32MultiArray&);
		// CPG:
			// AFDC - Right
		void rightLegFreqAFDC_CB(const std_msgs::Float64&);
		void rightCpgO0AFDC_CB(const std_msgs::Float64&);
		void rightCpgO1AFDC_CB(const std_msgs::Float64&);
		void rightCpgO2AFDC_CB(const std_msgs::Float64&);
			// AFDC - Left
		void leftLegFreqAFDC_CB(const std_msgs::Float64&);
		void leftCpgO0AFDC_CB(const std_msgs::Float64&);
		void leftCpgO1AFDC_CB(const std_msgs::Float64&);
		void leftCpgO2AFDC_CB(const std_msgs::Float64&);
			// SO2 Learn - Right
		void rightLegFreqSO2Learn_CB(const std_msgs::Float64&);
		void rightCpgO0SO2Learn_CB(const std_msgs::Float64&);
		void rightCpgO1SO2Learn_CB(const std_msgs::Float64&);
		void rightCpgO2SO2Learn_CB(const std_msgs::Float64&);
			// SO2 Learn - Left
		void leftLegFreqSO2Learn_CB(const std_msgs::Float64&);
		void leftCpgO0SO2Learn_CB(const std_msgs::Float64&);
		void leftCpgO1SO2Learn_CB(const std_msgs::Float64&);
		void leftCpgO2SO2Learn_CB(const std_msgs::Float64&);
			// SO2 Action - Right
		void rightLegFreqSO2Action_CB(const std_msgs::Float64&);
		void rightCpgO0SO2Action_CB(const std_msgs::Float64&);
		void rightCpgO1SO2Action_CB(const std_msgs::Float64&);
		void rightCpgO2SO2Action_CB(const std_msgs::Float64&);
			// SO2 Action - Left
		void leftLegFreqSO2Action_CB(const std_msgs::Float64&);
		void leftCpgO0SO2Action_CB(const std_msgs::Float64&);
		void leftCpgO1SO2Action_CB(const std_msgs::Float64&);
		void leftCpgO2SO2Action_CB(const std_msgs::Float64&);

};


#endif //EXVISPARALLELREALROS_H
