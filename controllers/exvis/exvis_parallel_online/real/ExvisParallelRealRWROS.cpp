/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealRWROS.h"

//***************************************************************************

ExvisParallelRealRWROS::ExvisParallelRealRWROS(int argc, char **argv) {
    // Create a ROS nodes
    int _argc = 0;
    char** _argv = NULL;
    ros::init(_argc,_argv,"ExvisParallelRealRWAssem");

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("ExvisParallelRealRWROS just started!");

    // --------------------------
    // Initialize Subscribers
    // --------------------------
    // TIME:
    timeStampCANSub = node.subscribe("/exvis/timeStampCAN", 1, &ExvisParallelRealRWROS::timeStampCAN_CB, this);
    // UI:
    modeSub = node.subscribe("/exvis/in/mode", 1, &ExvisParallelRealRWROS::mode_CB, this);
    typeConSub = node.subscribe("/exvis/con/type", 1, &ExvisParallelRealRWROS::typeCon_CB, this);
    speedConSub = node.subscribe("/exvis/con/speed", 1, &ExvisParallelRealRWROS::speedCon_CB, this);
    assistConSub = node.subscribe("/exvis/con/assist", 1, &ExvisParallelRealRWROS::assistCon_CB, this);
    notiNoSub = node.subscribe("/exvis/noti/notiNo", 1, &ExvisParallelRealRWROS::notiNo_CB, this);
    // EXO:
        // Feedback
    jointAngleSub = node.subscribe("/exvis/jointAngle", 1, &ExvisParallelRealRWROS::jointAngle_CB, this);
    jointTorqueSub = node.subscribe("/exvis/jointTorque", 1, &ExvisParallelRealRWROS::jointTorque_CB, this);
    motorTorqueSub = node.subscribe("/exvis/motorTorque", 1, &ExvisParallelRealRWROS::motorTorque_CB, this);
    footSwitchSub = node.subscribe("/exvis/footSwitch", 1, &ExvisParallelRealRWROS::footSwitch_CB, this);
        // Control
    jointAngleConSub = node.subscribe("/exvis/con/jointAngle", 1, &ExvisParallelRealRWROS::jointAngleCon_CB, this);
    torqueConSub = node.subscribe("/exvis/con/torque", 1, &ExvisParallelRealRWROS::torqueCon_CB, this);
    stiffnessConSub = node.subscribe("/exvis/con/stiffness", 1, &ExvisParallelRealRWROS::stiffnessCon_CB, this);
    // CPG:
        // AFDC
    flagStrAFDCSub = node.subscribe("/exvis/flag/strAFDC", 1, &ExvisParallelRealRWROS::flagStrAFDC_CB, this);
        // AFDC - Right
    rightLegFreqAFDCSub = node.subscribe("/exvis/cpg/rightLegFreqAFDC", 1, &ExvisParallelRealRWROS::rightLegFreqAFDC_CB, this);
    rightCpgO0AFDCSub = node.subscribe("/exvis/cpg/rightCpgO0AFDC", 1, &ExvisParallelRealRWROS::rightCpgO0AFDC_CB, this);
    rightCpgO1AFDCSub = node.subscribe("/exvis/cpg/rightCpgO1AFDC", 1, &ExvisParallelRealRWROS::rightCpgO1AFDC_CB, this);
    rightCpgO2AFDCSub = node.subscribe("/exvis/cpg/rightCpgO2AFDC", 1, &ExvisParallelRealRWROS::rightCpgO2AFDC_CB, this);
        // AFDC - Left
    leftLegFreqAFDCSub = node.subscribe("/exvis/cpg/leftLegFreqAFDC", 1, &ExvisParallelRealRWROS::leftLegFreqAFDC_CB, this);
    leftCpgO0AFDCSub = node.subscribe("/exvis/cpg/leftCpgO0AFDC", 1, &ExvisParallelRealRWROS::leftCpgO0AFDC_CB, this);
    leftCpgO1AFDCSub = node.subscribe("/exvis/cpg/leftCpgO1AFDC", 1, &ExvisParallelRealRWROS::leftCpgO1AFDC_CB, this);
    leftCpgO2AFDCSub = node.subscribe("/exvis/cpg/leftCpgO2AFDC", 1, &ExvisParallelRealRWROS::leftCpgO2AFDC_CB, this);
        // SO2 Action
    flagStrSO2ActionSub = node.subscribe("/exvis/flag/strSO2Action", 1, &ExvisParallelRealRWROS::flagStrSO2Action_CB, this);
    flagStrSO2ActionPhaseZeroSub = node.subscribe("/exvis/flag/strSO2ActionPhaseZero", 1, &ExvisParallelRealRWROS::flagStrSO2ActionPhaseZero_CB, this);
        // SO2 Action - Right
    rightLegFreqSO2ActionSub = node.subscribe("/exvis/cpg/rightLegFreqSO2Action", 1, &ExvisParallelRealRWROS::rightLegFreqSO2Action_CB, this);
    rightCpgO0SO2ActionSub = node.subscribe("/exvis/cpg/rightCpgO0SO2Action", 1, &ExvisParallelRealRWROS::rightCpgO0SO2Action_CB, this);
    rightCpgO1SO2ActionSub = node.subscribe("/exvis/cpg/rightCpgO1SO2Action", 1, &ExvisParallelRealRWROS::rightCpgO1SO2Action_CB, this);
    rightCpgO2SO2ActionSub = node.subscribe("/exvis/cpg/rightCpgO2SO2Action", 1, &ExvisParallelRealRWROS::rightCpgO2SO2Action_CB, this);
        // SO2 Action - Left
    leftLegFreqSO2ActionSub = node.subscribe("/exvis/cpg/leftLegFreqSO2Action", 1, &ExvisParallelRealRWROS::leftLegFreqSO2Action_CB, this);
    leftCpgO0SO2ActionSub = node.subscribe("/exvis/cpg/leftCpgO0SO2Action", 1, &ExvisParallelRealRWROS::leftCpgO0SO2Action_CB, this);
    leftCpgO1SO2ActionSub = node.subscribe("/exvis/cpg/leftCpgO1SO2Action", 1, &ExvisParallelRealRWROS::leftCpgO1SO2Action_CB, this);
    leftCpgO2SO2ActionSub = node.subscribe("/exvis/cpg/leftCpgO2SO2Action", 1, &ExvisParallelRealRWROS::leftCpgO2SO2Action_CB, this);

    // --------------------------
    // Initialize Publishers
    // --------------------------

    // -----------
    // Set Rate
    // -----------
    timeStep = 0.001; // sec (0.001 s = 1 ms)
    rate = new ros::Rate(1/timeStep);
//    rate = new ros::Rate(100);
//    rate = new ros::Rate(17*4); // 60hz
//    rate = new ros::Rate(50);
//    rate = new ros::Rate(29);
//    rate = new ros::Rate(11.1);
//    rate = new ros::Rate(10);
//    rate = new ros::Rate(1);
//    rate = new ros::Rate(0.1);
}

// **********************************************************
// Subscriber callback
// **********************************************************
// TIME:
void ExvisParallelRealRWROS::timeStampCAN_CB(const std_msgs::String& _timeStampCAN) {

    // Get the data from Topic into main node variable
	timeStampCAN = _timeStampCAN.data;
	cout << "timeStampCAN: " << timeStampCAN << endl;
}

// UI:
void ExvisParallelRealRWROS::mode_CB(const std_msgs::String& _mode) {

    // Get the data from Topic into main node variable
	mode = _mode.data; // 
	// cout << "mode: " << mode << endl;
}

void ExvisParallelRealRWROS::typeCon_CB(const std_msgs::Int32& _typeCon) {

    // Get the data from Topic into main node variable
	typeCon = _typeCon.data; // 
	// cout << "typeCon: " << typeCon << endl;
}

void ExvisParallelRealRWROS::speedCon_CB(const std_msgs::Int32& _speedCon) {

    // Get the data from Topic into main node variable
	speedCon = _speedCon.data; // C++ int = ROS Int32
	// cout << "speedCon: " << speedCon << endl;
}

void ExvisParallelRealRWROS::assistCon_CB(const std_msgs::Int32& _assistCon) {

    // Get the data from Topic into main node variable
	assistCon = _assistCon.data; // C++ int = ROS Int32
	// cout << "assistCon: " << assistCon << endl;
}

void ExvisParallelRealRWROS::notiNo_CB(const std_msgs::Int32& _notiNo) {

    // Get the data from Topic into main node variable
	notiNo = _notiNo.data;
	// cout << "notiNo: " << notiNo << endl;
}

// EXO:
    // Feedback signal
void ExvisParallelRealRWROS::jointAngle_CB(const std_msgs::Int32MultiArray& _jointAngle) {

    // Get the data from Topic into main node variable
	jointAngle = _jointAngle.data; // C++ int = ROS Int32
    // cout << "Sub...jointAngle" << endl;
	// cout << "jointAngle: " << jointAngle[0] << " " << jointAngle[1] << " " << jointAngle[2] << " " << jointAngle[3] << " " << jointAngle[4] << " " << jointAngle[5] << endl;
}

void ExvisParallelRealRWROS::jointTorque_CB(const std_msgs::Int32MultiArray& _jointTorque) {

    // Get the data from Topic into main node variable
	jointTorque = _jointTorque.data; // C++ int = ROS Int32
    // cout << "Sub...jointTorque" << endl;
	// cout << "jointTorque: " << jointTorque[0] << " " << jointTorque[1] << " " << jointTorque[2] << " " << jointTorque[3] << " " << jointTorque[4] << " " << jointTorque[5] << endl;
}

void ExvisParallelRealRWROS::motorTorque_CB(const std_msgs::Int32MultiArray& _motorTorque) {

    // Get the data from Topic into main node variable
	motorTorque = _motorTorque.data; // C++ int = ROS Int32
    // cout << "Sub...motorTorque" << endl;
	// cout << "motorTorque: " << motorTorque[0] << " " << motorTorque[1] << " " << motorTorque[2] << " " << motorTorque[3] << " " << motorTorque[4] << " " << motorTorque[5] << endl;
}

void ExvisParallelRealRWROS::footSwitch_CB(const std_msgs::Int32MultiArray& _footSwitch) {

    // Get the data from Topic into main node variable
	footSwitch = _footSwitch.data; // C++ int = ROS Int32
    // cout << "Sub...footSwitch" << endl;
	// cout << "footSwitch: " << footSwitch[0] << " " << footSwitch[1] << " " << footSwitch[2] << " " << footSwitch[3] << " " << footSwitch[4] << " " << footSwitch[5] << endl;
}

    // Control signal
void ExvisParallelRealRWROS::jointAngleCon_CB(const std_msgs::Int32MultiArray& _jointAngleCon) {

    // Get the data from Topic into main node variable
	jointAngleCon = _jointAngleCon.data;
    // cout << "Sub...jointAngleCon" << endl;
	// cout << "jointAngleCon: " << jointAngleCon[0] << " " << jointAngleCon[1] << " " << jointAngleCon[2] << " " << jointAngleCon[3] << " " << jointAngleCon[4] << " " << jointAngleCon[5] << endl;

}

void ExvisParallelRealRWROS::torqueCon_CB(const std_msgs::Int32MultiArray& _torqueCon) {

    // Get the data from Topic into main node variable
	torqueCon = _torqueCon.data;
    // cout << "Sub...torqueCon" << endl;
	// cout << "torqueCon: " << torqueCon[0] << " " << torqueCon[1] << " " << torqueCon[2] << " " << torqueCon[3] << " " << torqueCon[4] << " " << torqueCon[5] << endl;

}

void ExvisParallelRealRWROS::stiffnessCon_CB(const std_msgs::Int32MultiArray& _stiffnessCon) {

    // Get the data from Topic into main node variable
	stiffnessCon = _stiffnessCon.data;
    // cout << "Sub...stiffnessCon" << endl;
	// cout << "stiffnessCon: " << stiffnessCon[0] << " " << stiffnessCon[1] << " " << stiffnessCon[2] << " " << stiffnessCon[3] << " " << stiffnessCon[4] << " " << stiffnessCon[5] << endl;

}

// CPG:
    // AFDC
void ExvisParallelRealRWROS::flagStrAFDC_CB(const std_msgs::Bool& _flagStrAFDC) {

    // Get the data from Topic into main node variable
	flagStrAFDC = _flagStrAFDC.data; //
	// cout << "realRos->flagStrAFDC: " << flagStrAFDC << endl;
}
    // AFDC - Right
void ExvisParallelRealRWROS::rightLegFreqAFDC_CB(const std_msgs::Float64& _rightLegFreqAFDC) {

    // Get the data from Topic into main node variable
	rightLegFreqAFDC = _rightLegFreqAFDC.data; // C++ double = ROS Float64
	// cout << "rightLegFreqAFDC: " << rightLegFreqAFDC << endl;
}

void ExvisParallelRealRWROS::rightCpgO0AFDC_CB(const std_msgs::Float64& _rightCpgO0AFDC) {

    // Get the data from Topic into main node variable
	rightCpgO0AFDC = _rightCpgO0AFDC.data; // C++ double = ROS Float64
	// cout << "rightCpgO0AFDC: " << rightCpgO0AFDC << endl;
}

void ExvisParallelRealRWROS::rightCpgO1AFDC_CB(const std_msgs::Float64& _rightCpgO1AFDC) {

    // Get the data from Topic into main node variable
	rightCpgO1AFDC = _rightCpgO1AFDC.data; // C++ double = ROS Float64
	// cout << "rightCpgO1AFDC: " << rightCpgO1AFDC << endl;
}

void ExvisParallelRealRWROS::rightCpgO2AFDC_CB(const std_msgs::Float64& _rightCpgO2AFDC) {

    // Get the data from Topic into main node variable
	rightCpgO2AFDC = _rightCpgO2AFDC.data; // C++ double = ROS Float64
	// cout << "rightCpgO2AFDC: " << rightCpgO2AFDC << endl;
}

    // AFDC - Left
void ExvisParallelRealRWROS::leftLegFreqAFDC_CB(const std_msgs::Float64& _leftLegFreqAFDC) {

    // Get the data from Topic into main node variable
	leftLegFreqAFDC = _leftLegFreqAFDC.data; // C++ double = ROS Float64
	// cout << "leftLegFreqAFDC: " << leftLegFreqAFDC << endl;
}

void ExvisParallelRealRWROS::leftCpgO0AFDC_CB(const std_msgs::Float64& _leftCpgO0AFDC) {

    // Get the data from Topic into main node variable
	leftCpgO0AFDC = _leftCpgO0AFDC.data; // C++ double = ROS Float64
	// cout << "leftCpgO0AFDC: " << leftCpgO0AFDC << endl;
}

void ExvisParallelRealRWROS::leftCpgO1AFDC_CB(const std_msgs::Float64& _leftCpgO1AFDC) {

    // Get the data from Topic into main node variable
	leftCpgO1AFDC = _leftCpgO1AFDC.data; // C++ double = ROS Float64
	// cout << "leftCpgO1AFDC: " << leftCpgO1AFDC << endl;
}

void ExvisParallelRealRWROS::leftCpgO2AFDC_CB(const std_msgs::Float64& _leftCpgO2AFDC) {

    // Get the data from Topic into main node variable
	leftCpgO2AFDC = _leftCpgO2AFDC.data; // C++ double = ROS Float64
	// cout << "leftCpgO2AFDC: " << leftCpgO2AFDC << endl;
}
    // SO2 Action
void ExvisParallelRealRWROS::flagStrSO2Action_CB(const std_msgs::Bool& _flagStrSO2Action) {

    // Get the data from Topic into main node variable
	flagStrSO2Action = _flagStrSO2Action.data; //
	// cout << "realRos->flagStrSO2Action: " << flagStrSO2Action << endl;
}

void ExvisParallelRealRWROS::flagStrSO2ActionPhaseZero_CB(const std_msgs::Bool& _flagStrSO2ActionPhaseZero) {

    // Get the data from Topic into main node variable
	flagStrSO2ActionPhaseZero = _flagStrSO2ActionPhaseZero.data; //
	// cout << "realRos->flagStrSO2ActionPhaseZero: " << flagStrSO2ActionPhaseZero << endl;
}

    // SO2 Action - Right
void ExvisParallelRealRWROS::rightLegFreqSO2Action_CB(const std_msgs::Float64& _rightLegFreqSO2Action) {

    // Get the data from Topic into main node variable
	rightLegFreqSO2Action = _rightLegFreqSO2Action.data; // C++ double = ROS Float64
	// cout << "rightLegFreqSO2Action: " << rightLegFreqSO2Action << endl;
}

void ExvisParallelRealRWROS::rightCpgO0SO2Action_CB(const std_msgs::Float64& _rightCpgO0SO2Action) {

    // Get the data from Topic into main node variable
	rightCpgO0SO2Action = _rightCpgO0SO2Action.data; // C++ double = ROS Float64
	// cout << "rightCpgO0SO2Action: " << rightCpgO0SO2Action << endl;
}

void ExvisParallelRealRWROS::rightCpgO1SO2Action_CB(const std_msgs::Float64& _rightCpgO1SO2Action) {

    // Get the data from Topic into main node variable
	rightCpgO1SO2Action = _rightCpgO1SO2Action.data; // C++ double = ROS Float64
	// cout << "rightCpgO1SO2Action: " << rightCpgO1SO2Action << endl;
}

void ExvisParallelRealRWROS::rightCpgO2SO2Action_CB(const std_msgs::Float64& _rightCpgO2SO2Action) {

    // Get the data from Topic into main node variable
	rightCpgO2SO2Action = _rightCpgO2SO2Action.data; // C++ double = ROS Float64
	// cout << "rightCpgO2SO2Action: " << rightCpgO2SO2Action << endl;
}

    // SO2 Action - Left
void ExvisParallelRealRWROS::leftLegFreqSO2Action_CB(const std_msgs::Float64& _leftLegFreqSO2Action) {

    // Get the data from Topic into main node variable
	leftLegFreqSO2Action = _leftLegFreqSO2Action.data; // C++ double = ROS Float64
	// cout << "leftLegFreqSO2Action: " << leftLegFreqSO2Action << endl;
}

void ExvisParallelRealRWROS::leftCpgO0SO2Action_CB(const std_msgs::Float64& _leftCpgO0SO2Action) {

    // Get the data from Topic into main node variable
	leftCpgO0SO2Action = _leftCpgO0SO2Action.data; // C++ double = ROS Float64
	// cout << "leftCpgO0SO2Action: " << leftCpgO0SO2Action << endl;
}

void ExvisParallelRealRWROS::leftCpgO1SO2Action_CB(const std_msgs::Float64& _leftCpgO1SO2Action) {

    // Get the data from Topic into main node variable
	leftCpgO1SO2Action = _leftCpgO1SO2Action.data; // C++ double = ROS Float64
	// cout << "leftCpgO1SO2Action: " << leftCpgO1SO2Action << endl;
}

void ExvisParallelRealRWROS::leftCpgO2SO2Action_CB(const std_msgs::Float64& _leftCpgO2SO2Action) {

    // Get the data from Topic into main node variable
	leftCpgO2SO2Action = _leftCpgO2SO2Action.data; // C++ double = ROS Float64
	// cout << "leftCpgO2SO2Action: " << leftCpgO2SO2Action << endl;
}

// **********************************************************
// Publisher send function
// **********************************************************



// **********************************************************
// rosSpinOnce
// **********************************************************
void ExvisParallelRealRWROS::rosSpinOnce(){

	cout << "ExvisParallelRealRWROS node is spinning" << endl;

	ros::spinOnce();
    bool rateMet = rate->sleep();

    if(!rateMet)
    {
        ROS_ERROR("Sleep rate not met");
    }

}

ExvisParallelRealRWROS::~ExvisParallelRealRWROS() {
    ROS_INFO("ExvisParallelRealRWROS just terminated!");
    ros::shutdown();
}