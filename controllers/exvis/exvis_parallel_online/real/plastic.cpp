/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "plastic.h"

//***************************************************************************

plastic::plastic(double o0, double o1, double o2, double initial_phi, double _alpha, double _bi) {
	out0_t = o0;
	out1_t = o1;
	out2_t = o2;
	phi = initial_phi;
	alpha =_alpha;
	w20_t = 0;//0
	w02_t = 1;//1
	w2p_t = 0.01;//0.01
	learning = 1.3;//1.3
	A02 = 1;//1
	A20 = 1;//1
	A2p = 1;//1
	B02 = _bi;
	B20 = _bi;
	B2p = _bi;

	w00 = alpha*cos(phi);
	w01 = alpha*sin(phi);
	w10 = alpha*(-sin(phi));
	w11 = alpha*cos(phi);
}
plastic::plastic(double o0,double o1,double o2, double initial_phi,double _alpha) {


	out0_t = o0;
	out1_t = o1;
	out2_t = o2;
	phi = initial_phi;
	alpha =_alpha;
	w20_t = 0; //
	w02_t = 1; //1
	w2p_t = 0.03; //0.03

	// Default = 1.3 for CPG initial freq = 0.04
	learning = 1.3;

	A02 = 1;
	A20 = 1;
	A2p = 1;
	B02 = 0.01;
	B20 = 0.01;
	B2p = 0.01;

	w00 = alpha*cos(phi);
	w01 = alpha*sin(phi);
	w10 = alpha*(-sin(phi));
	w11 = alpha*cos(phi);

	xPer = 0;

	// TODO Auto-generated constructor stub

}

plastic::~plastic() {
	// TODO Auto-generated destructor stub
}

void plastic::updateWeights() {
	double e = 1;

	phi = phi + learning*w02_t*out2_t*w01*out1_t*e; //update phi and weights

	w00 = alpha*cos(phi);
	w01 = alpha*sin(phi);
	w10 = alpha*(-sin(phi));
	w11 = alpha*cos(phi);
}

double plastic::getOut0() {
	return out0_t;
}

double plastic::getOut1() {
	return out1_t;
}

double plastic::getOut2() {
	return out2_t;
}


void plastic::setPhi(double newPhi) {
	phi = newPhi;

}


double plastic::getFreq() {

	if (alpha != 1.01)
	{
		std::cout << "change alpha to compute frequency";
		return -1;
	}
	else
	return phi/(2*M_PI);



}

double plastic::getW20() {
	return w20_t;
}

double plastic::getW02() {
	return w02_t;
}

double plastic::getW2p() {
	return w2p_t;
}

double plastic::getW01() {
	return w01;
}

// -------------------------------------
// AFDC
// -------------------------------------
void plastic::update(double perturbation, int step, double timeStep) {
	double w20_init = 0;
	double w02_init = 1;//1
	double w2p_init = 0.03;//0.05//must check on this!!!!!

//	if (perturbation >= 0.1) {
//		xPer = 0.2;
//	}
//	if (perturbation <= -0.1) {
//		xPer = -0.2;
//	}

	xPer = perturbation;

	// Apply lower neuron update according to update rate
	// timeStep = 0.001 is working for this setup.
	// If we apply new timeStep, we find the ratio and use it to lower update rate.
	// Ex.
	// 		We use timeStep = 0.001 s (1 ms) -> We want every 0.1/0.001 = 100 step to update once.
	//
	// From testing, I have found out that:

	// initFreqCPG = 0.1 (1 Hz real world) -> need 10 round update for a cycle
	// This means:
	//		update time 10 ms x 10 = 100 ms is a cycle = 10 Hz cpg
	//		update time 50 ms x 10 = 500 ms is a cycle = 2 Hz cpg
	//		update time 100 ms x 10 = 1000 ms is a cycle = 1 Hz cpg
	//
	//
	// initFreqCPG = 0.01 (0.1 Hz real world) -> need 100 round update for a cycle
	// This means:
	//		update time 100 ms x 100 = 10000 ms is a cycle = 0.1 Hz cpg
	//
	//
	// initFreqCPG = 0.2 (2 Hz real world) -> need ? round update for a cycle
	// If we put (2 Hz, 50 ms), it is not work and not equal to (1 Hz, 50 ms).
	//
	//
	// initFreqCPG = 0.2 (2 Hz real world) -> need 20 round update for a cycle
	// This means:
	//		update time 50 ms x 20 = 1000 ms is a cycle = 1 Hz cpg ---> Fail
	//		update time 25 ms x 20 = 500 ms is a cycle = 2 Hz cpg ---> Fail
	//
	//
	// initFreqCPG = 0.2 (2 Hz real world) -> need 5 round update for a cycle
	// This means:
	//		update time 100 ms x 5 = 500 ms is a cycle = 2 Hz cpg ---> Fail
	//
	//
	// initFreqCPG = 0.01 (0.1 Hz real world) -> need 100 round update for a cycle
	// This means:
	// 		update time 10 ms x 100 = 1000 ms is a cycle = 1 Hz cpg
	//
	//
	// Please be noted that the graph of cpg freq is plotted from initFreqCPG
	// It will not reflect the real signal, if we do not use the proper update time.

	// double convNo = 0.001; // update CPG once every 1 ms (Not a good shape)
	double convNo = 0.01; // update CPG once every 10 ms (Work)
	// double convNo = 0.05; // update CPG once every 50 ms
	// double convNo = 0.1; // update CPG once every 100 ms

	if ((step % int(convNo/timeStep)) == 0){
		out0_t1 = tanh(w00*out0_t + w01*out1_t + w02_t*out2_t);
		out1_t1 = tanh(w10*out0_t + w11*out1_t);
		out2_t1 = tanh(w20_t*out0_t + w2p_t*xPer);

	//	// For testing
	//	out0_t1 = tanh(1.5*out0_t + (-0.4)*out1_t);
	//	out1_t1 = tanh(0.4*out0_t + 1.5*out1_t);
	//	out2_t1 = tanh(w20_t*out0_t + w2p_t*xPer);

	//	out0_t1 = tanh(w00*out0_t + w01*out1_t);
	//	out1_t1 = tanh(w10*out0_t + w11*out1_t);
	//	out2_t1 = tanh(w20_t*out0_t + w2p_t*xPer);


		updateWeights();

		w20_t1 = w20_t - A20*out2_t*out0_t - B20*(w20_t - w20_init);
		w02_t1 = w02_t - A02*out0_t*out2_t - B02*(w02_t - w02_init);
	//	w02_t1 = 0; // For testing
		w2p_t1 = w2p_t + A2p*out2_t*xPer - B2p*(w2p_t - w2p_init);


		out0_t = out0_t1;
		out1_t = out1_t1;
		out2_t = out2_t1;

		w20_t = w20_t1;
		w02_t = w02_t1;
		w2p_t = w2p_t1;
	}
}

// -------------------------------------
// SO2
// -------------------------------------
void plastic::updateSoTwo(double internal_phi, int step, double timeStep) {

	phi = internal_phi;

	double convNo = 0.01; // update CPG once every 10 ms

	if ((step % int(convNo/timeStep)) == 0){
		out0_t1 = tanh(w00*out0_t + w01*out1_t);
		out1_t1 = tanh(w10*out0_t + w11*out1_t);


		w00 = alpha*cos(phi);
		w01 = alpha*sin(phi);
		w10 = alpha*(-sin(phi));
		w11 = alpha*cos(phi);

		out0_t = out0_t1;
		out1_t = out1_t1;

	}
}






