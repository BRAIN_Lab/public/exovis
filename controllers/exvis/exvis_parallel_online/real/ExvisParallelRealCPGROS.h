/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#ifndef EXVISPARALLELREALCPGROS_H
#define EXVISPARALLELREALCPGROS_H

#include <stdio.h> // standard input / output functions
#include <unistd.h> // standard function definitions
#include <stdlib.h> // variable types, macros, functions
#include <string.h> // string function definitions
#include <iostream> // C++ to use cout
#include <fstream> // To read/write on file
#include <math.h>
#include <vector>
#include <pwd.h> // check directory
#include <sys/time.h>
#include <numeric> // for inner_product
#include <sys/stat.h> // check file existing

using namespace std;

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Int32MultiArray.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayLayout.h>


#include "jsoncpp/json/json.h"

#include "exvis_parallel_real_cpg_online/cpgService.h" // catkin_ws/devel/include
//***************************************************************************

class ExvisParallelRealCPGROS {
	public:
		// Public Global Variables

		// Read data -------------------
		string timeStampCAN;
		vector<int> jointAngle = {0, 0, 0, 0, 0, 0}; // int = 4 bytes
		vector<int> jointTorque = {0, 0, 0, 0, 0, 0};
		vector<int> footSwitch = {0, 0, 0, 0, 0, 0};
		vector<int> motorTorque = {0, 0, 0, 0, 0, 0};

		double timeStep;


		// CPG Create
		// Common
		// AFDC
		double initFreqRAFDC; // Internal freq unit
		double initFreqLAFDC; // Internal freq unit
		bool flagStrAFDC = false;
		double updateFreqRAFDC; // Internal freq unit
		double updateFreqLAFDC; // Internal freq unit
		bool flagUpdateAFDC = false;
		// SO2 Learn
		double initFreqRSO2Learn; // Internal freq unit
		double initFreqLSO2Learn; // Internal freq unit
		bool flagStrSO2Learn = false;
		double updateFreqRSO2Learn; // Internal freq unit
		double updateFreqLSO2Learn; // Internal freq unit
		bool flagUpdateSO2Learn = false;
		// SO2 Action
		double initFreqRSO2Action; // Internal freq unit
		double initFreqLSO2Action; // Internal freq unit
		bool flagStrSO2Action = false;
		double updateFreqRSO2Action; // Internal freq unit
		double updateFreqLSO2Action; // Internal freq unit
		bool flagUpdateSO2Action = false;



		// Public Methods
		ExvisParallelRealCPGROS(int argc, char *argv[]);
		~ExvisParallelRealCPGROS();
		
        // 
        void sendLegFreq(double cpgFreq, const string& cpgLeg, const string& cpgType, const string& cpgFunc);
        void sendCpgO0(double cpgO0, const string& cpgLeg, const string& cpgType, const string& cpgFunc);
		void sendCpgO1(double cpgO1, const string& cpgLeg, const string& cpgType, const string& cpgFunc);
        void sendCpgO2(double cpgO2, const string& cpgLeg, const string& cpgType, const string& cpgFunc);

		void rosSpinOnce();



	private:
		// Subscribers
		ros::Subscriber timeStampCANSub;
		ros::Subscriber jointAngleSub;
		ros::Subscriber jointTorqueSub;
		ros::Subscriber footSwitchSub;
		ros::Subscriber motorTorqueSub;

		ros::Subscriber notiCPGLearnSub;
		ros::Subscriber notiCPGActionSub;

		// Publishers
        // AFDC Right
		ros::Publisher rightLegFreqAFDCPub;
		ros::Publisher rightCpgO0AFDCPub;
		ros::Publisher rightCpgO1AFDCPub;
		ros::Publisher rightCpgO2AFDCPub;
		// AFDC Left
		ros::Publisher leftLegFreqAFDCPub;
		ros::Publisher leftCpgO0AFDCPub;
		ros::Publisher leftCpgO1AFDCPub;
		ros::Publisher leftCpgO2AFDCPub;

        // SO2 during Learn
		// Right
        ros::Publisher rightLegFreqSO2LearnPub;
        ros::Publisher rightCpgO0SO2LearnPub;
		ros::Publisher rightCpgO1SO2LearnPub;
		ros::Publisher rightCpgO2SO2LearnPub;
        // Left
        ros::Publisher leftLegFreqSO2LearnPub;
        ros::Publisher leftCpgO0SO2LearnPub;
		ros::Publisher leftCpgO1SO2LearnPub;
		ros::Publisher leftCpgO2SO2LearnPub;


		// SO2 during Action
		// Right
        ros::Publisher rightLegFreqSO2ActionPub;
        ros::Publisher rightCpgO0SO2ActionPub;
		ros::Publisher rightCpgO1SO2ActionPub;
		ros::Publisher rightCpgO2SO2ActionPub;
        // Left
        ros::Publisher leftLegFreqSO2ActionPub;
        ros::Publisher leftCpgO0SO2ActionPub;
		ros::Publisher leftCpgO1SO2ActionPub;
		ros::Publisher leftCpgO2SO2ActionPub;

		// Service
		ros::ServiceServer cpgServiceSrv; 

		// Private Global Variables
		ros::Rate* rate;

		// Private Methods
		// Data from CAN is only 1 byte which equals to unsigned char (0 to 255) or signed char (-128 to 127), but
		// we will convert it to int (-247483648 to 247483647) 4 bytes and send back to this subscriber.
		void timeStampCAN_CB(const std_msgs::String&);
		void jointAngle_CB(const std_msgs::Int32MultiArray&);
		void jointTorque_CB(const std_msgs::Int32MultiArray&);
		void footSwitch_CB(const std_msgs::Int32MultiArray&);
		void motorTorque_CB(const std_msgs::Int32MultiArray&);

		bool cpgService_CB(exvis_parallel_real_cpg_online::cpgService::Request&, exvis_parallel_real_cpg_online::cpgService::Response&);


};


#endif //EXVISPARALLELREALCPGROS_H
