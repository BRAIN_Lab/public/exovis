/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealAssistROS.h"

//***************************************************************************

ExvisParallelRealAssistROS::ExvisParallelRealAssistROS(int argc, char **argv) {
    // Create a ROS nodes
    int _argc = 0;
    char** _argv = NULL;
    ros::init(_argc,_argv,"ExvisParallelRealAssistAssem");

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("ExvisParallelRealAssistROS just started!");

    // Initialize Subscribers


    // Initialize Publishers
     assistPub = node.advertise<std_msgs::Int32>("/exvis/in/assist",1);

    // Set Rate
     rate = new ros::Rate(1000);
//    rate = new ros::Rate(17*4); // 60hz
//    rate = new ros::Rate(100);
//    rate = new ros::Rate(11.1);
}

// **********************************************************
// sendAssist
// **********************************************************
void ExvisParallelRealAssistROS::sendAssist(int assist) {
    std_msgs::Int32 assistP;
    assistP.data = assist;
    assistPub.publish(assistP);
    cout << "assist: " << assistP.data << endl;
    cout << "... assist Published !" << endl;
}


// **********************************************************
// rosSpinOnce
// **********************************************************
void ExvisParallelRealAssistROS::rosSpinOnce(){

	cout << "ExvisParallelRealAssistROS node is spinning" << endl;

	ros::spinOnce();
    bool rateMet = rate->sleep();

    if(!rateMet)
    {
        ROS_ERROR("Sleep rate not met");
    }

}

ExvisParallelRealAssistROS::~ExvisParallelRealAssistROS() {
    ROS_INFO("ExvisParallelRealAssistROS just terminated!");
    ros::shutdown();
}
