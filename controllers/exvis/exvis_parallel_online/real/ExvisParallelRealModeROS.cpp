/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealModeROS.h"

//***************************************************************************

ExvisParallelRealModeROS::ExvisParallelRealModeROS(int argc, char **argv) {
    // Create a ROS nodes
    int _argc = 0;
    char** _argv = NULL;
    ros::init(_argc,_argv,"ExvisParallelRealModeAssem");

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("ExvisParallelRealModeROS just started!");

    // Initialize Subscribers


    // Initialize Publishers
    modePub = node.advertise<std_msgs::String>("/exvis/in/mode",1);

    setpointValueInPub = node.advertise<std_msgs::Int32MultiArray>("/exvis/in/setpointValueIn",1);


    // Set Rate
     rate = new ros::Rate(1000);
//     rate = new ros::Rate(17*4); // 60hz
//     rate = new ros::Rate(100);
//     rate = new ros::Rate(11.1);
}

// **********************************************************
// sendMode
// **********************************************************
void ExvisParallelRealModeROS::sendMode(string mode) {
    std_msgs::String modeP;
    modeP.data = mode;
    modePub.publish(modeP);
    cout << "mode: " << modeP.data << " ... Published !" << endl;
}

void ExvisParallelRealModeROS::sendSetpointValueIn(vector<int> setpointValueInV) {
    std_msgs::Int32MultiArray array;
    array.data.clear();

    for (int e : setpointValueInV) {
        array.data.push_back(e);
    }

    setpointValueInPub.publish(array);
    
    cout << "setpointValueIn: " << setpointValueInV[0] << " " << setpointValueInV[1] << " " << setpointValueInV[2] << " " << setpointValueInV[3] << " " << setpointValueInV[4] << " " << setpointValueInV[5] << " ... Published !"<< endl;
}


// **********************************************************
// rosSpinOnce
// **********************************************************
void ExvisParallelRealModeROS::rosSpinOnce(){

	cout << "ExvisParallelRealModeROS node is spinning" << endl;

	ros::spinOnce();
    bool rateMet = rate->sleep();

    if(!rateMet)
    {
        ROS_ERROR("Sleep rate not met");
    }

}

ExvisParallelRealModeROS::~ExvisParallelRealModeROS() {
    ROS_INFO("ExvisParallelRealModeROS just terminated!");
    ros::shutdown();
}
