/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "DynamicVector.h"

//***************************************************************************

DynamicVector::DynamicVector(int size) {
	// TODO Auto-generated constructor stub
	for(int i=0;i<size;i++)
		vec.push_back(0);
}

int DynamicVector::getSize()
{
	return vec.size();
}

void DynamicVector::changeSize(unsigned long int newSize)
{
	double temp=vec.at(vec.size()-1);
	vec.resize(newSize,temp);
}


double DynamicVector::update(double value)
{
    if(vec.size() == 0)
    	return value;
    else if(vec.size() == 1)
	{
    	double temp=vec.at(0);
    	vec[0]=value;
    	return temp;

	}

    else
    {
	double temp=vec.at(0);
    for(int i=0;i<=vec.size()-2;i++)
		vec[i]=vec.at(i+1);
	vec[vec.size()-1] = value;

    return temp;
    }
}

DynamicVector::~DynamicVector() {
	// TODO Auto-generated destructor stub
}
