/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#ifndef DERIVATIVETRANSITIONREGISTER_H
#define DERIVATIVETRANSITIONREGISTER_H

#include <vector>
#include <math.h>
#include <iostream> //for plotting
#include <fstream> //plotting

//***************************************************************************

class derivativeTransitionRegister {
	public:
		derivativeTransitionRegister(double theresholdValueNegToZero, double theresholdValue0ToPos);
		virtual ~derivativeTransitionRegister();
		void registerDerivative(double derivativeHip, double signalHip,  double derivativeMotor, double signalMotor,int step, double amplitude);
		int counter;
		double getActualError();
		bool checked();
		double getHipValues();
		double getMotorValues();
		int getCounter();
		bool goodPhase();
	private:

		double motorValue, hipValue, thereshold0,theresholdPos, motorValue0ToPos, hipValue0ToPos;
};

#endif // DERIVATIVETRANSITIONREGISTER_H
