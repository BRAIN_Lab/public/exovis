/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealModeAssem.h"

//***************************************************************************

//--------------------------------------------------------------------------
// initializer
//--------------------------------------------------------------------------
ExvisParallelRealModeAssem::ExvisParallelRealModeAssem(int argc,char* argv[]) {

	// ROS related
    realRos = new ExvisParallelRealModeROS(argc, argv);

    if(ros::ok()) {
    	cout << "Initialize ExvisParallelRealModeAssem" << endl;
    }

}

//--------------------------------------------------------------------------
// runAssem
//--------------------------------------------------------------------------
bool ExvisParallelRealModeAssem::runAssem() {
	if(ros::ok()) {
		cout << "********************************************************" << endl;
		cout << "ExvisParallelRealModeAssem is running" << endl;
		cout << "********************************************************" << endl;
		cout << nodeCounter << endl;
		
		printf("Please select MODE OF CONTROL: \n");
		printf("I. Basic\n");
		printf(" 111 Position > Default pattern\n");
		printf(" 112 Position > Position setpoint\n");
		printf(" 113 Position > Discrete pattern\n");
		printf(" 122 Stiffness > Stiffness setpoint\n");
		printf(" 131 Torque > Zero torque\n");
		printf(" 132 Torque > Torque setpoint\n");
		printf("II. Adaptive\n");
		printf(" 21  Reflex&CPG\n");
		printf(" 22  Speed\n");
		printf(" 23  Pattern Pos\n");
		printf(" 24  Pattern Tor\n");
		printf(" 25  ChangeSpeed\n");
		printf("III. Device on, motor is passive\n");
		printf(" 3   Motor disable\n");
		printf("IV. Device on, motor is fixed\n");
		printf(" 4   Motor stop\n");
		printf(" 5   Check\n");
		printf(" 6   RBF discrete\n");


		while (true){
			getline(cin, mode);
			modeMain = (int)mode.at(0) - 48; // '0' in ascii is 48

			if ((!cin.fail()) && (modeMain >= 1 && modeMain <= 6)) {
				if (mode.size() == 1) {
					cout << "ModeMain: " << modeMain << endl;	
				} else if (mode.size() == 2) {
					modeSub = (int)mode.at(1) - 48;
					cout << "ModeMain: " << modeMain << " " << "ModeSub: " << modeSub << endl;
				} else if (mode.size() == 3) {
					modeSub = (int)mode.at(1) - 48;
					modeSubSub = (int)mode.at(2) - 48;
					cout << "ModeMain: " << modeMain << " "<< "ModeSub: " << modeSub << " " << "ModeSubSub: " << modeSubSub << endl;
				}
				// break;
			} else {
				cout << "Please enter correct value!" << endl;
				cin.clear();
				cin.ignore();
			}


			if (modeMain == 1 && modeSub == 1 && modeSubSub == 2) { // Position setpoint
				cout << "Position setpoint in int: RHip RKnee RAnkle LHip LKnee LAnkle" << endl;
				getSetpointIn();
			}

			if (modeMain == 1 && modeSub == 2 && modeSubSub == 2) { // Stiffness setpoint
				cout << "Stiffness setpoint in int: RHip RKnee RAnkle LHip LKnee LAnkle" << endl;
				getSetpointIn();
			}

			if (modeMain == 1 && modeSub == 3 && modeSubSub == 2) { // Torque setpoint
				cout << "Torque setpoint in int: RHip RKnee RAnkle LHip LKnee LAnkle" << endl;
				getSetpointIn();
			}



			break; // From while
		}

		cout << "--------------- Published ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tPublished: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->sendMode(mode);
		


		// + Node spin to have sub value --------------------------------
		cout << "--------------- Subscribed ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tSubscribed: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->rosSpinOnce();
		// - Node spin to have sub value --------------------------------

		nodeCounter++;
		cout << "********************************************************" << endl;
		return true;
		
	} else {
		cout << "Shutting down the node" << endl;
		return false;
	}
}

void ExvisParallelRealModeAssem::getSetpointIn(){

	string setpointText;
	string setpointValue[6]; // array of vectors
	vector<int> setpointValueIn = {0, 0, 0, 0, 0, 0};
	int numCounter = 0; // until 5
	bool flagValueIn = true;

	getline(cin, setpointText);
		
	if (!cin.fail()){
		int spaceCounter = 0;
		for (int i = 0; i <= setpointText.size(); i++) {
			if (numCounter <= 5){
				if (!isspace(setpointText[i]) && (setpointText[i] != '\n') && (setpointText[i] != NULL)) {
					spaceCounter = 0;
					// isdigit checks ASCII value of char
					// Enter causes NULL whose ASCII = 0, so it will cause isdigit true
					if (isdigit(setpointText[i]) || (setpointText[i] == '-')) { // allow negative sign 
						setpointValue[numCounter].push_back(setpointText[i]);
					} else {
						cout << "x" << setpointText[i] << "x" << endl;
						cout << "Please input in correct format" << endl;
						setpointValue[0] = "0";
						setpointValue[1] = "0";
						setpointValue[2] = "0";
						setpointValue[3] = "0";
						setpointValue[4] = "0";
						setpointValue[5] = "0";
						flagValueIn = false;
						break; // From for
					}
				} else { // space
					spaceCounter ++;
					if (spaceCounter == 1){ // First space
						numCounter++;
					}
				}
			} else {
				break;
			}
			
		}

	}
	
	if (numCounter == 6 && flagValueIn){
		setpointValueIn[0] = stoi(setpointValue[0]);
		setpointValueIn[1] = stoi(setpointValue[1]);
		setpointValueIn[2] = stoi(setpointValue[2]);
		setpointValueIn[3] = stoi(setpointValue[3]);
		setpointValueIn[4] = stoi(setpointValue[4]);
		setpointValueIn[5] = stoi(setpointValue[5]);
		// cout << "setpointValue: " << setpointValueIn[0] << " " << setpointValueIn[1] << " " << setpointValueIn[2] << " " << setpointValueIn[3] << " " << setpointValueIn[4] << " " << setpointValueIn[5] << endl;
		realRos->sendSetpointValueIn(setpointValueIn);
	}
}

ExvisParallelRealModeAssem::~ExvisParallelRealModeAssem() {
	
}
