/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealAssem.h"

//***************************************************************************

//--------------------------------------------------------------------------
// initializer
//--------------------------------------------------------------------------
ExvisParallelRealAssem::ExvisParallelRealAssem(int argc,char* argv[]) {

	// ROS related
    realRos = new ExvisParallelRealROS(argc, argv);


    // Initialize save file
	if ((homedir = getenv("HOME")) == NULL) {
		homedir = getpwuid(getuid())->pw_dir;
	}

	// + Info. for ZeroTorque in JSON ------------------------------
	// JSON read
	// Open: When enter mode 23 with jsonReadZeroTorqueFn()
	// Close: After reading inside jsonReadZeroTorqueFn(), close node
	strcpy(jsonReadZeroTorque, homedir);
	strcat(jsonReadZeroTorque, "/Experiment/exvisResult/exvisJSONZeroTorque.json");
	// - Info. for ZeroTorque in JSON ------------------------------


	// + Pattern adaptation result in JSON -------------------------------
	// JSON write
	// Open: After finishing learning in mode 23
	// Close: After finishing writing, close node
	strcpy(jsonWritePat, homedir);
	strcat(jsonWritePat, "/Experiment/exvisResult/patternResult.json");
	// JSON read
	// Open: Mode 23 before action
	// Close: After reading insdie jsonReadPatFn(), close node
	strcpy(jsonReadPat, homedir);
	strcat(jsonReadPat, "/Experiment/exvisResult/patternResult.json");
	// - Pattern adaptation result in JSON -------------------------------


	// + Weight in TXT ---------------------------------------------------
	// Open: Mode 23 during learning
	// Close: After finishing learning in mode 23, close node
	strcpy(txtWriteWeightPat, homedir);
	strcat(txtWriteWeightPat, "/Experiment/exvisResult/weightPat.txt");
	// - Weight in TXT ---------------------------------------------------


	// + Discrete pattern result in JSON -------------------------------
	// JSON write
	// Open: After finishing learning in mode 23
	// Close: After finishing writing, close node
	strcpy(jsonWritePatDist, homedir);
	strcat(jsonWritePatDist, "/Experiment/exvisResult/patternDistResult.json");
	// JSON read
	// Open: Mode 23 before action
	// Close: After reading insdie jsonReadPatFn(), close node
	strcpy(jsonReadPatDist, homedir);
	strcat(jsonReadPatDist, "/Experiment/exvisResult/patternDistResult.json");
	// - Discrete pattern result in JSON -------------------------------


    if(ros::ok()) {
    	cout << "Initialize ExvisParallelRealAssem" << endl;
    }

}

//--------------------------------------------------------------------------
// runAssem
//--------------------------------------------------------------------------
bool ExvisParallelRealAssem::runAssem() {

	if(ros::ok()) {
		cout << "********************************************************" << endl;
		cout << "ExvisParallelRealAssem is running" << endl;
		cout << "********************************************************" << endl;
		cout << nodeCounter << endl;


		// + Pub/Sub area --------------------------------
		cout << "--------------- Published ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tPublished: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;


		// Initialize AFDC ==========================================================
		if (!flagStrAFDC) {
			flagStrAFDC = realRos->sendCpgService("AFDC", "initOnline", initFreqRAFDC, initFreqRAFDC, 0, 0);
			cout << "flagStrAFDC: " << flagStrAFDC << endl;
		}
		realRos->sendFlagStrAFDC(flagStrAFDC);


		// ==========================================================
		// "Please select MODE OF CONTROL:
		// 1. Basic
		//	1.1) Position
		//		1.1.1) Default pattern
		//		1.1.2) Position setpoint
		//		1.1.3) Discrete pattern
		//	1.2) Stiffness
		//		1.2.2) Stiffness setpoint
		//	1.3) Torque
		//		1.3.1) Zero torque
		//		1.3.2) Torque setpoint
		// 2. Adaptive
		//	2.1) Reflex&CPGs, 
		// 	2.2) Speed
		//	2.3) Pattern Pos
		//	2.4) Pattern Tor
		//  2.5) ChangeSpeed
		// 3. Motor disable
		// 4. Motor stop
		// 5. Check
		// 6. RBF discrete

		switch (notiNo) {
			case 1 :
				// 1 = Caculation complete, wait user acknowledge, and start action
				// Need to complete mode 23
				modeMain = 2;
				modeSub = 3;
				break;
			
			default : // i.e., notiNo == 0; Can receive input normally
				modeMain = (int)(realRos->mode.at(0)) - 48;
				break;
		}

		switch (modeMain) {
			case 1 : { // 1. Basic
				adaptiveControlFlag = false;
				motorDisableFlag = false;
				motorStopFlag = false;
				checkFlag = false;
				rbfDistFlag = false;

				// 1.1) Position
				// 1.2) Stiffness
				// 1.3) Torque
				modeSub = (int)(realRos->mode.at(1)) - 48;

				// 1.1.1) Default pattern
				// 1.1.2) Postion setpoint
				// 1.1.3) Discrete pattern
				// 1.2.2) Stiffness setpoint
				// 1.3.1) Zero torque
				// 1.3.2) Torque setpoint
				modeSubSub = (int)(realRos->mode.at(2) - 48);

				basicControl(modeSub, modeSubSub, realRos->speed, realRos->assist);
				break;
			}
			case 2 : { // 2. Adaptive
				basicControlFlag = false;
				motorDisableFlag = false;
				motorStopFlag = false;
				checkFlag = false;
				rbfDistFlag = false;

				// 2.1) Reflex&CPG
				// 2.2) Speed
				// 2.3) Pattern Pos
				// 2.4) Pattern Tor
				// 2.5) ChangeSpeed
				modeSub = (int)(realRos->mode.at(1)) - 48;

				adaptiveControl(modeSub, realRos->speed, realRos->assist);

				break;
			}
			case 3 : {// 3. Motor disable
				basicControlFlag = false;
				adaptiveControlFlag = false;
				motorStopFlag = false;
				checkFlag = false;
				rbfDistFlag = false;

				// + To reset things in mode 23, 25 ------------
				// CPG:
				// 	Allow SO2Learn to start again
				flagStrSO2Learn = false;
				//  Allow SO2Action to start again
				flagStrSO2Action = false;
				// - To reset things in mode 23, 25 ------------


				// + To reset things in mode 113, 6 ------------
				// CPG:
				// 	Allow SO2Learn to start again
				rd_flagStrSO2Learn = false;
				//  Allow SO2Action to start again
				rd_flagStrSO2Action = false;
				// - To reset things in mode 113, 6 ------------

				motorDisable();
				break;
			}
			case 4 : {// 4. Motor stop
				basicControlFlag = false;
				adaptiveControlFlag = false;
				motorDisableFlag = false;
				checkFlag = false;
				rbfDistFlag = false;

				// + To reset things in mode 23, 25 ------------
				// CPG:
				// 	Allow SO2Learn to start again
				flagStrSO2Learn = false;
				//  Allow SO2Action to start again
				flagStrSO2Action = false;
				// - To reset things in mode 23, 25 ------------


				// + To reset things in mode 113, 6 ------------
				// CPG:
				// 	Allow SO2Learn to start again
				rd_flagStrSO2Learn = false;
				//  Allow SO2Action to start again
				rd_flagStrSO2Action = false;
				// - To reset things in mode 113, 6 ------------


				motorStop();
				break;
			}

			case 5 : {// 5. Check
				basicControlFlag = false;
				adaptiveControlFlag = false;
				motorDisableFlag = false;
				motorStopFlag = false;
				rbfDistFlag = false;

				// *NOTING FOR NOW

				break;
			}

			case 6 : {// 6. RBF discrete
				basicControlFlag = false;
				adaptiveControlFlag = false;
				motorDisableFlag = false;
				motorStopFlag = false;
				checkFlag = false;

				// This is to calculate RBF for discrete signal only.
				rbfDist(realRos, realRos->speed, realRos->assist);
				
				break;
			}
		
		}

		// Show data on screen ==========================================================
		showData();


		// - Pub/Sub area --------------------------------
		
		
		for (int i = 1; i <= 1; i++) {
			cout << "onlineVsManualFreq: " << realRos->onlineVsManualFreq << endl;
		}

		
		

		// + Node spin to have sub value --------------------------------
		cout << "--------------- Subscribed ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tSubscribed: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->rosSpinOnce();
		// - Node spin to have sub value --------------------------------

		nodeCounter++;
		cout << "********************************************************" << endl;
		return true;

	} else {
		cout << "Shutting down the node" << endl;
		return false;
	}
}


//--------------------------------------------------------------------------
// controller
//--------------------------------------------------------------------------

bool ExvisParallelRealAssem::basicControl(int basicModeSub, int basicModeSubSub, int speed, int assist){


	cout << "----------------- Mode of operation ------------------" << endl;
	cout << "BASIC MODE" << endl;
	cout << "Speed: " << speed << endl;
	cout << "Assist: " << assist << endl;

	// Check if basicControl is needed to start from beginning
	if (!basicControlFlag) {
		initialST = true;
		firstST = true;
		iFirstStep = 0;
		iNormalStep = 0;
		basicControlFlag = true;

	}

	basicModeSub = basicModeSub;
	basicModeSubSub = basicModeSubSub;
	switch (basicModeSub) {
		// Position control
		case 1 : {
			cout << "Basic > Position control" << endl;
			switch (basicModeSubSub) {
				case 1: {
					cout << "Basic > Possition control > Default Pattern" << endl;
					// Type of control
					realRos->sendTypeCon(1);

					// Set speed
					realRos->sendSpeedCon(speed);


					// For safety reason,
					// Speed 7 will limit %Assist up to 80%
					// Speed 8 will limit %Assist up to 60%
					// Speed 9 will limit %Assist up to 50%
					// Speed 10 will limit %Assist up to 40%
					if (speed == 7) {
						if (assist > 80) {
							assist = 80;
						}

					} else if (speed == 8) {
						if (assist > 60) {
							assist = 60;
						}
					} else if (speed == 9) {
						if (assist > 50) {
							assist = 50;
						}
					} else if (speed == 10) {
						if (assist > 40) {
							assist = 40;
						}
					}

					// Set percentage of assistance
					realRos->sendAssistCon(assist);



					// Initial step
					if (initialST) {
						// Set number of data per walking cycle
						realRos->sendDataPtsCon(1);
						// Joint angle
						realRos->sendJointAngleCon(InitST);
						initialST = false;
					}

					// First step
					if (firstST) {
						realRos->sendDataPtsCon(25);
						switch (speed) {
							case 1 : {
								if (nodeCounter%180 == 0) {
									if (iFirstStep < 25) {
										realRos->sendJointAngleCon(FirstStep[iFirstStep]);
										iFirstStep++;
									} else {
										firstST = false;
									}
								}
								break;
							}
							case 2 : {
								if (nodeCounter%168 == 0) {
									if (iFirstStep < 25) {
										realRos->sendJointAngleCon(FirstStep[iFirstStep]);
										iFirstStep++;
									} else {
										firstST = false;
									}
								}
								break;
							}
							case 3 : {
								if (nodeCounter%156 == 0) {
									if (iFirstStep < 25) {
										realRos->sendJointAngleCon(FirstStep[iFirstStep]);
										iFirstStep++;
									} else {
										firstST = false;
									}
								}
								break;
							}
							case 4 : {
								if (nodeCounter%144 == 0) {
									if (iFirstStep < 25) {
										realRos->sendJointAngleCon(FirstStep[iFirstStep]);
										iFirstStep++;
									} else {
										firstST = false;
									}
								}
								break;
							}
							case 5 : {
								if (nodeCounter%132 == 0) {
									if (iFirstStep < 25) {
										realRos->sendJointAngleCon(FirstStep[iFirstStep]);
										iFirstStep++;
									} else {
										firstST = false;
									}
								}
								break;
							}
							case 6 : {
								if (nodeCounter%120 == 0) {
									if (iFirstStep < 25) {
										realRos->sendJointAngleCon(FirstStep[iFirstStep]);
										iFirstStep++;
									} else {
										firstST = false;
									}
								}
								break;
							}
							case 7 : {
								if (nodeCounter%108 == 0) {
									if (iFirstStep < 25) {
										realRos->sendJointAngleCon(FirstStep[iFirstStep]);
										iFirstStep++;
									} else {
										firstST = false;
									}
								}
								break;
							}
							case 8 : {
								if (nodeCounter%96 == 0) {
									if (iFirstStep < 25) {
										realRos->sendJointAngleCon(FirstStep[iFirstStep]);
										iFirstStep++;
									} else {
										firstST = false;
									}
								}
								break;
							}
							case 9 : {
								if (nodeCounter%84 == 0) {
									if (iFirstStep < 25) {
										realRos->sendJointAngleCon(FirstStep[iFirstStep]);
										iFirstStep++;
									} else {
										firstST = false;
									}
								}
								break;
							}
							case 10 : {
								if (nodeCounter%72 == 0) {
									if (iFirstStep < 25) {
										realRos->sendJointAngleCon(FirstStep[iFirstStep]);
										iFirstStep++;
									} else {
										firstST = false;
									}
								}
								break;
							}

						}



					} else {

						// Normal step
						realRos->sendDataPtsCon(51);

						// Speed can be changed after finishing one cycle only.
						if ((speed != speedOld) && (iNormalStep != 0)) {
							speed = speedOld;
						}

						switch (speed) {
							case 1 : {
								if (nodeCounter%90 == 0) {
									if (iNormalStep < 50) {
										realRos->sendJointAngleCon(NormalStep[iNormalStep]);
										iNormalStep++;
									} else {
										iNormalStep = 0;
									}
								}
								break;
							}
							case 2 : {
								if (nodeCounter%84 == 0) {
									if (iNormalStep < 50) {
										realRos->sendJointAngleCon(NormalStep[iNormalStep]);
										iNormalStep++;
									} else {
										iNormalStep = 0;
									}
								}
								break;
							}
							case 3 : {
								if (nodeCounter%78 == 0) {
									if (iNormalStep < 50) {
										realRos->sendJointAngleCon(NormalStep[iNormalStep]);
										iNormalStep++;
									} else {
										iNormalStep = 0;
									}
								}
								break;
							}
							case 4 : {
								if (nodeCounter%72 == 0) {
									if (iNormalStep < 50) {
										realRos->sendJointAngleCon(NormalStep[iNormalStep]);
										iNormalStep++;
									} else {
										iNormalStep = 0;
									}
								}
								break;
							}
							case 5 : {
								if (nodeCounter%66 == 0) {
									if (iNormalStep < 50) {
										realRos->sendJointAngleCon(NormalStep[iNormalStep]);
										iNormalStep++;
									} else {
										iNormalStep = 0;
									}
								}
								break;
							}
							case 6 : {
								if (nodeCounter%60 == 0) {
									if (iNormalStep < 50) {
										realRos->sendJointAngleCon(NormalStep[iNormalStep]);
										iNormalStep++;
									} else {
										iNormalStep = 0;
									}
								}
								break;
							}
							case 7 : {
								if (nodeCounter%54 == 0) {
									if (iNormalStep < 50) {
										realRos->sendJointAngleCon(NormalStep[iNormalStep]);
										iNormalStep++;
									} else {
										iNormalStep = 0;
									}
								}
								break;
							}
							case 8 : {
								if (nodeCounter%48 == 0) {
									if (iNormalStep < 50) {
										realRos->sendJointAngleCon(NormalStep[iNormalStep]);
										iNormalStep++;
									} else {
										iNormalStep = 0;
									}
								}
								break;
							}
							case 9 : {
								if (nodeCounter%42 == 0) {
									if (iNormalStep < 50) {
										realRos->sendJointAngleCon(NormalStep[iNormalStep]);
										iNormalStep++;
									} else {
										iNormalStep = 0;
									}
								}
								break;
							}
							case 10 : {
								if (nodeCounter%36 == 0) {
									if (iNormalStep < 50) {
										realRos->sendJointAngleCon(NormalStep[iNormalStep]);
										iNormalStep++;
									} else {
										iNormalStep = 0;
									}
								}
								break;
							}

						}

						speedOld = speed;


					}

					break;
				} // case 1 Default pattern

				case 2 : {

					cout << "Basic > Position control > Position setpoint" << endl;
					// Type of control
					realRos->sendTypeCon(1);

					// Set speed
					realRos->sendSpeedCon(speed);


					// For safety reason,
					// Speed 7 will limit %Assist up to 80%
					// Speed 8 will limit %Assist up to 60%
					// Speed 9 will limit %Assist up to 50%
					// Speed 10 will limit %Assist up to 40%
					if (speed == 7) {
						if (assist > 80) {
							assist = 80;
						}

					} else if (speed == 8) {
						if (assist > 60) {
							assist = 60;
						}
					} else if (speed == 9) {
						if (assist > 50) {
							assist = 50;
						}
					} else if (speed == 10) {
						if (assist > 40) {
							assist = 40;
						}
					}

					// Set percentage of assistance
					realRos->sendAssistCon(assist);

					// Position setpoint
					PositionSetPoint = realRos->setpointValueIn;
					realRos->sendJointAngleCon(PositionSetPoint);


					break;
				} // case 2 Position setpoint


				case 3 : {

					cout << "Basic > Position control > Discrete pattern" << endl;
					// Type of control
					realRos->sendTypeCon(1);

					// Set speed
					realRos->sendSpeedCon(speed);


					// For safety reason,
					// Speed 7 will limit %Assist up to 80%
					// Speed 8 will limit %Assist up to 60%
					// Speed 9 will limit %Assist up to 50%
					// Speed 10 will limit %Assist up to 40%
					if (speed == 7) {
						if (assist > 80) {
							assist = 80;
						}

					} else if (speed == 8) {
						if (assist > 60) {
							assist = 60;
						}
					} else if (speed == 9) {
						if (assist > 50) {
							assist = 50;
						}
					} else if (speed == 10) {
						if (assist > 40) {
							assist = 40;
						}
					}

					// Set percentage of assistance
					realRos->sendAssistCon(assist);

					// Discrete
					discretePattern(realRos, speed, assist);


					break;
				} // case 3 Discrete
				
			} // switch

			break; 
		} // Position control

		// Stiffness control
		case 2 : {
			cout << "Basic mode: Stiffness control" << endl;
			switch (basicModeSubSub){
				case 2 : {
					cout << "Basic > Stiffness control > Stiffness setpoint" << endl;
					// Type of control
					realRos->sendTypeCon(2);

					// Set speed
					realRos->sendSpeedCon(speed);

					// Stiffness setpoint
					StiffnessSetPoint = realRos->setpointValueIn;
					realRos->sendStiffnessCon(StiffnessSetPoint);


					break;
				}

				break;
			}


			break;
		}

		// Torque control
		case 3 : {
			cout << "Basic > Torque control" << endl;
			switch (basicModeSubSub) {
				case 1 : {
					cout << "Basic > Torque control > Zero torque" << endl;
					// Type of control
					realRos->sendTypeCon(3);

					// Set speed
					realRos->sendSpeedCon(speed);

					// Set assist
					assist = 100;
					realRos->sendAssistCon(assist);

					// Torque setpoint
					realRos->sendTorqueCon(TorqueSetPoint);

					break;
				}

				case 2 : {
					cout << "Basic > Torque control > Torque setpoint" << endl;

					// Type of control
					realRos->sendTypeCon(3);

					// Set speed
					realRos->sendSpeedCon(speed);

					// Set assist
					assist = 100;
					realRos->sendAssistCon(assist);                                 

					// Torque setpoint
					TorqueSetPoint = realRos->setpointValueIn;
					realRos->sendTorqueCon(TorqueSetPoint);



					if (realRos->setpointValueIn[0] == 9999){
						if (iTorqueProfile < 10){
							realRos->sendTorqueCon(TorqueProfile[iTorqueProfile]);
						} else {
							iTorqueProfile = 0;
						}
					}

					break;

				}

			}

			break;
		}
	}

	return true;
}

bool ExvisParallelRealAssem::adaptiveControl(int adaptiveModeSub, int speed, int assist){

	cout << "----------------- Mode of operation ------------------" << endl;
	cout << "ADAPTIVE MODE" << endl;
	cout << "Speed: " << speed << endl;
	cout << "Assist: " << assist << endl;
	cout << "adaptiveModeSub: " << adaptiveModeSub << endl;

	// Check if adaptiveMode is needed to start from beginning
	if (!adaptiveControlFlag) {
		initialST = true;
		firstST = true;
		iFirstStep = 0;
		iNormalStep = 0;
		adaptiveControlFlag = true;

	}

	switch (adaptiveModeSub) {
		case 1 : {// Reflex&CPG
			cout << "AdaptiveMode > Reflex&CPG" << endl;

			// Type of control
			realRos->sendTypeCon(1);

			// Set speed
			realRos->sendSpeedCon(speed);


			// For safety reason,
			// Speed 7 will limit %Assist up to 80%
			// Speed 8 will limit %Assist up to 60%
			// Speed 9 will limit %Assist up to 50%
			// Speed 10 will limit %Assist up to 40%
			if (speed == 7) {
				if (assist > 80) {
					assist = 80;
				}

			} else if (speed == 8) {
				if (assist > 60) {
					assist = 60;
				}
			} else if (speed == 9) {
				if (assist > 50) {
					assist = 50;
				}
			} else if (speed == 10) {
				if (assist > 40) {
					assist = 40;
				}
			}

			// Set percentage of assistance
			realRos->sendAssistCon(assist);


			// Initial step
			if (initialST) {
				// Set number of data per walking cycle
				realRos->sendDataPtsCon(1);
				// Joint angle
				realRos->sendJointAngleCon(InitST);
				initialST = false;
			}

			// First step
			if (firstST) {
				realRos->sendDataPtsCon(25);
				switch (speed) {
					case 1 : {
						if (nodeCounter%180 == 0) {
							if (iFirstStep < 25) {
								realRos->sendJointAngleCon(FirstStep[iFirstStep]);
								iFirstStep++;
							} else {
								firstST = false;
							}
						}
						break;
					}
					case 2 : {
						if (nodeCounter%168 == 0) {
							if (iFirstStep < 25) {
								realRos->sendJointAngleCon(FirstStep[iFirstStep]);
								iFirstStep++;
							} else {
								firstST = false;
							}
						}
						break;
					}
					case 3 : {
						if (nodeCounter%156 == 0) {
							if (iFirstStep < 25) {
								realRos->sendJointAngleCon(FirstStep[iFirstStep]);
								iFirstStep++;
							} else {
								firstST = false;
							}
						}
						break;
					}
					case 4 : {
						if (nodeCounter%144 == 0) {
							if (iFirstStep < 25) {
								realRos->sendJointAngleCon(FirstStep[iFirstStep]);
								iFirstStep++;
							} else {
								firstST = false;
							}
						}
						break;
					}
					case 5 : {
						if (nodeCounter%132 == 0) {
							if (iFirstStep < 25) {
								realRos->sendJointAngleCon(FirstStep[iFirstStep]);
								iFirstStep++;
							} else {
								firstST = false;
							}
						}
						break;
					}
					case 6 : {
						if (nodeCounter%120 == 0) {
							if (iFirstStep < 25) {
								realRos->sendJointAngleCon(FirstStep[iFirstStep]);
								iFirstStep++;
							} else {
								firstST = false;
							}
						}
						break;
					}
					case 7 : {
						if (nodeCounter%108 == 0) {
							if (iFirstStep < 25) {
								realRos->sendJointAngleCon(FirstStep[iFirstStep]);
								iFirstStep++;
							} else {
								firstST = false;
							}
						}
						break;
					}
					case 8 : {
						if (nodeCounter%96 == 0) {
							if (iFirstStep < 25) {
								realRos->sendJointAngleCon(FirstStep[iFirstStep]);
								iFirstStep++;
							} else {
								firstST = false;
							}
						}
						break;
					}
					case 9 : {
						if (nodeCounter%84 == 0) {
							if (iFirstStep < 25) {
								realRos->sendJointAngleCon(FirstStep[iFirstStep]);
								iFirstStep++;
							} else {
								firstST = false;
							}
						}
						break;
					}
					case 10 : {
						if (nodeCounter%72 == 0) {
							if (iFirstStep < 25) {
								realRos->sendJointAngleCon(FirstStep[iFirstStep]);
								iFirstStep++;
							} else {
								firstST = false;
							}
						}
						break;
					}

				}



			} else { // Normal step

				cout << "Normal step" << endl;

				int angleRH = realRos->jointAngle[0];
				int angleRK = realRos->jointAngle[1];
				int angleRA = realRos->jointAngle[2];
				int angleLH = realRos->jointAngle[3];
				int angleLK = realRos->jointAngle[4];
				int angleLA = realRos->jointAngle[5];
				int fsHeelR = realRos->footSwitch[0];
				int fsToeR = realRos->footSwitch[1];
				int fsHeelL = realRos->footSwitch[2];
				int fsToeL = realRos->footSwitch[3];

				int yGR; // Right foot switch logic
				int yGL; // Left foot switch logic

				// Low pass filter
				// -


				// Define the state of feet touching the ground
				// Right leg at front
				if (angleRK < 10) { // Knee has to be straight
					if (fsToeR > 50) { // Foot switch active
						cout << "RightLegFront" << endl;
						yGR = 1;
						yGL = 0;
					}
				}

				// Left leg at front
				if (angleLK < 10) { // Knee has to be straight
					if (fsToeL > 50) { // Foot switch active
						cout << "LeftLegFront" << endl;
						yGR = 0;
						yGL = 1;
					}
				}



				// Speed can be changed after finishing one cycle only.
				if ((speed != speedOld) && (iNormalStep != 0)) {
					speed = speedOld;
				}


				switch (speed) {
					case 1 : {

						// Right leg at front
						if ((yGR == 1 && yGL == 0) || (lSwingFirst && !lSwingFinish)) {
							cout << "speed1|RightLegFront" << endl;
							lSwingFirst = true;

							// We need to swing left leg
							if (nodeCounter%90 == 0) {
								if (iNormalStep < 30) {
									realRos->sendJointAngleCon(NormalStepLSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									lSwingFinish = true;
								}
							}
						} // End GL/GR

						// Left leg at front
						if ((yGR == 0 && yGL == 1) || (rSwingFirst && !rSwingFinish)) {
							cout << "speed1|LeftLegFront" << endl;
							rSwingFirst = true;

							// We need to swing right leg
							if (nodeCounter%90 == 0) {
								if (iNormalStep < 21) {
									realRos->sendJointAngleCon(NormalStepRSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									rSwingFinish = true;
								}
							}
						} // End GL/GR

						break;
					}
					case 2 : {

						// Right leg at front
						if ((yGR == 1 && yGL == 0) || (lSwingFirst && !lSwingFinish)) {

							lSwingFirst = true;

							// We need to swing left leg
							if (nodeCounter%84 == 0) {
								if (iNormalStep < 30) {
									realRos->sendJointAngleCon(NormalStepLSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									lSwingFinish = true;
								}
							}
						} // End GL/GR

						// Left leg at front
						if ((yGR == 0 && yGL == 1) || (rSwingFirst && !rSwingFinish)) {

							rSwingFirst = true;

							// We need to swing right leg
							if (nodeCounter%84 == 0) {
								if (iNormalStep < 21) {
									realRos->sendJointAngleCon(NormalStepRSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									rSwingFinish = true;
								}
							}
						} // End GL/GR

						break;
					}
					case 3 : {

						// Right leg at front
						if ((yGR == 1 && yGL == 0) || (lSwingFirst && !lSwingFinish)) {

							lSwingFirst = true;

							// We need to swing left leg
							if (nodeCounter%78 == 0) {
								if (iNormalStep < 30) {
									realRos->sendJointAngleCon(NormalStepLSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									lSwingFinish = true;
								}
							}
						} // End GL/GR

						// Left leg at front
						if ((yGR == 0 && yGL == 1) || (rSwingFirst && !rSwingFinish)) {

							rSwingFirst = true;

							// We need to swing right leg
							if (nodeCounter%78 == 0) {
								if (iNormalStep < 21) {
									realRos->sendJointAngleCon(NormalStepRSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									rSwingFinish = true;
								}
							}
						} // End GL/GR

						break;
					}
					case 4 : {

						// Right leg at front
						if ((yGR == 1 && yGL == 0) || (lSwingFirst && !lSwingFinish)) {

							lSwingFirst = true;

							// We need to swing left leg
							if (nodeCounter%72 == 0) {
								if (iNormalStep < 30) {
									realRos->sendJointAngleCon(NormalStepLSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									lSwingFinish = true;
								}
							}
						} // End GL/GR

						// Left leg at front
						if ((yGR == 0 && yGL == 1) || (rSwingFirst && !rSwingFinish)) {

							rSwingFirst = true;

							// We need to swing right leg
							if (nodeCounter%72 == 0) {
								if (iNormalStep < 21) {
									realRos->sendJointAngleCon(NormalStepRSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									rSwingFinish = true;
								}
							}
						} // End GL/GR

						break;
					}
					case 5 : {

						// Right leg at front
						if ((yGR == 1 && yGL == 0) || (lSwingFirst && !lSwingFinish)) {

							lSwingFirst = true;

							// We need to swing left leg
							if (nodeCounter%66 == 0) {
								if (iNormalStep < 30) {
									realRos->sendJointAngleCon(NormalStepLSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									lSwingFinish = true;
								}
							}
						} // End GL/GR

						// Left leg at front
						if ((yGR == 0 && yGL == 1) || (rSwingFirst && !rSwingFinish)) {

							rSwingFirst = true;

							// We need to swing right leg
							if (nodeCounter%66 == 0) {
								if (iNormalStep < 21) {
									realRos->sendJointAngleCon(NormalStepRSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									rSwingFinish = true;
								}
							}
						} // End GL/GR

						break;
					}
					case 6 : {

						// Right leg at front
						if ((yGR == 1 && yGL == 0) || (lSwingFirst && !lSwingFinish)) {

							lSwingFirst = true;

							// We need to swing left leg
							if (nodeCounter%60 == 0) {
								if (iNormalStep < 30) {
									realRos->sendJointAngleCon(NormalStepLSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									lSwingFinish = true;
								}
							}
						} // End GL/GR

						// Left leg at front
						if ((yGR == 0 && yGL == 1) || (rSwingFirst && !rSwingFinish)) {

							rSwingFirst = true;

							// We need to swing right leg
							if (nodeCounter%60 == 0) {
								if (iNormalStep < 21) {
									realRos->sendJointAngleCon(NormalStepRSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									rSwingFinish = true;
								}
							}
						} // End GL/GR

						break;
					}
					case 7 : {
						if (nodeCounter%54 == 0) {
							if (iNormalStep < 21) {
								realRos->sendJointAngleCon(NormalStepLSwing[iNormalStep]);
								iNormalStep++;
							} else {
								iNormalStep = 0;
							}
						}
						break;
					}
					case 8 : {

						// Right leg at front
						if ((yGR == 1 && yGL == 0) || (lSwingFirst && !lSwingFinish)) {

							lSwingFirst = true;

							// We need to swing left leg
							if (nodeCounter%48 == 0) {
								if (iNormalStep < 30) {
									realRos->sendJointAngleCon(NormalStepLSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									lSwingFinish = true;
								}
							}
						} // End GL/GR

						// Left leg at front
						if ((yGR == 0 && yGL == 1) || (rSwingFirst && !rSwingFinish)) {

							rSwingFirst = true;

							// We need to swing right leg
							if (nodeCounter%48 == 0) {
								if (iNormalStep < 21) {
									realRos->sendJointAngleCon(NormalStepRSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									rSwingFinish = true;
								}
							}
						} // End GL/GR

						break;
					}
					case 9 : {

						// Right leg at front
						if ((yGR == 1 && yGL == 0) || (lSwingFirst && !lSwingFinish)) {

							lSwingFirst = true;

							// We need to swing left leg
							if (nodeCounter%42 == 0) {
								if (iNormalStep < 30) {
									realRos->sendJointAngleCon(NormalStepLSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									lSwingFinish = true;
								}
							}
						} // End GL/GR

						// Left leg at front
						if ((yGR == 0 && yGL == 1) || (rSwingFirst && !rSwingFinish)) {

							rSwingFirst = true;

							// We need to swing right leg
							if (nodeCounter%42 == 0) {
								if (iNormalStep < 21) {
									realRos->sendJointAngleCon(NormalStepRSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									rSwingFinish = true;
								}
							}
						} // End GL/GR

						break;
					}
					case 10 : {

						// Right leg at front
						if ((yGR == 1 && yGL == 0) || (lSwingFirst && !lSwingFinish)) {

							lSwingFirst = true;

							// We need to swing left leg
							if (nodeCounter%36 == 0) {
								if (iNormalStep < 30) {
									realRos->sendJointAngleCon(NormalStepLSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									lSwingFinish = true;
								}
							}
						} // End GL/GR

						// Left leg at front
						if ((yGR == 0 && yGL == 1) || (rSwingFirst && !rSwingFinish)) {

							rSwingFirst = true;

							// We need to swing right leg
							if (nodeCounter%36 == 0) {
								if (iNormalStep < 21) {
									realRos->sendJointAngleCon(NormalStepRSwing[iNormalStep]);
									iNormalStep++;
								} else {
									iNormalStep = 0;
									rSwingFinish = true;
								}
							}
						} // End GL/GR

						break;
					}

				} // End switch


				speedOld = speed;

			}

			break;
		} // End case = 1 Reflex


		case 2 : {
			cout << "AdaptiveMode > Speed" << endl;

			cout << "--------------- Speed adaptation ---------------" << endl;
			speedAdapt(realRos, speed, assist);


			break;
		} // End case = 2 Speed

		case 3 : {
			cout << "AdaptiveMode > Pattern Pos" << endl;

			// Pattern adaption =========================================================
			// To learn and generate pattern, then it will be used when we select 2. Adaptive

			// Down sampling
			// If we want 10 kernel, the interval between each point varies according to CPG freq.
			// Ex.
			// cpgFreqScale = 10
			// If cpgFreq = 0.05 (which is 0.5 Hz real world), we will have 1 cycle in (1/(cgpFreq*cpgFreqScale))*1000 = 2000 steps.
			// 2000 step / 10 kernel = 200 step/sampling.
			// So, we use 1/(cgpFreq*cpgFreqScale) as mod
			cout << "--------------- Pattern adaptation ---------------" << endl;
			patternAdapt(realRos, speed, assist);


			break;
		} // End case = 3 Pattern

		case 4 : {
			cout << "AdaptiveMode > Pattern Tor" << endl;

			break;
		}

		case 5 : {
			cout << "AdaptiveMode > ChangeSpeed" << endl;
			// Change speed manually using loaded pattern =========================================================
			changeSpeed(realRos, speed, assist);

			break;
		}


	} // End switch adaptiveMode


	return true;
}


bool ExvisParallelRealAssem::motorDisable(){

	cout << "----------------- Mode of operation ------------------" << endl;
	cout << "MOTOR DISABLE" << endl;
	realRos->sendTypeCon(4);

	return true;
}

bool ExvisParallelRealAssem::motorStop(){

	cout << "----------------- Mode of operation ------------------" << endl;
	cout << "MOTOR STOP" << endl;
	realRos->sendTypeCon(5);

	return true;
}


//--------------------------------------------------------------------------
// jsonReadZeroTorqueFn
//--------------------------------------------------------------------------
void ExvisParallelRealAssem::jsonReadZeroTorqueFn(){

	// ************************************************************************
	// * An Example to read json file and show on screen
	/*
	char c;
	c = ifs.get();
	if (!ifs.is_open()) {
		cout << "Failed to open" << endl;
	} else {
		cout << "Open file OK" << endl;
		cout << jsonText << endl;
		while (true){
			cout << c;
			c = ifs.get();
			if (ifs.eof()) break;
		}
		cout << endl;
	}
	*/
	// ************************************************************************

    // ************************************************************************
    // * An Example to read json file from "alice.json"
    // * {
    // *    "book":"Alice in Wonderland",
    // *    "year":1865,
    // *    "characters":
    // *    [
    // *        {"name":"Jabberwock", "chapter":1},
    // *        {"name":"Cheshire Cat", "chapter":6},
    // *        {"name":"Mad Hatter", "chapter":7}
    // *    ]
    // * }
    // ************************************************************************
    /*
    cout << "Book: " << objR["book"].asString() << endl;
    cout << "Year: " << objR["year"].asUInt() << endl;

    const Json::Value& characters = objR["characters"]; // array of json obj
    for (int i = 0; i < characters.size(); i++){
        cout << "    name: " << characters[i]["name"].asString();
        cout << " chapter: " << characters[i]["chapter"].asUInt();
        cout << endl;
    }
    */
    // ************************************************************************
	// Open file
	if (!ifsjRZT.is_open()) {
		ifsjRZT.open(jsonReadZeroTorque, ifstream::in);
	}

	ifsjRZT.clear();
	ifsjRZT.seekg(0, ios::beg);
	readerjRZT.parse(ifsjRZT, objjRZT); // reader can also read strings

	// TIME:
	jRZT_timeStampCAN_In = objjRZT[0]["EXO"][0]["timeStamp"];
	for (int i = 0; i < jRZT_timeStampCAN_In.size(); i++) {
		timeStampCAN_ZT.push_back(jRZT_timeStampCAN_In[i].asString());
	}
	// EXO:
		// Feedback signal - Right
	jRZT_rHipJ_FB_In = objjRZT[0]["EXO"][0]["hip"][0]["right"][0]["feedback"][0]["joint"];
	for (int i = 0; i < jRZT_rHipJ_FB_In.size(); i++) {
		rHipJ_FB_ZT.push_back(jRZT_rHipJ_FB_In[i].asDouble());
	}

	jRZT_rKneeJ_FB_In = objjRZT[0]["EXO"][0]["knee"][0]["right"][0]["feedback"][0]["joint"];
	for (int i = 0; i < jRZT_rKneeJ_FB_In.size(); i++) {
		rKneeJ_FB_ZT.push_back(jRZT_rKneeJ_FB_In[i].asDouble());
	}

	jRZT_rAnkleJ_FB_In = objjRZT[0]["EXO"][0]["ankle"][0]["right"][0]["feedback"][0]["joint"];
	for (int i = 0; i < jRZT_rAnkleJ_FB_In.size(); i++) {
		rAnkleJ_FB_ZT.push_back(jRZT_rAnkleJ_FB_In[i].asDouble());
	}

	jRZT_rHipT_FB_In = objjRZT[0]["EXO"][0]["hip"][0]["right"][0]["feedback"][0]["torque"];
	for (int i = 0; i < jRZT_rHipT_FB_In.size(); i++) {
		rHipT_FB_ZT.push_back(jRZT_rHipT_FB_In[i].asDouble());
	}

	jRZT_rKneeT_FB_In = objjRZT[0]["EXO"][0]["knee"][0]["right"][0]["feedback"][0]["torque"];
	for (int i = 0; i < jRZT_rKneeT_FB_In.size(); i++) {
		rKneeT_FB_ZT.push_back(jRZT_rKneeT_FB_In[i].asDouble());
	}

	jRZT_rAnkleT_FB_In = objjRZT[0]["EXO"][0]["ankle"][0]["right"][0]["feedback"][0]["torque"];
	for (int i = 0; i < jRZT_rAnkleT_FB_In.size(); i++) {
		rAnkleT_FB_ZT.push_back(jRZT_rAnkleT_FB_In[i].asDouble());
	}

	jRZT_rHipM_FB_In = objjRZT[0]["EXO"][0]["hip"][0]["right"][0]["feedback"][0]["motor"];
	for (int i = 0; i < jRZT_rHipM_FB_In.size(); i++) {
		rHipM_FB_ZT.push_back(jRZT_rHipM_FB_In[i].asDouble());
	}

	jRZT_rKneeM_FB_In = objjRZT[0]["EXO"][0]["knee"][0]["right"][0]["feedback"][0]["motor"];
	for (int i = 0; i < jRZT_rKneeM_FB_In.size(); i++) {
		rKneeM_FB_ZT.push_back(jRZT_rKneeM_FB_In[i].asDouble());
	}

	jRZT_rAnkleM_FB_In = objjRZT[0]["EXO"][0]["ankle"][0]["right"][0]["feedback"][0]["motor"];
	for (int i = 0; i < jRZT_rAnkleM_FB_In.size(); i++) {
		rAnkleM_FB_ZT.push_back(jRZT_rAnkleM_FB_In[i].asDouble());
	}

	jRZT_rFHeel_FB_In = objjRZT[0]["EXO"][0]["foot"][0]["right"][0]["heel"];
	for (int i = 0; i < jRZT_rFHeel_FB_In.size(); i++) {
		rFHeel_FB_ZT.push_back(jRZT_rFHeel_FB_In[i].asDouble());
	}

	jRZT_rFToe_FB_In = objjRZT[0]["EXO"][0]["foot"][0]["right"][0]["toe"];
	for (int i = 0; i < jRZT_rFToe_FB_In.size(); i++) {
		rFToe_FB_ZT.push_back(jRZT_rFToe_FB_In[i].asDouble());
	}
		// Feedback signal - Left
	jRZT_lHipJ_FB_In = objjRZT[0]["EXO"][0]["hip"][0]["left"][0]["feedback"][0]["joint"];
	for (int i = 0; i < jRZT_lHipJ_FB_In.size(); i++) {
		lHipJ_FB_ZT.push_back(jRZT_lHipJ_FB_In[i].asDouble());
	}

	jRZT_lKneeJ_FB_In = objjRZT[0]["EXO"][0]["knee"][0]["left"][0]["feedback"][0]["joint"];
	for (int i = 0; i < jRZT_lKneeJ_FB_In.size(); i++) {
		lKneeJ_FB_ZT.push_back(jRZT_lKneeJ_FB_In[i].asDouble());
	}

	jRZT_lAnkleJ_FB_In = objjRZT[0]["EXO"][0]["ankle"][0]["left"][0]["feedback"][0]["joint"];
	for (int i = 0; i < jRZT_lAnkleJ_FB_In.size(); i++) {
		lAnkleJ_FB_ZT.push_back(jRZT_lAnkleJ_FB_In[i].asDouble());
	}

	jRZT_lHipT_FB_In = objjRZT[0]["EXO"][0]["hip"][0]["left"][0]["feedback"][0]["torque"];
	for (int i = 0; i < jRZT_lHipT_FB_In.size(); i++) {
		lHipT_FB_ZT.push_back(jRZT_lHipT_FB_In[i].asDouble());
	}

	jRZT_lKneeT_FB_In = objjRZT[0]["EXO"][0]["knee"][0]["left"][0]["feedback"][0]["torque"];
	for (int i = 0; i < jRZT_lKneeT_FB_In.size(); i++) {
		lKneeT_FB_ZT.push_back(jRZT_lKneeT_FB_In[i].asDouble());
	}

	jRZT_lAnkleT_FB_In = objjRZT[0]["EXO"][0]["ankle"][0]["left"][0]["feedback"][0]["torque"];
	for (int i = 0; i < jRZT_lAnkleT_FB_In.size(); i++) {
		lAnkleT_FB_ZT.push_back(jRZT_lAnkleT_FB_In[i].asDouble());
	}

	jRZT_lHipM_FB_In = objjRZT[0]["EXO"][0]["hip"][0]["left"][0]["feedback"][0]["motor"];
	for (int i = 0; i < jRZT_lHipM_FB_In.size(); i++) {
		lHipM_FB_ZT.push_back(jRZT_lHipM_FB_In[i].asDouble());
	}

	jRZT_lKneeM_FB_In = objjRZT[0]["EXO"][0]["knee"][0]["left"][0]["feedback"][0]["motor"];
	for (int i = 0; i < jRZT_lKneeM_FB_In.size(); i++) {
		lKneeM_FB_ZT.push_back(jRZT_lKneeM_FB_In[i].asDouble());
	}

	jRZT_lAnkleM_FB_In = objjRZT[0]["EXO"][0]["ankle"][0]["left"][0]["feedback"][0]["motor"];
	for (int i = 0; i < jRZT_lAnkleM_FB_In.size(); i++) {
		lAnkleM_FB_ZT.push_back(jRZT_lAnkleM_FB_In[i].asDouble());
	}

	jRZT_lFHeel_FB_In = objjRZT[0]["EXO"][0]["foot"][0]["left"][0]["heel"];
	for (int i = 0; i < jRZT_lFHeel_FB_In.size(); i++) {
		lFHeel_FB_ZT.push_back(jRZT_lFHeel_FB_In[i].asDouble());
	}

	jRZT_lFToe_FB_In = objjRZT[0]["EXO"][0]["foot"][0]["left"][0]["toe"];
	for (int i = 0; i < jRZT_lFToe_FB_In.size(); i++) {
		lFToe_FB_ZT.push_back(jRZT_lFToe_FB_In[i].asDouble());
	}
	// CPG:
		// AFDC - Right
	jRZT_rCPG0AFDC_In = objjRZT[0]["CPG"][0]["AFDC"][0]["right"][0]["CPG0"];
	for (int i = 0; i < jRZT_rCPG0AFDC_In.size(); i++) {
		rCPG0AFDC_ZT.push_back(jRZT_rCPG0AFDC_In[i].asDouble());
	}

	jRZT_rCPG1AFDC_In = objjRZT[0]["CPG"][0]["AFDC"][0]["right"][0]["CPG1"];
	for (int i = 0; i < jRZT_rCPG1AFDC_In.size(); i++) {
		rCPG1AFDC_ZT.push_back(jRZT_rCPG1AFDC_In[i].asDouble());
	}

	jRZT_rCPGFreqAFDC_In = objjRZT[0]["CPG"][0]["AFDC"][0]["right"][0]["freq"];
	for (int i = 0; i < jRZT_rCPGFreqAFDC_In.size(); i++) {
		rCPGFreqAFDC_ZT.push_back(jRZT_rCPGFreqAFDC_In[i].asDouble());
	}
		// AFDC - Left
	jRZT_lCPG0AFDC_In = objjRZT[0]["CPG"][0]["AFDC"][0]["left"][0]["CPG0"];
	for (int i = 0; i < jRZT_lCPG0AFDC_In.size(); i++) {
		lCPG0AFDC_ZT.push_back(jRZT_lCPG0AFDC_In[i].asDouble());
	}

	jRZT_lCPG1AFDC_In = objjRZT[0]["CPG"][0]["AFDC"][0]["left"][0]["CPG1"];
	for (int i = 0; i < jRZT_lCPG1AFDC_In.size(); i++) {
		lCPG1AFDC_ZT.push_back(jRZT_lCPG1AFDC_In[i].asDouble());
	}

	jRZT_lCPGFreqAFDC_In = objjRZT[0]["CPG"][0]["AFDC"][0]["left"][0]["freq"];
	for (int i = 0; i < jRZT_lCPGFreqAFDC_In.size(); i++) {
		lCPGFreqAFDC_ZT.push_back(jRZT_lCPGFreqAFDC_In[i].asDouble());
	}

	if (ifsjRZT.is_open()) {
		ifsjRZT.close();
	}
	cout << "|||||||||||||||||||| 100% JSON ZERO TORQUE READ DATA COMPLETE" << endl;
}

//--------------------------------------------------------------------------
// jsonWritePatFn
//--------------------------------------------------------------------------
void ExvisParallelRealAssem::jsonWritePatFn(){

	// UI:
	objjWP[0]["speedFound"] = jWP_speedFound_Out;
	// EXO:
		// Feedback signal - Right
	objjWP[0]["rHipJ_FB_ZT_LowP"] = jWP_rHipJ_FB_ZT_LowP_Out;
	objjWP[0]["rKneeJ_FB_ZT_LowP"] = jWP_rKneeJ_FB_ZT_LowP_Out;
	objjWP[0]["rAnkleJ_FB_ZT_LowP"] = jWP_rAnkleJ_FB_ZT_LowP_Out;
	objjWP[0]["rHipJ_FB_ZT_LowP_ACycle"] = jWP_rHipJ_FB_ZT_LowP_ACycle_Out;
	objjWP[0]["rKneeJ_FB_ZT_LowP_ACycle"] = jWP_rKneeJ_FB_ZT_LowP_ACycle_Out;
	objjWP[0]["rAnkleJ_FB_ZT_LowP_ACycle"] = jWP_rAnkleJ_FB_ZT_LowP_ACycle_Out;
	objjWP[0]["rHipJ_FB_ZT_CyclesFound"] = jWP_rHipJ_FB_ZT_CyclesFound_Out;
	objjWP[0]["rKneeJ_FB_ZT_CyclesFound"] = jWP_rKneeJ_FB_ZT_CyclesFound_Out;
	objjWP[0]["rAnkleJ_FB_ZT_CyclesFound"] = jWP_rAnkleJ_FB_ZT_CyclesFound_Out;
	objjWP[0]["rHipJ_FB_ZT_LowP_Mx"] = jWP_rHipJ_FB_ZT_LowP_Mx_Out;
	objjWP[0]["rKneeJ_FB_ZT_LowP_Mx"] = jWP_rKneeJ_FB_ZT_LowP_Mx_Out;
	objjWP[0]["rAnkleJ_FB_ZT_LowP_Mx"] = jWP_rAnkleJ_FB_ZT_LowP_Mx_Out;
	objjWP[0]["rHipJ_FB_ZT_LowP_Target"] = jWP_rHipJ_FB_ZT_LowP_Target_Out;
	objjWP[0]["rKneeJ_FB_ZT_LowP_Target"] = jWP_rKneeJ_FB_ZT_LowP_Target_Out;
	objjWP[0]["rAnkleJ_FB_ZT_LowP_Target"] = jWP_rAnkleJ_FB_ZT_LowP_Target_Out;
	objjWP[0]["rHipJ_FB_ZT_LowP_Target_Sam"] = jWP_rHipJ_FB_ZT_LowP_Target_Sam_Out;
	objjWP[0]["rKneeJ_FB_ZT_LowP_Target_Sam"] = jWP_rKneeJ_FB_ZT_LowP_Target_Sam_Out;
	objjWP[0]["rAnkleJ_FB_ZT_LowP_Target_Sam"] = jWP_rAnkleJ_FB_ZT_LowP_Target_Sam_Out;
	objjWP[0]["rKernel"] = jWP_rKernel_Out;
	objjWP[0]["rHipJ_FB_ZT_LowP_Weight"] = jWP_rHipJ_FB_ZT_LowP_Weight_Out;
	objjWP[0]["rKneeJ_FB_ZT_LowP_Weight"] = jWP_rKneeJ_FB_ZT_LowP_Weight_Out;
	objjWP[0]["rAnkleJ_FB_ZT_LowP_Weight"] = jWP_rAnkleJ_FB_ZT_LowP_Weight_Out;
		// Feedback signal - Left
	objjWP[0]["lHipJ_FB_ZT_LowP"] = jWP_lHipJ_FB_ZT_LowP_Out;
	objjWP[0]["lKneeJ_FB_ZT_LowP"] = jWP_lKneeJ_FB_ZT_LowP_Out;
	objjWP[0]["lAnkleJ_FB_ZT_LowP"] = jWP_lAnkleJ_FB_ZT_LowP_Out;
	objjWP[0]["lHipJ_FB_ZT_LowP_ACycle"] = jWP_lHipJ_FB_ZT_LowP_ACycle_Out;
	objjWP[0]["lKneeJ_FB_ZT_LowP_ACycle"] = jWP_lKneeJ_FB_ZT_LowP_ACycle_Out;
	objjWP[0]["lAnkleJ_FB_ZT_LowP_ACycle"] = jWP_lAnkleJ_FB_ZT_LowP_ACycle_Out;
	objjWP[0]["lHipJ_FB_ZT_CyclesFound"] = jWP_lHipJ_FB_ZT_CyclesFound_Out;
	objjWP[0]["lKneeJ_FB_ZT_CyclesFound"] = jWP_lKneeJ_FB_ZT_CyclesFound_Out;
	objjWP[0]["lAnkleJ_FB_ZT_CyclesFound"] = jWP_lAnkleJ_FB_ZT_CyclesFound_Out;
	objjWP[0]["lHipJ_FB_ZT_LowP_Mx"] = jWP_lHipJ_FB_ZT_LowP_Mx_Out;
	objjWP[0]["lKneeJ_FB_ZT_LowP_Mx"] = jWP_lKneeJ_FB_ZT_LowP_Mx_Out;
	objjWP[0]["lAnkleJ_FB_ZT_LowP_Mx"] = jWP_lAnkleJ_FB_ZT_LowP_Mx_Out;
	objjWP[0]["lHipJ_FB_ZT_LowP_Target"] = jWP_lHipJ_FB_ZT_LowP_Target_Out;
	objjWP[0]["lKneeJ_FB_ZT_LowP_Target"] = jWP_lKneeJ_FB_ZT_LowP_Target_Out;
	objjWP[0]["lAnkleJ_FB_ZT_LowP_Target"] = jWP_lAnkleJ_FB_ZT_LowP_Target_Out;
	objjWP[0]["lHipJ_FB_ZT_LowP_Target_Sam"] = jWP_lHipJ_FB_ZT_LowP_Target_Sam_Out;
	objjWP[0]["lKneeJ_FB_ZT_LowP_Target_Sam"] = jWP_lKneeJ_FB_ZT_LowP_Target_Sam_Out;
	objjWP[0]["lAnkleJ_FB_ZT_LowP_Target_Sam"] = jWP_lAnkleJ_FB_ZT_LowP_Target_Sam_Out;
	objjWP[0]["lKernel"] = jWP_lKernel_Out;
	objjWP[0]["lHipJ_FB_ZT_LowP_Weight"] = jWP_lHipJ_FB_ZT_LowP_Weight_Out;
	objjWP[0]["lKneeJ_FB_ZT_LowP_Weight"] = jWP_lKneeJ_FB_ZT_LowP_Weight_Out;
	objjWP[0]["lAnkleJ_FB_ZT_LowP_Weight"] = jWP_lAnkleJ_FB_ZT_LowP_Weight_Out;
		// Control signal - Right
	objjWP[0]["rHipP_Control"] = jWP_rHipP_Control_Out;
	objjWP[0]["rKneeP_Control"] = jWP_rKneeP_Control_Out;
	objjWP[0]["rAnkleP_Control"] = jWP_rAnkleP_Control_Out;
		// Control signal - Left
	objjWP[0]["lHipP_Control"] = jWP_lHipP_Control_Out;
	objjWP[0]["lKneeP_Control"] = jWP_lKneeP_Control_Out;
	objjWP[0]["lAnkleP_Control"] = jWP_lAnkleP_Control_Out;
	// CPG:
		// SO2 Learn
	objjWP[0]["timeSO2LearnGen"] = jWP_timeSO2LearnGen_Out;
		// SO2 Learn - Right
	objjWP[0]["rTimeSO2LearnPhaseZero"] = jWP_rTimeSO2LearnPhaseZero_Out;
	objjWP[0]["rCPG0SO2Learn"] = jWP_rCPG0SO2Learn_Out;
	objjWP[0]["rCPG1SO2Learn"] = jWP_rCPG1SO2Learn_Out;
	objjWP[0]["rCPGFreqSO2Learn"] = jWP_rCPGFreqSO2Learn_Out;
	objjWP[0]["rMeanCPGTimeRW"] = jWP_rMeanCPGTimeRW_out;
	objjWP[0]["rCPG0SO2Learn_ACycle"] = jWP_rCPG0SO2Learn_ACycle_Out;
	objjWP[0]["rCPG1SO2Learn_ACycle"] = jWP_rCPG1SO2Learn_ACycle_Out;
	objjWP[0]["rCountCycle"] = jWP_rCountCycle_Out;
	objjWP[0]["rCPG0SO2Learn_CyclesFound"] = jWP_rCPG0SO2Learn_CyclesFound_Out;
	objjWP[0]["rCPG0SO2Learn_ACycle_Sam"] = jWP_rCPG0SO2Learn_ACycle_Sam_Out; // like an array = []
	objjWP[0]["rCPG1SO2Learn_ACycle_Sam"] = jWP_rCPG1SO2Learn_ACycle_Sam_Out;
		// SO2 Learn - Left
	objjWP[0]["lTimeSO2LearnPhase180"] = jWP_lTimeSO2LearnPhase180_Out;
	objjWP[0]["lCPG0SO2Learn"] = jWP_lCPG0SO2Learn_Out;
	objjWP[0]["lCPG1SO2Learn"] = jWP_lCPG1SO2Learn_Out;
	objjWP[0]["lCPGFreqSO2Learn"] = jWP_lCPGFreqSO2Learn_Out;
	objjWP[0]["lMeanCPGTimeRW"] = jWP_lMeanCPGTimeRW_out;
	objjWP[0]["lCPG0SO2Learn_ACycle"] = jWP_lCPG0SO2Learn_ACycle_Out;
	objjWP[0]["lCPG1SO2Learn_ACycle"] = jWP_lCPG1SO2Learn_ACycle_Out;
	objjWP[0]["lCountCycle"] = jWP_lCountCycle_Out;
	objjWP[0]["lCPG0SO2Learn_CyclesFound"] = jWP_lCPG0SO2Learn_CyclesFound_Out;
	objjWP[0]["lCPG0SO2Learn_ACycle_Sam"] = jWP_lCPG0SO2Learn_ACycle_Sam_Out; // like an array = []
	objjWP[0]["lCPG1SO2Learn_ACycle_Sam"] = jWP_lCPG1SO2Learn_ACycle_Sam_Out;

	ofsjWP << objjWP;

}

//--------------------------------------------------------------------------
// jsonReadPatFn
//--------------------------------------------------------------------------
void ExvisParallelRealAssem::jsonReadPatFn(){

	// Open file
	if (!ifsjRP.is_open()) {
		ifsjRP.open(jsonReadPat, ifstream::in);
	}

	ifsjRP.clear();
	ifsjRP.seekg(0, ios::beg);
	readerjRP.parse(ifsjRP, objjRP); // reader can also read strings


	// Better to clear vector before loading
	rHipWeightOnline.clear();
	rKneeWeightOnline.clear();
	rAnkleWeightOnline.clear();
	lHipWeightOnline.clear();
	lKneeWeightOnline.clear();
	lAnkleWeightOnline.clear();
	rCPG0SO2Learn_ACycle_Sam.clear();
	rCPG1SO2Learn_ACycle_Sam.clear();
	lCPG0SO2Learn_ACycle_Sam.clear();
	lCPG1SO2Learn_ACycle_Sam.clear();

	// EXO:
		// Feedback signal - Right
	jRP_rHipJ_FB_ZT_LowP_Weight_In = objjRP[0]["rHipJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRP_rHipJ_FB_ZT_LowP_Weight_In.size(); i++) {
		rHipWeightOnline.push_back(jRP_rHipJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

	jRP_rKneeJ_FB_ZT_LowP_Weight_In = objjRP[0]["rKneeJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRP_rKneeJ_FB_ZT_LowP_Weight_In.size(); i++) {
		rKneeWeightOnline.push_back(jRP_rKneeJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

	jRP_rAnkleJ_FB_ZT_LowP_Weight_In = objjRP[0]["rAnkleJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRP_rAnkleJ_FB_ZT_LowP_Weight_In.size(); i++) {
		rAnkleWeightOnline.push_back(jRP_rAnkleJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

		// Feedback signal - Left
	jRP_lHipJ_FB_ZT_LowP_Weight_In = objjRP[0]["lHipJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRP_lHipJ_FB_ZT_LowP_Weight_In.size(); i++) {
		lHipWeightOnline.push_back(jRP_lHipJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

	jRP_lKneeJ_FB_ZT_LowP_Weight_In = objjRP[0]["lKneeJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRP_lKneeJ_FB_ZT_LowP_Weight_In.size(); i++) {
		lKneeWeightOnline.push_back(jRP_lKneeJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

	jRP_lAnkleJ_FB_ZT_LowP_Weight_In = objjRP[0]["lAnkleJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRP_lAnkleJ_FB_ZT_LowP_Weight_In.size(); i++) {
		lAnkleWeightOnline.push_back(jRP_lAnkleJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

	// CPG:
		// SO2 Learn - Right
	jRP_rMeanCPGTimeRW_In = objjRP[0]["rMeanCPGTimeRW"][0];
	meanrCPGTimeRWOnline = jRP_rMeanCPGTimeRW_In.asDouble(); // Freq
	
	jRP_rCPG0SO2Learn_ACycle_Sam_In = objjRP[0]["rCPG0SO2Learn_ACycle_Sam"];
	for (int i = 0; i < jRP_rCPG0SO2Learn_ACycle_Sam_In.size(); i++) {
		rCPG0SO2Learn_ACycle_Sam.push_back(jRP_rCPG0SO2Learn_ACycle_Sam_In[i].asDouble());
	}

	jRP_rCPG1SO2Learn_ACycle_Sam_In = objjRP[0]["rCPG1SO2Learn_ACycle_Sam"];
	for (int i = 0; i < jRP_rCPG1SO2Learn_ACycle_Sam_In.size(); i++) {
		rCPG1SO2Learn_ACycle_Sam.push_back(jRP_rCPG1SO2Learn_ACycle_Sam_In[i].asDouble());
	}

		// SO2 Learn - Left
	jRP_lMeanCPGTimeRW_In = objjRP[0]["lMeanCPGTimeRW"][0];
	meanlCPGTimeRWOnline = jRP_lMeanCPGTimeRW_In.asDouble(); // Freq

	jRP_lCPG0SO2Learn_ACycle_Sam_In = objjRP[0]["lCPG0SO2Learn_ACycle_Sam"];
	for (int i = 0; i < jRP_lCPG0SO2Learn_ACycle_Sam_In.size(); i++) {
		lCPG0SO2Learn_ACycle_Sam.push_back(jRP_lCPG0SO2Learn_ACycle_Sam_In[i].asDouble());
	}

	jRP_lCPG1SO2Learn_ACycle_Sam_In = objjRP[0]["lCPG1SO2Learn_ACycle_Sam"];
	for (int i = 0; i < jRP_lCPG1SO2Learn_ACycle_Sam_In.size(); i++) {
		lCPG1SO2Learn_ACycle_Sam.push_back(jRP_lCPG1SO2Learn_ACycle_Sam_In[i].asDouble());
	}

	if (ifsjRP.is_open()) {
		ifsjRP.close();
	}

}

//--------------------------------------------------------------------------
// jsonWritePatDistFn
//--------------------------------------------------------------------------
void ExvisParallelRealAssem::jsonWritePatDistFn(){

	// UI:
	objjWPD[0]["WPD_speedFound"] = jWPD_speedFound_Out;
	// EXO:
		// Feedback signal - Right
	objjWPD[0]["WPD_rHipJ_FB_ZT_LowP"] = jWPD_rHipJ_FB_ZT_LowP_Out;
	objjWPD[0]["WPD_rKneeJ_FB_ZT_LowP"] = jWPD_rKneeJ_FB_ZT_LowP_Out;
	objjWPD[0]["WPD_rAnkleJ_FB_ZT_LowP"] = jWPD_rAnkleJ_FB_ZT_LowP_Out;
	objjWPD[0]["WPD_rHipJ_FB_ZT_LowP_Target"] = jWPD_rHipJ_FB_ZT_LowP_Target_Out;
	objjWPD[0]["WPD_rKneeJ_FB_ZT_LowP_Target"] = jWPD_rKneeJ_FB_ZT_LowP_Target_Out;
	objjWPD[0]["WPD_rAnkleJ_FB_ZT_LowP_Target"] = jWPD_rAnkleJ_FB_ZT_LowP_Target_Out;
	objjWPD[0]["WPD_rHipJ_FB_ZT_LowP_Target_Sam"] = jWPD_rHipJ_FB_ZT_LowP_Target_Sam_Out;
	objjWPD[0]["WPD_rKneeJ_FB_ZT_LowP_Target_Sam"] = jWPD_rKneeJ_FB_ZT_LowP_Target_Sam_Out;
	objjWPD[0]["WPD_rAnkleJ_FB_ZT_LowP_Target_Sam"] = jWPD_rAnkleJ_FB_ZT_LowP_Target_Sam_Out;
	objjWPD[0]["WPD_rKernel"] = jWPD_rKernel_Out;
	objjWPD[0]["WPD_rHipJ_FB_ZT_LowP_Weight"] = jWPD_rHipJ_FB_ZT_LowP_Weight_Out;
	objjWPD[0]["WPD_rKneeJ_FB_ZT_LowP_Weight"] = jWPD_rKneeJ_FB_ZT_LowP_Weight_Out;
	objjWPD[0]["WPD_rAnkleJ_FB_ZT_LowP_Weight"] = jWPD_rAnkleJ_FB_ZT_LowP_Weight_Out;
		// Feedback signal - Left
	objjWPD[0]["WPD_lHipJ_FB_ZT_LowP"] = jWPD_lHipJ_FB_ZT_LowP_Out;
	objjWPD[0]["WPD_lKneeJ_FB_ZT_LowP"] = jWPD_lKneeJ_FB_ZT_LowP_Out;
	objjWPD[0]["WPD_lAnkleJ_FB_ZT_LowP"] = jWPD_lAnkleJ_FB_ZT_LowP_Out;
	objjWPD[0]["WPD_lHipJ_FB_ZT_LowP_Target"] = jWPD_lHipJ_FB_ZT_LowP_Target_Out;
	objjWPD[0]["WPD_lKneeJ_FB_ZT_LowP_Target"] = jWPD_lKneeJ_FB_ZT_LowP_Target_Out;
	objjWPD[0]["WPD_lAnkleJ_FB_ZT_LowP_Target"] = jWPD_lAnkleJ_FB_ZT_LowP_Target_Out;
	objjWPD[0]["WPD_lHipJ_FB_ZT_LowP_Target_Sam"] = jWPD_lHipJ_FB_ZT_LowP_Target_Sam_Out;
	objjWPD[0]["WPD_lKneeJ_FB_ZT_LowP_Target_Sam"] = jWPD_lKneeJ_FB_ZT_LowP_Target_Sam_Out;
	objjWPD[0]["WPD_lAnkleJ_FB_ZT_LowP_Target_Sam"] = jWPD_lAnkleJ_FB_ZT_LowP_Target_Sam_Out;
	objjWPD[0]["WPD_lKernel"] = jWPD_lKernel_Out;
	objjWPD[0]["WPD_lHipJ_FB_ZT_LowP_Weight"] = jWPD_lHipJ_FB_ZT_LowP_Weight_Out;
	objjWPD[0]["WPD_lKneeJ_FB_ZT_LowP_Weight"] = jWPD_lKneeJ_FB_ZT_LowP_Weight_Out;
	objjWPD[0]["WPD_lAnkleJ_FB_ZT_LowP_Weight"] = jWPD_lAnkleJ_FB_ZT_LowP_Weight_Out;
		// Control signal - Right
	objjWPD[0]["WPD_rHipP_Control"] = jWPD_rHipP_Control_Out;
	objjWPD[0]["WPD_rKneeP_Control"] = jWPD_rKneeP_Control_Out;
	objjWPD[0]["WPD_rAnkleP_Control"] = jWPD_rAnkleP_Control_Out;
		// Control signal - Left
	objjWPD[0]["WPD_lHipP_Control"] = jWPD_lHipP_Control_Out;
	objjWPD[0]["WPD_lKneeP_Control"] = jWPD_lKneeP_Control_Out;
	objjWPD[0]["WPD_lAnkleP_Control"] = jWPD_lAnkleP_Control_Out;
	// CPG:
		// SO2 Learn
	objjWPD[0]["WPD_timeSO2LearnGen"] = jWPD_timeSO2LearnGen_Out;
		// SO2 Learn - Right
	objjWPD[0]["WPD_rTimeSO2LearnPhaseZero"] = jWPD_rTimeSO2LearnPhaseZero_Out;
	objjWPD[0]["WPD_rCPG0SO2Learn"] = jWPD_rCPG0SO2Learn_Out;
	objjWPD[0]["WPD_rCPG1SO2Learn"] = jWPD_rCPG1SO2Learn_Out;
	objjWPD[0]["WPD_rCPGFreqSO2Learn"] = jWPD_rCPGFreqSO2Learn_Out;
	objjWPD[0]["WPD_rMeanCPGTimeRW"] = jWPD_rMeanCPGTimeRW_out;
	objjWPD[0]["WPD_rCPG0SO2Learn_ACycle"] = jWPD_rCPG0SO2Learn_ACycle_Out;
	objjWPD[0]["WPD_rCPG1SO2Learn_ACycle"] = jWPD_rCPG1SO2Learn_ACycle_Out;
	objjWPD[0]["WPD_rCountCycle"] = jWPD_rCountCycle_Out;
	objjWPD[0]["WPD_rCPG0SO2Learn_CyclesFound"] = jWPD_rCPG0SO2Learn_CyclesFound_Out;
	objjWPD[0]["WPD_rCPG0SO2Learn_ACycle_Sam"] = jWPD_rCPG0SO2Learn_ACycle_Sam_Out; // like an array = []
	objjWPD[0]["WPD_rCPG1SO2Learn_ACycle_Sam"] = jWPD_rCPG1SO2Learn_ACycle_Sam_Out;
	objjWPD[0]["WPD_rCPG0SO2Learn_ACycle_Inter"] = jWPD_rCPG0SO2Learn_ACycle_Inter_Out;
	objjWPD[0]["WPD_rCPG1SO2Learn_ACycle_Inter"] = jWPD_rCPG1SO2Learn_ACycle_Inter_Out;
		// SO2 Learn - Left
	objjWPD[0]["WPD_lTimeSO2LearnPhase180"] = jWPD_lTimeSO2LearnPhase180_Out;
	objjWPD[0]["WPD_lCPG0SO2Learn"] = jWPD_lCPG0SO2Learn_Out;
	objjWPD[0]["WPD_lCPG1SO2Learn"] = jWPD_lCPG1SO2Learn_Out;
	objjWPD[0]["WPD_lCPGFreqSO2Learn"] = jWPD_lCPGFreqSO2Learn_Out;
	objjWPD[0]["WPD_lMeanCPGTimeRW"] = jWPD_lMeanCPGTimeRW_out;
	objjWPD[0]["WPD_lCPG0SO2Learn_ACycle"] = jWPD_lCPG0SO2Learn_ACycle_Out;
	objjWPD[0]["WPD_lCPG1SO2Learn_ACycle"] = jWPD_lCPG1SO2Learn_ACycle_Out;
	objjWPD[0]["WPD_lCountCycle"] = jWPD_lCountCycle_Out;
	objjWPD[0]["WPD_lCPG0SO2Learn_CyclesFound"] = jWPD_lCPG0SO2Learn_CyclesFound_Out;
	objjWPD[0]["WPD_lCPG0SO2Learn_ACycle_Sam"] = jWPD_lCPG0SO2Learn_ACycle_Sam_Out; // like an array = []
	objjWPD[0]["WPD_lCPG1SO2Learn_ACycle_Sam"] = jWPD_lCPG1SO2Learn_ACycle_Sam_Out;
	objjWPD[0]["WPD_lCPG0SO2Learn_ACycle_Inter"] = jWPD_lCPG0SO2Learn_ACycle_Inter_Out;
	objjWPD[0]["WPD_lCPG1SO2Learn_ACycle_Inter"] = jWPD_lCPG1SO2Learn_ACycle_Inter_Out;

	ofsjWPD << objjWPD;

}

//--------------------------------------------------------------------------
// jsonReadPatDistFn
//--------------------------------------------------------------------------
void ExvisParallelRealAssem::jsonReadPatDistFn(){

	// Open file
	if (!ifsjRPD.is_open()) {
		ifsjRPD.open(jsonReadPatDist, ifstream::in);
	}

	ifsjRPD.clear();
	ifsjRPD.seekg(0, ios::beg);
	readerjRPD.parse(ifsjRPD, objjRPD); // reader can also read strings


	// Better to clear vector before loading
	rd_rHipWeightOnline.clear();
	rd_rKneeWeightOnline.clear();
	rd_rAnkleWeightOnline.clear();
	rd_lHipWeightOnline.clear();
	rd_lKneeWeightOnline.clear();
	rd_lAnkleWeightOnline.clear();
	WPD_rCPG0SO2Learn_ACycle_Sam.clear();
	WPD_rCPG1SO2Learn_ACycle_Sam.clear();
	WPD_lCPG0SO2Learn_ACycle_Sam.clear();
	WPD_lCPG1SO2Learn_ACycle_Sam.clear();
	WPD_rCPG0SO2Learn_ACycle_Inter.clear();
	WPD_rCPG1SO2Learn_ACycle_Inter.clear();
	WPD_lCPG0SO2Learn_ACycle_Inter.clear();
	WPD_lCPG1SO2Learn_ACycle_Inter.clear();

	// EXO:
		// Feedback signal - Right
	jRPD_rHipJ_FB_ZT_LowP_Weight_In = objjRPD[0]["WPD_rHipJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRPD_rHipJ_FB_ZT_LowP_Weight_In.size(); i++) {
		rd_rHipWeightOnline.push_back(jRPD_rHipJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

	jRPD_rKneeJ_FB_ZT_LowP_Weight_In = objjRPD[0]["WPD_rKneeJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRPD_rKneeJ_FB_ZT_LowP_Weight_In.size(); i++) {
		rd_rKneeWeightOnline.push_back(jRPD_rKneeJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

	jRPD_rAnkleJ_FB_ZT_LowP_Weight_In = objjRPD[0]["WPD_rAnkleJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRPD_rAnkleJ_FB_ZT_LowP_Weight_In.size(); i++) {
		rd_rAnkleWeightOnline.push_back(jRPD_rAnkleJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

		// Feedback signal - Left
	jRPD_lHipJ_FB_ZT_LowP_Weight_In = objjRPD[0]["WPD_lHipJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRPD_lHipJ_FB_ZT_LowP_Weight_In.size(); i++) {
		rd_lHipWeightOnline.push_back(jRPD_lHipJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

	jRPD_lKneeJ_FB_ZT_LowP_Weight_In = objjRPD[0]["WPD_lKneeJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRPD_lKneeJ_FB_ZT_LowP_Weight_In.size(); i++) {
		rd_lKneeWeightOnline.push_back(jRPD_lKneeJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

	jRPD_lAnkleJ_FB_ZT_LowP_Weight_In = objjRPD[0]["WPD_lAnkleJ_FB_ZT_LowP_Weight"][0];
	for (int i = 0; i < jRPD_lAnkleJ_FB_ZT_LowP_Weight_In.size(); i++) {
		rd_lAnkleWeightOnline.push_back(jRPD_lAnkleJ_FB_ZT_LowP_Weight_In[i].asDouble());
	}

	// CPG:
		// SO2 Learn - Right
	jRPD_rMeanCPGTimeRW_In = objjRPD[0]["WPD_rMeanCPGTimeRW"][0];
	rd_meanrCPGTimeRWOnline = jRPD_rMeanCPGTimeRW_In.asDouble(); // Freq
	
	jRPD_rCPG0SO2Learn_ACycle_Sam_In = objjRPD[0]["WPD_rCPG0SO2Learn_ACycle_Sam"];
	for (int i = 0; i < jRPD_rCPG0SO2Learn_ACycle_Sam_In.size(); i++) {
		WPD_rCPG0SO2Learn_ACycle_Sam.push_back(jRPD_rCPG0SO2Learn_ACycle_Sam_In[i].asDouble());
	}

	jRPD_rCPG1SO2Learn_ACycle_Sam_In = objjRPD[0]["WPD_rCPG1SO2Learn_ACycle_Sam"];
	for (int i = 0; i < jRPD_rCPG1SO2Learn_ACycle_Sam_In.size(); i++) {
		WPD_rCPG1SO2Learn_ACycle_Sam.push_back(jRPD_rCPG1SO2Learn_ACycle_Sam_In[i].asDouble());
	}

	jRPD_rCPG0SO2Learn_ACycle_Inter_In = objjRPD[0]["WPD_rCPG0SO2Learn_ACycle_Inter"];
	for (int i = 0; i < jRPD_rCPG0SO2Learn_ACycle_Inter_In.size(); i++) {
		WPD_rCPG0SO2Learn_ACycle_Inter.push_back(jRPD_rCPG0SO2Learn_ACycle_Inter_In[i].asDouble());
	}

	jRPD_rCPG1SO2Learn_ACycle_Inter_In = objjRPD[0]["WPD_rCPG1SO2Learn_ACycle_Inter"];
	for (int i = 0; i < jRPD_rCPG1SO2Learn_ACycle_Inter_In.size(); i++) {
		WPD_rCPG1SO2Learn_ACycle_Inter.push_back(jRPD_rCPG1SO2Learn_ACycle_Inter_In[i].asDouble());
	}

		// SO2 Learn - Left
	jRPD_lMeanCPGTimeRW_In = objjRPD[0]["WPD_lMeanCPGTimeRW"][0];
	rd_meanlCPGTimeRWOnline = jRPD_lMeanCPGTimeRW_In.asDouble(); // Freq

	jRPD_lCPG0SO2Learn_ACycle_Sam_In = objjRPD[0]["WPD_lCPG0SO2Learn_ACycle_Sam"];
	for (int i = 0; i < jRPD_lCPG0SO2Learn_ACycle_Sam_In.size(); i++) {
		WPD_lCPG0SO2Learn_ACycle_Sam.push_back(jRPD_lCPG0SO2Learn_ACycle_Sam_In[i].asDouble());
	}

	jRPD_lCPG1SO2Learn_ACycle_Sam_In = objjRPD[0]["WPD_lCPG1SO2Learn_ACycle_Sam"];
	for (int i = 0; i < jRPD_lCPG1SO2Learn_ACycle_Sam_In.size(); i++) {
		WPD_lCPG1SO2Learn_ACycle_Sam.push_back(jRPD_lCPG1SO2Learn_ACycle_Sam_In[i].asDouble());
	}

	jRPD_lCPG0SO2Learn_ACycle_Inter_In = objjRPD[0]["WPD_lCPG0SO2Learn_ACycle_Inter"];
	for (int i = 0; i < jRPD_lCPG0SO2Learn_ACycle_Inter_In.size(); i++) {
		WPD_lCPG0SO2Learn_ACycle_Inter.push_back(jRPD_lCPG0SO2Learn_ACycle_Inter_In[i].asDouble());
	}

	jRPD_lCPG1SO2Learn_ACycle_Inter_In = objjRPD[0]["WPD_lCPG1SO2Learn_ACycle_Inter"];
	for (int i = 0; i < jRPD_lCPG1SO2Learn_ACycle_Inter_In.size(); i++) {
		WPD_lCPG1SO2Learn_ACycle_Inter.push_back(jRPD_lCPG1SO2Learn_ACycle_Inter_In[i].asDouble());
	}

	if (ifsjRPD.is_open()) {
		ifsjRPD.close();
	}

}

//--------------------------------------------------------------------------
// showData
//--------------------------------------------------------------------------
void ExvisParallelRealAssem::showData() {

	cout << "----------------- Read data ------------------" << endl;
	printf(
			// "  - R JointAngle ID:110 DATA:%02x %02x %02x %02x %02x %02x\n",
			"  - R JointAngle ID:110 DATA:%03d %03d %03d %03d %03d %03d\n",
			(realRos->jointAngle)[0], (realRos->jointAngle)[1],
			(realRos->jointAngle)[2], (realRos->jointAngle)[3],
			(realRos->jointAngle)[4], (realRos->jointAngle)[5]
	);
	printf(
			// "  - R JointTorque ID:120 DATA:%02x %02x %02x %02x %02x %02x\n",
			"  - R JointTorque ID:120 DATA:%03d %03d %03d %03d %03d %03d\n",
			(realRos->jointTorque)[0], (realRos->jointTorque)[1],
			(realRos->jointTorque)[2], (realRos->jointTorque)[3],
			(realRos->jointTorque)[4], (realRos->jointTorque)[5]
	);
	printf(
			// "  - R FootSwitch ID:130 DATA:%02x %02x %02x %02x %02x %02x\n",
			"  - R FootSwitch ID:130 DATA:%03d %03d %03d %03d %03d %03d\n",
			(realRos->footSwitch)[0], (realRos->footSwitch)[1],
			(realRos->footSwitch)[2], (realRos->footSwitch)[3],
			(realRos->footSwitch)[4], (realRos->footSwitch)[5]
	);
	printf(
			//"  - R MotorTorque ID:140 DATA:%02x %02x %02x %02x %02x %02x\n",
			"  - R MotorTorque ID:140 DATA:%03d %03d %03d %03d %03d %03d\n",
			(realRos->motorTorque)[0], (realRos->motorTorque)[1],
			(realRos->motorTorque)[2], (realRos->motorTorque)[3],
			(realRos->motorTorque)[4], (realRos->motorTorque)[5]
	);
	printf(
			"  - R ExoStatus ID:150 DATA:%03d %03d %03d %03d %03d %03d\n",
			(realRos->exoStatus)[0], (realRos->exoStatus)[1],
			(realRos->exoStatus)[2], (realRos->exoStatus)[3],
			(realRos->exoStatus)[4], (realRos->exoStatus)[5]
	);

}


//--------------------------------------------------------------------------
// discrete pattern
//--------------------------------------------------------------------------
bool ExvisParallelRealAssem::discretePattern(ExvisParallelRealROS * realRos, int speed, int assist){

	if (!rd_checkJsonWritePatDist_1) { // Check only once
		rd_existPatternDistResult_1 = exists_File(jsonWritePatDist);
		rd_checkJsonWritePatDist_1 = true;
	}

	if (rd_cntCPGAction == rd_endConverge - 1) {
		cout << "END OF DISCRETE PATTERN" << endl;
		// cout << "MOTOR DISABLE" << endl;
		// realRos->sendTypeCon(4);
		realRos->mode = "3"; 
	} else {

		// *********************************************
		// * Action
		// *********************************************
		// We are going to use weight from the learning phase to implement online pattern generation here.
		// We will use CPGs signal as clock to drive the walking instead of using (recorded pattern + calculated time).
		if (rd_actionStart || rd_existPatternDistResult_1) {

			cout << "***************************************" << endl;
			cout << "************* Action ****************" << endl;
			cout << "***************************************" << endl;

			if (!realRos->notiNo_1_Done) {
				rd_notiNo = 1;
				realRos->sendNotiNo(rd_notiNo); 
			} else { // notiNo_1_Done == True
				rd_notiNo = 0; // Go back to normal, can process further
				realRos->sendNotiNo(rd_notiNo);
				
				cout << "5.1) Load weights" << endl;
				if (!rd_actionLoadWeight) {
					// ----------------------------------
					// 1. Load weight from patternDistResult.json
					// ----------------------------------
					if (ofsjWPD.is_open()) {
						ofsjWPD.close();
						cout << "Save file to: " << jsonWritePatDist << endl; // patternDistResult.json
					}
					
					jsonReadPatDistFn(); // To read weight
					
					rd_actionLoadWeight = true;

				} // End rd_actionLoadWeight
				cout << "rd_actionLoadWeight: " << rd_actionLoadWeight << endl;


				// ----------------------------------
				// 2. Start CPGs with learned frequency
				// ----------------------------------
				// Initialize SO2 Action ==========================================================
				// cout << "5.2.1) Start CPGs with learned freq." << endl;
				// rd_initFreqRSO2Action = rd_meanrCPGFreqRW/rd_cpgFreqScale;
				// rd_initFreqLSO2Action = rd_meanrCPGFreqRW/rd_cpgFreqScale; // Use from right
				// if (!rd_flagStrSO2Action) {
				// 	rd_flagStrSO2Action = realRos->sendCpgService("SO2", "initAction", rd_initFreqRSO2Action, rd_initFreqLSO2Action, 0, 0);
				// 	cout << "rd_flagStrSO2Action: " << rd_flagStrSO2Action << endl;
				// }
				// realRos->sendFlagStrSO2Action(rd_flagStrSO2Action);


				// + ONLINE vs MANUAL FREQ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
				// + TEST INCREASING ONLINE CPG FREQUENCY ----------------------------------
				// rd_cnt_incFreq += 1;
				// if ((rd_cnt_incFreq % 1000) == 0 && !rd_stopIncFreq){
				// 	rd_meanrCPGFreqRW += 0.1; // Freq range = 0 - 1 Hz real world
				// 	rd_meanlCPGFreqRW += 0.1; // Freq range = 0 - 1 Hz real world

				// 	rd_internalrFreqCPG = rd_meanrCPGFreqRW/rd_cpgFreqScale; // From learning step; Real world freq -> Internal freq
				// 	rd_internallFreqCPG = rd_meanlCPGFreqRW/rd_cpgFreqScale;
				// }

				// if (rd_cnt_incFreq == 5000){
				// 	rd_stopIncFreq = true;
				// }
				// - TEST INCREASING ONLINE CPG FREQUENCY ----------------------------------


				// + Choose between -----------------------------------------------------
				// 1. Online 
				// 2. Manual input freq
				// 3. Manual inc/dec freq
				// cout << "5.2.2) Select between Manual vs. Online freq." << endl;
				// switch(realRos->onlineVsManualFreq) {
				// 	case 0:
				// 		// Current freq = Previous freq -----------------
				// 		cout << endl;
				// 		for (int i = 1; i <= 1; i++) {
				// 			cout << "ENTER CASE 0: FREEZED FREQ" << endl;
				// 		}
				// 		if (!rd_setCurrentFreqFirstTime){
				// 			rd_rCurrentFreq = rd_meanrCPGFreqRW/rd_cpgFreqScale;
				// 			rd_lCurrentFreq = rd_meanlCPGFreqRW/rd_cpgFreqScale;
				// 			rd_setCurrentFreqFirstTime = true;
				// 		} else {
				// 			rd_rCurrentFreq = realRos->rightLegFreqSO2Action; // SO2
				// 			rd_lCurrentFreq = realRos->leftLegFreqSO2Action;
				// 		}
				// 		break;					

				// 	case 1:
				// 		// + UPDATE CURRENT Freq. WITH ONLINE Freq. ---------------
				// 		cout << endl;
				// 		for (int i = 1; i <= 1; i++) {
				// 			cout << "ENTER CASE 1: ONLINE FREQ" << endl;
				// 		}
				// 		if (!rd_setCurrentFreqFirstTime){
				// 			rd_rCurrentFreq = rd_meanrCPGFreqRW/rd_cpgFreqScale;
				// 			rd_lCurrentFreq = rd_meanlCPGFreqRW/rd_cpgFreqScale;
				// 			rd_setCurrentFreqFirstTime = true;
				// 		} else {
				// 			rd_rCurrentFreq = realRos->rightLegFreqSO2Action; // SO2
				// 			rd_lCurrentFreq = realRos->leftLegFreqSO2Action;
				// 		}

				// 		rd_rOnlineFreq = realRos->rightLegFreqAFDC; // Learn from AFDC
				// 		rd_lOnlineFreq = realRos->leftLegFreqAFDC;

				// 		// From learning step; Real world freq -> Internal freq
				// 		// Freq_final = learningRate*Freq_tar + (1-learningRate)*Freq_feed
				// 		// For example, 0.5 Hz real world is internalxFreqCPG = 0.5/100 = 0.005
				// 		rd_internalrFreqCPG = (1 - rd_learningRateFreq)*rd_rCurrentFreq + rd_learningRateFreq*rd_rOnlineFreq; // Same as = rCurrentFreq - learningRateFreq*(rCurrentFreq - rOnlineFreq)
				// 		rd_internallFreqCPG = (1 - rd_learningRateFreq)*rd_lCurrentFreq + rd_learningRateFreq*rd_lOnlineFreq;
				// 		// - UPDATE CURRENT Freq. WITH ONLINE Freq. ---------------
				// 		break;
				
				// 	case 2:
				// 		// Manually input freq e.g., 0.5 Hz
				// 		cout << endl;
				// 		for (int i = 1; i <= 1; i++) {
				// 			cout << "ENTER CASE 2: MAN INPUT FREQ" << endl;
				// 		}
				// 		rd_freqManual = realRos->freqManual; // Hz real world
				// 		rd_internalrFreqCPG = rd_freqManual/rd_cpgFreqScale; // e.g., 0.5/100 = 0.005
				// 		rd_internallFreqCPG = rd_freqManual/rd_cpgFreqScale;
				// 		break;

				// 	case 3:
				// 		// Manually inc/dec freq, w = inc +0.01 Hz real world, s = dec -0.01 Hz real world
				// 		cout << endl;
				// 		for (int i = 1; i <= 1; i++) {
				// 			cout << "ENTER CASE 3: MAN INC/DEC FREQ" << endl;
				// 		}
						
				// 		rd_internalrFreqCPG = realRos->rightLegFreqSO2Action + realRos->upDownFreqVal; // e.g., +/- 0.0001
				// 		rd_internallFreqCPG = realRos->leftLegFreqSO2Action + realRos->upDownFreqVal;
						
				// 		// Force to Case 0 to maintain current freq
				// 		// So, it is needed to press again to inc/dec freq
				// 		realRos->onlineVsManualFreq = 0;

				// 		break;

				// 	default:
				// 		// onlineVsManualFreq == -1
				// 		cout << endl;
				// 		for (int i = 1; i <= 1; i++) {
				// 			cout << "ENTER DEFAULT CASE: CONSTANT LEARNED FREQ" << endl;
				// 		}
				// 		// + Freezed CPG freq. from learning step ----------------
				// 		rd_internalrFreqCPG = rd_meanrCPGFreqRW/rd_cpgFreqScale; // From learning step; Real world freq -> Internal freq
				// 		rd_internallFreqCPG = rd_meanlCPGFreqRW/rd_cpgFreqScale;
				// 		// - Freezed CPG freq. from learning step ----------------
				// 		break;
				
				// }
				// - ONLINE vs MANUAL FREQ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

				// for (int i = 1; i <= 1; i++) {
				// 	cout << endl;
				// 	cout << "CURRENT FREQ from SO2" << endl;
				// 	cout << "rd_rCurrentFreq: " << rd_rCurrentFreq << " " << "rd_lCurrentFreq: " << rd_lCurrentFreq << " " << "Hz internal" << endl;
				// 	cout << endl;
				// 	cout << "ONLINE FREQ from AFDC" << endl;
				// 	cout << "rd_rOnlineFreq: " << rd_rOnlineFreq << " " << "rd_lOnlineFreq: " << rd_lOnlineFreq << " " << "Hz internal" << endl;
				// 	cout << endl;
				// 	cout << "INTERNAL FREQ from SO2" << endl;
				// 	cout << "rd_internalrFreqCPG: " << rd_internalrFreqCPG << " " << "rd_internallFreqCPG: " << rd_internallFreqCPG << " " << "Hz internal" << endl; 
				// 	cout << endl;
				// }

				
				// Update SO2 Action ==========================================================
				// cout << "5.2.3) Update CPGs freq." << endl;
				// rd_flagUpdateSO2Action = realRos->sendCpgService("SO2", "updateAction", 0, 0, rd_internalrFreqCPG, rd_internallFreqCPG);
				// cout << "rd_flagUpdateSO2Action: " << rd_flagUpdateSO2Action << endl;


				// ----------------------------------
				// 3. Check phase of right CPG0 to be at 0
				// ----------------------------------
				// cout << "5.3) Check phase of right CPG0 to be at 0" << endl;
				// if (!rd_actionCPGGen) {
				// 	cout << "... Check phase of Right CPG0 to start action ..." << endl;
				// 	rd_rCPG0_RealTime[1] = realRos->rightCpgO0SO2Action;
				// 	rd_actionCPGGen = checkStartOnline(rd_rCPG0_RealTime[0], rd_rCPG0_RealTime[1]);
				// 	rd_rCPG0_RealTime[0] = rd_rCPG0_RealTime[1];

				// 	if (rd_actionCPGGen) {
				// 		rd_actionOnline = true;
				// 	}
				// } // End rd_actionCPGGen
				// cout << "rd_actionCPGGen: " << rd_actionCPGGen << endl;
				// realRos->sendFlagStrSO2ActionPhaseZero(rd_actionCPGGen);
				
				rd_actionOnline = true;
				if (rd_actionOnline) {
					// ----------------------------------
					// 4. Create kernel online
					// ----------------------------------
					// Kernel online for 1 timestep
					cout << "5.4) Create kernel online" << endl;

					// Get CPG data
					// double rd_rCPG0Online = realRos->rightCpgO0SO2Action;
					// double rd_rCPG1Online = realRos->rightCpgO1SO2Action;
					// double rd_lCPG0Online = realRos->leftCpgO0SO2Action;
					// double rd_lCPG1Online = realRos->leftCpgO1SO2Action;

					double rd_rCPG0Online = WPD_rCPG0SO2Learn_ACycle_Inter[rd_cntCPGAction];
					double rd_rCPG1Online = WPD_rCPG1SO2Learn_ACycle_Inter[rd_cntCPGAction];
					double rd_lCPG0Online = WPD_lCPG0SO2Learn_ACycle_Inter[rd_cntCPGAction];
					double rd_lCPG1Online = WPD_lCPG1SO2Learn_ACycle_Inter[rd_cntCPGAction];


					// Right ----------
					cout << "... Creating rKERNEL_ONLINE MATRIX ..." << endl;
					for (int i = 0; i < WPD_kernelSize; i++) {
						rd_rKernelOnline[i] = exp( -(pow((rd_rCPG0Online - WPD_rCPG0SO2Learn_ACycle_Sam[i]), 2) + pow((rd_rCPG1Online - WPD_rCPG1SO2Learn_ACycle_Sam[i]), 2))/s );
					}

					// Left ----------
					cout << "... Creating lKERNEL_ONLINE MATRIX ..." << endl;
					for (int i = 0; i < WPD_kernelSize; i++) {
						// Minus sign is for phase inversion
						rd_lKernelOnline[i] = exp( -(pow((rd_lCPG0Online - WPD_lCPG0SO2Learn_ACycle_Sam[i]), 2) + pow((rd_lCPG1Online - WPD_lCPG1SO2Learn_ACycle_Sam[i]), 2))/s );
					}


					// // + For debuging --------------------
					// cout << "rd_rCPG0Online: " << rd_rCPG0Online << endl;
					// cout << "rd_rCPG1Online: " << rd_rCPG1Online << endl;
					// cout << "rd_lCPG0Online: " << rd_lCPG0Online << endl;
					// cout << "rd_lCPG1Online: " << rd_lCPG1Online << endl;

					// cout << "rd_rHipWeightOnline: [";
					// for(int i = 0; i < rd_rHipWeightOnline.size(); i++){
					// 	cout << rd_rHipWeightOnline[i] << " ";
					// }
					// cout << endl;

					// cout << "WPD_rCPG0SO2Learn_ACycle_Sam: [";
					// for(int i = 0; i < WPD_rCPG0SO2Learn_ACycle_Sam.size(); i++){
					// 	cout << WPD_rCPG0SO2Learn_ACycle_Sam[i] << " ";
					// }
					// cout << endl;

					// cout << "rd_rKernelOnline: [";
					// for(int i = 0; i < rd_rKernelOnline.size(); i++){
					// 	cout << rd_rKernelOnline[i] << " ";
					// }
					// cout << endl;

					// while(cin.get() != '\n'){
					// 	;
					// }
					// // - For debuging --------------------


					// ----------------------------------
					// 5. Generate Pattern online
					// ----------------------------------
					cout << "5.5) Generate Pattern online" << endl;

					// Right ---------------------------------------------
					// Hip
					rd_rHipOnline = int(inner_product(begin(rd_rHipWeightOnline), end(rd_rHipWeightOnline), begin(rd_rKernelOnline), 0.0));
					// Knee
					rd_rKneeOnline = int(inner_product(begin(rd_rKneeWeightOnline), end(rd_rKneeWeightOnline), begin(rd_rKernelOnline), 0.0));
					// Ankle
					rd_rAnkleOnline = int(inner_product(begin(rd_rAnkleWeightOnline), end(rd_rAnkleWeightOnline), begin(rd_rKernelOnline), 0.0));

					// Left ---------------------------------------------
					// Hip
					rd_lHipOnline = int(inner_product(begin(rd_lHipWeightOnline), end(rd_lHipWeightOnline), begin(rd_lKernelOnline), 0.0));
					// Knee
					rd_lKneeOnline = int(inner_product(begin(rd_lKneeWeightOnline), end(rd_lKneeWeightOnline), begin(rd_lKernelOnline), 0.0));
					// Ankle
					rd_lAnkleOnline = int(inner_product(begin(rd_lAnkleWeightOnline), end(rd_lAnkleWeightOnline), begin(rd_lKernelOnline), 0.0));
					

					if (realRos->switchFlag) { // Switch pattern between R and L
						rd_adaptiveNormalOnline = {rd_lHipOnline, rd_lKneeOnline, rd_lAnkleOnline, rd_rHipOnline, rd_rKneeOnline, rd_rAnkleOnline};
					} else { // Default
						rd_adaptiveNormalOnline = {rd_rHipOnline, rd_rKneeOnline, rd_rAnkleOnline, rd_lHipOnline, rd_lKneeOnline, rd_lAnkleOnline};
					}
					



					// ----------------------------------
					// 6. Control
					// ----------------------------------
					cout << "5.6) Control online" << endl;

					// Type of control: Position control
					cout << "Pattern adaptation: Position control" << endl;
					realRos->sendTypeCon(1);

					// Set speed
					// We can use the speed value from command or overwrite here
					speed = rd_speedFound;
					realRos->sendSpeedCon(speed);

					// For safety reason,
					// Speed 7 will limit %Assist up to 80%
					// Speed 8 will limit %Assist up to 60%
					// Speed 9 will limit %Assist up to 50%
					// Speed 10 will limit %Assist up to 40%
					if (speed == 7) {
						if (assist > 80) {
							assist = 80;
						}

					} else if (speed == 8) {
						if (assist > 60) {
							assist = 60;
						}
					} else if (speed == 9) {
						if (assist > 50) {
							assist = 50;
						}
					} else if (speed == 10) {
						if (assist > 40) {
							assist = 40;
						}
					}

					// Set percentage of assistance
					realRos->sendAssistCon(assist);



					// Send joint angle vector of all positions of both legs
					realRos->sendJointAngleCon(rd_adaptiveNormalOnline);

		//			switch (speed) {
		//				case 1 : {
		//					if (nodeCounter%90 == 0) {
		//						realRos->sendJointAngleCon(rd_adaptiveNormalOnline);
		//					}
		//					break;
		//				}
		//				case 2 : {
		//					if (nodeCounter%84 == 0) {
		//						realRos->sendJointAngleCon(rd_adaptiveNormalOnline);
		//					}
		//					break;
		//				}
		//				case 3 : {
		//					if (nodeCounter%78 == 0) {
		//						realRos->sendJointAngleCon(rd_adaptiveNormalOnline);
		//					}
		//					break;
		//				}
		//				case 4 : {
		//					if (nodeCounter%72 == 0) {
		//						realRos->sendJointAngleCon(rd_adaptiveNormalOnline);
		//					}
		//					break;
		//				}
		//				case 5 : {
		//					if (nodeCounter%66 == 0) {
		//						realRos->sendJointAngleCon(rd_adaptiveNormalOnline);
		//					}
		//					break;
		//				}
		//				case 6 : {
		//					if (nodeCounter%60 == 0) {
		//						realRos->sendJointAngleCon(rd_adaptiveNormalOnline);
		//					}
		//					break;
		//				}
		//				case 7 : {
		//					if (nodeCounter%54 == 0) {
		//						realRos->sendJointAngleCon(rd_adaptiveNormalOnline);
		//					}
		//					break;
		//				}
		//				case 8 : {
		//					if (nodeCounter%48 == 0) {
		//						realRos->sendJointAngleCon(rd_adaptiveNormalOnline);
		//					}
		//					break;
		//				}
		//				case 9 : {
		//					if (nodeCounter%42 == 0) {
		//						realRos->sendJointAngleCon(rd_adaptiveNormalOnline);
		//					}
		//					break;
		//				}
		//				case 10 : {
		//					if (nodeCounter%36 == 0) {
		//						realRos->sendJointAngleCon(rd_adaptiveNormalOnline);
		//					}
		//					break;
		//				}
		//
		//			} // End Switch speed

					rd_speedOld = speed;



				} // End rd_actionOnline
				cout << "rd_actionOnline: " << rd_actionOnline << endl;
			}

		} // End rd_actionStart
		cout << "rd_actionStart: " << rd_actionStart << " " << "rd_existPatternDistResult_1: " << rd_existPatternDistResult_1 << endl;
	
		rd_cntCPGAction++;

	}


	return true;

}


//--------------------------------------------------------------------------
// speed adaptation
//--------------------------------------------------------------------------
bool ExvisParallelRealAssem::speedAdapt(ExvisParallelRealROS * realRos, int speed, int assist){

	if (!learnLoadSpeed) {

		// *********************************************
		// * Learning
		// *********************************************

		// Clear variable
		rCPGFreqRW.clear();
		lCPGFreqRW.clear();

		// -----------------------------
		// 1. Retrieve json data
		// -----------------------------

		jsonReadZeroTorqueFn();


		// ------------------------------------------
		// 2. Extract CPG
		// ------------------------------------------
		// Right now, we estimate that after 60,000 prog. step
		// the walking freq. converges, so we start looking for
		// the walking speed.

		cout << "timeStampCAN_ZT... : " << timeStampCAN_ZT.size() << endl;

		endConverge = timeStampCAN_ZT.size(); // The size of record

		// 2.1) Change CPGFreq to real world freq ---------------------
		for (int i = 0; i < endConverge; i++) {

			// Right --------------
			rCPGFreqRW.push_back(rCPGFreqAFDC_ZT[i]*cpgFreqScale); // Hz

			// Left --------------
			lCPGFreqRW.push_back(lCPGFreqAFDC_ZT[i]*cpgFreqScale); // Hz

//			cout << "rCPGFreqRW: " << rCPGFreqRW[i] << " " << "lCPGFreqRW: " << lCPGFreqRW[i] << endl;
		}

		cout << "FREQ END 2.1" << endl;

		// 2.2) Find average walking freq. from CPG freq. (in real world) ---------
		// Right --------------
		double sumrCPGFreqRW = 0;
		for (int i = strConverge; i < endConverge; i++) {
			sumrCPGFreqRW += rCPGFreqRW[i];
		}
		meanrCPGFreqRW = sumrCPGFreqRW/(rCPGFreqRW.size()-strConverge+1);
		meanrCPGTimeRW = 1/meanrCPGFreqRW; // sec

		// Left --------------
		double sumlCPGFreqRW = 0;
		for (int i = strConverge; i < endConverge; i++) {
			sumlCPGFreqRW += lCPGFreqRW[i];
		}
		meanlCPGFreqRW = sumlCPGFreqRW/(lCPGFreqRW.size()-strConverge+1);
		meanlCPGTimeRW = 1/meanlCPGFreqRW; // sec

		cout << "FREQ END 2.2" << endl;

		// 2.3) Convert time to Exo speed
		// Average time value from both leg
		avgTimeBothLegs = round(((meanrCPGTimeRW + meanlCPGTimeRW)/2)*10)/10; // round to one decimal

		// Right and Left
		if (avgTimeBothLegs >= 4.5) {
			speed = 1;
		}

		if ((avgTimeBothLegs >= 4.2) && (avgTimeBothLegs < 4.5)) {
			speed = 2;
		}

		if ((avgTimeBothLegs >= 3.9) && (avgTimeBothLegs < 4.2)) {
			speed = 3;
		}

		if ((avgTimeBothLegs >= 3.6) && (avgTimeBothLegs < 3.9)) {
			speed = 4;
		}

		if ((avgTimeBothLegs >= 3.3) && (avgTimeBothLegs < 3.6)) {
			speed = 5;
		}

		if ((avgTimeBothLegs >= 3.0) && (avgTimeBothLegs < 3.3)) {
			speed = 6;
		}

		if ((avgTimeBothLegs >= 2.7) && (avgTimeBothLegs < 3.0)) {
			speed = 7;
		}

		if ((avgTimeBothLegs >= 2.4) && (avgTimeBothLegs < 2.7)) {
			speed = 8;
		}

		if ((avgTimeBothLegs >= 2.1) && (avgTimeBothLegs < 2.4)) {
			speed = 9;
		}

		if ((avgTimeBothLegs >= 1.8) && (avgTimeBothLegs < 2.1)) {
			speed = 10;
		}

		learnLoadSpeed = true; // To have learning step once
		learnStartSpeed = true;

	} // End learn frequency

	cout << "END LEARN FREQUENCY" << endl;
	cout << "meanrCPGTimeRW: " << meanrCPGTimeRW << endl;
	cout << "meanlCPGTimeRW: " << meanlCPGTimeRW << endl;
	cout << "avgTimeBothLegs: " << avgTimeBothLegs << endl;
	cout << "speed: " << speed << endl;

	if (learnStartSpeed) {

		// + This section is the same code as for Basic mode position control ------------
		cout << "Speed adaptation: Position control" << endl;
		// Type of control
		realRos->sendTypeCon(1);

		// Set speed
		realRos->sendSpeedCon(speed);


		// For safety reason,
		// Speed 7 will limit %Assist up to 80%
		// Speed 8 will limit %Assist up to 60%
		// Speed 9 will limit %Assist up to 50%
		// Speed 10 will limit %Assist up to 40%
		if (speed == 7) {
			if (assist > 80) {
				assist = 80;
			}

		} else if (speed == 8) {
			if (assist > 60) {
				assist = 60;
			}
		} else if (speed == 9) {
			if (assist > 50) {
				assist = 50;
			}
		} else if (speed == 10) {
			if (assist > 40) {
				assist = 40;
			}
		}

		// Set percentage of assistance
		realRos->sendAssistCon(assist);



		// Initial step
		if (initialST) {
			// Set number of data per walking cycle
			realRos->sendDataPtsCon(1);
			// Joint angle
			realRos->sendJointAngleCon(InitST);
			initialST = false;
		}


		// First step
		if (firstST) {
			realRos->sendDataPtsCon(25);
			switch (speed) {
				case 1 : {
					if (nodeCounter%180 == 0) {
						if (iFirstStep < 25) {
							realRos->sendJointAngleCon(FirstStep[iFirstStep]);
							iFirstStep++;
						} else {
							firstST = false;
						}
					}
					break;
				}
				case 2 : {
					if (nodeCounter%168 == 0) {
						if (iFirstStep < 25) {
							realRos->sendJointAngleCon(FirstStep[iFirstStep]);
							iFirstStep++;
						} else {
							firstST = false;
						}
					}
					break;
				}
				case 3 : {
					if (nodeCounter%156 == 0) {
						if (iFirstStep < 25) {
							realRos->sendJointAngleCon(FirstStep[iFirstStep]);
							iFirstStep++;
						} else {
							firstST = false;
						}
					}
					break;
				}
				case 4 : {
					if (nodeCounter%144 == 0) {
						if (iFirstStep < 25) {
							realRos->sendJointAngleCon(FirstStep[iFirstStep]);
							iFirstStep++;
						} else {
							firstST = false;
						}
					}
					break;
				}
				case 5 : {
					if (nodeCounter%132 == 0) {
						if (iFirstStep < 25) {
							realRos->sendJointAngleCon(FirstStep[iFirstStep]);
							iFirstStep++;
						} else {
							firstST = false;
						}
					}
					break;
				}
				case 6 : {
					if (nodeCounter%120 == 0) {
						if (iFirstStep < 25) {
							realRos->sendJointAngleCon(FirstStep[iFirstStep]);
							iFirstStep++;
						} else {
							firstST = false;
						}
					}
					break;
				}
				case 7 : {
					if (nodeCounter%108 == 0) {
						if (iFirstStep < 25) {
							realRos->sendJointAngleCon(FirstStep[iFirstStep]);
							iFirstStep++;
						} else {
							firstST = false;
						}
					}
					break;
				}
				case 8 : {
					if (nodeCounter%96 == 0) {
						if (iFirstStep < 25) {
							realRos->sendJointAngleCon(FirstStep[iFirstStep]);
							iFirstStep++;
						} else {
							firstST = false;
						}
					}
					break;
				}
				case 9 : {
					if (nodeCounter%84 == 0) {
						if (iFirstStep < 25) {
							realRos->sendJointAngleCon(FirstStep[iFirstStep]);
							iFirstStep++;
						} else {
							firstST = false;
						}
					}
					break;
				}
				case 10 : {
					if (nodeCounter%72 == 0) {
						if (iFirstStep < 25) {
							realRos->sendJointAngleCon(FirstStep[iFirstStep]);
							iFirstStep++;
						} else {
							firstST = false;
						}
					}
					break;
				}

			}



		} else {

			// Normal step
			realRos->sendDataPtsCon(51);

			// Speed can be changed after finishing one cycle only.
			if ((speed != speedOld) && (iNormalStep != 0)) {
				speed = speedOld;
			}

			switch (speed) {
				case 1 : {
					if (nodeCounter%90 == 0) {
						if (iNormalStep < 50) {
							realRos->sendJointAngleCon(NormalStep[iNormalStep]);
							iNormalStep++;
						} else {
							iNormalStep = 0;
						}
					}
					break;
				}
				case 2 : {
					if (nodeCounter%84 == 0) {
						if (iNormalStep < 50) {
							realRos->sendJointAngleCon(NormalStep[iNormalStep]);
							iNormalStep++;
						} else {
							iNormalStep = 0;
						}
					}
					break;
				}
				case 3 : {
					if (nodeCounter%78 == 0) {
						if (iNormalStep < 50) {
							realRos->sendJointAngleCon(NormalStep[iNormalStep]);
							iNormalStep++;
						} else {
							iNormalStep = 0;
						}
					}
					break;
				}
				case 4 : {
					if (nodeCounter%72 == 0) {
						if (iNormalStep < 50) {
							realRos->sendJointAngleCon(NormalStep[iNormalStep]);
							iNormalStep++;
						} else {
							iNormalStep = 0;
						}
					}
					break;
				}
				case 5 : {
					if (nodeCounter%66 == 0) {
						if (iNormalStep < 50) {
							realRos->sendJointAngleCon(NormalStep[iNormalStep]);
							iNormalStep++;
						} else {
							iNormalStep = 0;
						}
					}
					break;
				}
				case 6 : {
					if (nodeCounter%60 == 0) {
						if (iNormalStep < 50) {
							realRos->sendJointAngleCon(NormalStep[iNormalStep]);
							iNormalStep++;
						} else {
							iNormalStep = 0;
						}
					}
					break;
				}
				case 7 : {
					if (nodeCounter%54 == 0) {
						if (iNormalStep < 50) {
							realRos->sendJointAngleCon(NormalStep[iNormalStep]);
							iNormalStep++;
						} else {
							iNormalStep = 0;
						}
					}
					break;
				}
				case 8 : {
					if (nodeCounter%48 == 0) {
						if (iNormalStep < 50) {
							realRos->sendJointAngleCon(NormalStep[iNormalStep]);
							iNormalStep++;
						} else {
							iNormalStep = 0;
						}
					}
					break;
				}
				case 9 : {
					if (nodeCounter%42 == 0) {
						if (iNormalStep < 50) {
							realRos->sendJointAngleCon(NormalStep[iNormalStep]);
							iNormalStep++;
						} else {
							iNormalStep = 0;
						}
					}
					break;
				}
				case 10 : {
					if (nodeCounter%36 == 0) {
						if (iNormalStep < 50) {
							realRos->sendJointAngleCon(NormalStep[iNormalStep]);
							iNormalStep++;
						} else {
							iNormalStep = 0;
						}
					}
					break;
				}

			}

			speedOld = speed;


		} // End Normal step

		// - This section is the same code as for Basic mode position control ------------

	} // End Speed adaptation

	return true;

}


//--------------------------------------------------------------------------
// pattern adaptation
//--------------------------------------------------------------------------
bool ExvisParallelRealAssem::patternAdapt(ExvisParallelRealROS * realRos, int speed, int assist){


	cout << "***************************************" << endl;
	cout << "********* Pattern Adaptation **********" << endl;
	cout << "***************************************" << endl;


	// *********************************************
	// * Load and check conditions
	// *********************************************
	
	if (!learnLoad) {

		cout << "............................................" << endl;
		cout << "............. Check Condition .............." << endl;
		cout << "............................................" << endl;

		// Clear variable
		rCPGFreqRW.clear();
		lCPGFreqRW.clear();


		// -----------------------------
		// 1. Retrieve data
		// -----------------------------
		cout << "1. Retrieve data" << endl;

		// 1.1) Read file
		cout << "1.1) Read exvisJSONZeroTorque.json" << endl;
		jsonReadZeroTorqueFn(); // exvisJSONZeroTorque.json = Record during Free mode


		// ------------------------------------------
		// 2. Extract CPG and jointAngle for a cycle
		// ------------------------------------------
		// Right now, we estimate that after 60,000 prog. step
		// the walking freq. converges, so we start looking for
		// the target pattern after that time.
		cout << "2. Extract CPG and jointAngle for a cycle" << endl;

		endConverge = timeStampCAN_ZT.size(); // The size of record
		cout << "endConverge = " << endConverge << endl;


		// + Check if there is patternResult.json -----------------------------------
		// If YES, we will not do the learing, but using the freq. from recorded .json
		if (!checkJsonWritePat_1) {

			existPatternResult_1 = exists_File(jsonWritePat);

			if (existPatternResult_1) {

				jsonReadPatFn(); // To read freq, weight

				meanrCPGTimeRW = meanrCPGTimeRWOnline; // Read from .json
				meanlCPGTimeRW = meanlCPGTimeRWOnline; // Read from .json
				meanrCPGFreqRW = 1/meanrCPGTimeRWOnline; // Read from .json
				meanlCPGFreqRW = 1/meanlCPGTimeRWOnline; // Read from .json
				cout << "FROM JSON " << endl;
			}
			checkJsonWritePat_1 = true;
		}

		if (!existPatternResult_1) {
			
			cout << "rCPGFreqAFDC_ZT.size() = " << rCPGFreqAFDC_ZT.size() << endl;
			// while(cin.get() != '\n'){
			// 	;
			// }

			// 2.1) Change CPGFreq to real world freq ---------------------
			cout << "2.1) Change CPGFreq to real world freq" << endl;
			for (int i = 0; i < endConverge; i++) {
				// Right --------------
				rCPGFreqRW.push_back(rCPGFreqAFDC_ZT[i]*cpgFreqScale); // Hz

				// Left --------------
				lCPGFreqRW.push_back(lCPGFreqAFDC_ZT[i]*cpgFreqScale); // Hz
			}


			// 2.2) Find average walking freq. from CPG freq. (in real world) ---------
			cout << "2.2) Find average walking freq. from CPG freq. (in real world)" << endl;
			// Right --------------
			double sumrCPGFreqRW = 0;
			for (int i = strConverge; i < endConverge; i++) {
				sumrCPGFreqRW += rCPGFreqRW[i];
			}
			meanrCPGFreqRW = sumrCPGFreqRW/(rCPGFreqRW.size()-strConverge+1);
			meanrCPGTimeRW = 1/meanrCPGFreqRW; // sec

			// Left --------------
			double sumlCPGFreqRW = 0;
			for (int i = strConverge; i < endConverge; i++) {
				sumlCPGFreqRW += lCPGFreqRW[i];
			}

			// meanlCPGFreqRW = meanrCPGFreqRW; // assume both legs equal freq.
			meanlCPGFreqRW = sumlCPGFreqRW/(lCPGFreqRW.size()-strConverge+1);
			meanlCPGTimeRW = 1/meanlCPGFreqRW; // sec

		}


		// + Convert time to Exo speed --------------------------------------
		// Average time value from both leg
		avgTimeBothLegs = round(((meanrCPGTimeRW + meanlCPGTimeRW)/2)*10)/10; // round to one decimal

		// Right and Left
		if (avgTimeBothLegs >= 4.5) {
			speedFound = 1;
		}

		if ((avgTimeBothLegs >= 4.2) && (avgTimeBothLegs < 4.5)) {
			speedFound = 2;
		}

		if ((avgTimeBothLegs >= 3.9) && (avgTimeBothLegs < 4.2)) {
			speedFound = 3;
		}

		if ((avgTimeBothLegs >= 3.6) && (avgTimeBothLegs < 3.9)) {
			speedFound = 4;
		}

		if ((avgTimeBothLegs >= 3.3) && (avgTimeBothLegs < 3.6)) {
			speedFound = 5;
		}

		if ((avgTimeBothLegs >= 3.0) && (avgTimeBothLegs < 3.3)) {
			speedFound = 6;
		}

		if ((avgTimeBothLegs >= 2.7) && (avgTimeBothLegs < 3.0)) {
			speedFound = 7;
		}

		if ((avgTimeBothLegs >= 2.4) && (avgTimeBothLegs < 2.7)) {
			speedFound = 8;
		}

		if ((avgTimeBothLegs >= 2.1) && (avgTimeBothLegs < 2.4)) {
			speedFound = 9;
		}

		if ((avgTimeBothLegs >= 1.8) && (avgTimeBothLegs < 2.1)) {
			speedFound = 10;
		}
		// - Convert time to Exo speed --------------------------------------

		if (!existPatternResult_1) {
			// + For patternResult.json ---------------
			jWP_rMeanCPGTimeRW_out.append(meanrCPGTimeRW);
			jWP_lMeanCPGTimeRW_out.append(meanlCPGTimeRW);
			jWP_speedFound_Out.append(speedFound);
			// - For patternResult.json ---------------
		}

		cout << "meanrCPGTimeRW: " << meanrCPGTimeRW << " " << "meanlCPGTimeRW: " << meanlCPGTimeRW << endl;
		cout << "meanrCPGFreqRW: " << meanrCPGFreqRW << " " << "meanlCPGFreqRW: " << meanlCPGFreqRW << endl;
		cout << "speed: " << speedFound << endl;

		learnLoad = true;
	} // End learnLoad
	cout << "learnLoad: " << learnLoad << endl;

	// while(cin.get() != '\n'){
	// 	;
	// }


	// If patternResult.json has already existed, it does not need to do learning part.
	if (!existPatternResult_1) {
		// *********************************************
		// * Learning
		// *********************************************
		// --------------------------------------------------------------------------------------------------
		// + This part around loop to go outside patternAdapt() to complete prog. cycle
		// --------------------------------------------------------------------------------------------------
		cout << "***************************************" << endl;
		cout << "*************** Learning **************" << endl;
		cout << "***************************************" << endl;

		if (!(learnrCPGCollect && learnlCPGCollect)) {

			// 2.3) Generate CPG0 with freq. from user and will be used as source of learning RBF -------
			// Initialize SO2 Learn ==========================================================
			cout << "2.3) Generate CPG0 with freq. from user" << endl;
			initFreqRSO2Learn = meanrCPGFreqRW/cpgFreqScale;
			initFreqLSO2Learn = meanrCPGFreqRW/cpgFreqScale; // use from right
			if (!flagStrSO2Learn) {
				flagStrSO2Learn = realRos->sendCpgService("SO2", "initLearn", initFreqRSO2Learn, initFreqLSO2Learn, 0, 0);
				cout << "flagStrSO2Learn: " << flagStrSO2Learn << endl;

				// + For patternResult.json ---
				timeSO2LearnGen = realRos->timeStampCAN; // Use timeStampCAN the same as record node

				jWP_timeSO2LearnGen_Out.append(timeSO2LearnGen);
				// - For patternResult.json ---

			}


			// + Record both CPGs when they start to check how they behave ---
			// + For patternResult.json ---
			// Right
			rCPG0SO2Learn.push_back(realRos->rightCpgO0SO2Learn);
			rCPG1SO2Learn.push_back(realRos->rightCpgO1SO2Learn);

			jWP_rCPG0SO2Learn_Out.append(rCPG0SO2Learn.back());
			jWP_rCPG1SO2Learn_Out.append(rCPG1SO2Learn.back());

			rCPGFreqSO2Learn = realRos->rightLegFreqSO2Learn;
			jWP_rCPGFreqSO2Learn_Out.append(rCPGFreqSO2Learn);

			// Left
			lCPG0SO2Learn.push_back(realRos->leftCpgO0SO2Learn);
			lCPG1SO2Learn.push_back(realRos->leftCpgO1SO2Learn);

			jWP_lCPG0SO2Learn_Out.append(lCPG0SO2Learn.back());
			jWP_lCPG1SO2Learn_Out.append(lCPG1SO2Learn.back());

			lCPGFreqSO2Learn = realRos->leftLegFreqSO2Learn;
			jWP_lCPGFreqSO2Learn_Out.append(lCPGFreqSO2Learn);

			// - For patternResult.json ---
			// - Record both CPGs when they start to check how they behave ---
		}
		cout << "learnrCPGCollect: " << learnrCPGCollect << " " << "learnlCPGCollect: " << learnlCPGCollect << endl;

		// Right -----
		// 2.4.1) Check phase of right CPG0 to be at 0 -----------
		cout << "2.4.1) Check phase of right CPG0 to be at 0" << endl;
		if (!learnrCPGGen) {
			cout << "... Check phase of Right CPG0 to start collecting ..." << endl;
			// rCPG0_LearnTime[1] = cpgAdaptiveLearn->getRightCpgO0_SO2();
			rCPG0_LearnTime[1] = realRos->rightCpgO0SO2Learn;
			learnrCPGGen = checkStartOnline(rCPG0_LearnTime[0], rCPG0_LearnTime[1]);
			rCPG0_LearnTime[0] = rCPG0_LearnTime[1];

			if (learnrCPGGen) {
				cout << "Right CPG0 is phase 0" << endl;

				rCPG0SO2Learn_ACycle.clear();
				rCPG1SO2Learn_ACycle.clear();

				// + For patternResult.json ---
				rTimeSO2LearnPhaseZero = realRos->timeStampCAN;

				jWP_rTimeSO2LearnPhaseZero_Out.append(rTimeSO2LearnPhaseZero);
				// - For patternResult.json ---
			}
		} // End learnrCPGGen
		cout << "learnrCPGGen: " << learnrCPGGen << endl;

		// 2.4.2) Collect right CPG0 data for 1 cycle counted from prog. step ---------
		cout << "2.4.2) Collect right CPG0 data for 1 cycle" << endl;
		if (learnrCPGGen && !learnrCPGEnd) {
			// + Check the end of a cycle ---
			rCPG0_RealTime[1] = realRos->rightCpgO0SO2Learn;
			learnrCPGEnd = checkStartOnline(rCPG0_RealTime[0], rCPG0_RealTime[1]);
			rCPG0_RealTime[0] = rCPG0_RealTime[1];
			// - Check the end of a cycle ---

			if (!learnrCPGEnd) {
				// + For patternResult.json ---
				rCPG0SO2Learn_ACycle.push_back(realRos->rightCpgO0SO2Learn);
				rCPG1SO2Learn_ACycle.push_back(realRos->rightCpgO1SO2Learn);

				jWP_rCPG0SO2Learn_ACycle_Out.append(rCPG0SO2Learn_ACycle.back());
				jWP_rCPG1SO2Learn_ACycle_Out.append(rCPG1SO2Learn_ACycle.back());
				// - For patternResult.json ---
			}

			if (learnrCPGEnd) {
				cout << "End collect right CPG0 data" << endl;
				cout << "rCPG0SO2Learn_ACycle size: " << rCPG0SO2Learn_ACycle.size() << " " << "rCPG1SO2Learn_ACycle size: " << rCPG0SO2Learn_ACycle.size() << endl;

				// + For patternResult.json ---
				rCountCycle = rCPG0SO2Learn_ACycle.size(); // One cycle in time step
				rCPG0SO2Learn_CyclesFound = 1;

				jWP_rCountCycle_Out.append(rCountCycle);
				jWP_rCPG0SO2Learn_CyclesFound_Out.append(rCPG0SO2Learn_CyclesFound);
				// - For patternResult.json ---

				learnrCPGCollect = true;
			}
		} // End collect

		// Left -----
		// 2.4.3) Check phase of left CPG0 to be at 180 -----------
		cout << "2.4.3) Check phase of left CPG0 to be at 180" << endl;
		if (!learnlCPGGen) {
			cout << "... Check phase of Left CPG0 to start collecting ..." << endl;
			lCPG0_LearnTime[1] = realRos->leftCpgO0SO2Learn;
			learnlCPGGen = checkStartOnline180(lCPG0_LearnTime[0], lCPG0_LearnTime[1]);
			lCPG0_LearnTime[0] = lCPG0_LearnTime[1];

			if (learnlCPGGen) {
				cout << "Left CPG0 is phase 180" << endl;

				lCPG0SO2Learn_ACycle.clear();
				lCPG1SO2Learn_ACycle.clear();

				// + For patternResult.json ---
				lTimeSO2LearnPhase180 = realRos->timeStampCAN;

				jWP_lTimeSO2LearnPhase180_Out.append(lTimeSO2LearnPhase180);
				// - For patternResult.json ---
			}
		} // End learnlCPGGen

		// 2.4.4) Collect CPG0 data for 1 cycle counted from prog. step ---------
		cout << "2.4.4) Collect left CPG0 data for 1 cycle" << endl;
		if (learnlCPGGen && !learnlCPGEnd) {
			// + Check the end of a cycle ---
			lCPG0_LearnTime[1] = realRos->leftCpgO0SO2Learn;
			learnlCPGEnd = checkStartOnline180(lCPG0_LearnTime[0], lCPG0_LearnTime[1]);
			lCPG0_LearnTime[0] = lCPG0_LearnTime[1];
			// - Check the end of a cycle ---

			if (!learnlCPGEnd) {
				// + For patternResult.json ---
				lCPG0SO2Learn_ACycle.push_back(realRos->leftCpgO0SO2Learn);
				lCPG1SO2Learn_ACycle.push_back(realRos->leftCpgO1SO2Learn);

				jWP_lCPG0SO2Learn_ACycle_Out.append(lCPG0SO2Learn_ACycle.back());
				jWP_lCPG1SO2Learn_ACycle_Out.append(lCPG1SO2Learn_ACycle.back());
				// - For patternResult.json ---
			}

			if (learnlCPGEnd) {
				cout << "End collect left CPG0 data" << endl;
				cout << "lCPG0SO2Learn_ACycle size: " << lCPG0SO2Learn_ACycle.size() << " " << "lCPG1SO2Learn_ACycle size: " << lCPG1SO2Learn_ACycle.size() << endl;

				// + For patternResult.json ---
				lCountCycle = lCPG0SO2Learn_ACycle.size();
				lCPG0SO2Learn_CyclesFound = 1;

				jWP_lCountCycle_Out.append(lCountCycle);
				jWP_lCPG0SO2Learn_CyclesFound_Out.append(lCPG0SO2Learn_CyclesFound);
				// - For patternResult.json ---

				learnlCPGCollect = true;
			}
		} // End collect
		// --------------------------------------------------------------------------------------------------
		// - This part around loop to go outside patternAdapt() to complete prog. cycle.
		// -------------------------------------------------------------------------------------------------


		if (learnrCPGCollect && learnlCPGCollect && !learnStart) {

			// 2.5) Do Low-pass filter to smooth the signal from step behavior -----------
			cout << "2.5) Low-pass filter" << endl;
			// Right --------------
			double rHipJ_FB_ZT_LowP_t_1 = rHipJ_FB_ZT[0];
			double rKneeJ_FB_ZT_LowP_t_1 = rKneeJ_FB_ZT[0];
			double rAnkleJ_FB_ZT_LowP_t_1 = rAnkleJ_FB_ZT[0];
			// Left --------------
			double lHipJ_FB_ZT_LowP_t_1 = lHipJ_FB_ZT[0];
			double lKneeJ_FB_ZT_LowP_t_1 = lKneeJ_FB_ZT[0];
			double lAnkleJ_FB_ZT_LowP_t_1 = lAnkleJ_FB_ZT[0];

			for (int i = 0; i < endConverge; i++) {
				// + For patternResult.json ---
				// Right
				rHipJ_FB_ZT_LowP.push_back(0.9*rHipJ_FB_ZT_LowP_t_1 + 0.1*rHipJ_FB_ZT[i]);
				rHipJ_FB_ZT_LowP_t_1 = rHipJ_FB_ZT_LowP.back();
				rKneeJ_FB_ZT_LowP.push_back(0.9*rKneeJ_FB_ZT_LowP_t_1 + 0.1*rKneeJ_FB_ZT[i]);
				rKneeJ_FB_ZT_LowP_t_1 = rKneeJ_FB_ZT_LowP.back();
				rAnkleJ_FB_ZT_LowP.push_back(0.9*rAnkleJ_FB_ZT_LowP_t_1 + 0.1*rAnkleJ_FB_ZT[i]);
				rAnkleJ_FB_ZT_LowP_t_1 = rAnkleJ_FB_ZT_LowP.back();
				// Left
				lHipJ_FB_ZT_LowP.push_back(0.9*lHipJ_FB_ZT_LowP_t_1 + 0.1*lHipJ_FB_ZT[i]);
				lHipJ_FB_ZT_LowP_t_1 = lHipJ_FB_ZT_LowP.back();
				lKneeJ_FB_ZT_LowP.push_back(0.9*lKneeJ_FB_ZT_LowP_t_1 + 0.1*lKneeJ_FB_ZT[i]);
				lKneeJ_FB_ZT_LowP_t_1 = lKneeJ_FB_ZT_LowP.back();
				lAnkleJ_FB_ZT_LowP.push_back(0.9*lAnkleJ_FB_ZT_LowP_t_1 + 0.1*lAnkleJ_FB_ZT[i]);
				lAnkleJ_FB_ZT_LowP_t_1 = lAnkleJ_FB_ZT_LowP.back();

				jWP_rHipJ_FB_ZT_LowP_Out.append(rHipJ_FB_ZT_LowP.back());
				jWP_rKneeJ_FB_ZT_LowP_Out.append(rKneeJ_FB_ZT_LowP.back());
				jWP_rAnkleJ_FB_ZT_LowP_Out.append(rAnkleJ_FB_ZT_LowP.back());
				jWP_lHipJ_FB_ZT_LowP_Out.append(lHipJ_FB_ZT_LowP.back());
				jWP_lKneeJ_FB_ZT_LowP_Out.append(lKneeJ_FB_ZT_LowP.back());
				jWP_lAnkleJ_FB_ZT_LowP_Out.append(lAnkleJ_FB_ZT_LowP.back());
				// - For patternResult.json ---
			}

			// 2.6) Extract signals for 1 gait cycle --------------------
			// Ex. we want from time 60000 to End, index is 59999 to (End - 1)
			cout << "2.6) Extract signals for 1 gait cycle" << endl;
			cnt_rHip = rCountCycle;
			cnt_lHip = lCountCycle;

			// We have to checkStart which detects the zero crossing only for the "hip signal".
			// The knee and ankle will be recorded at the same time as hip.

			// + Right Hip Knee Ankle signal ---
			for (int i = strConverge; i < endConverge; i++) {
				if (!startRec_rHip) {
					// Check only for hip signal
					startRec_rHip = checkStart(rHipJ_FB_ZT_LowP[i-1], i, rHipJ_FB_ZT_LowP[i], &rHipJ_FB_ZT_LowP_ACycle, cnt_rHip);

					if (startRec_rHip) {
						// + For patternResult.json ---
						rKneeJ_FB_ZT_LowP_ACycle.push_back(rKneeJ_FB_ZT_LowP[i-1]);
						rAnkleJ_FB_ZT_LowP_ACycle.push_back(rAnkleJ_FB_ZT_LowP[i-1]);

						jWP_rHipJ_FB_ZT_LowP_ACycle_Out.append(rHipJ_FB_ZT_LowP[i-1]);
						jWP_rKneeJ_FB_ZT_LowP_ACycle_Out.append(rKneeJ_FB_ZT_LowP[i-1]);
						jWP_rAnkleJ_FB_ZT_LowP_ACycle_Out.append(rAnkleJ_FB_ZT_LowP[i-1]);
						// - For patternResult.json ---
					}
				}

				if (startRec_rHip) {
					if (cnt_rHip > 0) {
						// + For patternResult.json ---
						rHipJ_FB_ZT_LowP_ACycle.push_back(rHipJ_FB_ZT_LowP[i]); // rHip record
						rKneeJ_FB_ZT_LowP_ACycle.push_back(rKneeJ_FB_ZT_LowP[i]); // rKnee record
						rAnkleJ_FB_ZT_LowP_ACycle.push_back(rAnkleJ_FB_ZT_LowP[i]); // rAnkle record

						jWP_rHipJ_FB_ZT_LowP_ACycle_Out.append(rHipJ_FB_ZT_LowP[i]);
						jWP_rKneeJ_FB_ZT_LowP_ACycle_Out.append(rKneeJ_FB_ZT_LowP[i]);
						jWP_rAnkleJ_FB_ZT_LowP_ACycle_Out.append(rAnkleJ_FB_ZT_LowP[i]);
						// - For patternResult.json ---

						cnt_rHip -= 1;
					} else if (cnt_rHip == 0) {
						rHipJ_FB_ZT_CyclesFound += 1;
						rKneeJ_FB_ZT_CyclesFound += 1;
						rAnkleJ_FB_ZT_CyclesFound += 1;

						// + For patternResult.json ---
						rHipJ_FB_ZT_LowP_Mx.push_back(rHipJ_FB_ZT_LowP_ACycle);
						rKneeJ_FB_ZT_LowP_Mx.push_back(rKneeJ_FB_ZT_LowP_ACycle);
						rAnkleJ_FB_ZT_LowP_Mx.push_back(rAnkleJ_FB_ZT_LowP_ACycle);

						jWP_rHipJ_FB_ZT_LowP_Mx_Out.append(jWP_rHipJ_FB_ZT_LowP_ACycle_Out);
						jWP_rKneeJ_FB_ZT_LowP_Mx_Out.append(jWP_rKneeJ_FB_ZT_LowP_ACycle_Out);
						jWP_rAnkleJ_FB_ZT_LowP_Mx_Out.append(jWP_rAnkleJ_FB_ZT_LowP_ACycle_Out);
						// - For patternResult.json ---

						rHipJ_FB_ZT_LowP_ACycle.clear();
						rKneeJ_FB_ZT_LowP_ACycle.clear();
						rAnkleJ_FB_ZT_LowP_ACycle.clear();

						jWP_rHipJ_FB_ZT_LowP_ACycle_Out.clear();
						jWP_rKneeJ_FB_ZT_LowP_ACycle_Out.clear();
						jWP_rAnkleJ_FB_ZT_LowP_ACycle_Out.clear();

						startRec_rHip = false;
						cnt_rHip = rCountCycle;
					}
				}
			}
			// - Right Hip Knee Ankle signal ---

			// + Left Hip Knee Ankle signal ---
			for (int i = strConverge; i < endConverge; i++) {
				if (!startRec_lHip) {
					startRec_lHip = checkStart180(lHipJ_FB_ZT_LowP[i-1], i, lHipJ_FB_ZT_LowP[i], &lHipJ_FB_ZT_LowP_ACycle, cnt_lHip);

					if (startRec_lHip) {
						// + For patternResult.json ---
						lKneeJ_FB_ZT_LowP_ACycle.push_back(lKneeJ_FB_ZT_LowP[i-1]);
						lAnkleJ_FB_ZT_LowP_ACycle.push_back(lAnkleJ_FB_ZT_LowP[i-1]);

						jWP_lHipJ_FB_ZT_LowP_ACycle_Out.append(lHipJ_FB_ZT_LowP[i-1]);
						jWP_lKneeJ_FB_ZT_LowP_ACycle_Out.append(lKneeJ_FB_ZT_LowP[i-1]);
						jWP_lAnkleJ_FB_ZT_LowP_ACycle_Out.append(lAnkleJ_FB_ZT_LowP[i-1]);
						// - For patternResult.json ---
					}
				}

				if (startRec_lHip) {
					if (cnt_lHip > 0) {
						// + For patternResult.json ---
						lHipJ_FB_ZT_LowP_ACycle.push_back(lHipJ_FB_ZT_LowP[i]); // lHip record
						lKneeJ_FB_ZT_LowP_ACycle.push_back(lKneeJ_FB_ZT_LowP[i]); // lKnee record
						lAnkleJ_FB_ZT_LowP_ACycle.push_back(lAnkleJ_FB_ZT_LowP[i]); // lAnkle record

						jWP_lHipJ_FB_ZT_LowP_ACycle_Out.append(lHipJ_FB_ZT_LowP[i]);
						jWP_lKneeJ_FB_ZT_LowP_ACycle_Out.append(lKneeJ_FB_ZT_LowP[i]);
						jWP_lAnkleJ_FB_ZT_LowP_ACycle_Out.append(lAnkleJ_FB_ZT_LowP[i]);
						// - For patternResult.json ---

						cnt_lHip -= 1;
					} else if (cnt_lHip == 0) {
						lHipJ_FB_ZT_CyclesFound += 1;
						lKneeJ_FB_ZT_CyclesFound += 1;
						lAnkleJ_FB_ZT_CyclesFound += 1;

						// + For patternResult.json ---
						lHipJ_FB_ZT_LowP_Mx.push_back(lHipJ_FB_ZT_LowP_ACycle);
						lKneeJ_FB_ZT_LowP_Mx.push_back(lKneeJ_FB_ZT_LowP_ACycle);
						lAnkleJ_FB_ZT_LowP_Mx.push_back(lAnkleJ_FB_ZT_LowP_ACycle);

						jWP_lHipJ_FB_ZT_LowP_Mx_Out.append(jWP_lHipJ_FB_ZT_LowP_ACycle_Out);
						jWP_lKneeJ_FB_ZT_LowP_Mx_Out.append(jWP_lKneeJ_FB_ZT_LowP_ACycle_Out);
						jWP_lAnkleJ_FB_ZT_LowP_Mx_Out.append(jWP_lAnkleJ_FB_ZT_LowP_ACycle_Out);
						// - For patternResult.json ---

						lHipJ_FB_ZT_LowP_ACycle.clear();
						lKneeJ_FB_ZT_LowP_ACycle.clear();
						lAnkleJ_FB_ZT_LowP_ACycle.clear();

						jWP_lHipJ_FB_ZT_LowP_ACycle_Out.clear();
						jWP_lKneeJ_FB_ZT_LowP_ACycle_Out.clear();
						jWP_lAnkleJ_FB_ZT_LowP_ACycle_Out.clear();

						startRec_lHip = false;
						cnt_lHip = lCountCycle;
					}
				}
			}
			// - Left Hip Knee Ankle signal ---

			// + For patternResult.json ---
			jWP_rHipJ_FB_ZT_CyclesFound_Out.append(rHipJ_FB_ZT_CyclesFound);
			jWP_rKneeJ_FB_ZT_CyclesFound_Out.append(rKneeJ_FB_ZT_CyclesFound);
			jWP_rAnkleJ_FB_ZT_CyclesFound_Out.append(rAnkleJ_FB_ZT_CyclesFound);
			jWP_lHipJ_FB_ZT_CyclesFound_Out.append(lHipJ_FB_ZT_CyclesFound);
			jWP_lKneeJ_FB_ZT_CyclesFound_Out.append(lKneeJ_FB_ZT_CyclesFound);
			jWP_lAnkleJ_FB_ZT_CyclesFound_Out.append(lAnkleJ_FB_ZT_CyclesFound);
			// - For patternResult.json ---

			cout << "rCountCycle: " << rCountCycle << " " << "lCountCycle: " << lCountCycle << endl;
			cout << "rHipJ_FB_ZT_CyclesFound: " << rHipJ_FB_ZT_CyclesFound << " " << "rKneeJ_FB_ZT_CyclesFound: " << rKneeJ_FB_ZT_CyclesFound << " " << "rAnkleJ_FB_ZT_CyclesFound: " << rAnkleJ_FB_ZT_CyclesFound << endl;
			cout << "lHipJ_FB_ZT_CyclesFound: " << lHipJ_FB_ZT_CyclesFound << " " << "lKneeJ_FB_ZT_CyclesFound: " << lKneeJ_FB_ZT_CyclesFound << " " << "lAnkleJ_FB_ZT_CyclesFound: " << lAnkleJ_FB_ZT_CyclesFound << endl;


			// 2.7) Average graph ---------------------------------
			cout << "2.7) Average right/left hip, knee, and ankle" << endl;
			rHipJ_FB_ZT_LowP_Target.clear();
			rKneeJ_FB_ZT_LowP_Target.clear();
			rAnkleJ_FB_ZT_LowP_Target.clear();
			lHipJ_FB_ZT_LowP_Target.clear();
			lKneeJ_FB_ZT_LowP_Target.clear();
			lAnkleJ_FB_ZT_LowP_Target.clear();

			// Right ------------
			// + Hip signal ---
			// rHipJ_FB_ZT_LowP_Mx = { {Cycle#1}, {Cycle#2}, ...}
			cout << "rHipJ_FB_ZT_LowP_Target: ";
			double sumrHipCycleGMx = 0;
			for (int j = 0; j < rCountCycle; j++) { // per cycle
				for (int i = 0; i < rHipJ_FB_ZT_CyclesFound; i++) { // how many cycles
					sumrHipCycleGMx += rHipJ_FB_ZT_LowP_Mx[i][j];
				}

				// + For patternResult.json ---
				rHipJ_FB_ZT_LowP_Target.push_back(sumrHipCycleGMx/rHipJ_FB_ZT_CyclesFound);

				jWP_rHipJ_FB_ZT_LowP_Target_Out.append(rHipJ_FB_ZT_LowP_Target.back());
				// - For patternResult.json ---

				sumrHipCycleGMx = 0;
				cout << " " << j << ": " << rHipJ_FB_ZT_LowP_Target.back();
			}
			cout << endl;
			cout << "End avg right hip" << endl;
			// - Hip signal ---

			// + Knee signal ---
			double sumrKneeCycleGMx = 0;
			for (int j = 0; j < rCountCycle; j++) {
				for (int i = 0; i < rKneeJ_FB_ZT_CyclesFound; i++) {
					sumrKneeCycleGMx += rKneeJ_FB_ZT_LowP_Mx[i][j];
				}

				// + For patternResult.json ---
				rKneeJ_FB_ZT_LowP_Target.push_back(sumrKneeCycleGMx/rKneeJ_FB_ZT_CyclesFound);

				jWP_rKneeJ_FB_ZT_LowP_Target_Out.append(rKneeJ_FB_ZT_LowP_Target.back());
				// - For patternResult.json ---

				sumrKneeCycleGMx = 0;
			}
			cout << "End avg right knee" << endl;
			// - Knee signal ---

			// + Ankle signal ---
			double sumrAnkleCycleGMx = 0;
			for (int j = 0; j < rCountCycle; j++) {
				for (int i = 0; i < rAnkleJ_FB_ZT_CyclesFound; i++) {
					sumrAnkleCycleGMx += rAnkleJ_FB_ZT_LowP_Mx[i][j];
				}
				// + For patternResult.json ---
				rAnkleJ_FB_ZT_LowP_Target.push_back(sumrAnkleCycleGMx/rAnkleJ_FB_ZT_CyclesFound);

				jWP_rAnkleJ_FB_ZT_LowP_Target_Out.append(rAnkleJ_FB_ZT_LowP_Target.back());
				// - For patternResult.json ---

				sumrAnkleCycleGMx = 0;
			}
			cout << "End avg right ankle" << endl;
			// - Ankle signal ---


			// Left ------------
			// + Hip signal ---
			double sumlHipCycleGMx = 0;
			for (int j = 0; j < lCountCycle; j++) {
				for (int i = 0; i < lHipJ_FB_ZT_CyclesFound; i++) {
					sumlHipCycleGMx += lHipJ_FB_ZT_LowP_Mx[i][j];
				}
				// + For patternResult.json ---
				lHipJ_FB_ZT_LowP_Target.push_back(sumlHipCycleGMx/lHipJ_FB_ZT_CyclesFound);

				jWP_lHipJ_FB_ZT_LowP_Target_Out.append(lHipJ_FB_ZT_LowP_Target.back());
				// - For patternResult.json ---

				sumlHipCycleGMx = 0;
			}
			cout << "End avg left hip" << endl;
			// - Hip signal ---

			// + Knee signal ---
			double sumlKneeCycleGMx = 0;
			for (int j = 0; j < lCountCycle; j++) {
				for (int i = 0; i < lKneeJ_FB_ZT_CyclesFound; i++) {
					sumlKneeCycleGMx += lKneeJ_FB_ZT_LowP_Mx[i][j];
				}
				// + For patternResult.json ---
				lKneeJ_FB_ZT_LowP_Target.push_back(sumlKneeCycleGMx/lKneeJ_FB_ZT_CyclesFound);

				jWP_lKneeJ_FB_ZT_LowP_Target_Out.append(lKneeJ_FB_ZT_LowP_Target.back());
				// - For patternResult.json ---

				sumlKneeCycleGMx = 0;
			}
			cout << "End avg left knee" << endl;
			// - Knee signal ---

			// + Ankle signal ---
			double sumlAnkleCycleGMx = 0;
			for (int j = 0; j < lCountCycle; j++) {
				for (int i = 0; i < lAnkleJ_FB_ZT_CyclesFound; i++) {
					sumlAnkleCycleGMx += lAnkleJ_FB_ZT_LowP_Mx[i][j];
				}
				// + For patternResult.json ---
				lAnkleJ_FB_ZT_LowP_Target.push_back(sumlAnkleCycleGMx/lAnkleJ_FB_ZT_CyclesFound);

				jWP_lAnkleJ_FB_ZT_LowP_Target_Out.append(lAnkleJ_FB_ZT_LowP_Target.back());
				// - For patternResult.json ---

				sumlAnkleCycleGMx = 0;
			}
			cout << "End avg left ankle" << endl;
			// - Ankle signal ---


			// ----------------------------------
			// 3. Do the sampling of CPG0 & CPG1
			// and also sampling target signals
			// ----------------------------------
			// Sampling every "samplingStep" step ---------
			// We do this sampling data once and will use it during online as well.
			cout << "3. Do the sampling of CPG0 & CPG1 " << endl;
			samplingStep = rCountCycle/kernelSize;
			cout << "rCountCycle: " << rCountCycle << "/" << "kernelSize: " << kernelSize << endl;
			cout << "= samplingStep: " << samplingStep << endl;

			for (int nCPG = 0; nCPG < rCountCycle; nCPG++) {
				if ((nCPG + 1)%samplingStep == 0){

					// Store sampling data ----------
					// Right ---------
					rCPG0SO2Learn_ACycle_Sam[nSam] = rCPG0SO2Learn_ACycle[nCPG];
					rCPG1SO2Learn_ACycle_Sam[nSam] = rCPG1SO2Learn_ACycle[nCPG];
					rHipJ_FB_ZT_LowP_Target_Sam[nSam] = rHipJ_FB_ZT_LowP_Target[nCPG];
					rKneeJ_FB_ZT_LowP_Target_Sam[nSam] = rKneeJ_FB_ZT_LowP_Target[nCPG];
					rAnkleJ_FB_ZT_LowP_Target_Sam[nSam] = rAnkleJ_FB_ZT_LowP_Target[nCPG];

					// Left ----------
					lCPG0SO2Learn_ACycle_Sam[nSam] = lCPG0SO2Learn_ACycle[nCPG];
					lCPG1SO2Learn_ACycle_Sam[nSam] = lCPG1SO2Learn_ACycle[nCPG];
					lHipJ_FB_ZT_LowP_Target_Sam[nSam] = lHipJ_FB_ZT_LowP_Target[nCPG];
					lKneeJ_FB_ZT_LowP_Target_Sam[nSam] = lKneeJ_FB_ZT_LowP_Target[nCPG];
					lAnkleJ_FB_ZT_LowP_Target_Sam[nSam] = lAnkleJ_FB_ZT_LowP_Target[nCPG];

					// + For patternResult.json ---
					// Right ---------
					jWP_rCPG0SO2Learn_ACycle_Sam_Out.append(rCPG0SO2Learn_ACycle[nCPG]);
					jWP_rCPG1SO2Learn_ACycle_Sam_Out.append(rCPG1SO2Learn_ACycle[nCPG]);
					jWP_rHipJ_FB_ZT_LowP_Target_Sam_Out.append(rHipJ_FB_ZT_LowP_Target[nCPG]); // Hip
					jWP_rKneeJ_FB_ZT_LowP_Target_Sam_Out.append(rKneeJ_FB_ZT_LowP_Target[nCPG]); // Knee
					jWP_rAnkleJ_FB_ZT_LowP_Target_Sam_Out.append(rAnkleJ_FB_ZT_LowP_Target[nCPG]); // Ankle
					// Left ----------
					jWP_lCPG0SO2Learn_ACycle_Sam_Out.append(lCPG0SO2Learn_ACycle[nCPG]);
					jWP_lCPG1SO2Learn_ACycle_Sam_Out.append(lCPG1SO2Learn_ACycle[nCPG]);
					jWP_lHipJ_FB_ZT_LowP_Target_Sam_Out.append(lHipJ_FB_ZT_LowP_Target[nCPG]); // Hip
					jWP_lKneeJ_FB_ZT_LowP_Target_Sam_Out.append(lKneeJ_FB_ZT_LowP_Target[nCPG]); // Knee
					jWP_lAnkleJ_FB_ZT_LowP_Target_Sam_Out.append(lAnkleJ_FB_ZT_LowP_Target[nCPG]); // Ankle
					// - For patternResult.json ---

					nSam++;
				}
			}
			cout << "nSam: " << nSam << endl;

			// ----------------------------------
			// 4. Calculating pattern
			// ----------------------------------
			cout << "4. Calculating pattern" << endl;
			// Right
			vector<vector<double>> rKernel{rCountCycle, vector<double>(kernelSize, 0)}; // = [rCountCycle][kernelSize]
			// Left
			vector<vector<double>> lKernel{lCountCycle, vector<double>(kernelSize, 0)};

			// + weightPat.txt -----
			// Store timeStampCAN when calculate the weight
			myFile_weightPat.open(txtWriteWeightPat);
			myFile_weightPat << realRos->timeStampCAN << endl;
			// - weightPat.txt -----

			// 4.1) Create Kernel matrix ---------------------------------------------
			// Mathematically, we should have
			// Weight x Kernel = [20 col][20 row  x 2000 col]
			// Due to C++ vector to keep element row-major order, so
			// Weigh[i] -> {x, x, x, ..., x} = 20-row vector
			// Kernel -> {{20 row}, {20 row}, ..., {20 row}} = 2000-row vector each have 20-row vector = 2000 row x 20 col
			// rCountCycle x kernelSize e.g 2000x20
			cout << "4.1) Create Kernel" << endl;

			// Right ----------
			cout << "... Creating rKERNEL MATRIX ..." << endl;

			for (int i = 0; i < rCountCycle; i++) { // row e.g. 2000

				jWP_rKernel_Row_Out.clear(); // Clear for every i

				for (int j = 0; j < kernelSize; j++) { // col e.g. 20

					rKernel[i][j] = exp( -(pow((rCPG0SO2Learn_ACycle[i] - rCPG0SO2Learn_ACycle_Sam[j]), 2) + pow((rCPG1SO2Learn_ACycle[i] - rCPG1SO2Learn_ACycle_Sam[j]), 2))/s );

					// + For patternResult.json ---
					jWP_rKernel_Row_Out.append(rKernel[i][j]);
					// - For patternResult.json ---
				}

				// + For patternResult.json ---
				jWP_rKernel_Out.append(jWP_rKernel_Row_Out);
				// - For patternResult.json ---

			}

			// Left ----------
			cout << "... Creating lKERNEL MATRIX ..." << endl;

			for (int i = 0; i < lCountCycle; i++) { // row e.g. 2000

				jWP_lKernel_Row_Out.clear(); // Clear for every i

				for (int j = 0; j < kernelSize; j++) { // col e.g. 20

					lKernel[i][j] = exp( -(pow((lCPG0SO2Learn_ACycle[i] - lCPG0SO2Learn_ACycle_Sam[j]), 2) + pow((lCPG1SO2Learn_ACycle[i] - lCPG1SO2Learn_ACycle_Sam[j]), 2))/s );

					// + For patternResult.json ---
					jWP_lKernel_Row_Out.append(lKernel[i][j]);
					// - For patternResult.json ---

				}

				// + For patternResult.json ---
				jWP_lKernel_Out.append(jWP_lKernel_Row_Out);
				// - For patternResult.json ---

			}

			// Set weight to 0
			// Right ----------
			rHipJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0);
			rKneeJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0);
			rAnkleJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0);
			// Left ----------
			lHipJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0);
			lKneeJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0);
			lAnkleJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0);
			cout << "... Empty Weight Mx. ..." << endl;


			// 4.2) Learning RBF -----------------------------------------------------------
			cout << "4.2) Learning RBF" << endl;
			cout << "... Learing RBF ..." << endl;
			while (nLearn < learnLoop_RBF) { // Ex. learn for 400 rounds

				cout << "LearningRound # " << nLearn << "-------------------------" << endl;
				// + weightPat.txt -----
				myFile_weightPat << nLearn << endl;
				// - weightPat.txt -----

				// Create new variables for result
				// Right ----------
				rHipP_Control_Sam = vector<double>(kernelSize, 0);
				rKneeP_Control_Sam = vector<double>(kernelSize, 0);
				rAnkleP_Control_Sam = vector<double>(kernelSize, 0);
				// Left ----------
				lHipP_Control_Sam = vector<double>(kernelSize, 0);
				lKneeP_Control_Sam = vector<double>(kernelSize, 0);
				lAnkleP_Control_Sam = vector<double>(kernelSize, 0);
				cout << "... Empty Result Mx. ..." << endl;

				// Clear JSON variable for each learning round
				// Right ----------
				jWP_rHipJ_FB_ZT_LowP_Weight_Row_Out.clear();
				jWP_rKneeJ_FB_ZT_LowP_Weight_Row_Out.clear();
				jWP_rAnkleJ_FB_ZT_LowP_Weight_Row_Out.clear();
				// Left ----------
				jWP_lHipJ_FB_ZT_LowP_Weight_Row_Out.clear();
				jWP_lKneeJ_FB_ZT_LowP_Weight_Row_Out.clear();
				jWP_lAnkleJ_FB_ZT_LowP_Weight_Row_Out.clear();


				// 4.2.1) Inner product to create result ------------------------
				// Ex.
				// rHipJ_FB_ZT_LowP_Weight_Row 20x1
				// rKernel 2000x20
				// rHipP_Control 2000x1
				// rHipJ_FB_ZT_LowP_Target 20x1
				// rHipP_Control_Sam 20x1
				cout << "4.2.1) Inner product" << endl;
				cout << "... Inner product ..." << endl;

				// + Calculate only at sampling point ----------------------------
				// Just only the sampling one will be used to update weight
				double innProR;
				double innProL;

				// Right ---------------------------------------------
				// + Hip -----------------------------
				nSamResult = 0; // Reset counter
				for (int i = samplingStep - 1; i < rCountCycle; i = i + samplingStep) {
					innProR = inner_product(begin(rHipJ_FB_ZT_LowP_Weight_Row), end(rHipJ_FB_ZT_LowP_Weight_Row), begin(rKernel[i]), 0.0);
					rHipP_Control_Sam[nSamResult] = innProR;

					nSamResult++;
				}
				// - Hip -----------------------------

				// + Knee -----------------------------
				nSamResult = 0; // Reset counter
				for (int i = samplingStep - 1; i < rCountCycle; i = i + samplingStep) {
					innProR = inner_product(begin(rKneeJ_FB_ZT_LowP_Weight_Row), end(rKneeJ_FB_ZT_LowP_Weight_Row), begin(rKernel[i]), 0.0);
					rKneeP_Control_Sam[nSamResult] = innProR;

					nSamResult++;
				}
				// - Knee -----------------------------


				// + Ankle -----------------------------
				nSamResult = 0; // Reset counter
				for (int i = samplingStep - 1; i < rCountCycle; i = i + samplingStep) {
					innProR = inner_product(begin(rAnkleJ_FB_ZT_LowP_Weight_Row), end(rAnkleJ_FB_ZT_LowP_Weight_Row), begin(rKernel[i]), 0.0);
					rAnkleP_Control_Sam[nSamResult] = innProR;

					nSamResult++;
				}
				// - Ankle -----------------------------


				// Left ---------------------------------------------
				// + Hip -----------------------------
				nSamResult = 0; // Reset counter
				for (int i = samplingStep - 1; i < lCountCycle; i = i + samplingStep) {
					innProL = inner_product(begin(lHipJ_FB_ZT_LowP_Weight_Row), end(lHipJ_FB_ZT_LowP_Weight_Row), begin(lKernel[i]), 0.0);
					lHipP_Control_Sam[nSamResult] = innProL;

					nSamResult++;
				}
				// - Hip -----------------------------

				// + Knee -----------------------------
				nSamResult = 0; // Reset counter
				for (int i = samplingStep - 1; i < lCountCycle; i = i + samplingStep) {
					innProL = inner_product(begin(lKneeJ_FB_ZT_LowP_Weight_Row), end(lKneeJ_FB_ZT_LowP_Weight_Row), begin(lKernel[i]), 0.0);
					lKneeP_Control_Sam[nSamResult] = innProL;

					nSamResult++;
				}
				// - Knee -----------------------------

				// + Ankle -----------------------------
				nSamResult = 0; // Reset counter
				for (int i = samplingStep - 1; i < lCountCycle; i = i + samplingStep) {
					innProL = inner_product(begin(lAnkleJ_FB_ZT_LowP_Weight_Row), end(lAnkleJ_FB_ZT_LowP_Weight_Row), begin(lKernel[i]), 0.0);
					lAnkleP_Control_Sam[nSamResult] = innProL;

					nSamResult++;
				}
				// - Ankle -----------------------------

				// - Calculate only at sampling point ----------------------------


				// 4.2.2) Update weight -----------------------------------------------
				cout << "4.2.2) Update weight" << endl;
				cout << "... Update weight ..." << endl;
				// Right ---------------------------------------------
				// + Hip -----------------------------
				// + weightPat.txt -----
				myFile_weightPat << "rHip" << " ";
				// - weightPat.txt -----
				for (int i = 0; i < kernelSize; i++) {
					rHipJ_FB_ZT_LowP_Weight_Row[i] = rHipJ_FB_ZT_LowP_Weight_Row[i] + alpha_RBF*(rHipJ_FB_ZT_LowP_Target_Sam[i] - rHipP_Control_Sam[i]);

					// + For patternResult.json ---
					jWP_rHipJ_FB_ZT_LowP_Weight_Row_Out.append(rHipJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---
					
					// + weightPat.txt -----
					myFile_weightPat << rHipJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// - weightPat.txt -----

				}
				// + weightPat.txt -----
				myFile_weightPat << "\n";
				// - weightPat.txt -----
				// - Hip -----------------------------


				// + Knee -----------------------------
				// + weightPat.txt -----
				myFile_weightPat << "rKnee" << " ";
				// - weightPat.txt -----
				for (int i = 0; i < kernelSize; i++) {
					rKneeJ_FB_ZT_LowP_Weight_Row[i] = rKneeJ_FB_ZT_LowP_Weight_Row[i] + alpha_RBF*(rKneeJ_FB_ZT_LowP_Target_Sam[i] - rKneeP_Control_Sam[i]);

					// + For patternResult.json ---
					jWP_rKneeJ_FB_ZT_LowP_Weight_Row_Out.append(rKneeJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---

					// + weightPat.txt -----
					myFile_weightPat << rKneeJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// - weightPat.txt -----

				}
				// + weightPat.txt -----
				myFile_weightPat << "\n";
				// - weightPat.txt -----
				// - Knee -----------------------------


				// + Ankle -----------------------------
				// + weightPat.txt -----
				myFile_weightPat << "rAnkle" << " ";
				// - weightPat.txt -----
				for (int i = 0; i < kernelSize; i++) {
					rAnkleJ_FB_ZT_LowP_Weight_Row[i] = rAnkleJ_FB_ZT_LowP_Weight_Row[i] + alpha_RBF*(rAnkleJ_FB_ZT_LowP_Target_Sam[i] - rAnkleP_Control_Sam[i]);

					// + For patternResult.json ---
					jWP_rAnkleJ_FB_ZT_LowP_Weight_Row_Out.append(rAnkleJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---

					// + weightPat.txt -----
					myFile_weightPat << rAnkleJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// - weightPat.txt -----

				}
				// + weightPat.txt -----
				myFile_weightPat << "\n";
				// - weightPat.txt -----
				// - Ankle -----------------------------


				// Left ---------------------------------------------
				// + Hip -----------------------------
				// + weightPat.txt -----
				myFile_weightPat << "lHip" << " ";
				// - weightPat.txt -----
				for (int i = 0; i < kernelSize; i++) {
					lHipJ_FB_ZT_LowP_Weight_Row[i] = lHipJ_FB_ZT_LowP_Weight_Row[i] + alpha_RBF*(lHipJ_FB_ZT_LowP_Target_Sam[i] - lHipP_Control_Sam[i]);

					// + For patternResult.json ---
					jWP_lHipJ_FB_ZT_LowP_Weight_Row_Out.append(lHipJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---

					// + weightPat.txt -----
					myFile_weightPat << lHipJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// - weightPat.txt -----

				}
				// + weightPat.txt -----
				myFile_weightPat << "\n";
				// - weightPat.txt -----
				// - Hip -----------------------------


				// + Knee -----------------------------
				// + weightPat.txt -----
				myFile_weightPat << "lKnee" << " ";
				// - weightPat.txt -----
				for (int i = 0; i < kernelSize; i++) {
					lKneeJ_FB_ZT_LowP_Weight_Row[i] = lKneeJ_FB_ZT_LowP_Weight_Row[i] + alpha_RBF*(lKneeJ_FB_ZT_LowP_Target_Sam[i] - lKneeP_Control_Sam[i]);

					// + For patternResult.json ---
					jWP_lKneeJ_FB_ZT_LowP_Weight_Row_Out.append(lKneeJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---

					// + weightPat.txt -----
					myFile_weightPat << lKneeJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// - weightPat.txt -----
				}
				// + weightPat.txt -----
				myFile_weightPat << "\n";
				// - weightPat.txt -----
				// - Knee -----------------------------


				// + Ankle -----------------------------
				// + weightPat.txt -----
				myFile_weightPat << "lAnkle" << " ";
				// - weightPat.txt -----
				for (int i = 0; i < kernelSize; i++) {
					lAnkleJ_FB_ZT_LowP_Weight_Row[i] = lAnkleJ_FB_ZT_LowP_Weight_Row[i] + alpha_RBF*(lAnkleJ_FB_ZT_LowP_Target_Sam[i] - lAnkleP_Control_Sam[i]);

					// + For patternResult.json ---
					jWP_lAnkleJ_FB_ZT_LowP_Weight_Row_Out.append(lAnkleJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---

					// + weightPat.txt -----
					myFile_weightPat << lAnkleJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// - weightPat.txt -----
				}
				// + weightPat.txt -----
				myFile_weightPat << "\n";
				// - weightPat.txt -----
				// - Ankle -----------------------------

				nLearn++;

			} // End while

			// + For patternResult.json ---
			// Keep only final weight to JSON
			jWP_rHipJ_FB_ZT_LowP_Weight_Out.append(jWP_rHipJ_FB_ZT_LowP_Weight_Row_Out);
			jWP_rKneeJ_FB_ZT_LowP_Weight_Out.append(jWP_rKneeJ_FB_ZT_LowP_Weight_Row_Out);
			jWP_rAnkleJ_FB_ZT_LowP_Weight_Out.append(jWP_rAnkleJ_FB_ZT_LowP_Weight_Row_Out);
			jWP_lHipJ_FB_ZT_LowP_Weight_Out.append(jWP_lHipJ_FB_ZT_LowP_Weight_Row_Out);
			jWP_lKneeJ_FB_ZT_LowP_Weight_Out.append(jWP_lKneeJ_FB_ZT_LowP_Weight_Row_Out);
			jWP_lAnkleJ_FB_ZT_LowP_Weight_Out.append(jWP_lAnkleJ_FB_ZT_LowP_Weight_Row_Out);
			// - For patternResult.json ---

			nLearn = 0; // reset learning round counter


			// 4.3) Final result -------------------------------------------------------------------------
			// Actually, if we would like to generate online pattern, only the final weight is needed.
			// Here, we create final pattern as a result just in case to check the pattern.
			cout << "4.3) Final result" << endl;
			cout << "... Final result ..." << endl;

			rHipP_Control = vector<double>(rCountCycle, 0);
			rKneeP_Control = vector<double>(rCountCycle, 0);
			rAnkleP_Control = vector<double>(rCountCycle, 0);

			lHipP_Control = vector<double>(lCountCycle, 0);
			lKneeP_Control = vector<double>(lCountCycle, 0);
			lAnkleP_Control = vector<double>(lCountCycle, 0);


			// + For patternResult.json ---
			jWP_rHipP_Control_Out.clear();
			jWP_rKneeP_Control_Out.clear();
			jWP_rAnkleP_Control_Out.clear();
			jWP_lHipP_Control_Out.clear();
			jWP_lKneeP_Control_Out.clear();
			jWP_lAnkleP_Control_Out.clear();
			// - For patternResult.json ---

			// Right ---------------------------------------------
			// Hip
			for (int i = 0; i < rCountCycle; i++) {
				rHipP_Control[i] = inner_product(begin(rHipJ_FB_ZT_LowP_Weight_Row), end(rHipJ_FB_ZT_LowP_Weight_Row), begin(rKernel[i]), 0.0);

				// + For patternResult.json ---
				jWP_rHipP_Control_Out.append(rHipP_Control[i]);
				// - For patternResult.json ---

			}
			// Knee
			for (int i = 0; i < rCountCycle; i++) {
				rKneeP_Control[i] = inner_product(begin(rKneeJ_FB_ZT_LowP_Weight_Row), end(rKneeJ_FB_ZT_LowP_Weight_Row), begin(rKernel[i]), 0.0);

				// + For patternResult.json ---
				jWP_rKneeP_Control_Out.append(rKneeP_Control[i]);
				// - For patternResult.json ---

			}
			// Ankle
			for (int i = 0; i < rCountCycle; i++) {
				rAnkleP_Control[i] = inner_product(begin(rAnkleJ_FB_ZT_LowP_Weight_Row), end(rAnkleJ_FB_ZT_LowP_Weight_Row), begin(rKernel[i]), 0.0);

				// + For patternResult.json ---
				jWP_rAnkleP_Control_Out.append(rAnkleP_Control[i]);
				// - For patternResult.json ---

			}



			// Left ---------------------------------------------
			// Hip
			for (int i = 0; i < lCountCycle; i++) {
				lHipP_Control[i] = inner_product(begin(lHipJ_FB_ZT_LowP_Weight_Row), end(lHipJ_FB_ZT_LowP_Weight_Row), begin(lKernel[i]), 0.0);

				// + For patternResult.json ---
				jWP_lHipP_Control_Out.append(lHipP_Control[i]);
				// - For patternResult.json ---

			}

			// Knee
			for (int i = 0; i < lCountCycle; i++) {
				lKneeP_Control[i] = inner_product(begin(lKneeJ_FB_ZT_LowP_Weight_Row), end(lKneeJ_FB_ZT_LowP_Weight_Row), begin(lKernel[i]), 0.0);

				// + For patternResult.json ---
				jWP_lKneeP_Control_Out.append(lKneeP_Control[i]);
				// - For patternResult.json ---

			}

			// Ankle
			for (int i = 0; i < lCountCycle; i++) {
				lAnkleP_Control[i] = inner_product(begin(lAnkleJ_FB_ZT_LowP_Weight_Row), end(lAnkleJ_FB_ZT_LowP_Weight_Row), begin(lKernel[i]), 0.0);

				// + For patternResult.json ---
				jWP_lAnkleP_Control_Out.append(lAnkleP_Control[i]);
				// - For patternResult.json ---

			}


			// 4.4) Save result to file after ending of learning ----------------------------------------------
			// Check if patternResult.json exists in the folder, so no need to overwrite.
			// This will enable us to use this file again during actionStart with correct info.
			cout << "4.4) Save result to file" << endl;

			if (!existPatternResult_1) {
				cout << "WRITE PATTERN RESULT" << endl;
				ofsjWP.open(jsonWritePat, ofstream::trunc);
				jsonWritePatFn();
				ofsjWP.close();
				cout << "Save file to: " << jsonWritePat << endl; // patternResult.json
			}
			
			learnStart = true; // To have learning step once
			actionStart = true; // To start action
		} // End learnStart
		cout << "learnrCPGCollect: "<< learnrCPGCollect << " " << "learnlCPGCollect: " << learnlCPGCollect << endl;
		cout << "learnStart: " << learnStart << endl;
	}


	// *********************************************
	// * Action
	// *********************************************
	// We are going to use weight from the learning phase to implement online pattern generation here.
	// We will use CPGs signal as clock to drive the walking instead of using (recorded pattern + calculated time).
	if (actionStart || existPatternResult_1) {

		cout << "***************************************" << endl;
		cout << "************* Action ****************" << endl;
		cout << "***************************************" << endl;

		if (!realRos->notiNo_1_Done) {
			notiNo = 1;
			realRos->sendNotiNo(notiNo); 
		} else { // notiNo_1_Done == True
			notiNo = 0; // Go back to normal, can process further
			realRos->sendNotiNo(notiNo);
			
			cout << "5.1) Load weights" << endl;
			if (!actionLoadWeight) {
				// ----------------------------------
				// 1. Load weight from patternResult.json
				// ----------------------------------
				if (ofsjWP.is_open()) {
					ofsjWP.close();
					cout << "Save file to: " << jsonWritePat << endl; // patternResult.json
				}
				
				// If patternResult.json is existed, no need to load again. We have loaded at the beginning.
				if (!existPatternResult_1) {
					jsonReadPatFn(); // To read weight
				}
				actionLoadWeight = true;

			} // End actionLoadWeight
			cout << "actionLoadWeight: " << actionLoadWeight << endl;


			// ----------------------------------
			// 2. Start CPGs with learned frequency
			// ----------------------------------
			// Initialize SO2 Action ==========================================================
			cout << "5.2.1) Start CPGs with learned freq." << endl;
			initFreqRSO2Action = meanrCPGFreqRW/cpgFreqScale;
			initFreqLSO2Action = meanrCPGFreqRW/cpgFreqScale; // Use from right
			if (!flagStrSO2Action) {
				flagStrSO2Action = realRos->sendCpgService("SO2", "initAction", initFreqRSO2Action, initFreqLSO2Action, 0, 0);
				cout << "flagStrSO2Action: " << flagStrSO2Action << endl;
			}
			realRos->sendFlagStrSO2Action(flagStrSO2Action);
			


			// + ONLINE vs MANUAL FREQ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
			// + TEST INCREASING ONLINE CPG FREQUENCY ----------------------------------
			// cnt_incFreq += 1;
			// if ((cnt_incFreq % 1000) == 0 && !stopIncFreq){
			// 	meanrCPGFreqRW += 0.1; // Freq range = 0 - 1 Hz real world
			// 	meanlCPGFreqRW += 0.1; // Freq range = 0 - 1 Hz real world

			// 	internalrFreqCPG = meanrCPGFreqRW/cpgFreqScale; // From learning step; Real world freq -> Internal freq
			// 	internallFreqCPG = meanlCPGFreqRW/cpgFreqScale;
			// }

			// if (cnt_incFreq == 5000){
			// 	stopIncFreq = true;
			// }
			// - TEST INCREASING ONLINE CPG FREQUENCY ----------------------------------


			// + Choose between -----------------------------------------------------
			// 1. Online 
			// 2. Manual input freq
			// 3. Manual inc/dec freq
			cout << "5.2.2) Select between Manual vs. Online freq." << endl;
			switch(realRos->onlineVsManualFreq) {
				case 0:
					// Current freq = Previous freq -----------------
					cout << endl;
					for (int i = 1; i <= 1; i++) {
						cout << "ENTER CASE 0: FREEZED FREQ" << endl;
					}
					if (!setCurrentFreqFirstTime){
						rCurrentFreq = meanrCPGFreqRW/cpgFreqScale;
						lCurrentFreq = meanlCPGFreqRW/cpgFreqScale;
						setCurrentFreqFirstTime = true;
					} else {
						rCurrentFreq = realRos->rightLegFreqSO2Action; // SO2
						lCurrentFreq = realRos->leftLegFreqSO2Action;
					}
					break;					

				case 1:
					// + UPDATE CURRENT Freq. WITH ONLINE Freq. ---------------
					cout << endl;
					for (int i = 1; i <= 1; i++) {
						cout << "ENTER CASE 1: ONLINE FREQ" << endl;
					}
					if (!setCurrentFreqFirstTime){
						rCurrentFreq = meanrCPGFreqRW/cpgFreqScale;
						lCurrentFreq = meanlCPGFreqRW/cpgFreqScale;
						setCurrentFreqFirstTime = true;
					} else {
						rCurrentFreq = realRos->rightLegFreqSO2Action; // SO2
						lCurrentFreq = realRos->leftLegFreqSO2Action;
					}

					rOnlineFreq = realRos->rightLegFreqAFDC; // Learn from AFDC
					lOnlineFreq = realRos->leftLegFreqAFDC;

					// From learning step; Real world freq -> Internal freq
					// Freq_final = learningRate*Freq_tar + (1-learningRate)*Freq_feed
					// For example, 0.5 Hz real world is internalxFreqCPG = 0.5/100 = 0.005
					internalrFreqCPG = (1 - learningRateFreq)*rCurrentFreq + learningRateFreq*rOnlineFreq; // Same as = rCurrentFreq - learningRateFreq*(rCurrentFreq - rOnlineFreq)
					internallFreqCPG = (1 - learningRateFreq)*lCurrentFreq + learningRateFreq*lOnlineFreq;
					// - UPDATE CURRENT Freq. WITH ONLINE Freq. ---------------
					break;
			
				case 2:
					// Manually input freq e.g., 0.5 Hz
					cout << endl;
					for (int i = 1; i <= 1; i++) {
						cout << "ENTER CASE 2: MAN INPUT FREQ" << endl;
					}
					freqManual = realRos->freqManual; // Hz real world
					internalrFreqCPG = freqManual/cpgFreqScale; // e.g., 0.5/100 = 0.005
					internallFreqCPG = freqManual/cpgFreqScale;
					break;

				case 3:
					// Manually inc/dec freq, w = inc +0.01 Hz real world, s = dec -0.01 Hz real world
					cout << endl;
					for (int i = 1; i <= 1; i++) {
						cout << "ENTER CASE 3: MAN INC/DEC FREQ" << endl;
					}
					
					internalrFreqCPG = realRos->rightLegFreqSO2Action + realRos->upDownFreqVal; // e.g., +/- 0.0001
					internallFreqCPG = realRos->leftLegFreqSO2Action + realRos->upDownFreqVal;
					
					// Force to Case 0 to maintain current freq
					// So, it is needed to press again to inc/dec freq
					realRos->onlineVsManualFreq = 0;

					break;

				default:
					// onlineVsManualFreq == -1
					cout << endl;
					for (int i = 1; i <= 1; i++) {
						cout << "ENTER DEFAULT CASE: CONSTANT LEARNED FREQ" << endl;
					}
					// + Freezed CPG freq. from learning step ----------------
					internalrFreqCPG = meanrCPGFreqRW/cpgFreqScale; // From learning step; Real world freq -> Internal freq
					internallFreqCPG = meanlCPGFreqRW/cpgFreqScale;
					// - Freezed CPG freq. from learning step ----------------
					break;
			
			}
			// - ONLINE vs MANUAL FREQ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

			for (int i = 1; i <= 1; i++) {
				cout << endl;
				cout << "CURRENT FREQ from SO2" << endl;
				cout << "rCurrentFreq: " << rCurrentFreq << " " << "lCurrentFreq: " << lCurrentFreq << " " << "Hz internal" << endl;
				cout << endl;
				cout << "ONLINE FREQ from AFDC" << endl;
				cout << "rOnlineFreq: " << rOnlineFreq << " " << "lOnlineFreq: " << lOnlineFreq << " " << "Hz internal" << endl;
				cout << endl;
				cout << "INTERNAL FREQ from SO2" << endl;
				cout << "internalrFreqCPG: " << internalrFreqCPG << " " << "internallFreqCPG: " << internallFreqCPG << " " << "Hz internal" << endl; 
				cout << endl;
			}

			
			// Update SO2 Action ==========================================================
			cout << "5.2.3) Update CPGs freq." << endl;
			flagUpdateSO2Action = realRos->sendCpgService("SO2", "updateAction", 0, 0, internalrFreqCPG, internallFreqCPG);
			cout << "flagUpdateSO2Action: " << flagUpdateSO2Action << endl;


			// ----------------------------------
			// 3. Check phase of right CPG0 to be at 0
			// ----------------------------------
			cout << "5.3) Check phase of right CPG0 to be at 0" << endl;
			if (!actionCPGGen) {
				cout << "... Check phase of Right CPG0 to start action ..." << endl;
				rCPG0_RealTime[1] = realRos->rightCpgO0SO2Action;
				actionCPGGen = checkStartOnline(rCPG0_RealTime[0], rCPG0_RealTime[1]);
				rCPG0_RealTime[0] = rCPG0_RealTime[1];

				if (actionCPGGen) {
					actionOnline = true;
				}
			} // End actionCPGGen
			cout << "actionCPGGen: " << actionCPGGen << endl;
			realRos->sendFlagStrSO2ActionPhaseZero(actionCPGGen);
			
			
			if (actionOnline) {
				// ----------------------------------
				// 4. Create kernel online
				// ----------------------------------
				// Kernel online for 1 timestep
				cout << "5.4) Create kernel online" << endl;

				// Get CPG data
				double rCPG0Online = realRos->rightCpgO0SO2Action;
				double rCPG1Online = realRos->rightCpgO1SO2Action;
				double lCPG0Online = realRos->leftCpgO0SO2Action;
				double lCPG1Online = realRos->leftCpgO1SO2Action;

				// Right ----------
				cout << "... Creating rKERNEL_ONLINE MATRIX ..." << endl;
				for (int i = 0; i < kernelSize; i++) {
					rKernelOnline[i] = exp( -(pow((rCPG0Online - rCPG0SO2Learn_ACycle_Sam[i]), 2) + pow((rCPG1Online - rCPG1SO2Learn_ACycle_Sam[i]), 2))/s );
				}

				// Left ----------
				cout << "... Creating lKERNEL_ONLINE MATRIX ..." << endl;
				for (int i = 0; i < kernelSize; i++) {
					// Minus sign is for phase inversion
					lKernelOnline[i] = exp( -(pow((lCPG0Online - lCPG0SO2Learn_ACycle_Sam[i]), 2) + pow((lCPG1Online - lCPG1SO2Learn_ACycle_Sam[i]), 2))/s );
				}


				// // + For debuging --------------------
				// cout << "rCPG0Online: " << rCPG0Online << endl;
				// cout << "rCPG1Online: " << rCPG1Online << endl;
				// cout << "lCPG0Online: " << lCPG0Online << endl;
				// cout << "lCPG1Online: " << lCPG1Online << endl;

				// cout << "rHipWeightOnline: [";
				// for(int i = 0; i < rHipWeightOnline.size(); i++){
				// 	cout << rHipWeightOnline[i] << " ";
				// }
				// cout << endl;

				// cout << "rCPG0SO2Learn_ACycle_Sam: [";
				// for(int i = 0; i < rCPG0SO2Learn_ACycle_Sam.size(); i++){
				// 	cout << rCPG0SO2Learn_ACycle_Sam[i] << " ";
				// }
				// cout << endl;

				// cout << "rKernelOnline: [";
				// for(int i = 0; i < rKernelOnline.size(); i++){
				// 	cout << rKernelOnline[i] << " ";
				// }
				// cout << endl;

				// while(cin.get() != '\n'){
				// 	;
				// }
				// // - For debuging --------------------


				// ----------------------------------
				// 5. Generate Pattern online
				// ----------------------------------
				cout << "5.5) Generate Pattern online" << endl;

				// Right ---------------------------------------------
				// Hip
				rHipOnline = int(inner_product(begin(rHipWeightOnline), end(rHipWeightOnline), begin(rKernelOnline), 0.0));
				// Knee
				rKneeOnline = int(inner_product(begin(rKneeWeightOnline), end(rKneeWeightOnline), begin(rKernelOnline), 0.0));
				// Ankle
				rAnkleOnline = int(inner_product(begin(rAnkleWeightOnline), end(rAnkleWeightOnline), begin(rKernelOnline), 0.0));

				// Left ---------------------------------------------
				// Hip
				lHipOnline = int(inner_product(begin(lHipWeightOnline), end(lHipWeightOnline), begin(lKernelOnline), 0.0));
				// Knee
				lKneeOnline = int(inner_product(begin(lKneeWeightOnline), end(lKneeWeightOnline), begin(lKernelOnline), 0.0));
				// Ankle
				lAnkleOnline = int(inner_product(begin(lAnkleWeightOnline), end(lAnkleWeightOnline), begin(lKernelOnline), 0.0));
				

				if (realRos->switchFlag) { // Switch pattern between R and L
					adaptiveNormalOnline = {lHipOnline, lKneeOnline, lAnkleOnline, rHipOnline, rKneeOnline, rAnkleOnline};
				} else { // Default
					adaptiveNormalOnline = {rHipOnline, rKneeOnline, rAnkleOnline, lHipOnline, lKneeOnline, lAnkleOnline};
				}
				



				// ----------------------------------
				// 6. Control
				// ----------------------------------
				cout << "5.6) Control online" << endl;

				// Type of control: Position control
				cout << "Pattern adaptation: Position control" << endl;
				realRos->sendTypeCon(1);

				// Set speed
				// We can use the speed value from command or overwrite here
				speed = speedFound;
				realRos->sendSpeedCon(speed);

				// For safety reason,
				// Speed 7 will limit %Assist up to 80%
				// Speed 8 will limit %Assist up to 60%
				// Speed 9 will limit %Assist up to 50%
				// Speed 10 will limit %Assist up to 40%
				if (speed == 7) {
					if (assist > 80) {
						assist = 80;
					}

				} else if (speed == 8) {
					if (assist > 60) {
						assist = 60;
					}
				} else if (speed == 9) {
					if (assist > 50) {
						assist = 50;
					}
				} else if (speed == 10) {
					if (assist > 40) {
						assist = 40;
					}
				}

				// Set percentage of assistance
				realRos->sendAssistCon(assist);



				// Send joint angle vector of all positions of both legs
				realRos->sendJointAngleCon(adaptiveNormalOnline);

	//			switch (speed) {
	//				case 1 : {
	//					if (nodeCounter%90 == 0) {
	//						realRos->sendJointAngleCon(adaptiveNormalOnline);
	//					}
	//					break;
	//				}
	//				case 2 : {
	//					if (nodeCounter%84 == 0) {
	//						realRos->sendJointAngleCon(adaptiveNormalOnline);
	//					}
	//					break;
	//				}
	//				case 3 : {
	//					if (nodeCounter%78 == 0) {
	//						realRos->sendJointAngleCon(adaptiveNormalOnline);
	//					}
	//					break;
	//				}
	//				case 4 : {
	//					if (nodeCounter%72 == 0) {
	//						realRos->sendJointAngleCon(adaptiveNormalOnline);
	//					}
	//					break;
	//				}
	//				case 5 : {
	//					if (nodeCounter%66 == 0) {
	//						realRos->sendJointAngleCon(adaptiveNormalOnline);
	//					}
	//					break;
	//				}
	//				case 6 : {
	//					if (nodeCounter%60 == 0) {
	//						realRos->sendJointAngleCon(adaptiveNormalOnline);
	//					}
	//					break;
	//				}
	//				case 7 : {
	//					if (nodeCounter%54 == 0) {
	//						realRos->sendJointAngleCon(adaptiveNormalOnline);
	//					}
	//					break;
	//				}
	//				case 8 : {
	//					if (nodeCounter%48 == 0) {
	//						realRos->sendJointAngleCon(adaptiveNormalOnline);
	//					}
	//					break;
	//				}
	//				case 9 : {
	//					if (nodeCounter%42 == 0) {
	//						realRos->sendJointAngleCon(adaptiveNormalOnline);
	//					}
	//					break;
	//				}
	//				case 10 : {
	//					if (nodeCounter%36 == 0) {
	//						realRos->sendJointAngleCon(adaptiveNormalOnline);
	//					}
	//					break;
	//				}
	//
	//			} // End Switch speed

				speedOld = speed;



			} // End actionOnline
			cout << "actionOnline: " << actionOnline << endl;
		}

	} // End actionStart
	cout << "actionStart: " << actionStart << " " << "existPatternResult_1: " << existPatternResult_1 << endl;

	return true;

}


//--------------------------------------------------------------------------
// change speed
//--------------------------------------------------------------------------
bool ExvisParallelRealAssem::changeSpeed(ExvisParallelRealROS * realRos, int speed, int assist){

	cout << "***************************************" << endl;
	cout << "************* Action ****************" << endl;
	cout << "***************************************" << endl;
	
	cout << "5.1) Load weights" << endl;
	if (!cs_actionLoadWeight) {
		// ----------------------------------
		// 1. Load weight from patternResult.json
		// ----------------------------------
		jsonReadPatFn(); // To read weight
		cs_actionLoadWeight = true;

	} // End actionLoadWeight
	cout << "cs_actionLoadWeight: " << cs_actionLoadWeight << endl;


	// ----------------------------------
	// 2. Start CPGs with setting frequency
	// ----------------------------------
	// Initialize SO2 Action ==========================================================
	cout << "5.2.1) Start CPGs with learned freq." << endl;
	
	
	// meanrCPGFreqRW = 0.3; // Hz
	meanrCPGTimeRW = meanrCPGTimeRWOnline; // Read from .json
	meanrCPGFreqRW = 1/meanrCPGTimeRWOnline; // Read from .json
	meanlCPGFreqRW = meanrCPGFreqRW; // Read from .json



	initFreqRSO2Action = meanrCPGFreqRW/cpgFreqScale;
	initFreqLSO2Action = meanrCPGFreqRW/cpgFreqScale; // Use from right
	if (!flagStrSO2Action) {
		flagStrSO2Action = realRos->sendCpgService("SO2", "initAction", initFreqRSO2Action, initFreqLSO2Action, 0, 0);
		cout << "flagStrSO2Action: " << flagStrSO2Action << endl;
	}
	realRos->sendFlagStrSO2Action(flagStrSO2Action);
	


	// + ONLINE vs MANUAL FREQ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
	// + TEST INCREASING ONLINE CPG FREQUENCY ----------------------------------
	// cnt_incFreq += 1;
	// if ((cnt_incFreq % 1000) == 0 && !stopIncFreq){
	// 	meanrCPGFreqRW += 0.1; // Freq range = 0 - 1 Hz real world
	// 	meanlCPGFreqRW += 0.1; // Freq range = 0 - 1 Hz real world

	// 	internalrFreqCPG = meanrCPGFreqRW/cpgFreqScale; // From learning step; Real world freq -> Internal freq
	// 	internallFreqCPG = meanlCPGFreqRW/cpgFreqScale;
	// }

	// if (cnt_incFreq == 5000){
	// 	stopIncFreq = true;
	// }
	// - TEST INCREASING ONLINE CPG FREQUENCY ----------------------------------


	// + Choose between -----------------------------------------------------
	// 1. Online 
	// 2. Manual input freq
	// 3. Manual inc/dec freq
	cout << "5.2.2) Select between Manual vs. Online freq." << endl;
	switch(realRos->onlineVsManualFreq) {
		case 0:
			// Current freq = Previous freq -----------------
			cout << endl;
			for (int i = 1; i <= 1; i++) {
				cout << "ENTER CASE 0: FREEZED FREQ" << endl;
			}
			if (!cs_setCurrentFreqFirstTime){
				rCurrentFreq = meanrCPGFreqRW/cpgFreqScale;
				lCurrentFreq = meanlCPGFreqRW/cpgFreqScale;
				cs_setCurrentFreqFirstTime = true;
			} else {
				rCurrentFreq = realRos->rightLegFreqSO2Action; // SO2
				lCurrentFreq = realRos->leftLegFreqSO2Action;
			}
			break;					

		case 1:
			// + UPDATE CURRENT Freq. WITH ONLINE Freq. ---------------
			cout << endl;
			for (int i = 1; i <= 1; i++) {
				cout << "ENTER CASE 1: ONLINE FREQ" << endl;
			}
			if (!cs_setCurrentFreqFirstTime){
				rCurrentFreq = meanrCPGFreqRW/cpgFreqScale;
				lCurrentFreq = meanlCPGFreqRW/cpgFreqScale;
				cs_setCurrentFreqFirstTime = true;
			} else {
				rCurrentFreq = realRos->rightLegFreqSO2Action; // SO2
				lCurrentFreq = realRos->leftLegFreqSO2Action;
			}

			rOnlineFreq = realRos->rightLegFreqAFDC; // Learn from AFDC
			lOnlineFreq = realRos->leftLegFreqAFDC;

			// From learning step; Real world freq -> Internal freq
			// Freq_final = learningRate*Freq_tar + (1-learningRate)*Freq_feed
			// For example, 0.5 Hz real world is internalxFreqCPG = 0.5/100 = 0.005
			internalrFreqCPG = (1 - learningRateFreq)*rCurrentFreq + learningRateFreq*rOnlineFreq; // Same as = rCurrentFreq - learningRateFreq*(rCurrentFreq - rOnlineFreq)
			internallFreqCPG = (1 - learningRateFreq)*lCurrentFreq + learningRateFreq*lOnlineFreq;
			// - UPDATE CURRENT Freq. WITH ONLINE Freq. ---------------
			break;
	
		case 2:
			// Manually input freq e.g., 0.5 Hz
			cout << endl;
			for (int i = 1; i <= 1; i++) {
				cout << "ENTER CASE 2: MAN INPUT FREQ" << endl;
			}
			freqManual = realRos->freqManual; // Hz real world
			internalrFreqCPG = freqManual/cpgFreqScale; // e.g., 0.5/100 = 0.005
			internallFreqCPG = freqManual/cpgFreqScale;
			break;

		case 3:
			// Manually inc/dec freq, w = inc +0.01 Hz real world, s = dec -0.01 Hz real world
			cout << endl;
			for (int i = 1; i <= 1; i++) {
				cout << "ENTER CASE 3: MAN INC/DEC FREQ" << endl;
			}
			
			internalrFreqCPG = realRos->rightLegFreqSO2Action + realRos->upDownFreqVal; // e.g., +/- 0.0001
			internallFreqCPG = realRos->leftLegFreqSO2Action + realRos->upDownFreqVal;
			
			// Force to Case 0 to maintain current freq
			// So, it is needed to press again to inc/dec freq
			realRos->onlineVsManualFreq = 0;

			break;

		default:
			// onlineVsManualFreq == -1
			cout << endl;
			for (int i = 1; i <= 1; i++) {
				cout << "ENTER DEFAULT CASE: CONSTANT LEARNED FREQ" << endl;
			}
			// + Freezed CPG freq. from learning step ----------------
			internalrFreqCPG = meanrCPGFreqRW/cpgFreqScale; // From learning step; Real world freq -> Internal freq
			internallFreqCPG = meanlCPGFreqRW/cpgFreqScale;
			// - Freezed CPG freq. from learning step ----------------
			break;
	
	}
	// - ONLINE vs MANUAL FREQ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	for (int i = 1; i <= 1; i++) {
		cout << endl;
		cout << "CURRENT FREQ from SO2" << endl;
		cout << "rCurrentFreq: " << rCurrentFreq << " " << "lCurrentFreq: " << lCurrentFreq << " " << "Hz internal" << endl;
		cout << endl;
		cout << "ONLINE FREQ from AFDC" << endl;
		cout << "rOnlineFreq: " << rOnlineFreq << " " << "lOnlineFreq: " << lOnlineFreq << " " << "Hz internal" << endl;
		cout << endl;
		cout << "INTERNAL FREQ from SO2" << endl;
		cout << "internalrFreqCPG: " << internalrFreqCPG << " " << "internallFreqCPG: " << internallFreqCPG << " " << "Hz internal" << endl; 
		cout << endl;
	}

	
	// Update SO2 Action ==========================================================
	cout << "5.2.3) Update CPGs freq." << endl;
	flagUpdateSO2Action = realRos->sendCpgService("SO2", "updateAction", 0, 0, internalrFreqCPG, internallFreqCPG);
	cout << "flagUpdateSO2Action: " << flagUpdateSO2Action << endl;


	// ----------------------------------
	// 3. Check phase of right CPG0 to be at 0
	// ----------------------------------
	cout << "5.3) Check phase of right CPG0 to be at 0" << endl;
	if (!actionCPGGen) {
		cout << "... Check phase of Right CPG0 to start action ..." << endl;
		rCPG0_RealTime[1] = realRos->rightCpgO0SO2Action;
		actionCPGGen = checkStartOnline(rCPG0_RealTime[0], rCPG0_RealTime[1]);
		rCPG0_RealTime[0] = rCPG0_RealTime[1];

		if (actionCPGGen) {
			actionOnline = true;
		}
	} // End actionCPGGen
	cout << "actionCPGGen: " << actionCPGGen << endl;
	realRos->sendFlagStrSO2ActionPhaseZero(actionCPGGen);
	
	
	if (actionOnline) {
		// ----------------------------------
		// 4. Create kernel online
		// ----------------------------------
		// Kernel online for 1 timestep
		cout << "5.4) Create kernel online" << endl;

		// Get CPG data
		double rCPG0Online = realRos->rightCpgO0SO2Action;
		double rCPG1Online = realRos->rightCpgO1SO2Action;
		double lCPG0Online = realRos->leftCpgO0SO2Action;
		double lCPG1Online = realRos->leftCpgO1SO2Action;

		// Right ----------
		cout << "... Creating rKERNEL_ONLINE MATRIX ..." << endl;
		for (int i = 0; i < kernelSize; i++) {
			rKernelOnline[i] = exp( -(pow((rCPG0Online - rCPG0SO2Learn_ACycle_Sam[i]), 2) + pow((rCPG1Online - rCPG1SO2Learn_ACycle_Sam[i]), 2))/s );
		}

		// Left ----------
		cout << "... Creating lKERNEL_ONLINE MATRIX ..." << endl;
		for (int i = 0; i < kernelSize; i++) {
			// Minus sign is for phase inversion
			lKernelOnline[i] = exp( -(pow((lCPG0Online - lCPG0SO2Learn_ACycle_Sam[i]), 2) + pow((lCPG1Online - lCPG1SO2Learn_ACycle_Sam[i]), 2))/s );
		}


		// // + For debuging --------------------
		// cout << "rCPG0Online: " << rCPG0Online << endl;
		// cout << "rCPG1Online: " << rCPG1Online << endl;
		// cout << "lCPG0Online: " << lCPG0Online << endl;
		// cout << "lCPG1Online: " << lCPG1Online << endl;

		// cout << "rHipWeightOnline: [";
		// for(int i = 0; i < rHipWeightOnline.size(); i++){
		// 	cout << rHipWeightOnline[i] << " ";
		// }
		// cout << endl;

		// cout << "rCPG0SO2Learn_ACycle_Sam: [";
		// for(int i = 0; i < rCPG0SO2Learn_ACycle_Sam.size(); i++){
		// 	cout << rCPG0SO2Learn_ACycle_Sam[i] << " ";
		// }
		// cout << endl;

		// cout << "rKernelOnline: [";
		// for(int i = 0; i < rKernelOnline.size(); i++){
		// 	cout << rKernelOnline[i] << " ";
		// }
		// cout << endl;

		// while(cin.get() != '\n'){
		// 	;
		// }
		// // - For debuging --------------------


		// ----------------------------------
		// 5. Generate Pattern online
		// ----------------------------------
		cout << "5.5) Generate Pattern online" << endl;

		// Right ---------------------------------------------
		// Hip
		rHipOnline = int(inner_product(begin(rHipWeightOnline), end(rHipWeightOnline), begin(rKernelOnline), 0.0));
		// Knee
		rKneeOnline = int(inner_product(begin(rKneeWeightOnline), end(rKneeWeightOnline), begin(rKernelOnline), 0.0));
		// Ankle
		rAnkleOnline = int(inner_product(begin(rAnkleWeightOnline), end(rAnkleWeightOnline), begin(rKernelOnline), 0.0));

		// Left ---------------------------------------------
		// Hip
		lHipOnline = int(inner_product(begin(lHipWeightOnline), end(lHipWeightOnline), begin(lKernelOnline), 0.0));
		// Knee
		lKneeOnline = int(inner_product(begin(lKneeWeightOnline), end(lKneeWeightOnline), begin(lKernelOnline), 0.0));
		// Ankle
		lAnkleOnline = int(inner_product(begin(lAnkleWeightOnline), end(lAnkleWeightOnline), begin(lKernelOnline), 0.0));
		

		if (realRos->switchFlag) { // Switch pattern between R and L
			adaptiveNormalOnline = {lHipOnline, lKneeOnline, lAnkleOnline, rHipOnline, rKneeOnline, rAnkleOnline};
		} else { // Default
			adaptiveNormalOnline = {rHipOnline, rKneeOnline, rAnkleOnline, lHipOnline, lKneeOnline, lAnkleOnline};
		}
		



		// ----------------------------------
		// 6. Control
		// ----------------------------------
		cout << "5.6) Control online" << endl;

		// Type of control: Position control
		cout << "Pattern adaptation: Position control" << endl;
		realRos->sendTypeCon(1);

		// Set speed
		// We can use the speed value from command or overwrite here
		speed = speedFound;
		realRos->sendSpeedCon(speed);

		// For safety reason,
		// Speed 7 will limit %Assist up to 80%
		// Speed 8 will limit %Assist up to 60%
		// Speed 9 will limit %Assist up to 50%
		// Speed 10 will limit %Assist up to 40%
		if (speed == 7) {
			if (assist > 80) {
				assist = 80;
			}

		} else if (speed == 8) {
			if (assist > 60) {
				assist = 60;
			}
		} else if (speed == 9) {
			if (assist > 50) {
				assist = 50;
			}
		} else if (speed == 10) {
			if (assist > 40) {
				assist = 40;
			}
		}

		// Set percentage of assistance
		realRos->sendAssistCon(assist);



		// Send joint angle vector of all positions of both legs
		realRos->sendJointAngleCon(adaptiveNormalOnline);

//			switch (speed) {
//				case 1 : {
//					if (nodeCounter%90 == 0) {
//						realRos->sendJointAngleCon(adaptiveNormalOnline);
//					}
//					break;
//				}
//				case 2 : {
//					if (nodeCounter%84 == 0) {
//						realRos->sendJointAngleCon(adaptiveNormalOnline);
//					}
//					break;
//				}
//				case 3 : {
//					if (nodeCounter%78 == 0) {
//						realRos->sendJointAngleCon(adaptiveNormalOnline);
//					}
//					break;
//				}
//				case 4 : {
//					if (nodeCounter%72 == 0) {
//						realRos->sendJointAngleCon(adaptiveNormalOnline);
//					}
//					break;
//				}
//				case 5 : {
//					if (nodeCounter%66 == 0) {
//						realRos->sendJointAngleCon(adaptiveNormalOnline);
//					}
//					break;
//				}
//				case 6 : {
//					if (nodeCounter%60 == 0) {
//						realRos->sendJointAngleCon(adaptiveNormalOnline);
//					}
//					break;
//				}
//				case 7 : {
//					if (nodeCounter%54 == 0) {
//						realRos->sendJointAngleCon(adaptiveNormalOnline);
//					}
//					break;
//				}
//				case 8 : {
//					if (nodeCounter%48 == 0) {
//						realRos->sendJointAngleCon(adaptiveNormalOnline);
//					}
//					break;
//				}
//				case 9 : {
//					if (nodeCounter%42 == 0) {
//						realRos->sendJointAngleCon(adaptiveNormalOnline);
//					}
//					break;
//				}
//				case 10 : {
//					if (nodeCounter%36 == 0) {
//						realRos->sendJointAngleCon(adaptiveNormalOnline);
//					}
//					break;
//				}
//
//			} // End Switch speed

		speedOld = speed;



	} // End actionOnline
	cout << "actionOnline: " << actionOnline << endl;




}

// This is to calculate RBF for discrete signal.
bool ExvisParallelRealAssem::rbfDist(ExvisParallelRealROS * realRos, int speed, int assist){
	
	cout << "***************************************" << endl;
	cout << "********* RBF Discrete **********" << endl;
	cout << "***************************************" << endl;


	// *********************************************
	// * Load and check conditions
	// *********************************************
	
	if (!rd_learnLoad) {

		cout << "............................................" << endl;
		cout << "............. Check Condition .............." << endl;
		cout << "............................................" << endl;


		// -----------------------------
		// 1. Retrieve data
		// -----------------------------
		cout << "1. Retrieve data" << endl;

		// 1.1) Read file
		cout << "1.1) Read exvisJSONZeroTorque.json" << endl;
		jsonReadZeroTorqueFn(); // exvisJSONZeroTorque.json = Record during Free mode


		// ------------------------------------------
		// 2. Extract jointAngle for a cycle
		// ------------------------------------------
		cout << "2. Extract jointAngle for a cycle" << endl;

		rd_endConverge = timeStampCAN_ZT.size(); // The size of record
		cout << "rd_endConverge = " << rd_endConverge << endl;


		// + Check if there is patternDistResult.json -----------------------------------
		// If YES, we will not do the learing, but using the pattern from recorded .json
		if (!rd_checkJsonWritePatDist_1) {

			rd_existPatternDistResult_1 = exists_File(jsonWritePatDist);

			if (rd_existPatternDistResult_1) {

				jsonReadPatDistFn(); // To read freq, weight

				rd_meanrCPGTimeRW = rd_meanrCPGTimeRWOnline; // Read from .json
				rd_meanlCPGTimeRW = rd_meanlCPGTimeRWOnline; // Read from .json
				rd_meanrCPGFreqRW = 1/rd_meanrCPGTimeRW; // Read from .json
				rd_meanlCPGFreqRW = 1/rd_meanlCPGTimeRW; // Read from .json
				cout << "FROM JSON " << endl;
			}
			rd_checkJsonWritePatDist_1 = true;
		}

		if (!rd_existPatternDistResult_1) {


			// 2.1) Find average walking freq. from signal's length ---------
			cout << "2.1) Find average walking freq. from signal's length" << endl;
			// Right --------------
			rd_meanrCPGTimeRW =  rd_endConverge*realRos->timeStep;// sec
			// rd_meanrCPGFreqRW = 1/rd_meanrCPGTimeRW; // Hz
			

			// Left --------------
			rd_meanlCPGTimeRW =  rd_endConverge*realRos->timeStep;// sec
			// rd_meanlCPGFreqRW = 1/rd_meanlCPGTimeRW; // Hz

			// Testing freq. in Hz -----
			// There is a problem that at the graph with e.g., 26 s giving low freq, as 0.04 Hz.
			// The current SO2 cannot generate good signals at this low freq. for a cycle.
			// If we use any freq., so it create multiple cpg cycles for a target signal.

			rd_meanrCPGFreqRW = 1; // Hz
			rd_meanlCPGFreqRW = 1; // Hz

		}

		// + Convert time to Exo speed --------------------------------------
		// Average time value from both leg
		rd_avgTimeBothLegs = round(((rd_meanrCPGTimeRW + rd_meanlCPGTimeRW)/2)*10)/10; // round to one decimal

		// Right and Left
		if (rd_avgTimeBothLegs >= 4.5) {
			rd_speedFound = 1;
		}

		if ((rd_avgTimeBothLegs >= 4.2) && (rd_avgTimeBothLegs < 4.5)) {
			rd_speedFound = 2;
		}

		if ((rd_avgTimeBothLegs >= 3.9) && (rd_avgTimeBothLegs < 4.2)) {
			rd_speedFound = 3;
		}

		if ((rd_avgTimeBothLegs >= 3.6) && (rd_avgTimeBothLegs < 3.9)) {
			rd_speedFound = 4;
		}

		if ((rd_avgTimeBothLegs >= 3.3) && (rd_avgTimeBothLegs < 3.6)) {
			rd_speedFound = 5;
		}

		if ((rd_avgTimeBothLegs >= 3.0) && (rd_avgTimeBothLegs < 3.3)) {
			rd_speedFound = 6;
		}

		if ((rd_avgTimeBothLegs >= 2.7) && (rd_avgTimeBothLegs < 3.0)) {
			rd_speedFound = 7;
		}

		if ((rd_avgTimeBothLegs >= 2.4) && (rd_avgTimeBothLegs < 2.7)) {
			rd_speedFound = 8;
		}

		if ((rd_avgTimeBothLegs >= 2.1) && (rd_avgTimeBothLegs < 2.4)) {
			rd_speedFound = 9;
		}

		if ((rd_avgTimeBothLegs >= 1.8) && (rd_avgTimeBothLegs < 2.1)) {
			rd_speedFound = 10;
		}
		// - Convert time to Exo speed --------------------------------------


		if (!rd_existPatternDistResult_1) {
			// + For patternDistResult.json ---------------
			jWPD_rMeanCPGTimeRW_out.append(rd_meanrCPGTimeRW);
			jWPD_lMeanCPGTimeRW_out.append(rd_meanlCPGTimeRW);
			jWPD_speedFound_Out.append(rd_speedFound);
			// - For patternResult.json ---------------
		}

		cout << "rd_meanrCPGTimeRW: " << rd_meanrCPGTimeRW << " " << "rd_meanlCPGTimeRW: " << rd_meanlCPGTimeRW << endl;
		cout << "rd_meanrCPGFreqRW: " << rd_meanrCPGFreqRW << " " << "rd_meanlCPGFreqRW: " << rd_meanlCPGFreqRW << endl;
		cout << "rd_speed: " << rd_speedFound << endl;

		rd_learnLoad = true;
	} // End rd_learnLoad
	cout << "rd_learnLoad: " << rd_learnLoad << endl;
	cout << "rd_meanrCPGTimeRW: " << rd_meanrCPGTimeRW << " " << "rd_meanlCPGTimeRW: " << rd_meanlCPGTimeRW << endl;
	cout << "rd_meanrCPGFreqRW: " << rd_meanrCPGFreqRW << " " << "rd_meanlCPGFreqRW: " << rd_meanlCPGFreqRW << endl;
	cout << "rd_speed: " << rd_speedFound << endl;


	// If patternDistResult.json has already existed, it does not need to do learning part.
	if (!rd_existPatternDistResult_1) {
		// *********************************************
		// * Learning
		// *********************************************
		// --------------------------------------------------------------------------------------------------
		// + This part around loop to go outside rbfDist() to complete prog. cycle
		// --------------------------------------------------------------------------------------------------
		cout << "***************************************" << endl;
		cout << "*************** Learning **************" << endl;
		cout << "***************************************" << endl;

		if (!(rd_learnrCPGCollect && rd_learnlCPGCollect)) {

			// 2.2) Generate CPG0 with freq. from user and will be used as source of learning RBF -------
			// Initialize SO2 Learn ==========================================================
			cout << "2.2) Generate CPG0 with freq. from user" << endl;
			rd_initFreqRSO2Learn = rd_meanrCPGFreqRW/rd_cpgFreqScale;
			rd_initFreqLSO2Learn = rd_meanrCPGFreqRW/rd_cpgFreqScale; // use from right
			if (!rd_flagStrSO2Learn) {
				rd_flagStrSO2Learn = realRos->sendCpgService("SO2", "initLearn", rd_initFreqRSO2Learn, rd_initFreqLSO2Learn, 0, 0);
				cout << "rd_flagStrSO2Learn: " << rd_flagStrSO2Learn << endl;

				// + For patternDistResult.json ---
				WPD_timeSO2LearnGen = realRos->timeStampCAN; // Use timeStampCAN the same as record node

				jWPD_timeSO2LearnGen_Out.append(WPD_timeSO2LearnGen);
				// - For patternDistResult.json ---

			}


			// + Record both CPGs when they start to check how they behave ---
			// + For patternDistResult.json ---
			// Right
			WPD_rCPG0SO2Learn.push_back(realRos->rightCpgO0SO2Learn);
			WPD_rCPG1SO2Learn.push_back(realRos->rightCpgO1SO2Learn);

			jWPD_rCPG0SO2Learn_Out.append(WPD_rCPG0SO2Learn.back());
			jWPD_rCPG1SO2Learn_Out.append(WPD_rCPG1SO2Learn.back());

			WPD_rCPGFreqSO2Learn = realRos->rightLegFreqSO2Learn;
			jWPD_rCPGFreqSO2Learn_Out.append(WPD_rCPGFreqSO2Learn);

			// Left
			WPD_lCPG0SO2Learn.push_back(realRos->leftCpgO0SO2Learn);
			WPD_lCPG1SO2Learn.push_back(realRos->leftCpgO1SO2Learn);

			jWPD_lCPG0SO2Learn_Out.append(WPD_lCPG0SO2Learn.back());
			jWPD_lCPG1SO2Learn_Out.append(WPD_lCPG1SO2Learn.back());

			WPD_lCPGFreqSO2Learn = realRos->leftLegFreqSO2Learn;
			jWPD_lCPGFreqSO2Learn_Out.append(WPD_lCPGFreqSO2Learn);

			// - For patternDistResult.json ---
			// - Record both CPGs when they start to check how they behave ---
		}
		cout << "rd_learnrCPGCollect: " << rd_learnrCPGCollect << " " << "rd_learnlCPGCollect: " << rd_learnlCPGCollect << endl;

		cout << "rd_cpgCollectWay: " << rd_cpgCollectWay << endl;		
		if (rd_cpgCollectWay == 1) {
			// Right -----
			// 2.3.1) Check phase of right CPG0 to be at 0 -----------
			cout << "2.3.1) Check phase of right CPG0 to be at 0" << endl;
			if (!rd_learnrCPGGen) {
				cout << "... Check phase of Right CPG0 to start collecting ..." << endl;
				// rd_rCPG0_LearnTime[1] = cpgAdaptiveLearn->getRightCpgO0_SO2();
				rd_rCPG0_LearnTime[1] = realRos->rightCpgO0SO2Learn;
				rd_learnrCPGGen = checkStartOnline(rd_rCPG0_LearnTime[0], rd_rCPG0_LearnTime[1]);
				rd_rCPG0_LearnTime[0] = rd_rCPG0_LearnTime[1];

				if (rd_learnrCPGGen) {
					cout << "Right CPG0 is phase 0" << endl;

					WPD_rCPG0SO2Learn_ACycle.clear();
					WPD_rCPG1SO2Learn_ACycle.clear();

					// + For patternResult.json ---
					WPD_rTimeSO2LearnPhaseZero = realRos->timeStampCAN;

					jWPD_rTimeSO2LearnPhaseZero_Out.append(WPD_rTimeSO2LearnPhaseZero);
					// - For patternResult.json ---
				}
			} // End learnrCPGGen
			cout << "rd_learnrCPGGen: " << rd_learnrCPGGen << endl;

			// 2.3.2) Collect right CPG0 data for 1 cycle counted from prog. step ---------
			cout << "2.3.2) Collect right CPG0 data for 1 cycle" << endl;
			if (rd_learnrCPGGen && !rd_learnrCPGEnd) {
				// + Check the end of a cycle ---
				rd_rCPG0_RealTime[1] = realRos->rightCpgO0SO2Learn;
				rd_learnrCPGEnd = checkStartOnline(rd_rCPG0_RealTime[0], rd_rCPG0_RealTime[1]);
				rd_rCPG0_RealTime[0] = rd_rCPG0_RealTime[1];
				// - Check the end of a cycle ---

				if (!rd_learnrCPGEnd) {
					// + For patternDistResult.json ---
					WPD_rCPG0SO2Learn_ACycle.push_back(realRos->rightCpgO0SO2Learn);
					WPD_rCPG1SO2Learn_ACycle.push_back(realRos->rightCpgO1SO2Learn);

					jWPD_rCPG0SO2Learn_ACycle_Out.append(WPD_rCPG0SO2Learn_ACycle.back());
					jWPD_rCPG1SO2Learn_ACycle_Out.append(WPD_rCPG1SO2Learn_ACycle.back());
					// - For patternDistResult.json ---
				}

				if (rd_learnrCPGEnd) {
					cout << "End collect right CPG0 data" << endl;
					cout << "WPD_rCPG0SO2Learn_ACycle size: " << WPD_rCPG0SO2Learn_ACycle.size() << " " << "WPD_rCPG1SO2Learn_ACycle size: " << WPD_rCPG0SO2Learn_ACycle.size() << endl;

					// + For patternDistResult.json ---
					WPD_rCountCycle = WPD_rCPG0SO2Learn_ACycle.size(); // One cycle in time step
					WPD_rCPG0SO2Learn_CyclesFound = 1;

					jWPD_rCountCycle_Out.append(WPD_rCountCycle);
					jWPD_rCPG0SO2Learn_CyclesFound_Out.append(WPD_rCPG0SO2Learn_CyclesFound);
					// - For patternDistResult.json ---

					rd_learnrCPGCollect = true;
				}
			} // End collect

			// Left -----
			// 2.3.3) Check phase of left CPG0 to be at 180 -----------
			cout << "2.3.3) Check phase of left CPG0 to be at 180" << endl;
			if (!rd_learnlCPGGen) {
				cout << "... Check phase of Left CPG0 to start collecting ..." << endl;
				rd_lCPG0_LearnTime[1] = realRos->leftCpgO0SO2Learn;
				rd_learnlCPGGen = checkStartOnline180(rd_lCPG0_LearnTime[0], rd_lCPG0_LearnTime[1]);
				rd_lCPG0_LearnTime[0] = rd_lCPG0_LearnTime[1];

				if (rd_learnlCPGGen) {
					cout << "Left CPG0 is phase 180" << endl;

					WPD_lCPG0SO2Learn_ACycle.clear();
					WPD_lCPG1SO2Learn_ACycle.clear();

					// + For patternDistResult.json ---
					WPD_lTimeSO2LearnPhase180 = realRos->timeStampCAN;

					jWPD_lTimeSO2LearnPhase180_Out.append(WPD_lTimeSO2LearnPhase180);
					// - For patternDistResult.json ---
				}
			} // End rd_learnlCPGGen

			// 2.3.4) Collect CPG0 data for 1 cycle counted from prog. step ---------
			cout << "2.3.4) Collect left CPG0 data for 1 cycle" << endl;
			if (rd_learnlCPGGen && !rd_learnlCPGEnd) {
				// + Check the end of a cycle ---
				rd_lCPG0_LearnTime[1] = realRos->leftCpgO0SO2Learn;
				rd_learnlCPGEnd = checkStartOnline180(rd_lCPG0_LearnTime[0], rd_lCPG0_LearnTime[1]);
				rd_lCPG0_LearnTime[0] = rd_lCPG0_LearnTime[1];
				// - Check the end of a cycle ---

				if (!rd_learnlCPGEnd) {
					// + For patternDistResult.json ---
					WPD_lCPG0SO2Learn_ACycle.push_back(realRos->leftCpgO0SO2Learn);
					WPD_lCPG1SO2Learn_ACycle.push_back(realRos->leftCpgO1SO2Learn);

					jWPD_lCPG0SO2Learn_ACycle_Out.append(WPD_lCPG0SO2Learn_ACycle.back());
					jWPD_lCPG1SO2Learn_ACycle_Out.append(WPD_lCPG1SO2Learn_ACycle.back());
					// - For patternDistResult.json ---
				}

				if (rd_learnlCPGEnd) {
					cout << "End collect left CPG0 data" << endl;
					cout << "WPD_lCPG0SO2Learn_ACycle size: " << WPD_lCPG0SO2Learn_ACycle.size() << " " << "WPD_lCPG1SO2Learn_ACycle size: " << WPD_lCPG1SO2Learn_ACycle.size() << endl;

					// + For patternDistResult.json ---
					WPD_lCountCycle = WPD_lCPG0SO2Learn_ACycle.size();
					WPD_lCPG0SO2Learn_CyclesFound = 1;

					jWPD_lCountCycle_Out.append(WPD_lCountCycle);
					jWPD_lCPG0SO2Learn_CyclesFound_Out.append(WPD_lCPG0SO2Learn_CyclesFound);
					// - For patternDistResult.json ---

					rd_learnlCPGCollect = true;
				}
			} // End collect
		} 

		// ----------------
		if (rd_cpgCollectWay == 2) {
			// Right -----
			// 2.3.1) Check phase of right CPG0 to be at 0 -----------
			cout << "2.3.1) Check phase of right CPG0 to be at 0" << endl;
			if (!rd_learnrCPGGen) {
				cout << "... Check phase of Right CPG0 to start collecting ..." << endl;
				// rd_rCPG0_LearnTime[1] = cpgAdaptiveLearn->getRightCpgO0_SO2();
				rd_rCPG0_LearnTime[1] = realRos->rightCpgO0SO2Learn;
				rd_learnrCPGGen = checkStartOnline(rd_rCPG0_LearnTime[0], rd_rCPG0_LearnTime[1]);
				rd_rCPG0_LearnTime[0] = rd_rCPG0_LearnTime[1];

				if (rd_learnrCPGGen) {
					cout << "Right CPG0 is phase 0" << endl;

					WPD_rCPG0SO2Learn_ACycle.clear();
					WPD_rCPG1SO2Learn_ACycle.clear();

					// + For patternResult.json ---
					WPD_rTimeSO2LearnPhaseZero = realRos->timeStampCAN;

					jWPD_rTimeSO2LearnPhaseZero_Out.append(WPD_rTimeSO2LearnPhaseZero);
					// - For patternResult.json ---
				}
			} // End learnrCPGGen
			cout << "rd_learnrCPGGen: " << rd_learnrCPGGen << endl;

			// 2.3.2) Collect right CPG0 data for multiple cycles counted from prog. step to equal signal's length ---------
			cout << "2.3.2) Collect right CPG0 data for multiple cycles, equalting signal's length" << endl;
			if (rd_learnrCPGGen && !rd_learnrCPGEnd) {
				// + Check the end of signal's length ---
				if (rd_cntrCpgEnd == rd_endConverge){
					rd_learnrCPGEnd = true;
					rd_cntrCpgEnd = 0;
				} else {
					rd_cntrCpgEnd++;
				}
				// - Check the end of signal's length ---

				if (!rd_learnrCPGEnd) {
					// + For patternDistResult.json ---
					WPD_rCPG0SO2Learn_ACycle.push_back(realRos->rightCpgO0SO2Learn);
					WPD_rCPG1SO2Learn_ACycle.push_back(realRos->rightCpgO1SO2Learn);

					jWPD_rCPG0SO2Learn_ACycle_Out.append(WPD_rCPG0SO2Learn_ACycle.back());
					jWPD_rCPG1SO2Learn_ACycle_Out.append(WPD_rCPG1SO2Learn_ACycle.back());
					// - For patternDistResult.json ---
				}

				if (rd_learnrCPGEnd) {
					cout << "End collect right CPG0 data" << endl;
					cout << "WPD_rCPG0SO2Learn_ACycle size: " << WPD_rCPG0SO2Learn_ACycle.size() << " " << "WPD_rCPG1SO2Learn_ACycle size: " << WPD_rCPG0SO2Learn_ACycle.size() << endl;

					// + For patternDistResult.json ---
					WPD_rCountCycle = WPD_rCPG0SO2Learn_ACycle.size(); // One cycle in time step
					WPD_rCPG0SO2Learn_CyclesFound = 1;

					jWPD_rCountCycle_Out.append(WPD_rCountCycle);
					jWPD_rCPG0SO2Learn_CyclesFound_Out.append(WPD_rCPG0SO2Learn_CyclesFound);
					// - For patternDistResult.json ---

					rd_learnrCPGCollect = true;
				}
			} // End collect

			// Left -----
			// 2.3.3) Check phase of left CPG0 to be at 180 -----------
			cout << "2.3.3) Check phase of left CPG0 to be at 180" << endl;
			if (!rd_learnlCPGGen) {
				cout << "... Check phase of Left CPG0 to start collecting ..." << endl;
				rd_lCPG0_LearnTime[1] = realRos->leftCpgO0SO2Learn;
				rd_learnlCPGGen = checkStartOnline180(rd_lCPG0_LearnTime[0], rd_lCPG0_LearnTime[1]);
				rd_lCPG0_LearnTime[0] = rd_lCPG0_LearnTime[1];

				if (rd_learnlCPGGen) {
					cout << "Left CPG0 is phase 180" << endl;

					WPD_lCPG0SO2Learn_ACycle.clear();
					WPD_lCPG1SO2Learn_ACycle.clear();

					// + For patternDistResult.json ---
					WPD_lTimeSO2LearnPhase180 = realRos->timeStampCAN;

					jWPD_lTimeSO2LearnPhase180_Out.append(WPD_lTimeSO2LearnPhase180);
					// - For patternDistResult.json ---
				}
			} // End rd_learnlCPGGen

			// 2.3.4) Collect CPG0 data for multiple cycles counted from prog. step to equal signal's length ---------
			cout << "2.3.4) Collect left CPG0 data for multiple cycles, equalting signal's length" << endl;
			if (rd_learnlCPGGen && !rd_learnlCPGEnd) {
				// + Check the end of signal's length ---
				
				if (rd_cntlCpgEnd == rd_endConverge){
					rd_learnlCPGEnd = true;
					rd_cntlCpgEnd = 0;
				} else{
					rd_cntlCpgEnd++;
				}
				// - Check the end of signal's length ---

				if (!rd_learnlCPGEnd) {
					// + For patternDistResult.json ---
					WPD_lCPG0SO2Learn_ACycle.push_back(realRos->leftCpgO0SO2Learn);
					WPD_lCPG1SO2Learn_ACycle.push_back(realRos->leftCpgO1SO2Learn);

					jWPD_lCPG0SO2Learn_ACycle_Out.append(WPD_lCPG0SO2Learn_ACycle.back());
					jWPD_lCPG1SO2Learn_ACycle_Out.append(WPD_lCPG1SO2Learn_ACycle.back());
					// - For patternDistResult.json ---
				}

				if (rd_learnlCPGEnd) {
					cout << "End collect left CPG0 data" << endl;
					cout << "WPD_lCPG0SO2Learn_ACycle size: " << WPD_lCPG0SO2Learn_ACycle.size() << " " << "WPD_lCPG1SO2Learn_ACycle size: " << WPD_lCPG1SO2Learn_ACycle.size() << endl;

					// + For patternDistResult.json ---
					WPD_lCountCycle = WPD_lCPG0SO2Learn_ACycle.size();
					WPD_lCPG0SO2Learn_CyclesFound = 1;

					jWPD_lCountCycle_Out.append(WPD_lCountCycle);
					jWPD_lCPG0SO2Learn_CyclesFound_Out.append(WPD_lCPG0SO2Learn_CyclesFound);
					// - For patternDistResult.json ---

					rd_learnlCPGCollect = true;
				}
			} // End collect
		}

		// ----------------
		if (rd_cpgCollectWay == 3) {
			// Right -----
			// 2.3.1) Check phase of right CPG0 to be at 0 -----------
			cout << "2.3.1) No checking phase of right CPG0" << endl;
			if (!rd_learnrCPGGen) {
				cout << "... Right CPG0: NO checking phase, start collecting ..." << endl;
				
				rd_learnrCPGGen = true;

				if (rd_learnrCPGGen) {
					cout << "Right CPG0 starts from beginning " << endl;

					WPD_rCPG0SO2Learn_ACycle.clear();
					WPD_rCPG1SO2Learn_ACycle.clear();

					// + For patternResult.json ---
					WPD_rTimeSO2LearnPhaseZero = realRos->timeStampCAN;

					jWPD_rTimeSO2LearnPhaseZero_Out.append(WPD_rTimeSO2LearnPhaseZero);
					// - For patternResult.json ---
				}
			} // End learnrCPGGen
			cout << "rd_learnrCPGGen: " << rd_learnrCPGGen << endl;

			// 2.3.2) Collect right CPG0 data for multiple cycles counted from prog. step to equal signal's length ---------
			cout << "2.3.2) Collect right CPG0 data for multiple cycles, equalting signal's length" << endl;
			if (rd_learnrCPGGen && !rd_learnrCPGEnd) {
				// + Check the end of signal's length ---
				if (rd_cntrCpgEnd == rd_endConverge){
					rd_learnrCPGEnd = true;
					rd_cntrCpgEnd = 0;
				} else {
					rd_cntrCpgEnd++;
				}
				// - Check the end of signal's length ---

				if (!rd_learnrCPGEnd) {
					// + For patternDistResult.json ---
					WPD_rCPG0SO2Learn_ACycle.push_back(realRos->rightCpgO0SO2Learn);
					WPD_rCPG1SO2Learn_ACycle.push_back(realRos->rightCpgO1SO2Learn);

					jWPD_rCPG0SO2Learn_ACycle_Out.append(WPD_rCPG0SO2Learn_ACycle.back());
					jWPD_rCPG1SO2Learn_ACycle_Out.append(WPD_rCPG1SO2Learn_ACycle.back());
					// - For patternDistResult.json ---
				}

				if (rd_learnrCPGEnd) {
					cout << "End collect right CPG0 data" << endl;
					cout << "WPD_rCPG0SO2Learn_ACycle size: " << WPD_rCPG0SO2Learn_ACycle.size() << " " << "WPD_rCPG1SO2Learn_ACycle size: " << WPD_rCPG0SO2Learn_ACycle.size() << endl;

					// + For patternDistResult.json ---
					WPD_rCountCycle = WPD_rCPG0SO2Learn_ACycle.size(); // One cycle in time step
					WPD_rCPG0SO2Learn_CyclesFound = 1;

					jWPD_rCountCycle_Out.append(WPD_rCountCycle);
					jWPD_rCPG0SO2Learn_CyclesFound_Out.append(WPD_rCPG0SO2Learn_CyclesFound);
					// - For patternDistResult.json ---

					rd_learnrCPGCollect = true;
				}
			} // End collect

			// Left -----
			// 2.3.3) Check phase of left CPG0 to be at 180 -----------
			cout << "2.3.3) No checking phase of left CPG0" << endl;
			if (!rd_learnlCPGGen) {
				cout << "... Check phase of Left CPG0 to start collecting ..." << endl;
				
				rd_learnlCPGGen = true;
				
				if (rd_learnlCPGGen) {
					cout << "Left CPG0 starts from beginning" << endl;

					WPD_lCPG0SO2Learn_ACycle.clear();
					WPD_lCPG1SO2Learn_ACycle.clear();

					// + For patternDistResult.json ---
					WPD_lTimeSO2LearnPhase180 = realRos->timeStampCAN;

					jWPD_lTimeSO2LearnPhase180_Out.append(WPD_lTimeSO2LearnPhase180);
					// - For patternDistResult.json ---
				}
			} // End rd_learnlCPGGen

			// 2.3.4) Collect CPG0 data for multiple cycles counted from prog. step to equal signal's length ---------
			cout << "2.3.4) Collect left CPG0 data for multiple cycles, equalting signal's length" << endl;
			if (rd_learnlCPGGen && !rd_learnlCPGEnd) {
				// + Check the end of signal's length ---
				
				if (rd_cntlCpgEnd == rd_endConverge){
					rd_learnlCPGEnd = true;
					rd_cntlCpgEnd = 0;
				} else{
					rd_cntlCpgEnd++;
				}
				// - Check the end of signal's length ---

				if (!rd_learnlCPGEnd) {
					// + For patternDistResult.json ---
					WPD_lCPG0SO2Learn_ACycle.push_back(realRos->leftCpgO0SO2Learn);
					WPD_lCPG1SO2Learn_ACycle.push_back(realRos->leftCpgO1SO2Learn);

					jWPD_lCPG0SO2Learn_ACycle_Out.append(WPD_lCPG0SO2Learn_ACycle.back());
					jWPD_lCPG1SO2Learn_ACycle_Out.append(WPD_lCPG1SO2Learn_ACycle.back());
					// - For patternDistResult.json ---
				}

				if (rd_learnlCPGEnd) {
					cout << "End collect left CPG0 data" << endl;
					cout << "WPD_lCPG0SO2Learn_ACycle size: " << WPD_lCPG0SO2Learn_ACycle.size() << " " << "WPD_lCPG1SO2Learn_ACycle size: " << WPD_lCPG1SO2Learn_ACycle.size() << endl;

					// + For patternDistResult.json ---
					WPD_lCountCycle = WPD_lCPG0SO2Learn_ACycle.size();
					WPD_lCPG0SO2Learn_CyclesFound = 1;

					jWPD_lCountCycle_Out.append(WPD_lCountCycle);
					jWPD_lCPG0SO2Learn_CyclesFound_Out.append(WPD_lCPG0SO2Learn_CyclesFound);
					// - For patternDistResult.json ---

					rd_learnlCPGCollect = true;
				}
			} // End collect
		}
		// --------------------------------------------------------------------------------------------------
		// - This part around loop to go outside rbfDist() to complete prog. cycle.
		// -------------------------------------------------------------------------------------------------


		if (rd_learnrCPGCollect && rd_learnlCPGCollect && !rd_learnStart) {

			// 2.4) Do Low-pass filter to smooth the signal from step behavior -----------
			cout << "2.4) Low-pass filter" << endl;
			// Right --------------
			double rd_rHipJ_FB_ZT_LowP_t_1 = rHipJ_FB_ZT[0];
			double rd_rKneeJ_FB_ZT_LowP_t_1 = rKneeJ_FB_ZT[0];
			double rd_rAnkleJ_FB_ZT_LowP_t_1 = rAnkleJ_FB_ZT[0];
			// Left --------------
			double rd_lHipJ_FB_ZT_LowP_t_1 = lHipJ_FB_ZT[0];
			double rd_lKneeJ_FB_ZT_LowP_t_1 = lKneeJ_FB_ZT[0];
			double rd_lAnkleJ_FB_ZT_LowP_t_1 = lAnkleJ_FB_ZT[0];

			for (int i = 0; i < rd_endConverge; i++) {
				// + For patternDistResult.json ---
				// Right
				WPD_rHipJ_FB_ZT_LowP.push_back(0.9*rd_rHipJ_FB_ZT_LowP_t_1 + 0.1*rHipJ_FB_ZT[i]);
				rd_rHipJ_FB_ZT_LowP_t_1 = WPD_rHipJ_FB_ZT_LowP.back();
				WPD_rKneeJ_FB_ZT_LowP.push_back(0.9*rd_rKneeJ_FB_ZT_LowP_t_1 + 0.1*rKneeJ_FB_ZT[i]);
				rd_rKneeJ_FB_ZT_LowP_t_1 = WPD_rKneeJ_FB_ZT_LowP.back();
				WPD_rAnkleJ_FB_ZT_LowP.push_back(0.9*rd_rAnkleJ_FB_ZT_LowP_t_1 + 0.1*rAnkleJ_FB_ZT[i]);
				rd_rAnkleJ_FB_ZT_LowP_t_1 = WPD_rAnkleJ_FB_ZT_LowP.back();
				// Left
				WPD_lHipJ_FB_ZT_LowP.push_back(0.9*rd_lHipJ_FB_ZT_LowP_t_1 + 0.1*lHipJ_FB_ZT[i]);
				rd_lHipJ_FB_ZT_LowP_t_1 = WPD_lHipJ_FB_ZT_LowP.back();
				WPD_lKneeJ_FB_ZT_LowP.push_back(0.9*rd_lKneeJ_FB_ZT_LowP_t_1 + 0.1*lKneeJ_FB_ZT[i]);
				rd_lKneeJ_FB_ZT_LowP_t_1 = WPD_lKneeJ_FB_ZT_LowP.back();
				WPD_lAnkleJ_FB_ZT_LowP.push_back(0.9*rd_lAnkleJ_FB_ZT_LowP_t_1 + 0.1*lAnkleJ_FB_ZT[i]);
				rd_lAnkleJ_FB_ZT_LowP_t_1 = WPD_lAnkleJ_FB_ZT_LowP.back();

				jWPD_rHipJ_FB_ZT_LowP_Out.append(WPD_rHipJ_FB_ZT_LowP.back());
				jWPD_rKneeJ_FB_ZT_LowP_Out.append(WPD_rKneeJ_FB_ZT_LowP.back());
				jWPD_rAnkleJ_FB_ZT_LowP_Out.append(WPD_rAnkleJ_FB_ZT_LowP.back());
				jWPD_lHipJ_FB_ZT_LowP_Out.append(WPD_lHipJ_FB_ZT_LowP.back());
				jWPD_lKneeJ_FB_ZT_LowP_Out.append(WPD_lKneeJ_FB_ZT_LowP.back());
				jWPD_lAnkleJ_FB_ZT_LowP_Out.append(WPD_lAnkleJ_FB_ZT_LowP.back());
				// - For patternDistResult.json ---
			}


			// 2.5) Average graph ---------------------------------
			cout << "2.5) Average right/left hip, knee, and ankle" << endl;
			WPD_rHipJ_FB_ZT_LowP_Target.clear();
			WPD_rKneeJ_FB_ZT_LowP_Target.clear();
			WPD_rAnkleJ_FB_ZT_LowP_Target.clear();
			WPD_lHipJ_FB_ZT_LowP_Target.clear();
			WPD_lKneeJ_FB_ZT_LowP_Target.clear();
			WPD_lAnkleJ_FB_ZT_LowP_Target.clear();

			cout << "clear target vector" << endl;
			double rd_targetSize = WPD_rHipJ_FB_ZT_LowP.size();
			cout << "rd_targetSize: " << rd_targetSize << endl; 

			// Right ------------
			// + Hip signal ---
			for (int i = 0; i < rd_targetSize; i++) {
				// + For patternDistResult.json ---
				WPD_rHipJ_FB_ZT_LowP_Target.push_back(WPD_rHipJ_FB_ZT_LowP[i]);

				jWPD_rHipJ_FB_ZT_LowP_Target_Out.append(WPD_rHipJ_FB_ZT_LowP_Target.back());
				// - For patternDistResult.json ---
			}
			cout << endl;
			cout << "End avg right hip" << endl;
			// - Hip signal ---

			// + Knee signal ---
			for (int i = 0; i < rd_targetSize; i++) {
				// + For patternDistResult.json ---
				WPD_rKneeJ_FB_ZT_LowP_Target.push_back(WPD_rKneeJ_FB_ZT_LowP[i]);

				jWPD_rKneeJ_FB_ZT_LowP_Target_Out.append(WPD_rKneeJ_FB_ZT_LowP_Target.back());
				// - For patternDistResult.json ---
			}
			cout << "End avg right knee" << endl;
			// - Knee signal ---

			// + Ankle signal ---
			for (int i = 0; i < rd_targetSize; i++) {
				// + For patternDistResult.json ---
				WPD_rAnkleJ_FB_ZT_LowP_Target.push_back(WPD_rAnkleJ_FB_ZT_LowP[i]);

				jWPD_rAnkleJ_FB_ZT_LowP_Target_Out.append(WPD_rAnkleJ_FB_ZT_LowP_Target.back());
				// - For patternDistResult.json ---
			}
			cout << "End avg right ankle" << endl;
			// - Ankle signal ---


			// // Left ------------
			// + Hip signal ---
			for (int i = 0; i < rd_targetSize; i++) {
				// + For patternDistResult.json ---
				WPD_lHipJ_FB_ZT_LowP_Target.push_back(WPD_lHipJ_FB_ZT_LowP[i]);

				jWPD_lHipJ_FB_ZT_LowP_Target_Out.append(WPD_lHipJ_FB_ZT_LowP_Target.back());
				// - For patternDistResult.json ---
			}
			cout << endl;
			cout << "End avg left hip" << endl;
			// - Hip signal ---

			// + Knee signal ---
			for (int i = 0; i < rd_targetSize; i++) {
				// + For patternDistResult.json ---
				WPD_lKneeJ_FB_ZT_LowP_Target.push_back(WPD_lKneeJ_FB_ZT_LowP[i]);

				jWPD_lKneeJ_FB_ZT_LowP_Target_Out.append(WPD_lKneeJ_FB_ZT_LowP_Target.back());
				// - For patternDistResult.json ---
			}
			cout << "End avg left knee" << endl;
			// - Knee signal ---

			// + Ankle signal ---
			for (int i = 0; i < rd_targetSize; i++) {
				// + For patternDistResult.json ---
				WPD_lAnkleJ_FB_ZT_LowP_Target.push_back(WPD_lAnkleJ_FB_ZT_LowP[i]);

				jWPD_lAnkleJ_FB_ZT_LowP_Target_Out.append(WPD_lAnkleJ_FB_ZT_LowP_Target.back());
				// - For patternDistResult.json ---
			}
			cout << "End avg left ankle" << endl;
			// - Ankle signal ---


			// ----------------------------------
			// 3.1) Do the sampling of CPG0 & CPG1
			// Interpolation of CPGs to cover signal's length
			// From sampling points -> linear interpolation between points
			// ----------------------------------
			cout << "3.2) Do the sampling of CPG0 & CPG1, and interpolate " << endl;
			rd_samplingStep_CPG = WPD_rCountCycle/WPD_kernelSize;
			cout << "WPD_rCountCycle: " << WPD_rCountCycle << "/" << "WPD_kernelSize: " << WPD_kernelSize << endl; 
			rd_samplingStep = rd_endConverge/WPD_kernelSize;
			cout << "rd_endConverge: " << rd_endConverge << "/" << "WPD_kernelSize: " << WPD_kernelSize << endl;
			cout << "= rd_samplingStep: " << rd_samplingStep << endl;
			
			for (int rd_nCPG = 0; rd_nCPG < WPD_rCountCycle; rd_nCPG++) {
				if ((rd_nCPG + 1)%rd_samplingStep_CPG == 0){
					// Right ---------
					WPD_rCPG0SO2Learn_ACycle_Sam[rd_nSamCPG] = WPD_rCPG0SO2Learn_ACycle[rd_nCPG];
					WPD_rCPG1SO2Learn_ACycle_Sam[rd_nSamCPG] = WPD_rCPG1SO2Learn_ACycle[rd_nCPG];
					// Left ----------
					WPD_lCPG0SO2Learn_ACycle_Sam[rd_nSamCPG] = WPD_lCPG0SO2Learn_ACycle[rd_nCPG];
					WPD_lCPG1SO2Learn_ACycle_Sam[rd_nSamCPG] = WPD_lCPG1SO2Learn_ACycle[rd_nCPG];

					// + For patternDistResult.json ---
					// Right ---------
					jWPD_rCPG0SO2Learn_ACycle_Sam_Out.append(WPD_rCPG0SO2Learn_ACycle[rd_nCPG]);
					jWPD_rCPG1SO2Learn_ACycle_Sam_Out.append(WPD_rCPG1SO2Learn_ACycle[rd_nCPG]);
					// Left ----------
					jWPD_lCPG0SO2Learn_ACycle_Sam_Out.append(WPD_lCPG0SO2Learn_ACycle[rd_nCPG]);
					jWPD_lCPG1SO2Learn_ACycle_Sam_Out.append(WPD_lCPG1SO2Learn_ACycle[rd_nCPG]);
					// - For patternDistResult.json ---

					rd_nSamCPG++;
				}	
			}
			cout << "rd_nSamCPG: " << rd_nSamCPG << endl;

			// After interpolation, the length of CPG will be the same as target signal -----	
			int i_Inter = 0;
			int dxRun = 1;
			for (int j = 0; j < rd_endConverge; j++) {
				if (j == 0) {
					WPD_rCPG0SO2Learn_ACycle_Inter.push_back(WPD_rCPG0SO2Learn_ACycle[0]);
					WPD_rCPG1SO2Learn_ACycle_Inter.push_back(WPD_rCPG1SO2Learn_ACycle[0]);
					WPD_lCPG0SO2Learn_ACycle_Inter.push_back(WPD_lCPG0SO2Learn_ACycle[0]);
					WPD_lCPG1SO2Learn_ACycle_Inter.push_back(WPD_lCPG1SO2Learn_ACycle[0]);
				} else if (j > rd_samplingStep*(i_Inter+1)) {
					i_Inter++;
					dxRun = 1;
				}

				if (i_Inter == 0) {
					// Right ---
					WPD_rCPG0SO2Learn_ACycle_Inter.push_back(((WPD_rCPG0SO2Learn_ACycle_Sam[i_Inter] - WPD_rCPG0SO2Learn_ACycle[0])/rd_samplingStep)*dxRun + WPD_rCPG0SO2Learn_ACycle[0]);
					WPD_rCPG1SO2Learn_ACycle_Inter.push_back(((WPD_rCPG1SO2Learn_ACycle_Sam[i_Inter] - WPD_rCPG1SO2Learn_ACycle[0])/rd_samplingStep)*dxRun + WPD_rCPG1SO2Learn_ACycle[0]);
					// Left ---
					WPD_lCPG0SO2Learn_ACycle_Inter.push_back(((WPD_lCPG0SO2Learn_ACycle_Sam[i_Inter] - WPD_lCPG0SO2Learn_ACycle[0])/rd_samplingStep)*dxRun + WPD_lCPG0SO2Learn_ACycle[0]);
					WPD_lCPG1SO2Learn_ACycle_Inter.push_back(((WPD_lCPG1SO2Learn_ACycle_Sam[i_Inter] - WPD_lCPG1SO2Learn_ACycle[0])/rd_samplingStep)*dxRun + WPD_lCPG1SO2Learn_ACycle[0]);
				} else if (i_Inter == WPD_kernelSize) {
					// Right ---
					WPD_rCPG0SO2Learn_ACycle_Inter.push_back(((WPD_rCPG0SO2Learn_ACycle.back() - WPD_rCPG0SO2Learn_ACycle_Sam[i_Inter-1])/((rd_endConverge-1) - rd_samplingStep*(i_Inter-1)))*dxRun + WPD_rCPG0SO2Learn_ACycle_Sam[i_Inter-1]);
					WPD_rCPG1SO2Learn_ACycle_Inter.push_back(((WPD_rCPG1SO2Learn_ACycle.back() - WPD_rCPG1SO2Learn_ACycle_Sam[i_Inter-1])/((rd_endConverge-1) - rd_samplingStep*(i_Inter-1)))*dxRun + WPD_rCPG1SO2Learn_ACycle_Sam[i_Inter-1]);
					// Left ---
					WPD_lCPG0SO2Learn_ACycle_Inter.push_back(((WPD_lCPG0SO2Learn_ACycle.back() - WPD_lCPG0SO2Learn_ACycle_Sam[i_Inter-1])/((rd_endConverge-1) - rd_samplingStep*(i_Inter-1)))*dxRun + WPD_lCPG0SO2Learn_ACycle_Sam[i_Inter-1]);
					WPD_lCPG1SO2Learn_ACycle_Inter.push_back(((WPD_lCPG1SO2Learn_ACycle.back() - WPD_lCPG1SO2Learn_ACycle_Sam[i_Inter-1])/((rd_endConverge-1) - rd_samplingStep*(i_Inter-1)))*dxRun + WPD_lCPG1SO2Learn_ACycle_Sam[i_Inter-1]);
				} else {
					// Right ---
					WPD_rCPG0SO2Learn_ACycle_Inter.push_back(((WPD_rCPG0SO2Learn_ACycle_Sam[i_Inter] - WPD_rCPG0SO2Learn_ACycle_Sam[i_Inter-1])/rd_samplingStep)*dxRun + WPD_rCPG0SO2Learn_ACycle_Sam[i_Inter-1]);
					WPD_rCPG1SO2Learn_ACycle_Inter.push_back(((WPD_rCPG1SO2Learn_ACycle_Sam[i_Inter] - WPD_rCPG1SO2Learn_ACycle_Sam[i_Inter-1])/rd_samplingStep)*dxRun + WPD_rCPG1SO2Learn_ACycle_Sam[i_Inter-1]);
					// Left ---
					WPD_lCPG0SO2Learn_ACycle_Inter.push_back(((WPD_lCPG0SO2Learn_ACycle_Sam[i_Inter] - WPD_lCPG0SO2Learn_ACycle_Sam[i_Inter-1])/rd_samplingStep)*dxRun + WPD_lCPG0SO2Learn_ACycle_Sam[i_Inter-1]);
					WPD_lCPG1SO2Learn_ACycle_Inter.push_back(((WPD_lCPG1SO2Learn_ACycle_Sam[i_Inter] - WPD_lCPG1SO2Learn_ACycle_Sam[i_Inter-1])/rd_samplingStep)*dxRun + WPD_lCPG1SO2Learn_ACycle_Sam[i_Inter-1]);
				}

				// + For patternDistResult.json ---
				// Right ---------
				jWPD_rCPG0SO2Learn_ACycle_Inter_Out.append(WPD_rCPG0SO2Learn_ACycle_Inter.back());
				jWPD_rCPG1SO2Learn_ACycle_Inter_Out.append(WPD_rCPG1SO2Learn_ACycle_Inter.back());
				// Left ----------
				jWPD_lCPG0SO2Learn_ACycle_Inter_Out.append(WPD_lCPG0SO2Learn_ACycle_Inter.back());
				jWPD_lCPG1SO2Learn_ACycle_Inter_Out.append(WPD_lCPG1SO2Learn_ACycle_Inter.back());
				// - For patternDistResult.json ---

				dxRun++;
			}
			

			// ----------------------------------
			// 3.2) sampling target signals
			// ----------------------------------
			// Sampling every "samplingStep" step ---------
			// We do this sampling data once and will use it during online as well.
			cout << "3.2) Do the sampling of target signals " << endl;
			cout << "rd_endConverge: " << rd_endConverge << "/" << "WPD_kernelSize: " << WPD_kernelSize << endl;
			cout << "= rd_samplingStep: " << rd_samplingStep << endl;

			for (int rd_nTar = 0; rd_nTar < rd_endConverge; rd_nTar++) {
				if ((rd_nTar + 1)%rd_samplingStep == 0){

					// Store sampling data ----------
					// Right ---------
					WPD_rHipJ_FB_ZT_LowP_Target_Sam[rd_nSamTar] = WPD_rHipJ_FB_ZT_LowP_Target[rd_nTar];
					WPD_rKneeJ_FB_ZT_LowP_Target_Sam[rd_nSamTar] = WPD_rKneeJ_FB_ZT_LowP_Target[rd_nTar];
					WPD_rAnkleJ_FB_ZT_LowP_Target_Sam[rd_nSamTar] = WPD_rAnkleJ_FB_ZT_LowP_Target[rd_nTar];

					// Left ----------
					WPD_lHipJ_FB_ZT_LowP_Target_Sam[rd_nSamTar] = WPD_lHipJ_FB_ZT_LowP_Target[rd_nTar];
					WPD_lKneeJ_FB_ZT_LowP_Target_Sam[rd_nSamTar] = WPD_lKneeJ_FB_ZT_LowP_Target[rd_nTar];
					WPD_lAnkleJ_FB_ZT_LowP_Target_Sam[rd_nSamTar] = WPD_lAnkleJ_FB_ZT_LowP_Target[rd_nTar];

					// + For patternDistResult.json ---
					// Right ---------
					jWPD_rHipJ_FB_ZT_LowP_Target_Sam_Out.append(WPD_rHipJ_FB_ZT_LowP_Target[rd_nTar]); // Hip
					jWPD_rKneeJ_FB_ZT_LowP_Target_Sam_Out.append(WPD_rKneeJ_FB_ZT_LowP_Target[rd_nTar]); // Knee
					jWPD_rAnkleJ_FB_ZT_LowP_Target_Sam_Out.append(WPD_rAnkleJ_FB_ZT_LowP_Target[rd_nTar]); // Ankle
					// Left ----------
					jWPD_lHipJ_FB_ZT_LowP_Target_Sam_Out.append(WPD_lHipJ_FB_ZT_LowP_Target[rd_nTar]); // Hip
					jWPD_lKneeJ_FB_ZT_LowP_Target_Sam_Out.append(WPD_lKneeJ_FB_ZT_LowP_Target[rd_nTar]); // Knee
					jWPD_lAnkleJ_FB_ZT_LowP_Target_Sam_Out.append(WPD_lAnkleJ_FB_ZT_LowP_Target[rd_nTar]); // Ankle
					// - For patternDistResult.json ---

					rd_nSamTar++;
				}
			}
			cout << "rd_nSamTar: " << rd_nSamTar << endl;



			// ----------------------------------
			// 4. Calculating pattern
			// ----------------------------------
			cout << "4. Calculating pattern" << endl;
			// Right
			vector<vector<double>> rd_rKernel{rd_endConverge, vector<double>(WPD_kernelSize, 0)}; // = [rd_endConverge][WPD_kernelSize]
			// Left
			vector<vector<double>> rd_lKernel{rd_endConverge, vector<double>(WPD_kernelSize, 0)};

			// // + weightPat.txt -----
			// // Store timeStampCAN when calculate the weight
			// myFile_weightPat.open(txtWriteWeightPat);
			// myFile_weightPat << realRos->timeStampCAN << endl;
			// // - weightPat.txt -----

			// 4.1) Create Kernel matrix ---------------------------------------------
			// Mathematically, we should have
			// Weight x Kernel = [20 col][20 row  x 2000 col]
			// Due to C++ vector to keep element row-major order, so
			// Weigh[i] -> {x, x, x, ..., x} = 20-row vector
			// Kernel -> {{20 row}, {20 row}, ..., {20 row}} = 2000-row vector each have 20-row vector = 2000 row x 20 col
			// rd_endConverge x WPD_kernelSize e.g 2000x20
			cout << "4.1) Create Kernel" << endl;

			// Right ----------
			cout << "... Creating rd_rKERNEL MATRIX ..." << endl;

			for (int i = 0; i < rd_endConverge; i++) { // row e.g. 2000

				jWPD_rKernel_Row_Out.clear(); // Clear for every i

				for (int j = 0; j < WPD_kernelSize; j++) { // col e.g. 20

					rd_rKernel[i][j] = exp( -(pow((WPD_rCPG0SO2Learn_ACycle_Inter[i] - WPD_rCPG0SO2Learn_ACycle_Sam[j]), 2) + pow((WPD_rCPG1SO2Learn_ACycle_Inter[i] - WPD_rCPG1SO2Learn_ACycle_Sam[j]), 2))/rd_s );

					// + For patternDistResult.json ---
					jWPD_rKernel_Row_Out.append(rd_rKernel[i][j]);
					// - For patternDistResult.json ---
				}

				// + For patternDistResult.json ---
				jWPD_rKernel_Out.append(jWPD_rKernel_Row_Out);
				// - For patternDistResult.json ---
				
			}

			// Left ----------
			cout << "... Creating rd_lKERNEL MATRIX ..." << endl;

			for (int i = 0; i < rd_endConverge; i++) { // row e.g. 2000

				jWPD_lKernel_Row_Out.clear(); // Clear for every i

				for (int j = 0; j < WPD_kernelSize; j++) { // col e.g. 20

					rd_lKernel[i][j] = exp( -(pow((WPD_lCPG0SO2Learn_ACycle_Inter[i] - WPD_lCPG0SO2Learn_ACycle_Sam[j]), 2) + pow((WPD_lCPG1SO2Learn_ACycle_Inter[i] - WPD_lCPG1SO2Learn_ACycle_Sam[j]), 2))/rd_s );

					// + For patternDistResult.json ---
					jWPD_lKernel_Row_Out.append(rd_lKernel[i][j]);
					// - For patternDistResult.json ---

				}

				// + For patternDistResult.json ---
				jWPD_lKernel_Out.append(jWPD_lKernel_Row_Out);
				// - For patternDistResult.json ---

			}

			// Set weight to 0
			// Right ----------
			WPD_rHipJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0);
			WPD_rKneeJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0);
			WPD_rAnkleJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0);
			// Left ----------
			WPD_lHipJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0);
			WPD_lKneeJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0);
			WPD_lAnkleJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0);
			cout << "... Empty Weight Mx. ..." << endl;


			// 4.2) Learning RBF -----------------------------------------------------------
			cout << "4.2) Learning RBF" << endl;
			cout << "... Learing RBF ..." << endl;
			while (rd_nLearn < rd_learnLoop_RBF) { // Ex. learn for 400 rounds

				cout << "LearningRound # " << rd_nLearn << "-------------------------" << endl;
				// // + weightPat.txt -----
				// myFile_weightPat << rd_nLearn << endl;
				// // - weightPat.txt -----

				// Create new variables for result
				// Right ----------
				WPD_rHipP_Control_Sam = vector<double>(WPD_kernelSize, 0);
				WPD_rKneeP_Control_Sam = vector<double>(WPD_kernelSize, 0);
				WPD_rAnkleP_Control_Sam = vector<double>(WPD_kernelSize, 0);
				// Left ----------
				WPD_lHipP_Control_Sam = vector<double>(WPD_kernelSize, 0);
				WPD_lKneeP_Control_Sam = vector<double>(WPD_kernelSize, 0);
				WPD_lAnkleP_Control_Sam = vector<double>(WPD_kernelSize, 0);
				cout << "... Empty Result Mx. ..." << endl;

				// Clear JSON variable for each learning round
				// Right ----------
				jWPD_rHipJ_FB_ZT_LowP_Weight_Row_Out.clear();
				jWPD_rKneeJ_FB_ZT_LowP_Weight_Row_Out.clear();
				jWPD_rAnkleJ_FB_ZT_LowP_Weight_Row_Out.clear();
				// Left ----------
				jWPD_lHipJ_FB_ZT_LowP_Weight_Row_Out.clear();
				jWPD_lKneeJ_FB_ZT_LowP_Weight_Row_Out.clear();
				jWPD_lAnkleJ_FB_ZT_LowP_Weight_Row_Out.clear();


				// 4.2.1) Inner product to create result ------------------------
				// Ex.
				// WPD_rHipJ_FB_ZT_LowP_Weight_Row 20x1
				// WPD_rKernel 2000x20
				// WPD_rHipP_Control 2000x1
				// WPD_rHipJ_FB_ZT_LowP_Target 20x1
				// WPD_rHipP_Control_Sam 20x1
				cout << "4.2.1) Inner product" << endl;
				cout << "... Inner product ..." << endl;

				// + Calculate only at sampling point ----------------------------
				// Just only the sampling one will be used to update weight
				double rd_innProR;
				double rd_innProL;

				// Right ---------------------------------------------
				// + Hip -----------------------------
				rd_nSamResult = 0; // Reset counter
				for (int i = rd_samplingStep - 1; i < rd_endConverge; i = i + rd_samplingStep) {
					rd_innProR = inner_product(begin(WPD_rHipJ_FB_ZT_LowP_Weight_Row), end(WPD_rHipJ_FB_ZT_LowP_Weight_Row), begin(rd_rKernel[i]), 0.0);
					WPD_rHipP_Control_Sam[rd_nSamResult] = rd_innProR;

					rd_nSamResult++;
				}
				// - Hip -----------------------------

				// + Knee -----------------------------
				rd_nSamResult = 0; // Reset counter
				for (int i = rd_samplingStep - 1; i < rd_endConverge; i = i + rd_samplingStep) {
					rd_innProR = inner_product(begin(WPD_rKneeJ_FB_ZT_LowP_Weight_Row), end(WPD_rKneeJ_FB_ZT_LowP_Weight_Row), begin(rd_rKernel[i]), 0.0);
					WPD_rKneeP_Control_Sam[rd_nSamResult] = rd_innProR;

					rd_nSamResult++;
				}
				// - Knee -----------------------------


				// + Ankle -----------------------------
				rd_nSamResult = 0; // Reset counter
				for (int i = rd_samplingStep - 1; i < rd_endConverge; i = i + rd_samplingStep) {
					rd_innProR = inner_product(begin(WPD_rAnkleJ_FB_ZT_LowP_Weight_Row), end(WPD_rAnkleJ_FB_ZT_LowP_Weight_Row), begin(rd_rKernel[i]), 0.0);
					WPD_rAnkleP_Control_Sam[rd_nSamResult] = rd_innProR;

					rd_nSamResult++;
				}
				// - Ankle -----------------------------


				// Left ---------------------------------------------
				// + Hip -----------------------------
				rd_nSamResult = 0; // Reset counter
				for (int i = rd_samplingStep - 1; i < rd_endConverge; i = i + rd_samplingStep) {
					rd_innProL = inner_product(begin(WPD_lHipJ_FB_ZT_LowP_Weight_Row), end(WPD_lHipJ_FB_ZT_LowP_Weight_Row), begin(rd_lKernel[i]), 0.0);
					WPD_lHipP_Control_Sam[rd_nSamResult] = rd_innProL;

					rd_nSamResult++;
				}
				// - Hip -----------------------------

				// + Knee -----------------------------
				rd_nSamResult = 0; // Reset counter
				for (int i = rd_samplingStep - 1; i < rd_endConverge; i = i + rd_samplingStep) {
					rd_innProL = inner_product(begin(WPD_lKneeJ_FB_ZT_LowP_Weight_Row), end(WPD_lKneeJ_FB_ZT_LowP_Weight_Row), begin(rd_lKernel[i]), 0.0);
					WPD_lKneeP_Control_Sam[rd_nSamResult] = rd_innProL;

					rd_nSamResult++;
				}
				// - Knee -----------------------------

				// + Ankle -----------------------------
				rd_nSamResult = 0; // Reset counter
				for (int i = rd_samplingStep - 1; i < rd_endConverge; i = i + rd_samplingStep) {
					rd_innProL = inner_product(begin(WPD_lAnkleJ_FB_ZT_LowP_Weight_Row), end(WPD_lAnkleJ_FB_ZT_LowP_Weight_Row), begin(rd_lKernel[i]), 0.0);
					WPD_lAnkleP_Control_Sam[rd_nSamResult] = rd_innProL;

					rd_nSamResult++;
				}
				// - Ankle -----------------------------

				// - Calculate only at sampling point ----------------------------


				// 4.2.2) Update weight -----------------------------------------------
				cout << "4.2.2) Update weight" << endl;
				cout << "... Update weight ..." << endl;
				// Right ---------------------------------------------
				// + Hip -----------------------------
				// // + weightPat.txt -----
				// myFile_weightPat << "rHip" << " ";
				// // - weightPat.txt -----
				for (int i = 0; i < WPD_kernelSize; i++) {
					WPD_rHipJ_FB_ZT_LowP_Weight_Row[i] = WPD_rHipJ_FB_ZT_LowP_Weight_Row[i] + rd_alpha_RBF*(WPD_rHipJ_FB_ZT_LowP_Target_Sam[i] - WPD_rHipP_Control_Sam[i]);

					// + For patternResult.json ---
					jWPD_rHipJ_FB_ZT_LowP_Weight_Row_Out.append(WPD_rHipJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---
					
					// // + weightPat.txt -----
					// myFile_weightPat << rHipJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// // - weightPat.txt -----

				}
				// // + weightPat.txt -----
				// myFile_weightPat << "\n";
				// // - weightPat.txt -----
				// - Hip -----------------------------


				// + Knee -----------------------------
				// // + weightPat.txt -----
				// myFile_weightPat << "rKnee" << " ";
				// // - weightPat.txt -----
				for (int i = 0; i < WPD_kernelSize; i++) {
					WPD_rKneeJ_FB_ZT_LowP_Weight_Row[i] = WPD_rKneeJ_FB_ZT_LowP_Weight_Row[i] + rd_alpha_RBF*(WPD_rKneeJ_FB_ZT_LowP_Target_Sam[i] - WPD_rKneeP_Control_Sam[i]);

					// + For patternResult.json ---
					jWPD_rKneeJ_FB_ZT_LowP_Weight_Row_Out.append(WPD_rKneeJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---

					// // + weightPat.txt -----
					// myFile_weightPat << rKneeJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// // - weightPat.txt -----

				}
				// // + weightPat.txt -----
				// myFile_weightPat << "\n";
				// // - weightPat.txt -----
				// - Knee -----------------------------


				// + Ankle -----------------------------
				// // + weightPat.txt -----
				// myFile_weightPat << "rAnkle" << " ";
				// // - weightPat.txt -----
				for (int i = 0; i < WPD_kernelSize; i++) {
					WPD_rAnkleJ_FB_ZT_LowP_Weight_Row[i] = WPD_rAnkleJ_FB_ZT_LowP_Weight_Row[i] + rd_alpha_RBF*(WPD_rAnkleJ_FB_ZT_LowP_Target_Sam[i] - WPD_rAnkleP_Control_Sam[i]);

					// + For patternResult.json ---
					jWPD_rAnkleJ_FB_ZT_LowP_Weight_Row_Out.append(WPD_rAnkleJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---

					// // + weightPat.txt -----
					// myFile_weightPat << rAnkleJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// // - weightPat.txt -----

				}
				// // + weightPat.txt -----
				// myFile_weightPat << "\n";
				// // - weightPat.txt -----
				// - Ankle -----------------------------


				// Left ---------------------------------------------
				// + Hip -----------------------------
				// // + weightPat.txt -----
				// myFile_weightPat << "lHip" << " ";
				// // - weightPat.txt -----
				for (int i = 0; i < WPD_kernelSize; i++) {
					WPD_lHipJ_FB_ZT_LowP_Weight_Row[i] = WPD_lHipJ_FB_ZT_LowP_Weight_Row[i] + rd_alpha_RBF*(WPD_lHipJ_FB_ZT_LowP_Target_Sam[i] - WPD_lHipP_Control_Sam[i]);

					// + For patternResult.json ---
					jWPD_lHipJ_FB_ZT_LowP_Weight_Row_Out.append(WPD_lHipJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---

					// // + weightPat.txt -----
					// myFile_weightPat << lHipJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// // - weightPat.txt -----

				}
				// // + weightPat.txt -----
				// myFile_weightPat << "\n";
				// // - weightPat.txt -----
				// - Hip -----------------------------


				// + Knee -----------------------------
				// // + weightPat.txt -----
				// myFile_weightPat << "lKnee" << " ";
				// // - weightPat.txt -----
				for (int i = 0; i < WPD_kernelSize; i++) {
					WPD_lKneeJ_FB_ZT_LowP_Weight_Row[i] = WPD_lKneeJ_FB_ZT_LowP_Weight_Row[i] + rd_alpha_RBF*(WPD_lKneeJ_FB_ZT_LowP_Target_Sam[i] - WPD_lKneeP_Control_Sam[i]);

					// + For patternResult.json ---
					jWPD_lKneeJ_FB_ZT_LowP_Weight_Row_Out.append(WPD_lKneeJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---

					// // + weightPat.txt -----
					// myFile_weightPat << lKneeJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// // - weightPat.txt -----
				}
				// // + weightPat.txt -----
				// myFile_weightPat << "\n";
				// // - weightPat.txt -----
				// - Knee -----------------------------


				// + Ankle -----------------------------
				// // + weightPat.txt -----
				// myFile_weightPat << "lAnkle" << " ";
				// // - weightPat.txt -----
				for (int i = 0; i < WPD_kernelSize; i++) {
					WPD_lAnkleJ_FB_ZT_LowP_Weight_Row[i] = WPD_lAnkleJ_FB_ZT_LowP_Weight_Row[i] + rd_alpha_RBF*(WPD_lAnkleJ_FB_ZT_LowP_Target_Sam[i] - WPD_lAnkleP_Control_Sam[i]);

					// + For patternResult.json ---
					jWPD_lAnkleJ_FB_ZT_LowP_Weight_Row_Out.append(WPD_lAnkleJ_FB_ZT_LowP_Weight_Row[i]);
					// - For patternResult.json ---

					// // + weightPat.txt -----
					// myFile_weightPat << lAnkleJ_FB_ZT_LowP_Weight_Row[i] << " "; // Keep track every nLearn
					// // - weightPat.txt -----
				}
				// // + weightPat.txt -----
				// myFile_weightPat << "\n";
				// // - weightPat.txt -----
				// - Ankle -----------------------------

				rd_nLearn++;

			} // End while

			// + For patternDistResult.json ---
			// Keep only final weight to JSON
			jWPD_rHipJ_FB_ZT_LowP_Weight_Out.append(jWPD_rHipJ_FB_ZT_LowP_Weight_Row_Out);
			jWPD_rKneeJ_FB_ZT_LowP_Weight_Out.append(jWPD_rKneeJ_FB_ZT_LowP_Weight_Row_Out);
			jWPD_rAnkleJ_FB_ZT_LowP_Weight_Out.append(jWPD_rAnkleJ_FB_ZT_LowP_Weight_Row_Out);
			jWPD_lHipJ_FB_ZT_LowP_Weight_Out.append(jWPD_lHipJ_FB_ZT_LowP_Weight_Row_Out);
			jWPD_lKneeJ_FB_ZT_LowP_Weight_Out.append(jWPD_lKneeJ_FB_ZT_LowP_Weight_Row_Out);
			jWPD_lAnkleJ_FB_ZT_LowP_Weight_Out.append(jWPD_lAnkleJ_FB_ZT_LowP_Weight_Row_Out);
			// - For patternDistResult.json ---

			rd_nLearn = 0; // reset learning round counter


			// 4.3) Final result -------------------------------------------------------------------------
			// Actually, if we would like to generate online pattern, only the final weight is needed.
			// Here, we create final pattern as a result just in case to check the pattern.
			cout << "4.3) Final result" << endl;
			cout << "... Final result ..." << endl;

			WPD_rHipP_Control = vector<double>(rd_endConverge, 0);
			WPD_rKneeP_Control = vector<double>(rd_endConverge, 0);
			WPD_rAnkleP_Control = vector<double>(rd_endConverge, 0);

			WPD_lHipP_Control = vector<double>(rd_endConverge, 0);
			WPD_lKneeP_Control = vector<double>(rd_endConverge, 0);
			WPD_lAnkleP_Control = vector<double>(rd_endConverge, 0);


			// + For patternDistResult.json ---
			jWPD_rHipP_Control_Out.clear();
			jWPD_rKneeP_Control_Out.clear();
			jWPD_rAnkleP_Control_Out.clear();
			jWPD_lHipP_Control_Out.clear();
			jWPD_lKneeP_Control_Out.clear();
			jWPD_lAnkleP_Control_Out.clear();
			// - For patternDistResult.json ---

			// Right ---------------------------------------------
			// Hip
			for (int i = 0; i < rd_endConverge; i++) {
				WPD_rHipP_Control[i] = inner_product(begin(WPD_rHipJ_FB_ZT_LowP_Weight_Row), end(WPD_rHipJ_FB_ZT_LowP_Weight_Row), begin(rd_rKernel[i]), 0.0);

				// + For patternDistResult.json ---
				jWPD_rHipP_Control_Out.append(WPD_rHipP_Control[i]);
				// - For patternDistResult.json ---

			}
			// Knee
			for (int i = 0; i < rd_endConverge; i++) {
				WPD_rKneeP_Control[i] = inner_product(begin(WPD_rKneeJ_FB_ZT_LowP_Weight_Row), end(WPD_rKneeJ_FB_ZT_LowP_Weight_Row), begin(rd_rKernel[i]), 0.0);

				// + For patternDistResult.json ---
				jWPD_rKneeP_Control_Out.append(WPD_rKneeP_Control[i]);
				// - For patternDistResult.json ---

			}
			// Ankle
			for (int i = 0; i < rd_endConverge; i++) {
				WPD_rAnkleP_Control[i] = inner_product(begin(WPD_rAnkleJ_FB_ZT_LowP_Weight_Row), end(WPD_rAnkleJ_FB_ZT_LowP_Weight_Row), begin(rd_rKernel[i]), 0.0);

				// + For patternDistResult.json ---
				jWPD_rAnkleP_Control_Out.append(WPD_rAnkleP_Control[i]);
				// - For patternDistResult.json ---

			}



			// Left ---------------------------------------------
			// Hip
			for (int i = 0; i < rd_endConverge; i++) {
				WPD_lHipP_Control[i] = inner_product(begin(WPD_lHipJ_FB_ZT_LowP_Weight_Row), end(WPD_lHipJ_FB_ZT_LowP_Weight_Row), begin(rd_lKernel[i]), 0.0);

				// + For patternDistResult.json ---
				jWPD_lHipP_Control_Out.append(WPD_lHipP_Control[i]);
				// - For patternDistResult.json ---

			}

			// Knee
			for (int i = 0; i < rd_endConverge; i++) {
				WPD_lKneeP_Control[i] = inner_product(begin(WPD_lKneeJ_FB_ZT_LowP_Weight_Row), end(WPD_lKneeJ_FB_ZT_LowP_Weight_Row), begin(rd_lKernel[i]), 0.0);

				// + For patternDistResult.json ---
				jWPD_lKneeP_Control_Out.append(WPD_lKneeP_Control[i]);
				// - For patternDistResult.json ---

			}

			// Ankle
			for (int i = 0; i < rd_endConverge; i++) {
				WPD_lAnkleP_Control[i] = inner_product(begin(WPD_lAnkleJ_FB_ZT_LowP_Weight_Row), end(WPD_lAnkleJ_FB_ZT_LowP_Weight_Row), begin(rd_lKernel[i]), 0.0);

				// + For patternDistResult.json ---
				jWPD_lAnkleP_Control_Out.append(WPD_lAnkleP_Control[i]);
				// - For patternDistResult.json ---

			}


			// 4.4) Save result to file after ending of learning ----------------------------------------------
			// Check if patternDistResult.json exists in the folder, so no need to overwrite.
			// This will enable us to use this file again during actionStart with correct info.
			cout << "4.4) Save result to file" << endl;

			if (!rd_existPatternDistResult_1) {
				cout << "WRITE PATTERN RESULT" << endl;
				ofsjWPD.open(jsonWritePatDist, ofstream::trunc);
				jsonWritePatDistFn();
				ofsjWPD.close();
				cout << "Save file to: " << jsonWritePatDist << endl; // patternDistResult.json
			}
			
			rd_learnStart = true; // To have learning step once
			rd_actionStart = true; // To start action
		} // End learnStart
		cout << "rd_learnrCPGCollect: "<< rd_learnrCPGCollect << " " << "rd_learnlCPGCollect: " << rd_learnlCPGCollect << endl;
		cout << "rd_learnStart: " << rd_learnStart << endl;

		cout << "#################################" << endl;
		cout << "######  END RBF DIST ############" << endl;
		cout << "#################################" << endl;
	}
	
	// Clear flag ---
	rd_checkJsonWritePatDist_1 = false;

}


// This is function to check start from phase 0 with record first data.
bool ExvisParallelRealAssem::checkStart(double data_i_1, int i, double data, vector<double> * cycleG, int & countCycle) {

	if ((data_i_1 < 0) && (data >= 0)) {
		(*cycleG).push_back(data_i_1);
		countCycle -= 1;
		return true;
	}

	return false;
}

// This is function to check start from phase 180 with record first data.
bool ExvisParallelRealAssem::checkStart180(double data_i_1, int i, double data, vector<double> * cycleG, int & countCycle) {

	if ((data_i_1 > 0) && (data <= 0)) {
		(*cycleG).push_back(data_i_1);
		countCycle -= 1;
		return true;
	}

	return false;
}

// This is function to check start from phase 0.
bool ExvisParallelRealAssem::checkStartOnline(double data_i_1, double data) {

	if ((data_i_1 < 0) && (data >= 0)) {

		return true;
	}

	return false;
}

// This is function to check start from phase 180.
bool ExvisParallelRealAssem::checkStartOnline180(double data_i_1, double data) {

	if ((data_i_1 > 0) && (data <= 0)) {

		return true;
	}

	return false;
}

// Check file existing
bool ExvisParallelRealAssem::exists_File(const std::string & name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}


ExvisParallelRealAssem::~ExvisParallelRealAssem() {

	// Close read exvisJSONZeroTorque.json ----
	if (ifsjRZT.is_open()) {
		ifsjRZT.close();
	}

	// Close record patternResult.json ---------
	if (ofsjWP.is_open()) {
		jsonWritePatFn();
		ofsjWP.close();
	}

	// Close read patternResult.json -----------
	if (ifsjRP.is_open()) {
		ifsjRP.close();
	}

	// Close record weightPat.txt --------------
	if (myFile_weightPat.is_open()) {
		myFile_weightPat.close();
	}
}

