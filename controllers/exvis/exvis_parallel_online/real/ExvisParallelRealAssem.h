/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#ifndef EXVISPARALLELREALASSEM_H
#define EXVISPARALLELREALASSEM_H

#include <stdio.h> // standard input / output functions
#include <unistd.h> // standard function definitions
#include <stdlib.h> // variable types, macros, functions
#include <string.h> // string function definitions
#include <iostream> // C++ to use cout
#include <fstream> // To read/write on file
#include <math.h>
#include <vector>
#include <pwd.h> // check directory
#include <sys/time.h>
#include <numeric> // for inner_product
#include <sys/stat.h> // check file existing

using namespace std;

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Int32MultiArray.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayLayout.h>



#include "ExvisParallelRealROS.h"

#include "jsoncpp/json/json.h"
//***************************************************************************

class ExvisParallelRealROS;
class ExvisParallelRealRWAssem;

class ExvisParallelRealAssem {
	public:
		ExvisParallelRealAssem(int argc,char* argv[]);
		~ExvisParallelRealAssem();
		bool runAssem();

	private:

		// Class obj ====================================
		ExvisParallelRealROS * realRos;
		// ==============================================

		int nodeCounter = 0;
		timespec tNode;

		int nodeCounter_start;
		bool flag_nodeCounter_start = false;

		// Record file ============================
		const char *homedir;

		// + To read exvisJSONZeroTorque.json ----------------------------
		// + JSON Reader -------------------
		char jsonReadZeroTorque[100]; // To store path for read
		ifstream ifsjRZT;
		// - JSON Reader -------------------
		
		Json::Reader readerjRZT;
	    Json::Value objjRZT; // json obj/record
	    // TIME:
		Json::Value jRZT_timeStampCAN_In;
		// EXO:
			// Feedback signal - Right
		Json::Value jRZT_rHipJ_FB_In; // Right Hip joint angle
		Json::Value jRZT_rKneeJ_FB_In; // Right Knee joint angle
		Json::Value jRZT_rAnkleJ_FB_In; // Right Ankle joint angle
		Json::Value jRZT_rHipT_FB_In; // Right Hip torque
		Json::Value jRZT_rKneeT_FB_In; // Right Knee torque
		Json::Value jRZT_rAnkleT_FB_In; // Right Ankle torque
		Json::Value jRZT_rHipM_FB_In; // Right Hip motor torque
		Json::Value jRZT_rKneeM_FB_In; // Right Knee motor torque
		Json::Value jRZT_rAnkleM_FB_In; // Right Ankle motor torque
		Json::Value jRZT_rFHeel_FB_In; // Right Foot heel
		Json::Value jRZT_rFToe_FB_In; // Right Foot toe	
			// Feedback signal - Left
		Json::Value jRZT_lHipJ_FB_In; 
		Json::Value jRZT_lKneeJ_FB_In; 
		Json::Value jRZT_lAnkleJ_FB_In; 
		Json::Value jRZT_lHipT_FB_In; 
		Json::Value jRZT_lKneeT_FB_In; 
		Json::Value jRZT_lAnkleT_FB_In; 
		Json::Value jRZT_lHipM_FB_In; 
		Json::Value jRZT_lKneeM_FB_In; 
		Json::Value jRZT_lAnkleM_FB_In;
		Json::Value jRZT_lFHeel_FB_In; 
		Json::Value jRZT_lFToe_FB_In;
		// CPG:
			// AFDC - Right
		Json::Value jRZT_rCPG0AFDC_In;
		Json::Value jRZT_rCPG1AFDC_In;
		Json::Value jRZT_rCPG2AFDC_In;
		Json::Value jRZT_rCPGFreqAFDC_In;
			// AFDC - Left
		Json::Value jRZT_lCPG0AFDC_In;
		Json::Value jRZT_lCPG1AFDC_In;
		Json::Value jRZT_lCPG2AFDC_In;
		Json::Value jRZT_lCPGFreqAFDC_In;

		// Variable and vectors to be used together with json ------
		// TIME:
		vector<string> timeStampCAN_ZT;
		// EXO:
			// Feedback signal - Right
		vector<double> rHipJ_FB_ZT;
		vector<double> rKneeJ_FB_ZT;
		vector<double> rAnkleJ_FB_ZT;
		vector<double> rHipT_FB_ZT;
		vector<double> rKneeT_FB_ZT;
		vector<double> rAnkleT_FB_ZT;
		vector<double> rHipM_FB_ZT;
		vector<double> rKneeM_FB_ZT;
		vector<double> rAnkleM_FB_ZT;
		vector<double> rFHeel_FB_ZT;
		vector<double> rFToe_FB_ZT;
			// Feedback signal - Left
		vector<double> lHipJ_FB_ZT;
		vector<double> lKneeJ_FB_ZT;
		vector<double> lAnkleJ_FB_ZT;
		vector<double> lHipT_FB_ZT;
		vector<double> lKneeT_FB_ZT;
		vector<double> lAnkleT_FB_ZT;
		vector<double> lHipM_FB_ZT;
		vector<double> lKneeM_FB_ZT;
		vector<double> lAnkleM_FB_ZT;
		vector<double> lFHeel_FB_ZT;
		vector<double> lFToe_FB_ZT;
		// CPG:
			// AFDC - Right
		vector<double> rCPG0AFDC_ZT;
		vector<double> rCPG1AFDC_ZT;
		vector<double> rCPGFreqAFDC_ZT;
			// AFDC - Left
		vector<double> lCPG0AFDC_ZT;
		vector<double> lCPG1AFDC_ZT;
		vector<double> lCPGFreqAFDC_ZT;
	    // - To read exvisJSONZeroTorque.json ----------------------------


	    // + To record Adaptivev Pattern Positon Calculation in JSON ---------
		// patternResult.json
		// + JSON Writer -------------------
		char jsonWritePat[100]; // To store path for write
		ofstream ofsjWP;
		// - JSON Writer -------------------

		Json::Value objjWP; // json obj/record
		// UI:
		Json::Value jWP_speedFound_Out; // Convert CPG freq to Exo's discrete speed
		// EXO:
			// Feedback signal - Right
		Json::Value jWP_rHipJ_FB_ZT_LowP_Out; // Right low-pass hip signal
		Json::Value jWP_rKneeJ_FB_ZT_LowP_Out;
		Json::Value jWP_rAnkleJ_FB_ZT_LowP_Out;
		Json::Value jWP_rHipJ_FB_ZT_LowP_ACycle_Out; // Right low-pass hip signal for one cycle = [x, x, ...]
		Json::Value jWP_rKneeJ_FB_ZT_LowP_ACycle_Out;
		Json::Value jWP_rAnkleJ_FB_ZT_LowP_ACycle_Out;
		Json::Value jWP_rHipJ_FB_ZT_CyclesFound_Out; // Number of found cycles of right hip
		Json::Value jWP_rKneeJ_FB_ZT_CyclesFound_Out;
		Json::Value jWP_rAnkleJ_FB_ZT_CyclesFound_Out;
		Json::Value jWP_rHipJ_FB_ZT_LowP_Mx_Out; // Matrix of cycles of right low-pass hip = [[x, x, ...], [x, x, ...]]
		Json::Value jWP_rKneeJ_FB_ZT_LowP_Mx_Out;
		Json::Value jWP_rAnkleJ_FB_ZT_LowP_Mx_Out;
		Json::Value jWP_rHipJ_FB_ZT_LowP_Target_Out; // This is the "Target" signal having one cycle length
		Json::Value jWP_rKneeJ_FB_ZT_LowP_Target_Out;
		Json::Value jWP_rAnkleJ_FB_ZT_LowP_Target_Out;
		Json::Value jWP_rHipJ_FB_ZT_LowP_Target_Sam_Out; // Sampling data for one cycle target
		Json::Value jWP_rKneeJ_FB_ZT_LowP_Target_Sam_Out;
		Json::Value jWP_rAnkleJ_FB_ZT_LowP_Target_Sam_Out;
		Json::Value jWP_rKernel_Row_Out; // Each row in Kernel
		Json::Value jWP_rKernel_Out; // The whole Kernel
		Json::Value jWP_rHipJ_FB_ZT_LowP_Weight_Row_Out; // Each row in Weight
		Json::Value jWP_rHipJ_FB_ZT_LowP_Weight_Out; // The whole Weight
		Json::Value jWP_rKneeJ_FB_ZT_LowP_Weight_Row_Out;
		Json::Value jWP_rKneeJ_FB_ZT_LowP_Weight_Out;
		Json::Value jWP_rAnkleJ_FB_ZT_LowP_Weight_Row_Out;
		Json::Value jWP_rAnkleJ_FB_ZT_LowP_Weight_Out;
			// Feedback signal - Left
		Json::Value jWP_lHipJ_FB_ZT_LowP_Out;
		Json::Value jWP_lKneeJ_FB_ZT_LowP_Out;
		Json::Value jWP_lAnkleJ_FB_ZT_LowP_Out;
		Json::Value jWP_lHipJ_FB_ZT_LowP_ACycle_Out; 
		Json::Value jWP_lKneeJ_FB_ZT_LowP_ACycle_Out;
		Json::Value jWP_lAnkleJ_FB_ZT_LowP_ACycle_Out;
		Json::Value jWP_lHipJ_FB_ZT_CyclesFound_Out; 
		Json::Value jWP_lKneeJ_FB_ZT_CyclesFound_Out;
		Json::Value jWP_lAnkleJ_FB_ZT_CyclesFound_Out;
		Json::Value jWP_lHipJ_FB_ZT_LowP_Mx_Out; 
		Json::Value jWP_lKneeJ_FB_ZT_LowP_Mx_Out;
		Json::Value jWP_lAnkleJ_FB_ZT_LowP_Mx_Out;
		Json::Value jWP_lHipJ_FB_ZT_LowP_Target_Out; 
		Json::Value jWP_lKneeJ_FB_ZT_LowP_Target_Out;
		Json::Value jWP_lAnkleJ_FB_ZT_LowP_Target_Out;
		Json::Value jWP_lHipJ_FB_ZT_LowP_Target_Sam_Out; 
		Json::Value jWP_lKneeJ_FB_ZT_LowP_Target_Sam_Out;
		Json::Value jWP_lAnkleJ_FB_ZT_LowP_Target_Sam_Out;
		Json::Value jWP_lKernel_Row_Out;
		Json::Value jWP_lKernel_Out; 
		Json::Value jWP_lHipJ_FB_ZT_LowP_Weight_Row_Out; 
		Json::Value jWP_lHipJ_FB_ZT_LowP_Weight_Out; 
		Json::Value jWP_lKneeJ_FB_ZT_LowP_Weight_Row_Out;
		Json::Value jWP_lKneeJ_FB_ZT_LowP_Weight_Out;
		Json::Value jWP_lAnkleJ_FB_ZT_LowP_Weight_Row_Out;
		Json::Value jWP_lAnkleJ_FB_ZT_LowP_Weight_Out;
			// Control signal - Right
		Json::Value jWP_rHipP_Control_Out; // This is the "Control" signal generated from RBF
		Json::Value jWP_rKneeP_Control_Out;
		Json::Value jWP_rAnkleP_Control_Out;
		Json::Value jWP_rHipP_Control_Sam_Out; // // Sampling data for one cycle control
		Json::Value jWP_rKneeP_Control_Sam_Out;
		Json::Value jWP_rAnkleP_Control_Sam_Out;
			// Control signal - Left
		Json::Value jWP_lHipP_Control_Out;
		Json::Value jWP_lKneeP_Control_Out;
		Json::Value jWP_lAnkleP_Control_Out;
		Json::Value jWP_lHipP_Control_Sam_Out;
		Json::Value jWP_lKneeP_Control_Sam_Out;
		Json::Value jWP_lAnkleP_Control_Sam_Out;
		// CPG:
			// SO2 Learn
		Json::Value jWP_timeSO2LearnGen_Out; // Start time stamp (use timeStampCAN as same as record node)
			// SO2 Learn - Right
		Json::Value jWP_rTimeSO2LearnPhaseZero_Out; // Start time stamp of right CPG0 phase zero
		Json::Value jWP_rCPG0SO2Learn_Out;
		Json::Value jWP_rCPG1SO2Learn_Out;
		Json::Value jWP_rCPG2SO2Learn_Out;
		Json::Value jWP_rCPGFreqSO2Learn_Out;
		Json::Value jWP_rMeanCPGTimeRW_out; // Avg(1/CPGFreqRealWorld)
		Json::Value jWP_rCPG0SO2Learn_ACycle_Out; // Right CPG0 for one cycle
		Json::Value jWP_rCPG1SO2Learn_ACycle_Out;
		Json::Value jWP_rCPG2SO2Learn_ACycle_Out;
		Json::Value jWP_rCountCycle_Out; // Count a cycle of CPG0 in prog step
		Json::Value jWP_rCPG0SO2Learn_CyclesFound_Out; // Number of found cycles of CPG0 = 1
		Json::Value jWP_rCPG0SO2Learn_ACycle_Sam_Out; // Right CPG0 sampling data
		Json::Value jWP_rCPG1SO2Learn_ACycle_Sam_Out;	
			// SO2 Learn - Left
		Json::Value jWP_lTimeSO2LearnPhase180_Out; // Start time stamp of left CPG0 phase 180
		Json::Value jWP_lCPG0SO2Learn_Out;
		Json::Value jWP_lCPG1SO2Learn_Out;
		Json::Value jWP_lCPG2SO2Learn_Out;
		Json::Value jWP_lCPGFreqSO2Learn_Out;
		Json::Value jWP_lMeanCPGTimeRW_out;
		Json::Value jWP_lCPG0SO2Learn_ACycle_Out;
		Json::Value jWP_lCPG1SO2Learn_ACycle_Out;
		Json::Value jWP_lCPG2SO2Learn_ACycle_Out;
		Json::Value jWP_lCountCycle_Out;
		Json::Value jWP_lCPG0SO2Learn_CyclesFound_Out;
		Json::Value jWP_lCPG0SO2Learn_ACycle_Sam_Out;
		Json::Value jWP_lCPG1SO2Learn_ACycle_Sam_Out;


		// Variable and vectors to be used together with json ------
		int kernelSize = 30;
		// EXO:
			// Feedback signal - Right
		vector<double> rHipJ_FB_ZT_LowP;
		vector<double> rKneeJ_FB_ZT_LowP;
		vector<double> rAnkleJ_FB_ZT_LowP;
		vector<double> rHipJ_FB_ZT_LowP_ACycle;
		vector<double> rKneeJ_FB_ZT_LowP_ACycle;
		vector<double> rAnkleJ_FB_ZT_LowP_ACycle;
		int rHipJ_FB_ZT_CyclesFound = 0;
		int rKneeJ_FB_ZT_CyclesFound = 0;
		int rAnkleJ_FB_ZT_CyclesFound = 0;
		vector<vector<double>> rHipJ_FB_ZT_LowP_Mx;
		vector<vector<double>> rKneeJ_FB_ZT_LowP_Mx;
		vector<vector<double>> rAnkleJ_FB_ZT_LowP_Mx;
		vector<double> rHipJ_FB_ZT_LowP_Target;
		vector<double> rKneeJ_FB_ZT_LowP_Target;
		vector<double> rAnkleJ_FB_ZT_LowP_Target;
		vector<double> rHipJ_FB_ZT_LowP_Target_Sam = vector<double>(kernelSize, 0);
		vector<double> rKneeJ_FB_ZT_LowP_Target_Sam = vector<double>(kernelSize, 0); 
		vector<double> rAnkleJ_FB_ZT_LowP_Target_Sam = vector<double>(kernelSize, 0);
		// (vector of vector); row = rCountCycle filled with vector
		// = {	{0.0, 0.0, ... kernelSize},
		//		{0.0, 0.0, ... kernelSize},
		//		...
		//		rCountCycle
		//	}
		vector<vector<double>> rKernel;
		vector<double> rHipJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0); // {0.0, 0.0, ... kernelSize}
		vector<double> rKneeJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0);
		vector<double> rAnkleJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0);
			// Feedback signal - Left
		vector<double> lHipJ_FB_ZT_LowP;
		vector<double> lKneeJ_FB_ZT_LowP;
		vector<double> lAnkleJ_FB_ZT_LowP;
		vector<double> lHipJ_FB_ZT_LowP_ACycle;
		vector<double> lKneeJ_FB_ZT_LowP_ACycle;
		vector<double> lAnkleJ_FB_ZT_LowP_ACycle;
		int lHipJ_FB_ZT_CyclesFound = 0;
		int lKneeJ_FB_ZT_CyclesFound = 0;
		int lAnkleJ_FB_ZT_CyclesFound = 0;
		vector<vector<double>> lHipJ_FB_ZT_LowP_Mx;
		vector<vector<double>> lKneeJ_FB_ZT_LowP_Mx;
		vector<vector<double>> lAnkleJ_FB_ZT_LowP_Mx;
		vector<double> lHipJ_FB_ZT_LowP_Target;
		vector<double> lKneeJ_FB_ZT_LowP_Target;
		vector<double> lAnkleJ_FB_ZT_LowP_Target;
		vector<double> lHipJ_FB_ZT_LowP_Target_Sam = vector<double>(kernelSize, 0);
		vector<double> lKneeJ_FB_ZT_LowP_Target_Sam = vector<double>(kernelSize, 0); 
		vector<double> lAnkleJ_FB_ZT_LowP_Target_Sam = vector<double>(kernelSize, 0);
		vector<vector<double>> lKernel;
		vector<double> lHipJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0);
		vector<double> lKneeJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0);
		vector<double> lAnkleJ_FB_ZT_LowP_Weight_Row = vector<double>(kernelSize, 0);
			// Control signal - Right
		vector<double> rHipP_Control;
		vector<double> rKneeP_Control;
		vector<double> rAnkleP_Control;
		vector<double> rHipP_Control_Sam;
		vector<double> rKneeP_Control_Sam;
		vector<double> rAnkleP_Control_Sam;
			// Control signal - Left
		vector<double> lHipP_Control;
		vector<double> lKneeP_Control;
		vector<double> lAnkleP_Control;
		vector<double> lHipP_Control_Sam;
		vector<double> lKneeP_Control_Sam;
		vector<double> lAnkleP_Control_Sam;
		// CPG:
			// SO2 Learn
		string timeSO2LearnGen;
			// SO2 Learn - Right
		string rTimeSO2LearnPhaseZero;
		vector<double> rCPG0SO2Learn;
		vector<double> rCPG1SO2Learn;
		double rCPGFreqSO2Learn;
		vector<double> rCPG0SO2Learn_ACycle;
		vector<double> rCPG1SO2Learn_ACycle;
		int rCountCycle = 0; // Prog. step for 1 walking cycle. It depends on walking speed of subject
		int rCPG0SO2Learn_CyclesFound = 0;
		vector<double> rCPG0SO2Learn_ACycle_Sam = vector<double>(kernelSize, 0); // To store sampling CPG; row = kernelSize filled with all 0.0 = {0.0, 0.0, ... kernelSize}
		vector<double> rCPG1SO2Learn_ACycle_Sam = vector<double>(kernelSize, 0);
			// SO2 Learn - Left
		string lTimeSO2LearnPhase180;	
		vector<double> lCPG0SO2Learn;
		vector<double> lCPG1SO2Learn;
		double lCPGFreqSO2Learn;
		vector<double> lCPG0SO2Learn_ACycle;
		vector<double> lCPG1SO2Learn_ACycle;
		int lCountCycle = 0;
		int lCPG0SO2Learn_CyclesFound = 0;
		vector<double> lCPG0SO2Learn_ACycle_Sam = vector<double>(kernelSize, 0);
		vector<double> lCPG1SO2Learn_ACycle_Sam = vector<double>(kernelSize, 0);
	    // - To record Adaptivev Pattern Positon Calculation in JSON---------


	    // + To read Adaptive Pattern Positon Calculation in JSON ----------------------------
		// patternResult.json
		// + JSON Reader -------------------
	    char jsonReadPat[100]; // To store path for read
		ifstream ifsjRP;
		// - JSON Reader -------------------

	    Json::Reader readerjRP;
	    Json::Value objjRP; // json obj/record

		// EXO:
			// Feedback signal - Right
		Json::Value jRP_rHipJ_FB_ZT_LowP_Weight_In;
		Json::Value jRP_rKneeJ_FB_ZT_LowP_Weight_In;
		Json::Value jRP_rAnkleJ_FB_ZT_LowP_Weight_In;
			// Feedback signal - Left
		Json::Value jRP_lHipJ_FB_ZT_LowP_Weight_In;
		Json::Value jRP_lKneeJ_FB_ZT_LowP_Weight_In;
		Json::Value jRP_lAnkleJ_FB_ZT_LowP_Weight_In;
		// CPG:
			// SO2 Learn - Right
		Json::Value jRP_rMeanCPGTimeRW_In;
		Json::Value jRP_rCPG0SO2Learn_ACycle_Sam_In;
		Json::Value jRP_rCPG1SO2Learn_ACycle_Sam_In;
			// SO2 Learn - Left
		Json::Value jRP_lMeanCPGTimeRW_In;
		Json::Value jRP_lCPG0SO2Learn_ACycle_Sam_In;
		Json::Value jRP_lCPG1SO2Learn_ACycle_Sam_In;
	    // - To read Adaptive Pattern Positon Calculation in JSON ----------------------------


		// + To record weightPat.txt ----------------
		char txtWriteWeightPat[100];
		ofstream myFile_weightPat;
		// - To record weightPat.txt ----------------



		// ######## + For Discrete pattern ###########
		// + To record Discrete Pattern Positon Calculation in JSON ---------
		// patternDistResult.json
		// + JSON Writer -------------------
		char jsonWritePatDist[100]; // To store path for write
		ofstream ofsjWPD;
		// - JSON Writer -------------------

		Json::Value objjWPD; // json obj/record
		// UI:
		Json::Value jWPD_speedFound_Out; // Convert CPG freq to Exo's discrete speed
		// EXO:
			// Feedback signal - Right
		Json::Value jWPD_rHipJ_FB_ZT_LowP_Out; // Right low-pass hip signal
		Json::Value jWPD_rKneeJ_FB_ZT_LowP_Out;
		Json::Value jWPD_rAnkleJ_FB_ZT_LowP_Out;
		Json::Value jWPD_rHipJ_FB_ZT_LowP_Target_Out; // This is the "Target" signal
		Json::Value jWPD_rKneeJ_FB_ZT_LowP_Target_Out;
		Json::Value jWPD_rAnkleJ_FB_ZT_LowP_Target_Out;
		Json::Value jWPD_rHipJ_FB_ZT_LowP_Target_Sam_Out; // Sampling data for target
		Json::Value jWPD_rKneeJ_FB_ZT_LowP_Target_Sam_Out;
		Json::Value jWPD_rAnkleJ_FB_ZT_LowP_Target_Sam_Out;
		Json::Value jWPD_rKernel_Row_Out; // Each row in Kernel
		Json::Value jWPD_rKernel_Out; // The whole Kernel
		Json::Value jWPD_rHipJ_FB_ZT_LowP_Weight_Row_Out; // Each row in Weight
		Json::Value jWPD_rHipJ_FB_ZT_LowP_Weight_Out; // The whole Weight
		Json::Value jWPD_rKneeJ_FB_ZT_LowP_Weight_Row_Out;
		Json::Value jWPD_rKneeJ_FB_ZT_LowP_Weight_Out;
		Json::Value jWPD_rAnkleJ_FB_ZT_LowP_Weight_Row_Out;
		Json::Value jWPD_rAnkleJ_FB_ZT_LowP_Weight_Out;
			// Feedback signal - Left
		Json::Value jWPD_lHipJ_FB_ZT_LowP_Out; // Left low-pass hip signal
		Json::Value jWPD_lKneeJ_FB_ZT_LowP_Out;
		Json::Value jWPD_lAnkleJ_FB_ZT_LowP_Out;
		Json::Value jWPD_lHipJ_FB_ZT_LowP_Target_Out; // This is the "Target" signal
		Json::Value jWPD_lKneeJ_FB_ZT_LowP_Target_Out;
		Json::Value jWPD_lAnkleJ_FB_ZT_LowP_Target_Out;
		Json::Value jWPD_lHipJ_FB_ZT_LowP_Target_Sam_Out; // Sampling data for target
		Json::Value jWPD_lKneeJ_FB_ZT_LowP_Target_Sam_Out;
		Json::Value jWPD_lAnkleJ_FB_ZT_LowP_Target_Sam_Out;
		Json::Value jWPD_lKernel_Row_Out; // Each row in Kernel
		Json::Value jWPD_lKernel_Out; // The whole Kernel
		Json::Value jWPD_lHipJ_FB_ZT_LowP_Weight_Row_Out; // Each row in Weight
		Json::Value jWPD_lHipJ_FB_ZT_LowP_Weight_Out; // The whole Weight
		Json::Value jWPD_lKneeJ_FB_ZT_LowP_Weight_Row_Out;
		Json::Value jWPD_lKneeJ_FB_ZT_LowP_Weight_Out;
		Json::Value jWPD_lAnkleJ_FB_ZT_LowP_Weight_Row_Out;
		Json::Value jWPD_lAnkleJ_FB_ZT_LowP_Weight_Out;
			// Control signal - Right
		Json::Value jWPD_rHipP_Control_Out; // This is the "Control" signal generated from RBF
		Json::Value jWPD_rKneeP_Control_Out;
		Json::Value jWPD_rAnkleP_Control_Out;
		Json::Value jWPD_rHipP_Control_Sam_Out; // // Sampling data for one cycle control
		Json::Value jWPD_rKneeP_Control_Sam_Out;
		Json::Value jWPD_rAnkleP_Control_Sam_Out;
			// Control signal - Left
		Json::Value jWPD_lHipP_Control_Out;
		Json::Value jWPD_lKneeP_Control_Out;
		Json::Value jWPD_lAnkleP_Control_Out;
		Json::Value jWPD_lHipP_Control_Sam_Out;
		Json::Value jWPD_lKneeP_Control_Sam_Out;
		Json::Value jWPD_lAnkleP_Control_Sam_Out;
		// CPG:
			// SO2 Learn
		Json::Value jWPD_timeSO2LearnGen_Out; // Start time stamp (use timeStampCAN as same as record node)
			// SO2 Learn - Right
		Json::Value jWPD_rTimeSO2LearnPhaseZero_Out; // Start time stamp of right CPG0 phase zero
		Json::Value jWPD_rCPG0SO2Learn_Out;
		Json::Value jWPD_rCPG1SO2Learn_Out;
		Json::Value jWPD_rCPG2SO2Learn_Out;
		Json::Value jWPD_rCPGFreqSO2Learn_Out;
		Json::Value jWPD_rMeanCPGTimeRW_out; // Avg(1/CPGFreqRealWorld)
		Json::Value jWPD_rCPG0SO2Learn_ACycle_Out; // Right CPG0 for one cycle
		Json::Value jWPD_rCPG1SO2Learn_ACycle_Out;
		Json::Value jWPD_rCPG2SO2Learn_ACycle_Out;
		Json::Value jWPD_rCountCycle_Out; // Count a cycle of CPG0 in prog step
		Json::Value jWPD_rCPG0SO2Learn_CyclesFound_Out; // Number of found cycles of CPG0 = 1
		Json::Value jWPD_rCPG0SO2Learn_ACycle_Sam_Out; // Right CPG0 sampling data
		Json::Value jWPD_rCPG1SO2Learn_ACycle_Sam_Out;
		Json::Value jWPD_rCPG0SO2Learn_ACycle_Inter_Out; // Interpolated CPG
		Json::Value jWPD_rCPG1SO2Learn_ACycle_Inter_Out;
			// SO2 Learn - Left
		Json::Value jWPD_lTimeSO2LearnPhase180_Out; // Start time stamp of left CPG0 phase 180
		Json::Value jWPD_lCPG0SO2Learn_Out;
		Json::Value jWPD_lCPG1SO2Learn_Out;
		Json::Value jWPD_lCPG2SO2Learn_Out;
		Json::Value jWPD_lCPGFreqSO2Learn_Out;
		Json::Value jWPD_lMeanCPGTimeRW_out;
		Json::Value jWPD_lCPG0SO2Learn_ACycle_Out;
		Json::Value jWPD_lCPG1SO2Learn_ACycle_Out;
		Json::Value jWPD_lCPG2SO2Learn_ACycle_Out;
		Json::Value jWPD_lCountCycle_Out;
		Json::Value jWPD_lCPG0SO2Learn_CyclesFound_Out;
		Json::Value jWPD_lCPG0SO2Learn_ACycle_Sam_Out;
		Json::Value jWPD_lCPG1SO2Learn_ACycle_Sam_Out;
		Json::Value jWPD_lCPG0SO2Learn_ACycle_Inter_Out;
		Json::Value jWPD_lCPG1SO2Learn_ACycle_Inter_Out;

		// Variable and vectors to be used together with json ------
		int WPD_kernelSize = 100; // If we use CPG = 1 Hz, it will repeat 10 steps for now. So, to avoid repeating kernel value, 100 kernels is max.
		// EXO:
			// Feedback signal - Right
		vector<double> WPD_rHipJ_FB_ZT_LowP;
		vector<double> WPD_rKneeJ_FB_ZT_LowP;
		vector<double> WPD_rAnkleJ_FB_ZT_LowP;
		vector<double> WPD_rHipJ_FB_ZT_LowP_Target;
		vector<double> WPD_rKneeJ_FB_ZT_LowP_Target;
		vector<double> WPD_rAnkleJ_FB_ZT_LowP_Target;
		vector<double> WPD_rHipJ_FB_ZT_LowP_Target_Sam = vector<double>(WPD_kernelSize, 0);
		vector<double> WPD_rKneeJ_FB_ZT_LowP_Target_Sam = vector<double>(WPD_kernelSize, 0); 
		vector<double> WPD_rAnkleJ_FB_ZT_LowP_Target_Sam = vector<double>(WPD_kernelSize, 0);
		// (vector of vector); row = rCountCycle filled with vector
		// = {	{0.0, 0.0, ... kernelSize},
		//		{0.0, 0.0, ... kernelSize},
		//		...
		//		rCountCycle
		//	}
		vector<vector<double>> WPD_rKernel;
		vector<double> WPD_rHipJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0); // {0.0, 0.0, ... kernelSize}
		vector<double> WPD_rKneeJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0);
		vector<double> WPD_rAnkleJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0);
			// Feedback signal - Left
		vector<double> WPD_lHipJ_FB_ZT_LowP;
		vector<double> WPD_lKneeJ_FB_ZT_LowP;
		vector<double> WPD_lAnkleJ_FB_ZT_LowP;
		vector<double> WPD_lHipJ_FB_ZT_LowP_Target;
		vector<double> WPD_lKneeJ_FB_ZT_LowP_Target;
		vector<double> WPD_lAnkleJ_FB_ZT_LowP_Target;
		vector<double> WPD_lHipJ_FB_ZT_LowP_Target_Sam = vector<double>(WPD_kernelSize, 0);
		vector<double> WPD_lKneeJ_FB_ZT_LowP_Target_Sam = vector<double>(WPD_kernelSize, 0); 
		vector<double> WPD_lAnkleJ_FB_ZT_LowP_Target_Sam = vector<double>(WPD_kernelSize, 0);
		vector<vector<double>> WPD_lKernel;
		vector<double> WPD_lHipJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0);
		vector<double> WPD_lKneeJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0);
		vector<double> WPD_lAnkleJ_FB_ZT_LowP_Weight_Row = vector<double>(WPD_kernelSize, 0);
			// Control signal - Right
		vector<double> WPD_rHipP_Control;
		vector<double> WPD_rKneeP_Control;
		vector<double> WPD_rAnkleP_Control;
		vector<double> WPD_rHipP_Control_Sam;
		vector<double> WPD_rKneeP_Control_Sam;
		vector<double> WPD_rAnkleP_Control_Sam;
			// Control signal - Left
		vector<double> WPD_lHipP_Control;
		vector<double> WPD_lKneeP_Control;
		vector<double> WPD_lAnkleP_Control;
		vector<double> WPD_lHipP_Control_Sam;
		vector<double> WPD_lKneeP_Control_Sam;
		vector<double> WPD_lAnkleP_Control_Sam;
		// CPG:
			// SO2 Learn
		string WPD_timeSO2LearnGen;
			// SO2 Learn - Right
		string WPD_rTimeSO2LearnPhaseZero;
		vector<double> WPD_rCPG0SO2Learn;
		vector<double> WPD_rCPG1SO2Learn;
		double WPD_rCPGFreqSO2Learn;
		vector<double> WPD_rCPG0SO2Learn_ACycle; // Original CPG
		vector<double> WPD_rCPG1SO2Learn_ACycle;
		int WPD_rCountCycle = 0; // Prog. step for 1 walking cycle. It depends on walking speed of subject
		int WPD_rCPG0SO2Learn_CyclesFound = 0;
		vector<double> WPD_rCPG0SO2Learn_ACycle_Sam = vector<double>(WPD_kernelSize, 0); // To store sampling CPG; row = kernelSize filled with all 0.0 = {0.0, 0.0, ... kernelSize}
		vector<double> WPD_rCPG1SO2Learn_ACycle_Sam = vector<double>(WPD_kernelSize, 0);
		vector<double> WPD_rCPG0SO2Learn_ACycle_Inter; // Interpolated CPG
		vector<double> WPD_rCPG1SO2Learn_ACycle_Inter;
			// SO2 Learn - Left
		string WPD_lTimeSO2LearnPhase180;	
		vector<double> WPD_lCPG0SO2Learn;
		vector<double> WPD_lCPG1SO2Learn;
		double WPD_lCPGFreqSO2Learn;
		vector<double> WPD_lCPG0SO2Learn_ACycle;
		vector<double> WPD_lCPG1SO2Learn_ACycle;
		int WPD_lCountCycle = 0;
		int WPD_lCPG0SO2Learn_CyclesFound = 0;
		vector<double> WPD_lCPG0SO2Learn_ACycle_Sam = vector<double>(WPD_kernelSize, 0);
		vector<double> WPD_lCPG1SO2Learn_ACycle_Sam = vector<double>(WPD_kernelSize, 0);
		vector<double> WPD_lCPG0SO2Learn_ACycle_Inter;
		vector<double> WPD_lCPG1SO2Learn_ACycle_Inter;
		// - To record Discrete Pattern Positon Calculation in JSON ---------


		// + To read Discrete Pattern Positon Calculation in JSON ----------------------------
		// patternDistResult.json
		// + JSON Reader -------------------
	    char jsonReadPatDist[100]; // To store path for read
		ifstream ifsjRPD;
		// - JSON Reader -------------------

		Json::Reader readerjRPD;
	    Json::Value objjRPD; // json obj/record

		// EXO:
			// Feedback signal - Right
		Json::Value jRPD_rHipJ_FB_ZT_LowP_Weight_In;
		Json::Value jRPD_rKneeJ_FB_ZT_LowP_Weight_In;
		Json::Value jRPD_rAnkleJ_FB_ZT_LowP_Weight_In;
			// Feedback signal - Left
		Json::Value jRPD_lHipJ_FB_ZT_LowP_Weight_In;
		Json::Value jRPD_lKneeJ_FB_ZT_LowP_Weight_In;
		Json::Value jRPD_lAnkleJ_FB_ZT_LowP_Weight_In;
		// CPG:
			// SO2 Learn - Right
		Json::Value jRPD_rMeanCPGTimeRW_In;
		Json::Value jRPD_rCPG0SO2Learn_ACycle_Sam_In;
		Json::Value jRPD_rCPG1SO2Learn_ACycle_Sam_In;
		Json::Value jRPD_rCPG0SO2Learn_ACycle_Inter_In;
		Json::Value jRPD_rCPG1SO2Learn_ACycle_Inter_In;
			// SO2 Learn - Left
		Json::Value jRPD_lMeanCPGTimeRW_In;
		Json::Value jRPD_lCPG0SO2Learn_ACycle_Sam_In;
		Json::Value jRPD_lCPG1SO2Learn_ACycle_Sam_In;
		Json::Value jRPD_lCPG0SO2Learn_ACycle_Inter_In;
		Json::Value jRPD_lCPG1SO2Learn_ACycle_Inter_In;

		// - To read Discrete Pattern Positon Calculation in JSON ----------------------------
		// ######## - For Discrete pattern ###########


		
		// -------------------------------
		// Mode toggle logic
		// -------------------------------
		bool basicControlFlag = false;
		bool adaptiveControlFlag = false;
		bool motorDisableFlag = false;
		bool motorStopFlag = false;
		bool checkFlag = false;
		bool rbfDistFlag = false;

		int modeMain;
		int modeSub;
		int modeSubSub;

		bool enterModeBasicAdapFirstTime = false;
		bool enterModeBaicZeroTorque = false;
		
		// ---------------
		// 1. Basic mode:
		// ---------------
		// 	1.1) Position control
		//		1.1.1) Default pattern
		//		1.1.2) Position setpoint
		//		1.1.3) Discrete pattern
		// 	1.2) Stiffness control
		//		1.2.2) Stiffness setpoint
		// 	1.3) Torque control
		//		1.3.1) Zero torque
		//		1.3.2) Torque control
		int basicModeSub = 1;
		int basicModeSubSub;

		bool initialST = true;
		bool firstST = true;

		int speedOld;
		int iFirstStep = 0;
		int iNormalStep = 0;

		// Initial stage
		vector<int> InitST = {0, 0, 6, 0, 0, 6};

		// First step gait pattern
		// (array of vector)
		vector<int> FirstStep[25] = { // {RHipFS, RKneeFS, RAnkleFS, LHipFS, LKneeFS, LAnkleFS}
				{2, 2, 4, 0, 0, 6},
				{4, 4, 2, 0, 0, 6},
				{7, 7, 0, 0, 0, 6},
				{10, 10, -2, 0, 0, 6},
				{12, 13, -5, 0, 0, 7},
				{14, 17, -7, 0, 0, 7},
				{16, 21, -9, 0, 0, 8},
				{18, 26, -10, 0, 0, 8},
				{20, 31, -11, -1, 0, 9},
				{21, 38, -12, -1, 0, 10},
				{22, 43, -12, -1, 1, 11},
				{23, 47, -12, -2, 1, 11},
				{24, 50, -11, -2, 1, 12},
				{25, 50, -8, -3, 2, 12},
				{26, 47, -3, -3, 2, 12},
				{27, 41, 1, -4, 2, 12},
				{27, 33, 5, -5, 3, 13},
				{27, 25, 7, -6, 3, 13},
				{28, 19, 8, -7, 3, 14},
				{28, 13, 8, -8, 4, 16},
				{29, 9, 7, -8, 4, 17},
				{29, 7, 6, -9, 4, 18},
				{30, 6, 6, -10, 5, 19},
				{30, 5, 6, -12, 5, 20},
				{30, 5, 5, -13, 5, 20}
		};

		// Normal gait pattern
		vector<int> NormalStep[51] = {
				{30, 3, 5, -13, 5, 20},
				{29, 2, 4, -13, 5, 20},
				{28, 2, 3, -14, 6, 19},
				{26, 1, 2, -14, 7, 17},
				{24, 1, 2, -14, 8, 15},
				{22, 1, 3, -15, 9, 12},
				{20, 0, 4, -15, 11, 9},
				{18, 0, 6, -14, 13, 4},
				{16, 0, 7, -13, 16, -1},
				{14, 0, 8, -11, 20, -7},
				{12, 0, 10, -8, 25, -11},
				{10, 0, 11, -5, 32, -13},
				{8, 0, 11, -1, 39, -12},
				{6, 0, 12, 4, 50, -8},
				{4, 0, 12, 8, 58, -3},
				{2, 0, 12, 12, 60, 1},
				{0, 0, 12, 16, 57, 5},
				{-2, 0, 13, 20, 48, 7},
				{-3, 0, 13, 23, 39, 8},
				{-5, 0, 14, 26, 31, 8},
				{-6, 0, 16, 28, 22, 7},
				{-8, 0, 17, 29, 14, 6},
				{-9, 1, 18, 30, 9, 6},
				{-10, 2, 19, 30, 7, 6},
				{-11, 3, 20, 30, 6, 6},
				{-12, 4, 20, 29, 5, 5},
				{-13, 5, 20, 28, 4, 4},
				{-14, 7, 20, 26, 3, 3},
				{-15, 9, 19, 24, 2, 3},
				{-15, 11, 17, 22, 2, 2},
				{-14, 14, 15, 20, 1, 3},
				{-14, 17, 12, 18, 1, 4},
				{-13, 21, 9, 16, 1, 6},
				{-12, 27, 4, 14, 0, 7},
				{-11, 33, -1, 12, 0, 8},
				{-9, 40, -7, 10, 0, 10},
				{-6, 49, -12, 8, 0, 11},
				{-2, 56, -14, 6, 0, 11},
				{2, 59, -12, 4, 0, 12},
				{6, 60, -8, 2, 0, 12},
				{10, 57, -3, 0, 0, 12},
				{13, 46, 1, -2, 0, 12},
				{16, 35, 5, -4, 0, 13},
				{18, 26, 7, -6, 0, 13},
				{21, 19, 8, -7, 0, 14},
				{24, 13, 8, -9, 0, 16},
				{26, 9, 7, -10, 1, 17},
				{28, 7, 6, -11, 1, 18},
				{29, 6, 6, -11, 2, 19},
				{30, 5, 6, -12, 3, 20},
				{30, 4, 6, -12, 4, 20}
		};

		// Position setpoint
		vector<int> PositionSetPoint = {0, 0, 0, 0, 0, 0}; // {RHip, RKnee, RAnkle, LHip, LKnee, LAnkle}

		// Stiffness setpoint
		vector<int> StiffnessSetPoint = {0, 0, 0, 0, 0, 0}; // {RHip, RKnee, RAnkle, LHip, LKnee, LAnkle}

		// Torque setpoint
		vector<int> TorqueSetPoint = {0, 0, 0, 0, 0, 0}; // {RHip, RKnee, RAnkle, LHip, LKnee, LAnkle}

		int iTorqueProfile = 0;
		vector<int> TorqueProfile[10] = {
			{0, 0, 0, 5, 0, 0},
			{0, 0, 0, 10, 0, 0},
			{0, 0, 0, 15, 0, 0},
			{0, 0, 0, 20, 0, 0},
			{0, 0, 0, 25, 0, 0},
			{0, 0, 0, 30, 0, 0},
			{0, 0, 0, 35, 0, 0},
			{0, 0, 0, 40, 0, 0},
			{0, 0, 0, 45, 0, 0},
			{0, 0, 0, 50, 0, 0}
		};

		// 1.1.3) Discrete pattern
		// Start counter for CPG ------------
		int rd_cntCPGAction = 0;

		int rd_speedOld;

		// ************
		// * Action
		// ************
		bool rd_actionStart = false;
		bool rd_actionLoadWeight = false;
		bool rd_actionCPGGen = false;
		bool rd_actionOnline = false;

		// Notification No.:
        // 1 = Caculation complete, wait user acknowledge, and start action
		int rd_notiNo = 0;

		// Right
		vector<double> rd_rKernelOnline = vector<double>(WPD_kernelSize, 0);
		vector<double> rd_rHipWeightOnline;
		vector<double> rd_rKneeWeightOnline;
		vector<double> rd_rAnkleWeightOnline;
		double rd_meanrCPGTimeRWOnline;
		int rd_rHipOnline;
		int rd_rKneeOnline;
		int rd_rAnkleOnline;
		// Left
		vector<double> rd_lKernelOnline = vector<double>(WPD_kernelSize, 0);
		vector<double> rd_lHipWeightOnline;
		vector<double> rd_lKneeWeightOnline;
		vector<double> rd_lAnkleWeightOnline;
		double rd_meanlCPGTimeRWOnline;
		int rd_lHipOnline;
		int rd_lKneeOnline;
		int rd_lAnkleOnline;

		vector<int> rd_adaptiveNormalOnline; // Normal pattern for online adaptation


		// ---------------
		// 2. Adaptive mode:
		// ---------------
		
		// 	2.1) Reflex and CPGs
		// Right
		bool rSwingFirst = false; // To allow the program to check GL/GR first
		bool rSwingFinish = false; // To prevent the program from stopping swing in the middle way
		// Left
		bool lSwingFirst = false; // To allow the program to check GL/GR first
		bool lSwingFinish = false; // To prevent the program from stopping swing in the middle way

		// RH's angle -> Stance phase
		// LH's angle -> Swing phase
		vector<int> NormalStepLSwing[30] = {
			{30, 3, 5, -13, 5, 20},
			{29, 2, 4, -13, 5, 20},
			{28, 2, 3, -14, 6, 19},
			{26, 1, 2, -14, 7, 17},
			{24, 1, 2, -14, 8, 15},
			{22, 1, 3, -15, 9, 12},
			{20, 0, 4, -15, 11, 9},
			{18, 0, 6, -14, 13, 4},
			{16, 0, 7, -13, 16, -1},
			{14, 0, 8, -11, 20, -7},
			{12, 0, 10, -8, 25, -11},
			{10, 0, 11, -5, 32, -13},
			{8, 0, 11, -1, 39, -12},
			{6, 0, 12, 4, 50, -8},
			{4, 0, 12, 8, 58, -3},
			{2, 0, 12, 12, 60, 1},
			{0, 0, 12, 16, 57, 5},
			{-2, 0, 13, 20, 48, 7},
			{-3, 0, 13, 23, 39, 8},
			{-5, 0, 14, 26, 31, 8},
			{-6, 0, 16, 28, 22, 7},
			{-8, 0, 17, 29, 14, 6},
			{-9, 1, 18, 30, 9, 6},
			{-10, 2, 19, 30, 7, 6},
			{-11, 3, 20, 30, 6, 6},
			{-12, 4, 20, 29, 5, 5},
			{-13, 5, 20, 28, 4, 4},
			{-14, 7, 20, 26, 3, 3},
			{-15, 9, 19, 24, 2, 3},
			{-15, 11, 17, 22, 2, 2}
		};

		// RH's angle -> Swing phase
		// LH's angle -> Stance phase
		vector<int> NormalStepRSwing[21] = {
			{-14, 14, 15, 20, 1, 3},
			{-14, 17, 12, 18, 1, 4},
			{-13, 21, 9, 16, 1, 6},
			{-12, 27, 4, 14, 0, 7},
			{-11, 33, -1, 12, 0, 8},
			{-9, 40, -7, 10, 0, 10},
			{-6, 49, -12, 8, 0, 11},
			{-2, 56, -14, 6, 0, 11},
			{2, 59, -12, 4, 0, 12},
			{6, 60, -8, 2, 0, 12},
			{10, 57, -3, 0, 0, 12},
			{13, 46, 1, -2, 0, 12},
			{16, 35, 5, -4, 0, 13},
			{18, 26, 7, -6, 0, 13},
			{21, 19, 8, -7, 0, 14},
			{24, 13, 8, -9, 0, 16},
			{26, 9, 7, -10, 1, 17},
			{28, 7, 6, -11, 1, 18},
			{29, 6, 6, -11, 2, 19},
			{30, 5, 6, -12, 3, 20},
			{30, 4, 6, -12, 4, 20}
		};


		// 2.2) Speed
		bool learnLoadSpeed = false;
		bool learnStartSpeed = false;
		// Right
		double meanrCPGFreqRW = 0;
		double meanrCPGTimeRW = 0;
		// Left
		double meanlCPGFreqRW = 0;
		double meanlCPGTimeRW = 0;

		double avgTimeBothLegs;


		// 2.3) Pattern
		// ************
		// * Learning
		// ************
			// 1. Retrieve data
		bool learnLoad = false;
	
			// 2. Extract CPG and jointAngle for a cycle
		bool checkJsonWritePat_1 = false;
		bool existPatternResult_1 = false; // Check if patternResult.json saved or not
		vector<double> rCPGFreqRW; // cpgFreq in real world
		vector<double> lCPGFreqRW; // cpgFreq in real world
		int endConverge; // The size of record
		int strConverge = 50000; // e.g. 60000
		int speedFound;
		
			// 2.3, 2.4
		bool learnrCPGGen = false;
		bool learnrCPGEnd = false;
		bool learnlCPGGen = false;
		bool learnlCPGEnd = false;
		bool learnrCPGCollect = false;
		bool learnlCPGCollect = false;
		vector<double> rCPG0_LearnTime = {0.0, 0.0};
		vector<double> lCPG0_LearnTime = {0.0, 0.0};
		vector<double> rCPG0_RealTime = {0.0, 0.0};
		vector<double> lCPG0_RealTime = {0.0, 0.0};

			// 2.5
		bool learnStart = false;
			
			// 2.6
		int cnt_rHip;
		int cnt_lHip;
		bool startRec_rHip = false;
		bool startRec_lHip = false;

			// 3. 
		int samplingStep; // 1 cycle length / kernelSize
		int nSam = 0; // Counter to store sampling CPG signal, Target

			// 4.
		double s = 0.01; // Using in RBF kernel

			// 4.2
		int nLearn = 0;
		int learnLoop_RBF = 400;
		int nSamResult = 0; // Counter to store sampling Result
		double alpha_RBF = 0.1; // Learning rate


		// ************
		// * Action
		// ************
		bool actionStart = false;
		bool actionLoadWeight = false;
		bool actionCPGGen = false;
		bool actionOnline = false;

		// Notification No.:
        // 1 = Caculation complete, wait user acknowledge, and start action
		int notiNo = 0;
		
		// Right
		vector<double> rKernelOnline = vector<double>(kernelSize, 0);
		vector<double> rHipWeightOnline;
		vector<double> rKneeWeightOnline;
		vector<double> rAnkleWeightOnline;
		double meanrCPGTimeRWOnline;
		int rHipOnline;
		int rKneeOnline;
		int rAnkleOnline;
		// Left
		vector<double> lKernelOnline = vector<double>(kernelSize, 0);
		vector<double> lHipWeightOnline;
		vector<double> lKneeWeightOnline;
		vector<double> lAnkleWeightOnline;
		double meanlCPGTimeRWOnline;
		int lHipOnline;
		int lKneeOnline;
		int lAnkleOnline;

		vector<int> adaptiveNormalOnline; // Normal pattern for online adaptation


		// ---------------
		// CPGs related
		// ---------------
		// Common ---------
		// bool  cpgStatus;
		int CPGphase;
		// double cpgFreqScale = 10; // This number is to convert cpgFreq = phi/2*pi to real world freq.
		double cpgFreqScale = 100; // (Work)
		double internalrFreqCPG; // Right; To set internal freq. when using as SO2
		double internallFreqCPG; // Left; To set internal freq. when using as SO2

		// AFDC -----------
//		double initFreqRAFDC = 0.1; // This is internal freq which is = 1 Hz real world
//		double initFreqRAFDC = 0.2; // Cannot be used
		double initFreqRAFDC = 0.01; // 0.1 Hz real world (without mod in update); 1 Hz real world (with mod in update)
		double initFreqLAFDC = 0.01;
		bool flagStrAFDC = false;

		// SO2 Learn ------
		double initFreqRSO2Learn; // Internal freq unit
		double initFreqLSO2Learn; // Internal freq unit
		bool flagStrSO2Learn = false;
		
		// SO2 Action -----
		double initFreqRSO2Action; // Internal freq unit
		double initFreqLSO2Action; // Internal freq unit
		bool flagStrSO2Action = false;
		bool flagUpdateSO2Action = false;

		// For adjusting freq of CPG ----
		bool setCurrentFreqFirstTime = false;
		double rCurrentFreq = 0.01; // Current freq. internal
		double lCurrentFreq = 0.01; // Current freq. internal
		double rOnlineFreq = 0.01; // Online freq. internal
		double lOnlineFreq = 0.01; // Online freq. internal
		double learningRateFreq = 1; // Learning rate to combine current and online freq
		int cnt_incFreq = 0; // To count timestep to increase CPG freq
		bool stopIncFreq = false;
		double freqManual ; // // Manual vs. Online freq; e.g., 0.5 Hz


		// 2.5) changeSpeed
		// ************
		// * Action
		// ************
		bool cs_actionLoadWeight = false;

		// ---------------
		// CPGs related
		// ---------------
		// For adjusting freq of CPG ----
		bool cs_setCurrentFreqFirstTime = false;


		// ---------------
		// 6. RBF discrete:
		// ---------------
		// ************
		// * Load
		// ************
			// 1. Retrieve data
		bool rd_learnLoad = false;

		double rd_avgTimeBothLegs;
		int rd_speedFound = 6;
		// Right
		double rd_meanrCPGFreqRW = 0;
		double rd_meanrCPGTimeRW = 0;
		// Left
		double rd_meanlCPGFreqRW = 0;
		double rd_meanlCPGTimeRW = 0;

			// 2. Extract CPG and jointAngle for a cycle
			// 2.1
		bool rd_checkJsonWritePatDist_1 = false;
		bool rd_existPatternDistResult_1 = false; // Check if patternResultDist.json saved or not
		int rd_endConverge; // The size of record
		int rd_strConverge = 0;

		// ************
		// * Learning
		// ************
			// 2.2, 2.3
		bool rd_learnrCPGGen = false;
		bool rd_learnrCPGEnd = false;
		bool rd_learnlCPGGen = false;
		bool rd_learnlCPGEnd = false;
		bool rd_learnrCPGCollect = false;
		bool rd_learnlCPGCollect = false;
		vector<double> rd_rCPG0_LearnTime = {0.0, 0.0};
		vector<double> rd_lCPG0_LearnTime = {0.0, 0.0};
		vector<double> rd_rCPG0_RealTime = {0.0, 0.0};
		vector<double> rd_lCPG0_RealTime = {0.0, 0.0};

		// 1 = check start, collect for a cycle (Normal way)
		// 2 = check start, collect multiple cycles equaling signal's length
		// 3 = no checking, collect multiple cycles equaling signal's length
		int rd_cpgCollectWay = 1;

		int rd_cntrCpgEnd = 0;
		int rd_cntlCpgEnd = 0;
		
			// 2.4
		bool rd_learnStart = false;
			// 3. 
		int rd_samplingStep_CPG;	
		int rd_samplingStep; // 1 cycle length / kernelSize
		int rd_nSamCPG = 0; // Counter to store sampling CPG signal, Target
		int rd_nSamTar = 0;
			// 4.
		double rd_s = 0.01; // Using in RBF kernel
			// 4.2
		int rd_nLearn = 0;
		int rd_learnLoop_RBF = 1000;
		int rd_nSamResult = 0; // Counter to store sampling Result
		double rd_alpha_RBF = 0.1; // Learning rate


		// ---------------
		// CPGs related
		// ---------------
		// Common ---------
// 		bool  rd_cpgStatus;
		int rd_CPGphase;
//		double rd_cpgFreqScale = 10; // This number is to convert cpgFreq = phi/2*pi to real world freq.
		double rd_cpgFreqScale = 100;
		// double rd_cpgFreqScale = 1000;
		double rd_internalrFreqCPG; // Right; To set internal freq. when using as SO2
		double rd_internallFreqCPG; // Left; To set internal freq. when using as SO2

		// SO2 Learn ------
		double rd_initFreqRSO2Learn; // Internal freq unit
		double rd_initFreqLSO2Learn; // Internal freq unit
		bool rd_flagStrSO2Learn = false;
		
		// SO2 Action -----
		double rd_initFreqRSO2Action; // Internal freq unit
		double rd_initFreqLSO2Action; // Internal freq unit
		bool rd_flagStrSO2Action = false;
		bool rd_flagUpdateSO2Action = false;

		// For adjusting freq of CPG ----
		bool rd_setCurrentFreqFirstTime = false;
		double rd_rCurrentFreq = 0.01; // Current freq. internal
		double rd_lCurrentFreq = 0.01; // Current freq. internal
		double rd_rOnlineFreq = 0.01; // Online freq. internal
		double rd_lOnlineFreq = 0.01; // Online freq. internal
		double rd_learningRateFreq = 1; // Learning rate to combine current and online freq
		int rd_cnt_incFreq = 0; // To count timestep to increase CPG freq
		bool rd_stopIncFreq = false;
		double rd_freqManual ; // // Manual vs. Online freq; e.g., 0.5 Hz

		





		// ----------------------------------------------
		// Private Methods
		// ----------------------------------------------
		bool basicControl(int basicModeSub, int basicModeSubSub, int speed, int assist);
		bool adaptiveControl(int adaptiveModeSub, int speed, int assist);
		bool motorDisable();
		bool motorStop();

		
		void jsonReadZeroTorqueFn();
		void jsonWritePatFn();
		void jsonReadPatFn();
		void jsonWritePatDistFn();
		void jsonReadPatDistFn();
		void showData();
		bool discretePattern(ExvisParallelRealROS * realRos, int speed, int assist);
		bool speedAdapt(ExvisParallelRealROS * realRos, int speed, int assist);
		bool patternAdapt(ExvisParallelRealROS * realRos, int speed, int assist);
		bool checkStart(double data_i_1, int i, double data, vector<double> * cycleG, int & countCycle);
		bool checkStart180(double data_i_1, int i, double data, vector<double> * cycleG, int & countCycle);
		bool checkStartOnline(double data_i_1, double data);
		bool checkStartOnline180(double data_i_1, double data);
		bool exists_File(const std::string & name);
		bool changeSpeed(ExvisParallelRealROS * realRos, int speed, int assist);
		bool rbfDist(ExvisParallelRealROS * realRos, int speed, int assist);
		
};


#endif //EXVISPARALLELREALASSEM_H
