/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealNotiROS.h"

//***************************************************************************

ExvisParallelRealNotiROS::ExvisParallelRealNotiROS(int argc, char **argv) {
    // Create a ROS nodes
    int _argc = 0;
    char** _argv = NULL;
    ros::init(_argc,_argv,"ExvisParallelRealNotiAssem");

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("ExvisParallelRealNotiROS just started!");

    // Initialize Subscribers
    timeStampCANSub = node.subscribe("/exvis/timeStampCAN", 1, &ExvisParallelRealNotiROS::timeStampCAN_CB, this);
    
    notiNoSub = node.subscribe("/exvis/noti/notiNo", 1, &ExvisParallelRealNotiROS::notiNo_CB, this);

    // Initialize Publishers
    notiNo_1_DonePub = node.advertise<std_msgs::Bool>("/exvis/noti/notiNo_1_Done",1);




    // Set Rate
    timeStep = 0.001; // sec (0.001 s = 1 ms)
    rate = new ros::Rate(1/timeStep);
//    rate = new ros::Rate(100);
//    rate = new ros::Rate(17*4); // 60hz
//    rate = new ros::Rate(50);
//    rate = new ros::Rate(29);
//    rate = new ros::Rate(11.1);
//    rate = new ros::Rate(10);
//    rate = new ros::Rate(1);
//    rate = new ros::Rate(0.1);
}

// **********************************************************
// Subscriber callback
// **********************************************************
// Read data ------------------
void ExvisParallelRealNotiROS::timeStampCAN_CB(const std_msgs::String& _timeStampCAN) {

    // Get the data from Topic into main node variable
	timeStampCAN = _timeStampCAN.data;
	// cout << "timeStampCAN: " << timeStampCAN << endl;
}

void ExvisParallelRealNotiROS::notiNo_CB(const std_msgs::Int32& _notiNo) {

    // Get the data from Topic into main node variable
	notiNo = _notiNo.data;
	cout << "notiNo: " << notiNo << endl;
}


// **********************************************************
// Publisher send function
// **********************************************************
void ExvisParallelRealNotiROS::sendNotiDone(bool notiNoDone, int notiNumber) {
	std_msgs::Bool notiNoDoneP;
	notiNoDoneP.data = notiNoDone;

    switch (notiNumber) {
        case 1 :
	        notiNo_1_DonePub.publish(notiNoDoneP);
	        cout << "notiNo_1_Done: " << notiNoDone << " " << "... notiNoDone Published !" << endl;
    }
}


// **********************************************************
// rosSpinOnce
// **********************************************************
void ExvisParallelRealNotiROS::rosSpinOnce(){

	// cout << "ExvisParallelRealNotiROS node is spinning" << endl;

	ros::spinOnce();
    bool rateMet = rate->sleep();

    if(!rateMet)
    {
        ROS_ERROR("Sleep rate not met");
    }

}

ExvisParallelRealNotiROS::~ExvisParallelRealNotiROS() {
    ROS_INFO("ExvisParallelRealNotiROS just terminated!");
    ros::shutdown();
}