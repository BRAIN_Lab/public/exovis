/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealCPGAssem.h"

//***************************************************************************

//--------------------------------------------------------------------------
// initializer
//--------------------------------------------------------------------------
ExvisParallelRealCPGAssem::ExvisParallelRealCPGAssem(int argc,char* argv[]){

	// ROS related
    realRos = new ExvisParallelRealCPGROS(argc, argv);


	if(ros::ok()) {
    	cout << "Initialize ExvisParallelRealAssem" << endl;
    }

}

//--------------------------------------------------------------------------
// runAssem
//--------------------------------------------------------------------------
bool ExvisParallelRealCPGAssem::runAssem() {

	if(ros::ok()) {
		cout << "********************************************************" << endl;
		cout << "ExvisParallelRealCPGAssem is running" << endl;
		cout << "********************************************************" << endl;
		cout << nodeCounter << endl;


		// + Pub/Sub area --------------------------------
		cout << "--------------- Published ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tPublished: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		
		cout << "--------------- AFDC Online ---------------" << endl;
		if (realRos->flagStrAFDC) {
			CPGphase = 0;
			dinRightAFDC = new DynamicCpg(realRos->initFreqRAFDC*2*M_PI, "DinRight", oneCPG, CPGphase, "AFDC", "Online");
			CPGphase = 0;
			dinLeftAFDC = new DynamicCpg(realRos->initFreqLAFDC*2*M_PI, "DinLeft", oneCPG, CPGphase, "AFDC", "Online");
			CPGphase = 180;
			dinLeft180AFDC = new DynamicCpg(realRos->initFreqRAFDC*2*M_PI, "DinLeft", oneCPG, CPGphase, "AFDC", "Online");

			fbOld = 0;
			fbNew = 0;

			applyPer = false;
			checkStartPer = true;

			flagAssemStrAFDC = true;
			realRos->flagStrAFDC = false;
		}
		// cout << "realRos->flagStrAFDC: " << realRos->flagStrAFDC << endl;

		if (flagAssemStrAFDC) {
			if (realRos->flagUpdateAFDC) {
				// Update from input freq, it overrides from feedback
				// ...(not using for now)...
			} else {
				// Update from feedback
				flagAssemUpdateAFDC = updateAFDC(realRos->jointAngle[0], realRos->jointAngle[3], nodeCounter, realRos->timeStep);
				cout << "flagAssemUpdateAFDC: " << flagAssemUpdateAFDC << endl;
			}

			// Publish AFDC Right
			realRos->sendLegFreq(rightLegFreqAFDC, "R", "AFDC", "Online");
			realRos->sendCpgO0(rightCpgO0AFDC, "R", "AFDC", "Online");
			realRos->sendCpgO1(rightCpgO1AFDC, "R", "AFDC", "Online");
			realRos->sendCpgO2(rightCpgO2AFDC, "R", "AFDC", "Online");
			// Publish AFDC Left
			realRos->sendLegFreq(leftLegFreqAFDC, "L", "AFDC", "Online");
			realRos->sendCpgO0(leftCpgO0AFDC, "L", "AFDC", "Online");
			realRos->sendCpgO1(leftCpgO1AFDC, "L", "AFDC", "Online");
			realRos->sendCpgO2(leftCpgO2AFDC, "L", "AFDC", "Online");
			
		}
		

		cout << "--------------- SO2 Learn ---------------" << endl;
		if (realRos->flagStrSO2Learn) {
			CPGphase = 0;
			dinRightSO2Learn = new DynamicCpg(realRos->initFreqRSO2Learn*2*M_PI, "DinRight", oneCPG, CPGphase, "SO2", "Learn");
			CPGphase = 0;
			dinLeftSO2Learn = new DynamicCpg(realRos->initFreqLSO2Learn*2*M_PI, "DinLeft", oneCPG, CPGphase, "SO2", "Learn");
			CPGphase = 180;
			dinLeft180SO2Learn = new DynamicCpg(realRos->initFreqLSO2Learn*2*M_PI, "DinLeft", oneCPG, CPGphase, "SO2", "Learn");

			flagAssemStrSO2Learn = true;
			realRos->flagStrSO2Learn = false;
		}

		// cout << "realRos->flagStrSO2Learn: " << realRos->flagStrSO2Learn << endl;

		if (flagAssemStrSO2Learn) {

			if (realRos->flagUpdateSO2Learn) {
				// Update from external node input freq 
				// Put here means it overrides the internal freq
				internalrFreqCPG = realRos->updateFreqRSO2Learn;
				internallFreqCPG = realRos->updateFreqLSO2Learn;

			} else {
				// If there is no update, us the current freq
				internalrFreqCPG = dinRightSO2Learn->getCpg()->getFreq();
				internallFreqCPG = dinLeftSO2Learn->getCpg()->getFreq();
			}
			CPGphase = 180;
			flagAssemUpdateSO2Learn = updateSO2Learn(internalrFreqCPG, internallFreqCPG, nodeCounter, realRos->timeStep, CPGphase);
			cout << "flagAssemUpdateSO2Learn: " << flagAssemUpdateSO2Learn << endl;

			

			// Publish SO2 Learn Right
			realRos->sendLegFreq(rightLegFreqSO2Learn, "R", "SO2", "Learn");
			realRos->sendCpgO0(rightCpgO0SO2Learn, "R", "SO2", "Learn");
			realRos->sendCpgO1(rightCpgO1SO2Learn, "R", "SO2", "Learn");
			realRos->sendCpgO2(rightCpgO2SO2Learn, "R", "SO2", "Learn");		
			// Publish SO2 Learn Left
			realRos->sendLegFreq(leftLegFreqSO2Learn, "L", "SO2", "Learn");
			realRos->sendCpgO0(leftCpgO0SO2Learn, "L", "SO2", "Learn");
			realRos->sendCpgO1(leftCpgO1SO2Learn, "L", "SO2", "Learn");
			realRos->sendCpgO2(leftCpgO2SO2Learn, "L", "SO2", "Learn");
		}

		
		
		cout << "--------------- SO2 Action ---------------" << endl;
		if (realRos->flagStrSO2Action) {
			CPGphase = 0;
			dinRightSO2Action = new DynamicCpg(realRos->initFreqRSO2Action*2*M_PI, "DinRight", oneCPG, CPGphase, "SO2", "Action");
			CPGphase = 0;
			dinLeftSO2Action = new DynamicCpg(realRos->initFreqLSO2Action*2*M_PI, "DinLeft", oneCPG, CPGphase, "SO2", "Action");
			CPGphase = 180;
			dinLeft180SO2Action = new DynamicCpg(realRos->initFreqLSO2Action*2*M_PI, "DinLeft", oneCPG, CPGphase, "SO2", "Action");

			flagAssemStrSO2Action = true;
			realRos->flagStrSO2Action = false;
		}

		// cout << "realRos->flagStrSO2Action: " << realRos->flagStrSO2Action << endl;

		if (flagAssemStrSO2Action) {
			
			if (realRos->flagUpdateSO2Action) {
				// Update from external node input freq 
				// Put here means it overrides the internal freq
				internalrFreqCPG = realRos->updateFreqRSO2Action;
				internallFreqCPG = realRos->updateFreqLSO2Action;

			} else {
				// Update from internal node input freq
				// internalrFreqCPG = meanrCPGFreqRW/cpgFreqScale; // From learning step; Real world freq -> Internal freq
				// internallFreqCPG = meanlCPGFreqRW/cpgFreqScale;
			}
			CPGphase = 180;
			flagAssemUpdateSO2Action = updateSO2Action(internalrFreqCPG, internallFreqCPG, nodeCounter, realRos->timeStep, CPGphase);
			cout << "flagAssemUpdateSO2Action: " << flagAssemUpdateSO2Action << endl;

			// Publish SO2 Learn Right
			realRos->sendLegFreq(rightLegFreqSO2Action, "R", "SO2", "Action");
			realRos->sendCpgO0(rightCpgO0SO2Action, "R", "SO2", "Action");
			realRos->sendCpgO1(rightCpgO1SO2Action, "R", "SO2", "Action");
			realRos->sendCpgO2(rightCpgO2SO2Action, "R", "SO2", "Action");		
			// Publish SO2 Learn Left
			realRos->sendLegFreq(leftLegFreqSO2Action, "L", "SO2", "Action");
			realRos->sendCpgO0(leftCpgO0SO2Action, "L", "SO2", "Action");
			realRos->sendCpgO1(leftCpgO1SO2Action, "L", "SO2", "Action");
			realRos->sendCpgO2(leftCpgO2SO2Action, "L", "SO2", "Action");

		}

		// - Pub/Sub area --------------------------------
		
		

		// + Node spin to have sub value --------------------------------
		cout << "--------------- Subscribed ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tSubscribed: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->rosSpinOnce();
		// - Node spin to have sub value --------------------------------

		nodeCounter++;
		cout << "********************************************************" << endl;
		return true;

	} else {
		cout << "Shutting down the node" << endl;
		return false;
	}
}


//--------------------------------------------------------------------------
// AFDC
//--------------------------------------------------------------------------
bool ExvisParallelRealCPGAssem::updateAFDC(int rightHip, int leftHip, int nodeCounter, double timeStep) {

	if(oneCPG == true) { //using only one CPG in the controller
		cout <<"---------One CPGs---------"<< endl;

	} else { //using two cpgs

		cout <<"---------Two CPGs---------"<< endl;

		if (checkStartPer) {
			// Compute derivative of feedback signal
			// So, we will use this to detect the starting of the walking period

			fbNew = rightHip;
//			fbNew = leftHip;
			fbDerVal = fbNew - fbOld;
			fbOld = fbNew;

			if (fbDerVal > 0) { // Positive derivative
				fbDerValCounter += 1;
			}

			// Positive derivative more than xxx steps
			// 200 steps ~ 3 Walking step
			// 100 steps ~ 1 Walking step

			if (fbDerValCounter >= 100) { 
				applyPer = true;
				checkStartPer = false;
			}
		}



		// Right CPG controlling right leg
		CPGphase = 0;
		rightLegFreqAFDC = dinRightAFDC->generateOutputOneLeg(rightHip, nodeCounter, timeStep, CPGphase, applyPer);
		rightCpgO0AFDC = dinRightAFDC->getCpgO0();
		rightCpgO1AFDC = dinRightAFDC->getCpgO1();
		rightCpgO2AFDC = dinRightAFDC->getCpgO2();

		// Left CPG controlling left leg
		CPGphase = 180;
		leftLegFreqAFDC = dinLeftAFDC->generateOutputOneLeg(leftHip, nodeCounter, timeStep, CPGphase, applyPer);
		leftCpgO0AFDC = dinLeftAFDC->getCpgO0();
		leftCpgO1AFDC = dinLeftAFDC->getCpgO1();
		leftCpgO2AFDC = dinLeftAFDC->getCpgO2();


	} // Two CPGs

	return true;
}

//--------------------------------------------------------------------------
// SO2
//--------------------------------------------------------------------------
bool ExvisParallelRealCPGAssem::updateSO2Learn(double intrFreqCPG, double intlFreqCPG, int nodeCounter, double timeStep, int CPGphase) {

	// Right
	dinRightSO2Learn->soTwoMode(intrFreqCPG*2*M_PI, nodeCounter, timeStep);

	rightLegFreqSO2Learn = intrFreqCPG;
	rightCpgO0SO2Learn = dinRightSO2Learn->getCpgO0();
	rightCpgO1SO2Learn = dinRightSO2Learn->getCpgO1();
	rightCpgO2SO2Learn = dinRightSO2Learn->getCpgO2();

	// Left
	if (CPGphase == 0) {

		dinLeftSO2Learn->soTwoMode(intlFreqCPG*2*M_PI, nodeCounter, timeStep);

		leftLegFreqSO2Learn = intlFreqCPG;
		leftCpgO0SO2Learn = dinLeftSO2Learn->getCpgO0();
		leftCpgO1SO2Learn = dinLeftSO2Learn->getCpgO1();
		leftCpgO2SO2Learn = dinLeftSO2Learn->getCpgO2();

	} else if (CPGphase == 180) {

		dinLeft180SO2Learn->soTwoMode(intlFreqCPG*2*M_PI, nodeCounter, timeStep);

		leftLegFreqSO2Learn = intlFreqCPG;
		leftCpgO0SO2Learn = dinLeft180SO2Learn->getCpgO0();
		leftCpgO1SO2Learn = dinLeft180SO2Learn->getCpgO1();
		leftCpgO2SO2Learn = dinLeft180SO2Learn->getCpgO2();

	}

	return true;
}

bool ExvisParallelRealCPGAssem::updateSO2Action(double intrFreqCPG, double intlFreqCPG, int nodeCounter, double timeStep, int CPGphase) {

	// Right
	dinRightSO2Action->soTwoMode(intrFreqCPG*2*M_PI, nodeCounter, timeStep);

	rightLegFreqSO2Action = intrFreqCPG;
	rightCpgO0SO2Action = dinRightSO2Action->getCpgO0();
	rightCpgO1SO2Action = dinRightSO2Action->getCpgO1();
	rightCpgO2SO2Action = dinRightSO2Action->getCpgO2();

	// Left
	if (CPGphase == 0) {

		dinLeftSO2Action->soTwoMode(intlFreqCPG*2*M_PI, nodeCounter, timeStep);

		leftLegFreqSO2Action = intlFreqCPG;
		leftCpgO0SO2Action = dinLeftSO2Action->getCpgO0();
		leftCpgO1SO2Action = dinLeftSO2Action->getCpgO1();
		leftCpgO2SO2Action = dinLeftSO2Action->getCpgO2();

	} else if (CPGphase == 180) {

		dinLeft180SO2Action->soTwoMode(intlFreqCPG*2*M_PI, nodeCounter, timeStep);

		leftLegFreqSO2Action = intlFreqCPG;
		leftCpgO0SO2Action = dinLeft180SO2Action->getCpgO0();
		leftCpgO1SO2Action = dinLeft180SO2Action->getCpgO1();
		leftCpgO2SO2Action = dinLeft180SO2Action->getCpgO2();

	}

	return true;
}



ExvisParallelRealCPGAssem::~ExvisParallelRealCPGAssem(){

}
