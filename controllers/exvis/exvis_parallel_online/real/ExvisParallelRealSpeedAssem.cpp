/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealSpeedAssem.h"

//***************************************************************************

//--------------------------------------------------------------------------
// initializer
//--------------------------------------------------------------------------
ExvisParallelRealSpeedAssem::ExvisParallelRealSpeedAssem(int argc,char* argv[]) {

	// ROS related
    realRos = new ExvisParallelRealSpeedROS(argc, argv);

    if(ros::ok()) {
    	cout << "Initialize ExvisParallelRealSpeedAssem" << endl;
    }

}

//--------------------------------------------------------------------------
// runAssem
//--------------------------------------------------------------------------
bool ExvisParallelRealSpeedAssem::runAssem() {
	if(ros::ok()) {
		cout << "********************************************************" << endl;
		cout << "ExvisParallelRealSpeedAssem is running" << endl;
		cout << "********************************************************" << endl;
		cout << nodeCounter << endl;
		
		printf("Please select SPEED: \n");
		printf("1. Speed 1 \n");
		printf("2. Speed 2 \n");
		printf("3. Speed 3 \n");
		printf("4. Speed 4 \n");
		printf("5. Speed 5 \n");
		printf("6. Speed 6 \n");
		printf("7. Speed 7 \n");
		printf("8. Speed 8 \n");
		printf("9. Speed 9 \n");
		printf("10. Speed 10 \n");
		
		while (true){
			cin >> speed;
			if ((!cin.fail()) && (speed >= 1 && speed <= 10)) {
				break;
			} else {
				cin.clear();
				cin.ignore();
			}
		}

		cout << "--------------- Published ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tPublished: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->sendSpeed(speed);
		


		// + Node spin to have sub value --------------------------------
		cout << "--------------- Subscribed ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tSubscribed: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->rosSpinOnce();
		// - Node spin to have sub value --------------------------------

		nodeCounter++;
		cout << "********************************************************" << endl;
		return true;
		
	} else {
		cout << "Shutting down the node" << endl;
		return false;
	}
}

ExvisParallelRealSpeedAssem::~ExvisParallelRealSpeedAssem() {
	
}
