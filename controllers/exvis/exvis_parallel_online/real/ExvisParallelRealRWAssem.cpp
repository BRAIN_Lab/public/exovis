/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealRWAssem.h"

//***************************************************************************

//--------------------------------------------------------------------------
// initializer
//--------------------------------------------------------------------------
ExvisParallelRealRWAssem::ExvisParallelRealRWAssem(int argc, char* argv[]){

	// ROS related
    realRos = new ExvisParallelRealRWROS(argc, argv);

	// Initialize save file
	if ((homedir = getenv("HOME")) == NULL) {
		homedir = getpwuid(getuid())->pw_dir;
	}


	// + Exo and CPG signals in JSON -------------------------------
	// Open: When start mode 1 or 2 first time
	// Close: Close node
	strcpy(jsonWriteSignal, homedir);
	strcat(jsonWriteSignal, "/Experiment/exvisResult/exvisJSONSignal.json");
	// - Exo and CPG signals in JSON -------------------------------


	// + Info. for ZeroTorque in JSON ------------------------------
	// Open: When start mode 131
	// Close: Motor disable, motor stop, close node
	// JSON write
	strcpy(jsonWriteZeroTorque, homedir);
	strcat(jsonWriteZeroTorque, "/Experiment/exvisResult/exvisJSONZeroTorque.json");
	// - Info. for ZeroTorque in JSON ------------------------------


	// // + Info. for AdapPatPos during Action in JSON ---------------------------
	// // JSON write
	// strcpy(jsonWriteAdapPatPosAction, homedir);
	// strcat(jsonWriteAdapPatPosAction, "/Experiment/exvisResult/exvisJSONAdapPatPosAction.json");
	// // - Info. for AdapPat during Action in JSON ---------------------------


	if(ros::ok()) {
    	cout << "Initialize ExvisParallelRealRWAssem" << endl;
    }

}

//--------------------------------------------------------------------------
// runAssem
//--------------------------------------------------------------------------
bool ExvisParallelRealRWAssem::runAssem() {

	if(ros::ok()) {
		cout << "********************************************************" << endl;
		cout << "ExvisParallelRealRWAssem is running" << endl;
		cout << "********************************************************" << endl;
		cout << nodeCounter << endl;


		// + Pub/Sub area --------------------------------
		cout << "--------------- Published ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tPublished: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		
		if (realRos->mode.size() == 1) {
			modeMain = (int)(realRos->mode.at(0)) - 48;
		} else if (realRos->mode.size() == 2) {
			modeMain = (int)(realRos->mode.at(0)) - 48;
			modeSub = (int)(realRos->mode.at(1)) - 48;
		} else if (realRos->mode.size() == 3) {
			modeMain = (int)(realRos->mode.at(0)) - 48;
			modeSub = (int)(realRos->mode.at(1)) - 48;
			modeSubSub = (int)(realRos->mode.at(2) - 48);
		}

		// Check flag ===========================================================
		cout << "flagStrAFDC: " << realRos->flagStrAFDC << endl;
		cout << "flagStrSO2Action: " << realRos->flagStrSO2Action << endl;
		cout << "flagStrSO2ActionPhaseZero: " << realRos->flagStrSO2ActionPhaseZero << endl;



		// Record data ==========================================================
		// exvisJSONSignal.json ------------
		if (!enterModeBasicAdapFirstTime){
			// 3 = Motor disable first time to start record data in order to check signals 
			if ((modeMain == 1) || (modeMain == 2) || (modeMain == 3)) {
				enterModeBasicAdapFirstTime = true;
			}
		}

		if (enterModeBasicAdapFirstTime) {
			// // Check file existence
			// existJsonWriteSignal = exists_File(jsonWriteSignal); // Force to overwritten existing file

			if (!openJsonWriteSignal) {
				ofsjWS.open(jsonWriteSignal, ofstream::trunc); // Create file, close when close node.
				openJsonWriteSignal = true;
			}

			logJsonWriteSignalFn();
		}



		// exvisJSONZeroTorque.json ------------
		if ((modeMain == 1) && (modeSub == 3) && (modeSubSub == 1)) {
			enterModeBaicZeroTorque = true;
		}
		
		if (enterModeBaicZeroTorque) {
			// Check file existence
			// existJsonWriteZeroTorque = exists_File(jsonWriteZeroTorque); Force to overwritten existing file

			if (!openJsonWriteZeroTorque){
				ofsjWZT.open(jsonWriteZeroTorque, ofstream::trunc); // Create file, close when close node, motor disable, motor stop
				openJsonWriteZeroTorque = true;
			}

			logJsonWriteZeroTorqueFn();

		} else {
			// Reset ----------
			// Allow to open file again
			openJsonWriteZeroTorque = false; 
			
			// Clear vector and Json
			clearVarZeroTorqueFn();

		}



		// Motor disable and Motor Stop --------
		if (enterModeBaicZeroTorque && (modeMain == 3 || modeMain == 4)) { 
			if (ofsjWZT.is_open()) {
				// Write and close file
				jsonWriteZeroTorqueFn();
				ofsjWZT.close();
				cout << "Save file to: " << jsonWriteZeroTorque << endl;
				enterModeBaicZeroTorque = false;
			}
		}


		

		// - Pub/Sub area --------------------------------
		

		// + Node spin to have sub value --------------------------------
		cout << "--------------- Subscribed ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tSubscribed: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->rosSpinOnce();
		// - Node spin to have sub value --------------------------------

		nodeCounter++;
		cout << "********************************************************" << endl;
		return true;

	} else {
		cout << "Shutting down the node" << endl;
		return false;
	}
}


//--------------------------------------------------------------------------
// To write data into JSON format and save to file
//--------------------------------------------------------------------------
void ExvisParallelRealRWAssem::jsonWriteSignalFn(){
	cout << "----------------- WRITE JSON SIGNAL ------------------" << endl;
	// TIME:
	objjWS[0]["EXO"][0]["nodeCounter"] = jWS_nodeCounter_Out;
	objjWS[0]["EXO"][0]["timeStamp"] = jWS_timeStampCAN_Out;
	// UI:
	objjWS[0]["EXO"][0]["mode"] = jWS_mode_Out;
	objjWS[0]["EXO"][0]["typeCon"] = jWS_typeCon_Out;
	objjWS[0]["EXO"][0]["speed"] = jWS_speedCon_Out;
	objjWS[0]["EXO"][0]["assist"] = jWS_assistCon_Out;
	objjWS[0]["EXO"][0]["notiNo"] = jWS_notiNo_Out;
	// EXO:
		// Hip
	objjWSExoHip[0]["right"][0]["feedback"][0]["joint"] = jWS_rHipJ_FB_Out;
    objjWSExoHip[0]["right"][0]["feedback"][0]["torque"] = jWS_rHipT_FB_Out;
    objjWSExoHip[0]["right"][0]["feedback"][0]["motor"] = jWS_rHipM_FB_Out;
	objjWSExoHip[0]["left"][0]["feedback"][0]["joint"] = jWS_lHipJ_FB_Out;
    objjWSExoHip[0]["left"][0]["feedback"][0]["torque"] = jWS_lHipT_FB_Out;
    objjWSExoHip[0]["left"][0]["feedback"][0]["motor"] = jWS_lHipM_FB_Out;
    objjWSExoHip[0]["right"][0]["control"][0]["position"] = jWS_rHipP_Ctrl_Out;
    objjWSExoHip[0]["right"][0]["control"][0]["torque"] = jWS_rHipT_Ctrl_Out;
    objjWSExoHip[0]["right"][0]["control"][0]["stiffness"] = jWS_rHipS_Ctrl_Out;
	objjWSExoHip[0]["left"][0]["control"][0]["position"] = jWS_lHipP_Ctrl_Out;
    objjWSExoHip[0]["left"][0]["control"][0]["torque"] = jWS_lHipT_Ctrl_Out;
    objjWSExoHip[0]["left"][0]["control"][0]["stiffness"] = jWS_lHipS_Ctrl_Out;
		// Knee
	objjWSExoKnee[0]["right"][0]["feedback"][0]["joint"] = jWS_rKneeJ_FB_Out;
    objjWSExoKnee[0]["right"][0]["feedback"][0]["torque"] = jWS_rKneeT_FB_Out;
    objjWSExoKnee[0]["right"][0]["feedback"][0]["motor"] = jWS_rKneeM_FB_Out;
	objjWSExoKnee[0]["left"][0]["feedback"][0]["joint"] = jWS_lKneeJ_FB_Out;
    objjWSExoKnee[0]["left"][0]["feedback"][0]["torque"] = jWS_lKneeT_FB_Out;
    objjWSExoKnee[0]["left"][0]["feedback"][0]["motor"] = jWS_lKneeM_FB_Out;
    objjWSExoKnee[0]["right"][0]["control"][0]["position"] = jWS_rKneeP_Ctrl_Out;
    objjWSExoKnee[0]["right"][0]["control"][0]["torque"] = jWS_rKneeT_Ctrl_Out;
    objjWSExoKnee[0]["right"][0]["control"][0]["stiffness"] = jWS_rKneeS_Ctrl_Out;
	objjWSExoKnee[0]["left"][0]["control"][0]["position"] = jWS_lKneeP_Ctrl_Out;
    objjWSExoKnee[0]["left"][0]["control"][0]["torque"] = jWS_lKneeT_Ctrl_Out;
    objjWSExoKnee[0]["left"][0]["control"][0]["stiffness"] = jWS_lKneeS_Ctrl_Out;
		// Ankle 
	objjWSExoAnkle[0]["right"][0]["feedback"][0]["joint"] = jWS_rAnkleJ_FB_Out;
    objjWSExoAnkle[0]["right"][0]["feedback"][0]["torque"] = jWS_rAnkleT_FB_Out;
    objjWSExoAnkle[0]["right"][0]["feedback"][0]["motor"] = jWS_rAnkleM_FB_Out;
	objjWSExoAnkle[0]["left"][0]["feedback"][0]["joint"] = jWS_lAnkleJ_FB_Out;
    objjWSExoAnkle[0]["left"][0]["feedback"][0]["torque"] = jWS_lAnkleT_FB_Out;
    objjWSExoAnkle[0]["left"][0]["feedback"][0]["motor"] = jWS_lAnkleM_FB_Out;
    objjWSExoAnkle[0]["right"][0]["control"][0]["position"] = jWS_rAnkleP_Ctrl_Out;
    objjWSExoAnkle[0]["right"][0]["control"][0]["torque"] = jWS_rAnkleT_Ctrl_Out;
    objjWSExoAnkle[0]["right"][0]["control"][0]["stiffness"] = jWS_rAnkleS_Ctrl_Out;
	objjWSExoAnkle[0]["left"][0]["control"][0]["position"] = jWS_lAnkleP_Ctrl_Out;
    objjWSExoAnkle[0]["left"][0]["control"][0]["torque"] = jWS_lAnkleT_Ctrl_Out;
    objjWSExoAnkle[0]["left"][0]["control"][0]["stiffness"] = jWS_lAnkleS_Ctrl_Out;
		// Foot
	objjWSExoFoot[0]["right"][0]["heel"] = jWS_rFHeel_FB_Out;
	objjWSExoFoot[0]["left"][0]["heel"] = jWS_lFHeel_FB_Out;
    objjWSExoFoot[0]["right"][0]["toe"] = jWS_rFToe_FB_Out;
	objjWSExoFoot[0]["left"][0]["toe"] = jWS_lFToe_FB_Out;
	// Combine
	objjWS[0]["EXO"][0]["hip"] = objjWSExoHip;
	objjWS[0]["EXO"][0]["knee"] = objjWSExoKnee;
	objjWS[0]["EXO"][0]["ankle"] = objjWSExoAnkle;
    objjWS[0]["EXO"][0]["foot"] = objjWSExoFoot;
	
	// CPG
		// AFDC
	objjWSCPGAFDC[0]["right"][0]["nodeCounterStr"] = jWS_nodeAFDCGen_Out;
	objjWSCPGAFDC[0]["right"][0]["timeStampStr"] = jWS_timeAFDCGen_Out;
	objjWSCPGAFDC[0]["right"][0]["freq"] = jWS_rCPGFreqAFDC_Out;
	objjWSCPGAFDC[0]["right"][0]["CPG0"] = jWS_rCPG0AFDC_Out;
	objjWSCPGAFDC[0]["right"][0]["CPG1"] = jWS_rCPG1AFDC_Out;
	objjWSCPGAFDC[0]["right"][0]["CPG2"] = jWS_rCPG2AFDC_Out;
	objjWSCPGAFDC[0]["left"][0]["nodeCounterStr"] = jWS_nodeAFDCGen_Out;
	objjWSCPGAFDC[0]["left"][0]["timeStampStr"] = jWS_timeAFDCGen_Out;
	objjWSCPGAFDC[0]["left"][0]["freq"] = jWS_lCPGFreqAFDC_Out;
	objjWSCPGAFDC[0]["left"][0]["CPG0"] = jWS_lCPG0AFDC_Out;
	objjWSCPGAFDC[0]["left"][0]["CPG1"] = jWS_lCPG1AFDC_Out;
	objjWSCPGAFDC[0]["left"][0]["CPG2"] = jWS_lCPG2AFDC_Out;
		// SO2Action
	objjWSCPGSO2Action[0]["right"][0]["nodeCounterStr"] = jWS_nodeSO2ActionGen_Out;
	objjWSCPGSO2Action[0]["right"][0]["timeStampStr"] = jWS_timeSO2ActionGen_Out;
	objjWSCPGSO2Action[0]["right"][0]["nodeCounterPhaseZero"] = jWS_nodeSO2ActionPhaseZero_Out;
	objjWSCPGSO2Action[0]["right"][0]["timeStampPhaseZero"] = jWS_timeSO2ActionPhaseZero_Out;
	objjWSCPGSO2Action[0]["right"][0]["freq"] = jWS_rCPGFreqSO2Action_Out;
	objjWSCPGSO2Action[0]["right"][0]["CPG0"] = jWS_rCPG0SO2Action_Out;
	objjWSCPGSO2Action[0]["right"][0]["CPG1"] = jWS_rCPG1SO2Action_Out;
	objjWSCPGSO2Action[0]["right"][0]["CPG2"] = jWS_rCPG2SO2Action_Out;
	objjWSCPGSO2Action[0]["left"][0]["nodeCounterStr"] = jWS_nodeSO2ActionGen_Out;
	objjWSCPGSO2Action[0]["left"][0]["timeStampStr"] = jWS_timeSO2ActionGen_Out;
	objjWSCPGSO2Action[0]["left"][0]["nodeCounterPhaseZero"] = jWS_nodeSO2ActionPhaseZero_Out;
	objjWSCPGSO2Action[0]["left"][0]["timeStampPhaseZero"] = jWS_timeSO2ActionPhaseZero_Out;
	objjWSCPGSO2Action[0]["left"][0]["freq"] = jWS_lCPGFreqSO2Action_Out;
	objjWSCPGSO2Action[0]["left"][0]["CPG0"] = jWS_lCPG0SO2Action_Out;
	objjWSCPGSO2Action[0]["left"][0]["CPG1"] = jWS_lCPG1SO2Action_Out;
	objjWSCPGSO2Action[0]["left"][0]["CPG2"] = jWS_lCPG2SO2Action_Out;
	// Combine
	objjWS[0]["CPG"][0]["AFDC"] = objjWSCPGAFDC;
	objjWS[0]["CPG"][0]["SO2Action"] = objjWSCPGSO2Action;

	// For exvisJSONSignal.json
	if (ofsjWS.is_open()){
		ofsjWS << objjWS;
	}

}

//--------------------------------------------------------------------------
// To record data into j-array in each loop
//--------------------------------------------------------------------------
void ExvisParallelRealRWAssem::logJsonWriteSignalFn(){

    // + Store data in vector (just in case we want to use it) ----
    // TIME:
	nodeCounter_Out.push_back(nodeCounter);
    timeStampCAN_Out.push_back(realRos->timeStampCAN);
	// UI:
	mode_Out.push_back(realRos->mode);
	typeCon_Out.push_back(realRos->typeCon);
	speedCon_Out.push_back(realRos->speedCon);
	assistCon_Out.push_back(realRos->assistCon);
	notiNo_Out.push_back(realRos->notiNo);
	// EXO:
		// Feedback - Right
	rHipJ_FB_Out.push_back(realRos->jointAngle[0]);
	rKneeJ_FB_Out.push_back(realRos->jointAngle[1]);
	rAnkleJ_FB_Out.push_back(realRos->jointAngle[2]);
	rHipT_FB_Out.push_back(realRos->jointTorque[0]);
	rKneeT_FB_Out.push_back(realRos->jointTorque[1]);
	rAnkleT_FB_Out.push_back(realRos->jointTorque[2]);
	rHipM_FB_Out.push_back(realRos->motorTorque[0]);
	rKneeM_FB_Out.push_back(realRos->motorTorque[1]);
	rAnkleM_FB_Out.push_back(realRos->motorTorque[2]);
	rFHeel_FB_Out.push_back(realRos->footSwitch[0]);
	rFToe_FB_Out.push_back(realRos->footSwitch[1]);
		// Feedback - Left
	lHipJ_FB_Out.push_back(realRos->jointAngle[3]);
	lKneeJ_FB_Out.push_back(realRos->jointAngle[4]);
	lAnkleJ_FB_Out.push_back(realRos->jointAngle[5]);
	lHipT_FB_Out.push_back(realRos->jointTorque[3]);
	lKneeT_FB_Out.push_back(realRos->jointTorque[4]);
	lAnkleT_FB_Out.push_back(realRos->jointTorque[5]);
	lHipM_FB_Out.push_back(realRos->motorTorque[3]);
	lKneeM_FB_Out.push_back(realRos->motorTorque[4]);
	lAnkleM_FB_Out.push_back(realRos->motorTorque[5]);
	lFHeel_FB_Out.push_back(realRos->footSwitch[2]);
	lFToe_FB_Out.push_back(realRos->footSwitch[3]);
		// Control - Right
	rHipP_Ctrl_Out.push_back(realRos->jointAngleCon[0]);
	rKneeP_Ctrl_Out.push_back(realRos->jointAngleCon[1]);
	rAnkleP_Ctrl_Out.push_back(realRos->jointAngleCon[2]);
	rHipT_Ctrl_Out.push_back(realRos->torqueCon[0]);
	rKneeT_Ctrl_Out.push_back(realRos->torqueCon[1]);
	rAnkleT_Ctrl_Out.push_back(realRos->torqueCon[2]);
	rHipS_Ctrl_Out.push_back(realRos->stiffnessCon[0]);
	rKneeS_Ctrl_Out.push_back(realRos->stiffnessCon[1]);
	rAnkleS_Ctrl_Out.push_back(realRos->stiffnessCon[2]);
		// Control - Left
	lHipP_Ctrl_Out.push_back(realRos->jointAngleCon[3]);
	lKneeP_Ctrl_Out.push_back(realRos->jointAngleCon[4]);
	lAnkleP_Ctrl_Out.push_back(realRos->jointAngleCon[5]);
	lHipT_Ctrl_Out.push_back(realRos->torqueCon[3]);
	lKneeT_Ctrl_Out.push_back(realRos->torqueCon[4]);
	lAnkleT_Ctrl_Out.push_back(realRos->torqueCon[5]);
	lHipS_Ctrl_Out.push_back(realRos->stiffnessCon[3]);
	lKneeS_Ctrl_Out.push_back(realRos->stiffnessCon[4]);
	lAnkleS_Ctrl_Out.push_back(realRos->stiffnessCon[5]);
	// CPG:
		// AFDC
	if (!flagLogRecordStrAFDC && realRos->flagStrAFDC) {
		nodeAFDCGen_Out = nodeCounter;
		timeAFDCGen_Out = realRos->timeStampCAN;
	}
		// AFDC - Right
	rCPG0AFDC_Out.push_back(realRos->rightCpgO0AFDC); // push data one by one to vector
	rCPG1AFDC_Out.push_back(realRos->rightCpgO1AFDC);
	rCPG2AFDC_Out.push_back(realRos->rightCpgO2AFDC);
	rCPGFreqAFDC_Out.push_back(realRos->rightLegFreqAFDC);
		// AFDC - Left
	lCPG0AFDC_Out.push_back(realRos->leftCpgO0AFDC); // push data one by one to vector
	lCPG1AFDC_Out.push_back(realRos->leftCpgO1AFDC);
	lCPG2AFDC_Out.push_back(realRos->leftCpgO2AFDC);
	lCPGFreqAFDC_Out.push_back(realRos->leftLegFreqAFDC);
		// SO2
	if (!flagStrSO2ActionFristTime) { // Record NaN when not start SO2 Action first time
		if (!flagRecodSO2ActionFristTime) { // Record only once
			// SO2 Action
			nodeSO2ActionGen_Out = 0;
			timeSO2ActionGen_Out = "NaN";
			nodeSO2ActionPhaseZero_Out = 0;
			timeSO2ActionPhaseZero_Out = "NaN";
			// SO2 Action - Right
			rCPG0SO2Action_Out.push_back(nan(" ")); // null
			rCPG1SO2Action_Out.push_back(nan(" "));
			rCPG2SO2Action_Out.push_back(nan(" "));
			rCPGFreqSO2Action_Out.push_back(nan(" "));
			// SO2 Action - Left
			lCPG0SO2Action_Out.push_back(nan(" "));
			lCPG1SO2Action_Out.push_back(nan(" "));
			lCPG2SO2Action_Out.push_back(nan(" "));
			lCPGFreqSO2Action_Out.push_back(nan(" "));
		}

	} else { // Overwrite NaN by recording normally
		
		if (!flagLogRecordStrSO2Action) { // Record only once
			nodeSO2ActionGen_Out = nodeCounter;
			timeSO2ActionGen_Out = realRos->timeStampCAN;

			// + Clear NaN from variables -------------
			// SO2 Action - Right
			rCPG0SO2Action_Out.clear();
			rCPG1SO2Action_Out.clear();
			rCPG2SO2Action_Out.clear();
			rCPGFreqSO2Action_Out.clear();
			// SO2 Action - Left
			lCPG0SO2Action_Out.clear();
			lCPG1SO2Action_Out.clear();
			lCPG2SO2Action_Out.clear();
			lCPGFreqSO2Action_Out.clear();
			// - Clear NaN from variables -------------
		}

		if (realRos->flagStrSO2ActionPhaseZero && !flagLogStrSO2ActionPhaseZero) { // Flag from RealAssem, Record only once
			nodeSO2ActionPhaseZero_Out = nodeCounter;
			timeSO2ActionPhaseZero_Out = realRos->timeStampCAN;
		}


			// SO2 Action - Right
		rCPG0SO2Action_Out.push_back(realRos->rightCpgO0SO2Action);
		rCPG1SO2Action_Out.push_back(realRos->rightCpgO1SO2Action);
		rCPG2SO2Action_Out.push_back(realRos->rightCpgO2SO2Action);
		rCPGFreqSO2Action_Out.push_back(realRos->rightLegFreqSO2Action);
			// SO2 Action - Left
		lCPG0SO2Action_Out.push_back(realRos->leftCpgO0SO2Action);
		lCPG1SO2Action_Out.push_back(realRos->leftCpgO1SO2Action);
		lCPG2SO2Action_Out.push_back(realRos->leftCpgO2SO2Action);
		lCPGFreqSO2Action_Out.push_back(realRos->leftLegFreqSO2Action);
	
	}
	// - Store data in vector (just in case we want to use it) ----


	// + For Json -----------------------------------
	// Append element by element to json obj (array)
	if (!clearJArray) {
		// TIME:
		jWS_nodeCounter_Out.clear();
		jWS_timeStampCAN_Out.clear();		
		// UI:
		jWS_mode_Out.clear();
		jWS_typeCon_Out.clear(); 
		jWS_speedCon_Out.clear(); 
		jWS_assistCon_Out.clear();
		jWS_notiNo_Out.clear(); 
		// EXO:
			// Feedback signal - Right
		jWS_rHipJ_FB_Out.clear(); 
		jWS_rKneeJ_FB_Out.clear(); 
		jWS_rAnkleJ_FB_Out.clear(); 
		jWS_rHipT_FB_Out.clear(); 
		jWS_rKneeT_FB_Out.clear(); 
		jWS_rAnkleT_FB_Out.clear(); 
		jWS_rHipM_FB_Out.clear(); 
		jWS_rKneeM_FB_Out.clear(); 
		jWS_rAnkleM_FB_Out.clear(); 
		jWS_rFHeel_FB_Out.clear(); 
		jWS_rFToe_FB_Out.clear(); 
			// Feedback signal - Left
		jWS_lHipJ_FB_Out.clear(); 
		jWS_lKneeJ_FB_Out.clear(); 
		jWS_lAnkleJ_FB_Out.clear(); 
		jWS_lHipT_FB_Out.clear(); 
		jWS_lKneeT_FB_Out.clear(); 
		jWS_lAnkleT_FB_Out.clear(); 
		jWS_lHipM_FB_Out.clear(); 
		jWS_lKneeM_FB_Out.clear(); 
		jWS_lAnkleM_FB_Out.clear();
		jWS_lFHeel_FB_Out.clear(); 
		jWS_lFToe_FB_Out.clear(); 
			// Control signal - Right
		jWS_rHipP_Ctrl_Out.clear();
		jWS_rKneeP_Ctrl_Out.clear();
		jWS_rAnkleP_Ctrl_Out.clear(); 
		jWS_rHipT_Ctrl_Out.clear();
		jWS_rKneeT_Ctrl_Out.clear();
		jWS_rAnkleT_Ctrl_Out.clear();
		jWS_rHipS_Ctrl_Out.clear();
		jWS_rKneeS_Ctrl_Out.clear();
		jWS_rAnkleS_Ctrl_Out.clear();	
			// Control signal - Left
		jWS_lHipP_Ctrl_Out.clear();
		jWS_lKneeP_Ctrl_Out.clear(); 
		jWS_lAnkleP_Ctrl_Out.clear(); 
		jWS_lHipT_Ctrl_Out.clear(); 
		jWS_lKneeT_Ctrl_Out.clear(); 
		jWS_lAnkleT_Ctrl_Out.clear(); 
		jWS_lHipS_Ctrl_Out.clear(); 
		jWS_lKneeS_Ctrl_Out.clear(); 
		jWS_lAnkleS_Ctrl_Out.clear();
		// CPG:
			// AFDC
		jWS_nodeAFDCGen_Out.clear(); 
		jWS_timeAFDCGen_Out.clear(); 
			// AFDC - Right
		jWS_rCPG0AFDC_Out.clear();
		jWS_rCPG1AFDC_Out.clear();
		jWS_rCPG2AFDC_Out.clear();
		jWS_rCPGFreqAFDC_Out.clear();
			// AFDC - Left
		jWS_lCPG0AFDC_Out.clear();
		jWS_lCPG1AFDC_Out.clear();
		jWS_lCPG2AFDC_Out.clear();
		jWS_lCPGFreqAFDC_Out.clear();
			// SO2 
		jWS_nodeSO2ActionGen_Out.clear(); 
		jWS_timeSO2ActionGen_Out.clear(); 
		jWS_nodeSO2ActionPhaseZero_Out.clear(); 
		jWS_timeSO2ActionPhaseZero_Out.clear(); 
			// SO2 Action - Right
		jWS_rCPG0SO2Action_Out.clear();
		jWS_rCPG1SO2Action_Out.clear();
		jWS_rCPG2SO2Action_Out.clear();
		jWS_rCPGFreqSO2Action_Out.clear();
			// SO2 Action - Left
		jWS_lCPG0SO2Action_Out.clear();
		jWS_lCPG1SO2Action_Out.clear();
		jWS_lCPG2SO2Action_Out.clear();
		jWS_lCPGFreqSO2Action_Out.clear();

		clearJArray = true;
	}

	
	if (ofsjWS.is_open() && clearJArray){

		// TIME:
		jWS_nodeCounter_Out.append(nodeCounter_Out.back());
		jWS_timeStampCAN_Out.append(Json::Value(timeStampCAN_Out.back()));		
		// UI:
		jWS_mode_Out.append(mode_Out.back());
		jWS_typeCon_Out.append(typeCon_Out.back());
		jWS_speedCon_Out.append(speedCon_Out.back()); 
		jWS_assistCon_Out.append(assistCon_Out.back());
		jWS_notiNo_Out.append(notiNo_Out.back()); 
		// EXO:
			// Feedback signal - Right
		jWS_rHipJ_FB_Out.append(rHipJ_FB_Out.back()); 
		jWS_rKneeJ_FB_Out.append(rKneeJ_FB_Out.back()); 
		jWS_rAnkleJ_FB_Out.append(rAnkleJ_FB_Out.back()); 
		jWS_rHipT_FB_Out.append(rHipT_FB_Out.back()); 
		jWS_rKneeT_FB_Out.append(rKneeT_FB_Out.back()); 
		jWS_rAnkleT_FB_Out.append(rAnkleT_FB_Out.back()); 
		jWS_rHipM_FB_Out.append(rHipM_FB_Out.back()); 
		jWS_rKneeM_FB_Out.append(rKneeM_FB_Out.back()); 
		jWS_rAnkleM_FB_Out.append(rAnkleM_FB_Out.back()); 
		jWS_rFHeel_FB_Out.append(rFHeel_FB_Out.back()); 
		jWS_rFToe_FB_Out.append(rFToe_FB_Out.back()); 
			// Feedback signal - Left
		jWS_lHipJ_FB_Out.append(lHipJ_FB_Out.back()); 
		jWS_lKneeJ_FB_Out.append(lKneeJ_FB_Out.back()); 
		jWS_lAnkleJ_FB_Out.append(lAnkleJ_FB_Out.back()); 
		jWS_lHipT_FB_Out.append(lHipT_FB_Out.back()); 
		jWS_lKneeT_FB_Out.append(lKneeT_FB_Out.back()); 
		jWS_lAnkleT_FB_Out.append(lAnkleT_FB_Out.back()); 
		jWS_lHipM_FB_Out.append(lHipM_FB_Out.back()); 
		jWS_lKneeM_FB_Out.append(lKneeM_FB_Out.back()); 
		jWS_lAnkleM_FB_Out.append(lAnkleM_FB_Out.back());
		jWS_lFHeel_FB_Out.append(lFHeel_FB_Out.back()); 
		jWS_lFToe_FB_Out.append(lFToe_FB_Out.back()); 
			// Control signal - Right
		jWS_rHipP_Ctrl_Out.append(rHipP_Ctrl_Out.back());
		jWS_rKneeP_Ctrl_Out.append(rKneeP_Ctrl_Out.back());
		jWS_rAnkleP_Ctrl_Out.append(rAnkleP_Ctrl_Out.back()); 
		jWS_rHipT_Ctrl_Out.append(rHipT_Ctrl_Out.back());
		jWS_rKneeT_Ctrl_Out.append(rKneeT_Ctrl_Out.back());
		jWS_rAnkleT_Ctrl_Out.append(rAnkleT_Ctrl_Out.back());
		jWS_rHipS_Ctrl_Out.append(rHipS_Ctrl_Out.back());
		jWS_rKneeS_Ctrl_Out.append(rKneeS_Ctrl_Out.back());
		jWS_rAnkleS_Ctrl_Out.append(rAnkleS_Ctrl_Out.back());	
			// Control signal - Left
		jWS_lHipP_Ctrl_Out.append(lHipP_Ctrl_Out.back());
		jWS_lKneeP_Ctrl_Out.append(lKneeP_Ctrl_Out.back()); 
		jWS_lAnkleP_Ctrl_Out.append(lAnkleP_Ctrl_Out.back()); 
		jWS_lHipT_Ctrl_Out.append(lHipT_Ctrl_Out.back()); 
		jWS_lKneeT_Ctrl_Out.append(lKneeT_Ctrl_Out.back()); 
		jWS_lAnkleT_Ctrl_Out.append(lAnkleT_Ctrl_Out.back()); 
		jWS_lHipS_Ctrl_Out.append(lHipS_Ctrl_Out.back()); 
		jWS_lKneeS_Ctrl_Out.append(lKneeS_Ctrl_Out.back()); 
		jWS_lAnkleS_Ctrl_Out.append(lAnkleS_Ctrl_Out.back());
		// CPG:
			// AFDC
		if (!flagLogRecordStrAFDC && realRos->flagStrAFDC) {
			jWS_nodeAFDCGen_Out.append(nodeAFDCGen_Out); 
			jWS_timeAFDCGen_Out.append(Json::Value(timeAFDCGen_Out));
			flagLogRecordStrAFDC = true;
		}
			// AFDC - Right
		jWS_rCPG0AFDC_Out.append(rCPG0AFDC_Out.back());
		jWS_rCPG1AFDC_Out.append(rCPG1AFDC_Out.back());
		jWS_rCPG2AFDC_Out.append(rCPG2AFDC_Out.back());
		jWS_rCPGFreqAFDC_Out.append(rCPGFreqAFDC_Out.back());
			// AFDC - Left
		jWS_lCPG0AFDC_Out.append(lCPG0AFDC_Out.back());
		jWS_lCPG1AFDC_Out.append(lCPG1AFDC_Out.back());
		jWS_lCPG2AFDC_Out.append(lCPG2AFDC_Out.back());
		jWS_lCPGFreqAFDC_Out.append(lCPGFreqAFDC_Out.back());
			// SO2
		if (!flagStrSO2ActionFristTime) { // Record NaN when not start SO2 Action first time
			if (!flagRecodSO2ActionFristTime) { // Record only once
					// SO2 Action
				jWS_nodeSO2ActionGen_Out.append(nodeSO2ActionGen_Out); 
				jWS_timeSO2ActionGen_Out.append(Json::Value(timeSO2ActionGen_Out));
				jWS_nodeSO2ActionPhaseZero_Out.append(nodeSO2ActionPhaseZero_Out); 
				jWS_timeSO2ActionPhaseZero_Out.append(Json::Value(timeSO2ActionPhaseZero_Out)); 
					// SO2 Action - Right
				jWS_rCPG0SO2Action_Out.append(rCPG0SO2Action_Out.back());
				jWS_rCPG1SO2Action_Out.append(rCPG1SO2Action_Out.back());
				jWS_rCPG2SO2Action_Out.append(rCPG2SO2Action_Out.back());
				jWS_rCPGFreqSO2Action_Out.append(rCPGFreqSO2Action_Out.back());
					// SO2 Action - Left
				jWS_lCPG0SO2Action_Out.append(lCPG0SO2Action_Out.back());
				jWS_lCPG1SO2Action_Out.append(lCPG1SO2Action_Out.back());
				jWS_lCPG2SO2Action_Out.append(lCPG2SO2Action_Out.back());
				jWS_lCPGFreqSO2Action_Out.append(lCPGFreqSO2Action_Out.back());

				flagRecodSO2ActionFristTime = true;
			}
			if (realRos->flagStrSO2Action) { // Flag from RealAssem
				flagStrSO2ActionFristTime = true;
			}
		} else { // Overwrite NaN by recording normally

			if (!flagLogRecordStrSO2Action) { // Record only once

				// + Clear NaN from Json -------------
				jWS_nodeSO2ActionGen_Out.clear();
				jWS_timeSO2ActionGen_Out.clear();
				// - Clear NaN from Json -------------

				jWS_nodeSO2ActionGen_Out.append(nodeSO2ActionGen_Out); 
				jWS_timeSO2ActionGen_Out.append(Json::Value(timeSO2ActionGen_Out));


				// + Clear NaN from Json -------------
				jWS_rCPG0SO2Action_Out.clear();
				jWS_rCPG1SO2Action_Out.clear();
				jWS_rCPG2SO2Action_Out.clear();
				jWS_rCPGFreqSO2Action_Out.clear();
				jWS_lCPG0SO2Action_Out.clear();
				jWS_lCPG1SO2Action_Out.clear();
				jWS_lCPG2SO2Action_Out.clear();
				jWS_lCPGFreqSO2Action_Out.clear();
				// - Clear NaN from Json -------------
				
				flagLogRecordStrSO2Action = true;
			}

			if (realRos->flagStrSO2ActionPhaseZero && !flagLogStrSO2ActionPhaseZero) { // Flag from RealAssem, Record only once

				// + Clear NaN from Json -------------
				jWS_nodeSO2ActionPhaseZero_Out.clear();
				jWS_timeSO2ActionPhaseZero_Out.clear();
				// - Clear NaN from Json -------------

				jWS_nodeSO2ActionPhaseZero_Out.append(nodeSO2ActionPhaseZero_Out); 
				jWS_timeSO2ActionPhaseZero_Out.append(Json::Value(timeSO2ActionPhaseZero_Out));
			
				flagLogStrSO2ActionPhaseZero = true;
			}

				// SO2 Action - Right
			jWS_rCPG0SO2Action_Out.append(rCPG0SO2Action_Out.back());
			jWS_rCPG1SO2Action_Out.append(rCPG1SO2Action_Out.back());
			jWS_rCPG2SO2Action_Out.append(rCPG2SO2Action_Out.back());
			jWS_rCPGFreqSO2Action_Out.append(rCPGFreqSO2Action_Out.back());
				// SO2 Action - Left
			jWS_lCPG0SO2Action_Out.append(lCPG0SO2Action_Out.back());
			jWS_lCPG1SO2Action_Out.append(lCPG1SO2Action_Out.back());
			jWS_lCPG2SO2Action_Out.append(lCPG2SO2Action_Out.back());
			jWS_lCPGFreqSO2Action_Out.append(lCPGFreqSO2Action_Out.back());

		}
	}
}

void ExvisParallelRealRWAssem::jsonWriteZeroTorqueFn(){
	cout << "----------------- WRITE JSON ZERO TORQUE ------------------" << endl;
	// TIME:
	objjWZT[0]["EXO"][0]["nodeCounter"] = jWZT_nodeCounter_Out;
	objjWZT[0]["EXO"][0]["timeStamp"] = jWZT_timeStampCAN_Out;
	// UI:
	objjWZT[0]["EXO"][0]["mode"] = jWZT_mode_Out;
	objjWZT[0]["EXO"][0]["typeCon"] = jWZT_typeCon_Out;
	objjWZT[0]["EXO"][0]["speed"] = jWZT_speedCon_Out;
	objjWZT[0]["EXO"][0]["assist"] = jWZT_assistCon_Out;
	objjWZT[0]["EXO"][0]["notiNo"] = jWZT_notiNo_Out;
	// EXO:
		// Hip
	objjWZTExoHip[0]["right"][0]["feedback"][0]["joint"] = jWZT_rHipJ_FB_Out;
    objjWZTExoHip[0]["right"][0]["feedback"][0]["torque"] = jWZT_rHipT_FB_Out;
    objjWZTExoHip[0]["right"][0]["feedback"][0]["motor"] = jWZT_rHipM_FB_Out;
	objjWZTExoHip[0]["left"][0]["feedback"][0]["joint"] = jWZT_lHipJ_FB_Out;
    objjWZTExoHip[0]["left"][0]["feedback"][0]["torque"] = jWZT_lHipT_FB_Out;
    objjWZTExoHip[0]["left"][0]["feedback"][0]["motor"] = jWZT_lHipM_FB_Out;
    objjWZTExoHip[0]["right"][0]["control"][0]["position"] = jWZT_rHipP_Ctrl_Out;
    objjWZTExoHip[0]["right"][0]["control"][0]["torque"] = jWZT_rHipT_Ctrl_Out;
    objjWZTExoHip[0]["right"][0]["control"][0]["stiffness"] = jWZT_rHipS_Ctrl_Out;
	objjWZTExoHip[0]["left"][0]["control"][0]["position"] = jWZT_lHipP_Ctrl_Out;
    objjWZTExoHip[0]["left"][0]["control"][0]["torque"] = jWZT_lHipT_Ctrl_Out;
    objjWZTExoHip[0]["left"][0]["control"][0]["stiffness"] = jWZT_lHipS_Ctrl_Out;
		// Knee
	objjWZTExoKnee[0]["right"][0]["feedback"][0]["joint"] = jWZT_rKneeJ_FB_Out;
    objjWZTExoKnee[0]["right"][0]["feedback"][0]["torque"] = jWZT_rKneeT_FB_Out;
    objjWZTExoKnee[0]["right"][0]["feedback"][0]["motor"] = jWZT_rKneeM_FB_Out;
	objjWZTExoKnee[0]["left"][0]["feedback"][0]["joint"] = jWZT_lKneeJ_FB_Out;
    objjWZTExoKnee[0]["left"][0]["feedback"][0]["torque"] = jWZT_lKneeT_FB_Out;
    objjWZTExoKnee[0]["left"][0]["feedback"][0]["motor"] = jWZT_lKneeM_FB_Out;
    objjWZTExoKnee[0]["right"][0]["control"][0]["position"] = jWZT_rKneeP_Ctrl_Out;
    objjWZTExoKnee[0]["right"][0]["control"][0]["torque"] = jWZT_rKneeT_Ctrl_Out;
    objjWZTExoKnee[0]["right"][0]["control"][0]["stiffness"] = jWZT_rKneeS_Ctrl_Out;
	objjWZTExoKnee[0]["left"][0]["control"][0]["position"] = jWZT_lKneeP_Ctrl_Out;
    objjWZTExoKnee[0]["left"][0]["control"][0]["torque"] = jWZT_lKneeT_Ctrl_Out;
    objjWZTExoKnee[0]["left"][0]["control"][0]["stiffness"] = jWZT_lKneeS_Ctrl_Out;
		// Ankle 
	objjWZTExoAnkle[0]["right"][0]["feedback"][0]["joint"] = jWZT_rAnkleJ_FB_Out;
    objjWZTExoAnkle[0]["right"][0]["feedback"][0]["torque"] = jWZT_rAnkleT_FB_Out;
    objjWZTExoAnkle[0]["right"][0]["feedback"][0]["motor"] = jWZT_rAnkleM_FB_Out;
	objjWZTExoAnkle[0]["left"][0]["feedback"][0]["joint"] = jWZT_lAnkleJ_FB_Out;
    objjWZTExoAnkle[0]["left"][0]["feedback"][0]["torque"] = jWZT_lAnkleT_FB_Out;
    objjWZTExoAnkle[0]["left"][0]["feedback"][0]["motor"] = jWZT_lAnkleM_FB_Out;
    objjWZTExoAnkle[0]["right"][0]["control"][0]["position"] = jWZT_rAnkleP_Ctrl_Out;
    objjWZTExoAnkle[0]["right"][0]["control"][0]["torque"] = jWZT_rAnkleT_Ctrl_Out;
    objjWZTExoAnkle[0]["right"][0]["control"][0]["stiffness"] = jWZT_rAnkleS_Ctrl_Out;
	objjWZTExoAnkle[0]["left"][0]["control"][0]["position"] = jWZT_lAnkleP_Ctrl_Out;
    objjWZTExoAnkle[0]["left"][0]["control"][0]["torque"] = jWZT_lAnkleT_Ctrl_Out;
    objjWZTExoAnkle[0]["left"][0]["control"][0]["stiffness"] = jWZT_lAnkleS_Ctrl_Out;
		// Foot
	objjWZTExoFoot[0]["right"][0]["heel"] = jWZT_rFHeel_FB_Out;
	objjWZTExoFoot[0]["left"][0]["heel"] = jWZT_lFHeel_FB_Out;
    objjWZTExoFoot[0]["right"][0]["toe"] = jWZT_rFToe_FB_Out;
	objjWZTExoFoot[0]["left"][0]["toe"] = jWZT_lFToe_FB_Out;
	// Combine
	objjWZT[0]["EXO"][0]["hip"] = objjWZTExoHip;
	objjWZT[0]["EXO"][0]["knee"] = objjWZTExoKnee;
	objjWZT[0]["EXO"][0]["ankle"] = objjWZTExoAnkle;
    objjWZT[0]["EXO"][0]["foot"] = objjWZTExoFoot;
	
	// CPG
		// AFDC
	// objjWZTCPGAFDC[0]["right"][0]["nodeCounterStr"] = jWZT_nodeAFDCGen_Out;
	// objjWZTCPGAFDC[0]["right"][0]["timeStampStr"] = jWZT_timeAFDCGen_Out;
	objjWZTCPGAFDC[0]["right"][0]["freq"] = jWZT_rCPGFreqAFDC_Out;
	objjWZTCPGAFDC[0]["right"][0]["CPG0"] = jWZT_rCPG0AFDC_Out;
	objjWZTCPGAFDC[0]["right"][0]["CPG1"] = jWZT_rCPG1AFDC_Out;
	objjWZTCPGAFDC[0]["right"][0]["CPG2"] = jWZT_rCPG2AFDC_Out;
	// objjWZTCPGAFDC[0]["left"][0]["nodeCounterStr"] = jWZT_nodeAFDCGen_Out;
	// objjWZTCPGAFDC[0]["left"][0]["timeStampStr"] = jWZT_timeAFDCGen_Out;
	objjWZTCPGAFDC[0]["left"][0]["freq"] = jWZT_lCPGFreqAFDC_Out;
	objjWZTCPGAFDC[0]["left"][0]["CPG0"] = jWZT_lCPG0AFDC_Out;
	objjWZTCPGAFDC[0]["left"][0]["CPG1"] = jWZT_lCPG1AFDC_Out;
	objjWZTCPGAFDC[0]["left"][0]["CPG2"] = jWZT_lCPG2AFDC_Out;
		// SO2Action
	// objjWZTCPGSO2Action[0]["right"][0]["nodeCounterStr"] = jWZT_nodeSO2ActionGen_Out;
	// objjWZTCPGSO2Action[0]["right"][0]["timeStampStr"] = jWZT_timeSO2ActionGen_Out;
	// objjWZTCPGSO2Action[0]["right"][0]["nodeCounterPhaseZero"] = jWZT_nodeSO2ActionPhaseZero_Out;
	// objjWZTCPGSO2Action[0]["right"][0]["timeStampPhaseZero"] = jWZT_timeSO2ActionPhaseZero_Out;
	// objjWZTCPGSO2Action[0]["right"][0]["freq"] = jWZT_rCPGFreqSO2Action_Out;
	// objjWZTCPGSO2Action[0]["right"][0]["CPG0"] = jWZT_rCPG0SO2Action_Out;
	// objjWZTCPGSO2Action[0]["right"][0]["CPG1"] = jWZT_rCPG1SO2Action_Out;
	// objjWZTCPGSO2Action[0]["right"][0]["CPG2"] = jWZT_rCPG2SO2Action_Out;
	// objjWZTCPGSO2Action[0]["left"][0]["nodeCounterStr"] = jWZT_nodeSO2ActionGen_Out;
	// objjWZTCPGSO2Action[0]["left"][0]["timeStampStr"] = jWZT_timeSO2ActionGen_Out;
	// objjWZTCPGSO2Action[0]["left"][0]["nodeCounterPhaseZero"] = jWZT_nodeSO2ActionPhaseZero_Out;
	// objjWZTCPGSO2Action[0]["left"][0]["timeStampPhaseZero"] = jWZT_timeSO2ActionPhaseZero_Out;
	// objjWZTCPGSO2Action[0]["left"][0]["freq"] = jWZT_lCPGFreqSO2Action_Out;
	// objjWZTCPGSO2Action[0]["left"][0]["CPG0"] = jWZT_lCPG0SO2Action_Out;
	// objjWZTCPGSO2Action[0]["left"][0]["CPG1"] = jWZT_lCPG1SO2Action_Out;
	// objjWZTCPGSO2Action[0]["left"][0]["CPG2"] = jWZT_lCPG2SO2Action_Out;
	// Combine
	objjWZT[0]["CPG"][0]["AFDC"] = objjWZTCPGAFDC;
	// objjWZT[0]["CPG"][0]["SO2Action"] = objjWZTCPGSO2Action;

	// For exvisJSONZeroTorque.json
	if (ofsjWZT.is_open()){
		ofsjWZT << objjWZT;
	}

}

void ExvisParallelRealRWAssem::logJsonWriteZeroTorqueFn(){

    // + Store data in vector (just in case we want to use it) ----
    // TIME:
	nodeCounter_ZT_Out.push_back(nodeCounter);
    timeStampCAN_ZT_Out.push_back(realRos->timeStampCAN);
	// UI:
	mode_ZT_Out.push_back(realRos->mode);
	typeCon_ZT_Out.push_back(realRos->typeCon);
	speedCon_ZT_Out.push_back(realRos->speedCon);
	assistCon_ZT_Out.push_back(realRos->assistCon);
	notiNo_ZT_Out.push_back(realRos->notiNo);
	// EXO:
		// Feedback - Right
	rHipJ_FB_ZT_Out.push_back(realRos->jointAngle[0]);
	rKneeJ_FB_ZT_Out.push_back(realRos->jointAngle[1]);
	rAnkleJ_FB_ZT_Out.push_back(realRos->jointAngle[2]);
	rHipT_FB_ZT_Out.push_back(realRos->jointTorque[0]);
	rKneeT_FB_ZT_Out.push_back(realRos->jointTorque[1]);
	rAnkleT_FB_ZT_Out.push_back(realRos->jointTorque[2]);
	rHipM_FB_ZT_Out.push_back(realRos->motorTorque[0]);
	rKneeM_FB_ZT_Out.push_back(realRos->motorTorque[1]);
	rAnkleM_FB_ZT_Out.push_back(realRos->motorTorque[2]);
	rFHeel_FB_ZT_Out.push_back(realRos->footSwitch[0]);
	rFToe_FB_ZT_Out.push_back(realRos->footSwitch[1]);
		// Feedback - Left
	lHipJ_FB_ZT_Out.push_back(realRos->jointAngle[3]);
	lKneeJ_FB_ZT_Out.push_back(realRos->jointAngle[4]);
	lAnkleJ_FB_ZT_Out.push_back(realRos->jointAngle[5]);
	lHipT_FB_ZT_Out.push_back(realRos->jointTorque[3]);
	lKneeT_FB_ZT_Out.push_back(realRos->jointTorque[4]);
	lAnkleT_FB_ZT_Out.push_back(realRos->jointTorque[5]);
	lHipM_FB_ZT_Out.push_back(realRos->motorTorque[3]);
	lKneeM_FB_ZT_Out.push_back(realRos->motorTorque[4]);
	lAnkleM_FB_ZT_Out.push_back(realRos->motorTorque[5]);
	lFHeel_FB_ZT_Out.push_back(realRos->footSwitch[2]);
	lFToe_FB_ZT_Out.push_back(realRos->footSwitch[3]);
		// Control - Right
	rHipP_Ctrl_ZT_Out.push_back(realRos->jointAngleCon[0]);
	rKneeP_Ctrl_ZT_Out.push_back(realRos->jointAngleCon[1]);
	rAnkleP_Ctrl_ZT_Out.push_back(realRos->jointAngleCon[2]);
	rHipT_Ctrl_ZT_Out.push_back(realRos->torqueCon[0]);
	rKneeT_Ctrl_ZT_Out.push_back(realRos->torqueCon[1]);
	rAnkleT_Ctrl_ZT_Out.push_back(realRos->torqueCon[2]);
	rHipS_Ctrl_ZT_Out.push_back(realRos->stiffnessCon[0]);
	rKneeS_Ctrl_ZT_Out.push_back(realRos->stiffnessCon[1]);
	rAnkleS_Ctrl_ZT_Out.push_back(realRos->stiffnessCon[2]);
		// Control - Left
	lHipP_Ctrl_ZT_Out.push_back(realRos->jointAngleCon[3]);
	lKneeP_Ctrl_ZT_Out.push_back(realRos->jointAngleCon[4]);
	lAnkleP_Ctrl_ZT_Out.push_back(realRos->jointAngleCon[5]);
	lHipT_Ctrl_ZT_Out.push_back(realRos->torqueCon[3]);
	lKneeT_Ctrl_ZT_Out.push_back(realRos->torqueCon[4]);
	lAnkleT_Ctrl_ZT_Out.push_back(realRos->torqueCon[5]);
	lHipS_Ctrl_ZT_Out.push_back(realRos->stiffnessCon[3]);
	lKneeS_Ctrl_ZT_Out.push_back(realRos->stiffnessCon[4]);
	lAnkleS_Ctrl_ZT_Out.push_back(realRos->stiffnessCon[5]);
	// CPG:
		// AFDC
	// nodeAFDCGen_ZT_Out = ;
	// timeAFDCGen_ZT_Out = ;
		// AFDC - Right
	rCPG0AFDC_ZT_Out.push_back(realRos->rightCpgO0AFDC); // push data one by one to vector
	rCPG1AFDC_ZT_Out.push_back(realRos->rightCpgO1AFDC);
	rCPG2AFDC_ZT_Out.push_back(realRos->rightCpgO2AFDC);
	rCPGFreqAFDC_ZT_Out.push_back(realRos->rightLegFreqAFDC);
		// AFDC - Left
	lCPG0AFDC_ZT_Out.push_back(realRos->leftCpgO0AFDC); // push data one by one to vector
	lCPG1AFDC_ZT_Out.push_back(realRos->leftCpgO1AFDC);
	lCPG2AFDC_ZT_Out.push_back(realRos->leftCpgO2AFDC);
	lCPGFreqAFDC_ZT_Out.push_back(realRos->leftLegFreqAFDC);
		// SO2 Action
	// nodeSO2ActionGen_ZT_Out = ;
	// timeSO2ActionGen_ZT_Out = ;
	// nodeSO2ActionPhaseZero_ZT_Out = ;
	// timeSO2ActionPhaseZero_ZT_Out = ;
		// SO2 Action - Right
	// rCPG0SO2Action_ZT_Out.push_back(realRos->rightCpgO0SO2Action);
	// rCPG1SO2Action_ZT_Out.push_back(realRos->rightCpgO1SO2Action);
	// rCPG2SO2Action_ZT_Out.push_back(realRos->rightCpgO2SO2Action);
	// rCPGFreqSO2Action_ZT_Out.push_back(realRos->rightLegFreqSO2Action);
		// SO2 Action - Left
	// lCPG0SO2Action_ZT_Out.push_back(realRos->leftCpgO0SO2Action);
	// lCPG1SO2Action_ZT_Out.push_back(realRos->leftCpgO1SO2Action);
	// lCPG2SO2Action_ZT_Out.push_back(realRos->leftCpgO2SO2Action);
	// lCPGFreqSO2Action_ZT_Out.push_back(realRos->leftLegFreqSO2Action);
	// - Store data in vector (just in case we want to use it) ----


	// + For Json -----------------------------------
	// Append element by element to json obj (array)
	if (!clearJArray_ZT) {
		// TIME:
		jWZT_nodeCounter_Out.clear();
		jWZT_timeStampCAN_Out.clear();		
		// UI:
		jWZT_mode_Out.clear();
		jWZT_typeCon_Out.clear();
		jWZT_speedCon_Out.clear(); 
		jWZT_assistCon_Out.clear();
		jWZT_notiNo_Out.clear(); 
		// EXO:
			// Feedback signal - Right
		jWZT_rHipJ_FB_Out.clear(); 
		jWZT_rKneeJ_FB_Out.clear(); 
		jWZT_rAnkleJ_FB_Out.clear(); 
		jWZT_rHipT_FB_Out.clear(); 
		jWZT_rKneeT_FB_Out.clear(); 
		jWZT_rAnkleT_FB_Out.clear(); 
		jWZT_rHipM_FB_Out.clear(); 
		jWZT_rKneeM_FB_Out.clear(); 
		jWZT_rAnkleM_FB_Out.clear(); 
		jWZT_rFHeel_FB_Out.clear(); 
		jWZT_rFToe_FB_Out.clear(); 
			// Feedback signal - Left
		jWZT_lHipJ_FB_Out.clear(); 
		jWZT_lKneeJ_FB_Out.clear(); 
		jWZT_lAnkleJ_FB_Out.clear(); 
		jWZT_lHipT_FB_Out.clear(); 
		jWZT_lKneeT_FB_Out.clear(); 
		jWZT_lAnkleT_FB_Out.clear(); 
		jWZT_lHipM_FB_Out.clear(); 
		jWZT_lKneeM_FB_Out.clear(); 
		jWZT_lAnkleM_FB_Out.clear();
		jWZT_lFHeel_FB_Out.clear(); 
		jWZT_lFToe_FB_Out.clear(); 
			// Control signal - Right
		jWZT_rHipP_Ctrl_Out.clear();
		jWZT_rKneeP_Ctrl_Out.clear();
		jWZT_rAnkleP_Ctrl_Out.clear(); 
		jWZT_rHipT_Ctrl_Out.clear();
		jWZT_rKneeT_Ctrl_Out.clear();
		jWZT_rAnkleT_Ctrl_Out.clear();
		jWZT_rHipS_Ctrl_Out.clear();
		jWZT_rKneeS_Ctrl_Out.clear();
		jWZT_rAnkleS_Ctrl_Out.clear();	
			// Control signal - Left
		jWZT_lHipP_Ctrl_Out.clear();
		jWZT_lKneeP_Ctrl_Out.clear(); 
		jWZT_lAnkleP_Ctrl_Out.clear(); 
		jWZT_lHipT_Ctrl_Out.clear(); 
		jWZT_lKneeT_Ctrl_Out.clear(); 
		jWZT_lAnkleT_Ctrl_Out.clear(); 
		jWZT_lHipS_Ctrl_Out.clear(); 
		jWZT_lKneeS_Ctrl_Out.clear(); 
		jWZT_lAnkleS_Ctrl_Out.clear();
		// CPG:
			// AFDC
		// jWZT_nodeAFDCGen_Out.clear(); 
		// jWZT_timeAFDCGen_Out.clear(); 
			// AFDC - Right
		jWZT_rCPG0AFDC_Out.clear();
		jWZT_rCPG1AFDC_Out.clear();
		jWZT_rCPG2AFDC_Out.clear();
		jWZT_rCPGFreqAFDC_Out.clear();
			// AFDC - Left
		jWZT_lCPG0AFDC_Out.clear();
		jWZT_lCPG1AFDC_Out.clear();
		jWZT_lCPG2AFDC_Out.clear();
		jWZT_lCPGFreqAFDC_Out.clear();
			// SO2 
		// jWZT_nodeSO2ActionGen_Out.clear(); 
		// jWZT_timeSO2ActionGen_Out.clear(); 
		// jWZT_nodeSO2ActionPhaseZero_Out.clear(); 
		// jWZT_timeSO2ActionPhaseZero_Out.clear(); 
			// SO2 Action - Right
		// jWZT_rCPG0SO2Action_Out.clear();
		// jWZT_rCPG1SO2Action_Out.clear();
		// jWZT_rCPG2SO2Action_Out.clear();
		// jWZT_rCPGFreqSO2Action_Out.clear();
			// SO2 Action - Left
		// jWZT_lCPG0SO2Action_Out.clear();
		// jWZT_lCPG1SO2Action_Out.clear();
		// jWZT_lCPG2SO2Action_Out.clear();
		// jWZT_lCPGFreqSO2Action_Out.clear();

		clearJArray_ZT = true;
	}

	
	if (ofsjWZT.is_open() && clearJArray_ZT){

		// TIME:
		jWZT_nodeCounter_Out.append(nodeCounter_ZT_Out.back());
		jWZT_timeStampCAN_Out.append(Json::Value(timeStampCAN_ZT_Out.back()));		
		// UI:
		jWZT_mode_Out.append(mode_ZT_Out.back());
		jWZT_typeCon_Out.append(typeCon_ZT_Out.back());
		jWZT_speedCon_Out.append(speedCon_ZT_Out.back()); 
		jWZT_assistCon_Out.append(assistCon_ZT_Out.back());
		jWZT_notiNo_Out.append(notiNo_ZT_Out.back()); 
		// EXO:
			// Feedback signal - Right
		jWZT_rHipJ_FB_Out.append(rHipJ_FB_ZT_Out.back()); 
		jWZT_rKneeJ_FB_Out.append(rKneeJ_FB_ZT_Out.back()); 
		jWZT_rAnkleJ_FB_Out.append(rAnkleJ_FB_ZT_Out.back()); 
		jWZT_rHipT_FB_Out.append(rHipT_FB_ZT_Out.back()); 
		jWZT_rKneeT_FB_Out.append(rKneeT_FB_ZT_Out.back()); 
		jWZT_rAnkleT_FB_Out.append(rAnkleT_FB_ZT_Out.back()); 
		jWZT_rHipM_FB_Out.append(rHipM_FB_ZT_Out.back()); 
		jWZT_rKneeM_FB_Out.append(rKneeM_FB_ZT_Out.back()); 
		jWZT_rAnkleM_FB_Out.append(rAnkleM_FB_ZT_Out.back()); 
		jWZT_rFHeel_FB_Out.append(rFHeel_FB_ZT_Out.back()); 
		jWZT_rFToe_FB_Out.append(rFToe_FB_ZT_Out.back()); 
			// Feedback signal - Left
		jWZT_lHipJ_FB_Out.append(lHipJ_FB_ZT_Out.back()); 
		jWZT_lKneeJ_FB_Out.append(lKneeJ_FB_ZT_Out.back()); 
		jWZT_lAnkleJ_FB_Out.append(lAnkleJ_FB_ZT_Out.back()); 
		jWZT_lHipT_FB_Out.append(lHipT_FB_ZT_Out.back()); 
		jWZT_lKneeT_FB_Out.append(lKneeT_FB_ZT_Out.back()); 
		jWZT_lAnkleT_FB_Out.append(lAnkleT_FB_ZT_Out.back()); 
		jWZT_lHipM_FB_Out.append(lHipM_FB_ZT_Out.back()); 
		jWZT_lKneeM_FB_Out.append(lKneeM_FB_ZT_Out.back()); 
		jWZT_lAnkleM_FB_Out.append(lAnkleM_FB_ZT_Out.back());
		jWZT_lFHeel_FB_Out.append(lFHeel_FB_ZT_Out.back()); 
		jWZT_lFToe_FB_Out.append(lFToe_FB_ZT_Out.back()); 
			// Control signal - Right
		jWZT_rHipP_Ctrl_Out.append(rHipP_Ctrl_ZT_Out.back());
		jWZT_rKneeP_Ctrl_Out.append(rKneeP_Ctrl_ZT_Out.back());
		jWZT_rAnkleP_Ctrl_Out.append(rAnkleP_Ctrl_ZT_Out.back()); 
		jWZT_rHipT_Ctrl_Out.append(rHipT_Ctrl_ZT_Out.back());
		jWZT_rKneeT_Ctrl_Out.append(rKneeT_Ctrl_ZT_Out.back());
		jWZT_rAnkleT_Ctrl_Out.append(rAnkleT_Ctrl_ZT_Out.back());
		jWZT_rHipS_Ctrl_Out.append(rHipS_Ctrl_ZT_Out.back());
		jWZT_rKneeS_Ctrl_Out.append(rKneeS_Ctrl_ZT_Out.back());
		jWZT_rAnkleS_Ctrl_Out.append(rAnkleS_Ctrl_ZT_Out.back());	
			// Control signal - Left
		jWZT_lHipP_Ctrl_Out.append(lHipP_Ctrl_ZT_Out.back());
		jWZT_lKneeP_Ctrl_Out.append(lKneeP_Ctrl_ZT_Out.back()); 
		jWZT_lAnkleP_Ctrl_Out.append(lAnkleP_Ctrl_ZT_Out.back()); 
		jWZT_lHipT_Ctrl_Out.append(lHipT_Ctrl_ZT_Out.back()); 
		jWZT_lKneeT_Ctrl_Out.append(lKneeT_Ctrl_ZT_Out.back()); 
		jWZT_lAnkleT_Ctrl_Out.append(lAnkleT_Ctrl_ZT_Out.back()); 
		jWZT_lHipS_Ctrl_Out.append(lHipS_Ctrl_ZT_Out.back()); 
		jWZT_lKneeS_Ctrl_Out.append(lKneeS_Ctrl_ZT_Out.back()); 
		jWZT_lAnkleS_Ctrl_Out.append(lAnkleS_Ctrl_ZT_Out.back());
		// CPG:
			// AFDC
		jWZT_nodeAFDCGen_Out.append(nodeAFDCGen_ZT_Out); // null
		jWZT_timeAFDCGen_Out.append(Json::Value(timeAFDCGen_ZT_Out)); // null
			// AFDC - Right
		jWZT_rCPG0AFDC_Out.append(rCPG0AFDC_ZT_Out.back());
		jWZT_rCPG1AFDC_Out.append(rCPG1AFDC_ZT_Out.back());
		jWZT_rCPG2AFDC_Out.append(rCPG2AFDC_ZT_Out.back());
		jWZT_rCPGFreqAFDC_Out.append(rCPGFreqAFDC_ZT_Out.back());
			// AFDC - Left
		jWZT_lCPG0AFDC_Out.append(lCPG0AFDC_ZT_Out.back());
		jWZT_lCPG1AFDC_Out.append(lCPG1AFDC_ZT_Out.back());
		jWZT_lCPG2AFDC_Out.append(lCPG2AFDC_ZT_Out.back());
		jWZT_lCPGFreqAFDC_Out.append(lCPGFreqAFDC_ZT_Out.back());
			// SO2
		// jWZT_nodeSO2ActionGen_Out.append(nodeSO2ActionGen_ZT_Out); // null
		// jWZT_timeSO2ActionGen_Out.append(Json::Value(timeSO2ActionGen_ZT_Out)); // null
		// jWZT_nodeSO2ActionPhaseZero_Out.append(nodeSO2ActionPhaseZero_ZT_Out); // null
		// jWZT_timeSO2ActionPhaseZero_Out.append(Json::Value(timeSO2ActionPhaseZero_ZT_Out)); // null
			// SO2 Action - Right
		// jWZT_rCPG0SO2Action_Out.append(rCPG0SO2Action_ZT_Out.back()); // null
		// jWZT_rCPG1SO2Action_Out.append(rCPG1SO2Action_ZT_Out.back()); // null
		// jWZT_rCPG2SO2Action_Out.append(rCPG2SO2Action_ZT_Out.back()); // null
		// jWZT_rCPGFreqSO2Action_Out.append(rCPGFreqSO2Action_ZT_Out.back()); // null
			// SO2 Action - Left
		// jWZT_lCPG0SO2Action_Out.append(lCPG0SO2Action_ZT_Out.back()); // null
		// jWZT_lCPG1SO2Action_Out.append(lCPG1SO2Action_ZT_Out.back()); // null
		// jWZT_lCPG2SO2Action_Out.append(lCPG2SO2Action_ZT_Out.back()); // null
		// jWZT_lCPGFreqSO2Action_Out.append(lCPGFreqSO2Action_ZT_Out.back()); // null

	}

}

void ExvisParallelRealRWAssem::clearVarZeroTorqueFn() {
	
	// VARIABLES ===================================================
	// TIME:
	nodeCounter_ZT_Out.clear();
    timeStampCAN_ZT_Out.clear();
	// UI:
	mode_ZT_Out.clear();
	typeCon_ZT_Out.clear();
	speedCon_ZT_Out.clear();
	assistCon_ZT_Out.clear();
	notiNo_ZT_Out.clear();
	// EXO:
		// Feedback - Right
	rHipJ_FB_ZT_Out.clear();
	rKneeJ_FB_ZT_Out.clear();
	rAnkleJ_FB_ZT_Out.clear();
	rHipT_FB_ZT_Out.clear();
	rKneeT_FB_ZT_Out.clear();
	rAnkleT_FB_ZT_Out.clear();
	rHipM_FB_ZT_Out.clear();
	rKneeM_FB_ZT_Out.clear();
	rAnkleM_FB_ZT_Out.clear();
	rFHeel_FB_ZT_Out.clear();
	rFToe_FB_ZT_Out.clear();
		// Feedback - Left
	lHipJ_FB_ZT_Out.clear();
	lKneeJ_FB_ZT_Out.clear();
	lAnkleJ_FB_ZT_Out.clear();
	lHipT_FB_ZT_Out.clear();
	lKneeT_FB_ZT_Out.clear();
	lAnkleT_FB_ZT_Out.clear();
	lHipM_FB_ZT_Out.clear();
	lKneeM_FB_ZT_Out.clear();
	lAnkleM_FB_ZT_Out.clear();
	lFHeel_FB_ZT_Out.clear();
	lFToe_FB_ZT_Out.clear();
		// Control - Right
	rHipP_Ctrl_ZT_Out.clear();
	rKneeP_Ctrl_ZT_Out.clear();
	rAnkleP_Ctrl_ZT_Out.clear();
	rHipT_Ctrl_ZT_Out.clear();
	rKneeT_Ctrl_ZT_Out.clear();
	rAnkleT_Ctrl_ZT_Out.clear();
	rHipS_Ctrl_ZT_Out.clear();
	rKneeS_Ctrl_ZT_Out.clear();
	rAnkleS_Ctrl_ZT_Out.clear();
		// Control - Left
	lHipP_Ctrl_ZT_Out.clear();
	lKneeP_Ctrl_ZT_Out.clear();
	lAnkleP_Ctrl_ZT_Out.clear();
	lHipT_Ctrl_ZT_Out.clear();
	lKneeT_Ctrl_ZT_Out.clear();
	lAnkleT_Ctrl_ZT_Out.clear();
	lHipS_Ctrl_ZT_Out.clear();
	lKneeS_Ctrl_ZT_Out.clear();
	lAnkleS_Ctrl_ZT_Out.clear();
	// CPG:
		// AFDC
	// nodeAFDCGen_ZT_Out.clear();
	// timeAFDCGen_ZT_Out.clear();
		// AFDC - Right
	rCPG0AFDC_ZT_Out.clear();
	rCPG1AFDC_ZT_Out.clear();
	rCPG2AFDC_ZT_Out.clear();
	rCPGFreqAFDC_ZT_Out.clear();
		// AFDC - Left
	lCPG0AFDC_ZT_Out.clear();
	lCPG1AFDC_ZT_Out.clear();
	lCPG2AFDC_ZT_Out.clear();
	lCPGFreqAFDC_ZT_Out.clear();
		// SO2 Action
	// nodeSO2ActionGen_ZT_Out.clear();
	// timeSO2ActionGen_ZT_Out.clear();
	// nodeSO2ActionPhaseZero_ZT_Out.clear();
	// timeSO2ActionPhaseZero_ZT_Out.clear();
		// SO2 Action - Right
	// rCPG0SO2Action_ZT_Out.clear();
	// rCPG1SO2Action_ZT_Out.clear();
	// rCPG2SO2Action_ZT_Out.clear();
	// rCPGFreqSO2Action_ZT_Out.clear();
		// SO2 Action - Left
	// lCPG0SO2Action_ZT_Out.clear();
	// lCPG1SO2Action_ZT_Out.clear();
	// lCPG2SO2Action_ZT_Out.clear();
	// lCPGFreqSO2Action_ZT_Out.clear();
	

	// JSON ===================================================
	// TIME:
	jWZT_nodeCounter_Out.clear();
	jWZT_timeStampCAN_Out.clear();		
	// UI:
	jWZT_mode_Out.clear();
	jWZT_typeCon_Out.clear();
	jWZT_speedCon_Out.clear(); 
	jWZT_assistCon_Out.clear();
	jWZT_notiNo_Out.clear(); 
	// EXO:
		// Feedback signal - Right
	jWZT_rHipJ_FB_Out.clear(); 
	jWZT_rKneeJ_FB_Out.clear(); 
	jWZT_rAnkleJ_FB_Out.clear(); 
	jWZT_rHipT_FB_Out.clear(); 
	jWZT_rKneeT_FB_Out.clear(); 
	jWZT_rAnkleT_FB_Out.clear(); 
	jWZT_rHipM_FB_Out.clear(); 
	jWZT_rKneeM_FB_Out.clear(); 
	jWZT_rAnkleM_FB_Out.clear(); 
	jWZT_rFHeel_FB_Out.clear(); 
	jWZT_rFToe_FB_Out.clear(); 
		// Feedback signal - Left
	jWZT_lHipJ_FB_Out.clear(); 
	jWZT_lKneeJ_FB_Out.clear(); 
	jWZT_lAnkleJ_FB_Out.clear(); 
	jWZT_lHipT_FB_Out.clear(); 
	jWZT_lKneeT_FB_Out.clear(); 
	jWZT_lAnkleT_FB_Out.clear(); 
	jWZT_lHipM_FB_Out.clear(); 
	jWZT_lKneeM_FB_Out.clear(); 
	jWZT_lAnkleM_FB_Out.clear();
	jWZT_lFHeel_FB_Out.clear(); 
	jWZT_lFToe_FB_Out.clear(); 
		// Control signal - Right
	jWZT_rHipP_Ctrl_Out.clear();
	jWZT_rKneeP_Ctrl_Out.clear();
	jWZT_rAnkleP_Ctrl_Out.clear(); 
	jWZT_rHipT_Ctrl_Out.clear();
	jWZT_rKneeT_Ctrl_Out.clear();
	jWZT_rAnkleT_Ctrl_Out.clear();
	jWZT_rHipS_Ctrl_Out.clear();
	jWZT_rKneeS_Ctrl_Out.clear();
	jWZT_rAnkleS_Ctrl_Out.clear();	
		// Control signal - Left
	jWZT_lHipP_Ctrl_Out.clear();
	jWZT_lKneeP_Ctrl_Out.clear(); 
	jWZT_lAnkleP_Ctrl_Out.clear(); 
	jWZT_lHipT_Ctrl_Out.clear(); 
	jWZT_lKneeT_Ctrl_Out.clear(); 
	jWZT_lAnkleT_Ctrl_Out.clear(); 
	jWZT_lHipS_Ctrl_Out.clear(); 
	jWZT_lKneeS_Ctrl_Out.clear(); 
	jWZT_lAnkleS_Ctrl_Out.clear();
	// CPG:
		// AFDC
	// jWZT_nodeAFDCGen_Out.clear(); 
	// jWZT_timeAFDCGen_Out.clear(); 
		// AFDC - Right
	jWZT_rCPG0AFDC_Out.clear();
	jWZT_rCPG1AFDC_Out.clear();
	jWZT_rCPG2AFDC_Out.clear();
	jWZT_rCPGFreqAFDC_Out.clear();
		// AFDC - Left
	jWZT_lCPG0AFDC_Out.clear();
	jWZT_lCPG1AFDC_Out.clear();
	jWZT_lCPG2AFDC_Out.clear();
	jWZT_lCPGFreqAFDC_Out.clear();
		// SO2 
	// jWZT_nodeSO2ActionGen_Out.clear(); 
	// jWZT_timeSO2ActionGen_Out.clear(); 
	// jWZT_nodeSO2ActionPhaseZero_Out.clear(); 
	// jWZT_timeSO2ActionPhaseZero_Out.clear(); 
		// SO2 Action - Right
	// jWZT_rCPG0SO2Action_Out.clear();
	// jWZT_rCPG1SO2Action_Out.clear();
	// jWZT_rCPG2SO2Action_Out.clear();
	// jWZT_rCPGFreqSO2Action_Out.clear();
		// SO2 Action - Left
	// jWZT_lCPG0SO2Action_Out.clear();
	// jWZT_lCPG1SO2Action_Out.clear();
	// jWZT_lCPG2SO2Action_Out.clear();
	// jWZT_lCPGFreqSO2Action_Out.clear();

}


// Check file existing
bool ExvisParallelRealRWAssem::exists_File(const std::string & name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

ExvisParallelRealRWAssem::~ExvisParallelRealRWAssem(){

	// Close record exvisJSONSignal.json -------------
	if (ofsjWS.is_open()) {
		jsonWriteSignalFn();
		ofsjWS.close();
		cout << "Save file to: " << jsonWriteSignal << endl;
	}

	// Close record exvisJSONZeroTorque.json -------------
	if (ofsjWZT.is_open()) {
		jsonWriteZeroTorqueFn();
		ofsjWZT.close();
		cout << "Save file to: " << jsonWriteZeroTorque << endl;
	}

}
