/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealFreqAssem.h"

//***************************************************************************

//--------------------------------------------------------------------------
// initializer
//--------------------------------------------------------------------------
ExvisParallelRealFreqAssem::ExvisParallelRealFreqAssem(int argc,char* argv[]) {

	// ROS related
    realRos = new ExvisParallelRealFreqROS(argc, argv);

    if(ros::ok()) {
    	cout << "Initialize ExvisParallelRealFreqAssem" << endl;
    }

}

//--------------------------------------------------------------------------
// runAssem
//--------------------------------------------------------------------------
bool ExvisParallelRealFreqAssem::runAssem() {
	if(ros::ok()) {
		cout << "********************************************************" << endl;
		cout << "ExvisParallelRealFreqAssem is running" << endl;
		cout << "********************************************************" << endl;
		cout << nodeCounter << endl;
		
		printf("1 = Online freq \n");
		printf("2 = Manual input freq \n");
		printf("3 = Manual inc/dec freq \n");

		while(true){
			cin >> onlineVsManualFreq;
			if ((!cin.fail()) && (onlineVsManualFreq == 1)) {
				break;
			} else if ((!cin.fail()) && (onlineVsManualFreq == 2)) {
				printf("Please input FREQ as number between 0-1: \n");
				cin >> freqManual;
				if ((!cin.fail()) && (freqManual >= 0.0 && freqManual <= 1.0)) {
					break;
				}
			} else if ((!cin.fail()) && (onlineVsManualFreq == 3)) {
				printf("w = freq up (+0.01 Hz real world) \n");
				printf("s = freq down (-0.01 Hz real world) \n");
				cin >> upDownFreq;
				if ((!cin.fail()) && (upDownFreq == 'w')) {
					upDownFreqVal = 0.0001; // Internal freq unit
					break;
				}

				if ((!cin.fail()) && (upDownFreq == 's')) {
					upDownFreqVal = -0.0001; // Internal freq unit
					break;
				}
			} else {
				cin.clear();
				cin.ignore();
			}
		}

		

		cout << "--------------- Published ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tPublished: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		
		realRos->sendOnlineVsManualFreq(onlineVsManualFreq); // Mode select e.g., 1, 2, 3
		realRos->sendFreqManual(freqManual); // Freq val e.g., between 0.0 - 1.0
		realRos->sendUpDownFreqVal(upDownFreqVal); // Inc/dec val e.g., 0.01 or -0.01 Hz real world


		// + Node spin to have sub value --------------------------------
		cout << "--------------- Subscribed ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		cout << "tSubscribed: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->rosSpinOnce();
		// - Node spin to have sub value --------------------------------

		nodeCounter++;
		cout << "********************************************************" << endl;
		return true;
		
	} else {
		cout << "Shutting down the node" << endl;
		return false;
	}
}

ExvisParallelRealFreqAssem::~ExvisParallelRealFreqAssem() {
	
}
