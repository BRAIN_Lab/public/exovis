/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#ifndef PLASTIC_H
#define PLASTIC_H

#include <vector>
#include <stdio.h>
#include <math.h>
#include <iostream>
//***************************************************************************

class plastic {
	public:
		plastic(double o0, double o1, double o2, double initial_phi, double _alpha);
		plastic(double o0, double o1, double o2, double initial_phi, double _alpha, double _bi);
		void update(double perturbation, int step, double timeStep); // AFDC mode
		void updateSoTwo(double internal_phi, int step, double timeStep); // SO2 mode
		void printOutputs();
		void updateWeights();
		double getOut0();
		double getOut1();
		double getOut2();
		double getFreq();
		double getW20();
		double getW02();
		double getW2p();
		double getW01();
		virtual ~plastic();
		void setPhi(double newPhi);

	private:
		double w2p_t,w20_t,w02_t,w2p_t1,w20_t1,w02_t1; // plasticity
		double w00,w01,w10,w11; // weights
		double A20,A02,B20,B02,B2p,A2p; // parameters
		double out0_t,out1_t,out2_t,out0_t1,out1_t1,out2_t1; // outputs
		double phi, alpha,learning; // parameters
		double xPer;


};

#endif // PLASTIC_H
