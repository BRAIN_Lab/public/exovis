/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealCPGROS.h"

//***************************************************************************

ExvisParallelRealCPGROS::ExvisParallelRealCPGROS(int argc, char **argv) {
    // Create a ROS nodes
    int _argc = 0;
    char** _argv = NULL;
    ros::init(_argc,_argv,"ExvisParallelRealCPGAssem");

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("ExvisParallelRealCPGROS just started!");

    // Initialize Subscribers
    timeStampCANSub = node.subscribe("/exvis/timeStampCAN", 1, &ExvisParallelRealCPGROS::timeStampCAN_CB, this);
    jointAngleSub = node.subscribe("/exvis/jointAngle", 1, &ExvisParallelRealCPGROS::jointAngle_CB, this);
    jointTorqueSub = node.subscribe("/exvis/jointTorque", 1, &ExvisParallelRealCPGROS::jointTorque_CB, this);
    footSwitchSub = node.subscribe("/exvis/footSwitch", 1, &ExvisParallelRealCPGROS::footSwitch_CB, this);
    motorTorqueSub = node.subscribe("/exvis/motorTorque", 1, &ExvisParallelRealCPGROS::motorTorque_CB, this);


    // Initialize Publishers
    // AFDC Right
    rightLegFreqAFDCPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightLegFreqAFDC",1);
    rightCpgO0AFDCPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightCpgO0AFDC",1);
    rightCpgO1AFDCPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightCpgO1AFDC",1);
    rightCpgO2AFDCPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightCpgO2AFDC",1);
    // AFDC Left
    leftLegFreqAFDCPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftLegFreqAFDC",1);
    leftCpgO0AFDCPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftCpgO0AFDC",1);
    leftCpgO1AFDCPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftCpgO1AFDC",1);
    leftCpgO2AFDCPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftCpgO2AFDC",1);
    
    // SO2 during Learn
    // Right
    rightLegFreqSO2LearnPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightLegFreqSO2Learn",1);
    rightCpgO0SO2LearnPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightCpgO0SO2Learn",1);
    rightCpgO1SO2LearnPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightCpgO1SO2Learn",1);
    rightCpgO2SO2LearnPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightCpgO2SO2Learn",1);
    // Left
    leftLegFreqSO2LearnPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftLegFreqSO2Learn",1);
    leftCpgO0SO2LearnPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftCpgO0SO2Learn",1);
    leftCpgO1SO2LearnPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftCpgO1SO2Learn",1);
    leftCpgO2SO2LearnPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftCpgO2SO2Learn",1);

    // SO2 during Action
    // Right
    rightLegFreqSO2ActionPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightLegFreqSO2Action",1);
    rightCpgO0SO2ActionPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightCpgO0SO2Action",1);
    rightCpgO1SO2ActionPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightCpgO1SO2Action",1);
    rightCpgO2SO2ActionPub = node.advertise<std_msgs::Float64>("/exvis/cpg/rightCpgO2SO2Action",1);
    // Left
    leftLegFreqSO2ActionPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftLegFreqSO2Action",1);
    leftCpgO0SO2ActionPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftCpgO0SO2Action",1);
    leftCpgO1SO2ActionPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftCpgO1SO2Action",1);
    leftCpgO2SO2ActionPub = node.advertise<std_msgs::Float64>("/exvis/cpg/leftCpgO2SO2Action",1);

    // Initialize Service
    cpgServiceSrv = node.advertiseService("cpgService", &ExvisParallelRealCPGROS::cpgService_CB, this);

    // Set Rate
    timeStep = 0.001; // sec (0.001 s = 1 ms)
    rate = new ros::Rate(1/timeStep);
//    rate = new ros::Rate(100);
//    rate = new ros::Rate(17*4); // 60hz
//    rate = new ros::Rate(50);
//    rate = new ros::Rate(29);
//    rate = new ros::Rate(11.1);
//    rate = new ros::Rate(10);
//    rate = new ros::Rate(1);
//    rate = new ros::Rate(0.1);
}

// **********************************************************
// Subscriber callback
// **********************************************************
// Read data ------------------
void ExvisParallelRealCPGROS::timeStampCAN_CB(const std_msgs::String& _timeStampCAN) {

    // Get the data from Topic into main node variable
	timeStampCAN = _timeStampCAN.data;
	// cout << "timeStampCAN: " << timeStampCAN << endl;
}

void ExvisParallelRealCPGROS::jointAngle_CB(const std_msgs::Int32MultiArray& _jointAngle) {

    // Get the data from Topic into main node variable
	jointAngle = _jointAngle.data; // C++ int = ROS Int32
	// cout << "jointAngle: " << jointAngle[0] << " " << jointAngle[1] << " " << jointAngle[2] << " " << jointAngle[3] << " " << jointAngle[4] << " " << jointAngle[5] << endl;
}

void ExvisParallelRealCPGROS::jointTorque_CB(const std_msgs::Int32MultiArray& _jointTorque) {

    // Get the data from Topic into main node variable
	jointTorque = _jointTorque.data; // C++ int = ROS Int32
	// cout << "jointTorque: " << jointTorque[0] << " " << jointTorque[1] << " " << jointTorque[2] << " " << jointTorque[3] << " " << jointTorque[4] << " " << jointTorque[5] << endl;
}

void ExvisParallelRealCPGROS::footSwitch_CB(const std_msgs::Int32MultiArray& _footSwitch) {

    // Get the data from Topic into main node variable
	footSwitch = _footSwitch.data; // C++ int = ROS Int32
	// cout << "footSwitch: " << footSwitch[0] << " " << footSwitch[1] << " " << footSwitch[2] << " " << footSwitch[3] << " " << footSwitch[4] << " " << footSwitch[5] << endl;
}

void ExvisParallelRealCPGROS::motorTorque_CB(const std_msgs::Int32MultiArray& _motorTorque) {

    // Get the data from Topic into main node variable
	motorTorque = _motorTorque.data; // C++ int = ROS Int32
	// cout << "motorTorque: " << motorTorque[0] << " " << motorTorque[1] << " " << motorTorque[2] << " " << motorTorque[3] << " " << motorTorque[4] << " " << motorTorque[5] << endl;
}


// **********************************************************
// Publisher send function
// **********************************************************
void ExvisParallelRealCPGROS::sendLegFreq(double cpgFreq, const string& cpgLeg, const string& cpgType, const string& cpgFunc) {
    // publish the cpg freq:
    std_msgs::Float64 cpgFreqP;
    cpgFreqP.data = cpgFreq;

    if (cpgLeg == "R") {
        if (cpgType == "AFDC") {
            if (cpgFunc == "Online") {
                rightLegFreqAFDCPub.publish(cpgFreqP);
	            cout << "rightLegFreqAFDC: " << cpgFreq << " " << "... rightLegFreqAFDC Published !" << endl;
            }
        } else { // SO2
            if (cpgFunc == "Learn") {
                rightLegFreqSO2LearnPub.publish(cpgFreqP);
	            cout << "rightLegFreqSO2Learn: " << cpgFreq << " " << "... rightLegFreqSO2Learn Published !" << endl;
            } else { // Action
                rightLegFreqSO2ActionPub.publish(cpgFreqP);
	            cout << "rightLegFreqSO2Action: " << cpgFreq << " " << "... rightLegFreqSO2Action Published !" << endl;
            }
        }
    } else { // L
        if (cpgType == "AFDC") {
            if (cpgFunc == "Online") {
                leftLegFreqAFDCPub.publish(cpgFreqP);
	            cout << "leftLegFreqAFDC: " << cpgFreq << " " << "... leftLegFreqAFDC Published !" << endl;
            }
        } else {
            if (cpgFunc == "Learn") {
                leftLegFreqSO2LearnPub.publish(cpgFreqP);
	            cout << "leftLegFreqSO2Learn: " << cpgFreq << " " << "... leftLegFreqSO2Learn Published !" << endl;
            } else {
                leftLegFreqSO2ActionPub.publish(cpgFreqP);
	            cout << "leftLegFreqSO2Action: " << cpgFreq << " " << "... leftLegFreqSO2Action Published !" << endl;
            }
        }
    } 
}

void ExvisParallelRealCPGROS::sendCpgO0(double cpgO0, const string& cpgLeg, const string& cpgType, const string& cpgFunc) {
    // publish the cpg out 0:
    std_msgs::Float64 cpgO0P;
    cpgO0P.data = cpgO0;

    if (cpgLeg == "R") {
        if (cpgType == "AFDC") {
            if (cpgFunc == "Online") {
                rightCpgO0AFDCPub.publish(cpgO0P);
	            cout << "rightCpgO0AFDC: " << cpgO0 << " " << "... rightCpgO0AFDC Published !" << endl;
            }
        } else {
            if (cpgFunc == "Learn") {
                rightCpgO0SO2LearnPub.publish(cpgO0P);
	            cout << "rightCpgO0SO2Learn: " << cpgO0 << " " << "... rightCpgO0SO2Learn Published !" << endl;
            } else {
                rightCpgO0SO2ActionPub.publish(cpgO0P);
	            cout << "rightCpgO0SO2Action: " << cpgO0 << " " << "... rightCpgO0SO2Action Published !" << endl;
            }
        }
    } else { // L
        if (cpgType == "AFDC") {
            if (cpgFunc == "Online") {
                leftCpgO0AFDCPub.publish(cpgO0P);
	            cout << "leftCpgO0AFDC: " << cpgO0 << " " << "... leftCpgO0AFDC Published !" << endl;
            }
        } else {
            if (cpgFunc == "Learn") {
                leftCpgO0SO2LearnPub.publish(cpgO0P);
	            cout << "leftCpgO0SO2Learn: " << cpgO0 << " " << "... leftCpgO0SO2Learn Published !" << endl;
            } else {
                leftCpgO0SO2ActionPub.publish(cpgO0P);
	            cout << "leftCpgO0SO2Action: " << cpgO0 << " " << "... leftCpgO0SO2Action Published !" << endl;
            }
        }
    } 
}

void ExvisParallelRealCPGROS::sendCpgO1(double cpgO1, const string& cpgLeg, const string& cpgType, const string& cpgFunc) {
    // publish the cpg out 1:
    std_msgs::Float64 cpgO1P;
    cpgO1P.data = cpgO1;

    if (cpgLeg == "R") {
        if (cpgType == "AFDC") {
            if (cpgFunc == "Online") {
                rightCpgO1AFDCPub.publish(cpgO1P);
	            cout << "rightCpgO1AFDC: " << cpgO1 << " " << "... rightCpgO1AFDC Published !" << endl;
            }
        } else {
            if (cpgFunc == "Learn") {
                rightCpgO1SO2LearnPub.publish(cpgO1P);
	            cout << "rightCpgO1SO2Learn: " << cpgO1 << " " << "... rightCpgO1SO2Learn Published !" << endl;
            } else {
                rightCpgO1SO2ActionPub.publish(cpgO1P);
	            cout << "rightCpgO1SO2Action: " << cpgO1 << " " << "... rightCpgO1SO2Action Published !" << endl;
            }
        }
    } else {
        if (cpgType == "AFDC") {
            if (cpgFunc == "Online") {
                leftCpgO1AFDCPub.publish(cpgO1P);
	            cout << "leftCpgO1AFDC: " << cpgO1 << " " << "... leftCpgO1AFDC Published !" << endl;
            }
        } else {
            if (cpgFunc == "Learn") {
                leftCpgO1SO2LearnPub.publish(cpgO1P);
	            cout << "leftCpgO1SO2Learn: " << cpgO1 << " " << "... leftCpgO1SO2Learn Published !" << endl;
            } else {
                leftCpgO1SO2ActionPub.publish(cpgO1P);
	            cout << "leftCpgO1SO2Action: " << cpgO1 << " " << "... leftCpgO1SO2Action Published !" << endl;
            }
        }
    } 
}

void ExvisParallelRealCPGROS::sendCpgO2(double cpgO2, const string& cpgLeg, const string& cpgType, const string& cpgFunc) {
    // publish the cpg out 2:
    std_msgs::Float64 cpgO2P;
    cpgO2P.data = cpgO2;

    if (cpgLeg == "R") {
        if (cpgType == "AFDC") {
            if (cpgFunc == "Online") {
                rightCpgO2AFDCPub.publish(cpgO2P);
	            cout << "rightCpgO2AFDC: " << cpgO2 << " " << "... rightCpgO2AFDC Published !" << endl;
            }
        } else {
            if (cpgFunc == "Learn") {
                rightCpgO2SO2LearnPub.publish(cpgO2P);
	            cout << "rightCpgO2SO2Learn: " << cpgO2 << " " << "... rightCpgO2SO2Learn Published !" << endl;
            } else {
                rightCpgO2SO2ActionPub.publish(cpgO2P);
	            cout << "rightCpgO2SO2Action: " << cpgO2 << " " << "... rightCpgO2SO2Action Published !" << endl;
            }
        }
    } else {
        if (cpgType == "AFDC") {
            if (cpgFunc == "Online") {
                leftCpgO2AFDCPub.publish(cpgO2P);
	            cout << "leftCpgO2AFDC: " << cpgO2 << " " << "... leftCpgO2AFDC Published !" << endl;
            }
        } else {
            if (cpgFunc == "Learn") {
                leftCpgO2SO2LearnPub.publish(cpgO2P);
	            cout << "leftCpgO2SO2Learn: " << cpgO2 << " " << "... leftCpgO2SO2Learn Published !" << endl;
            } else {
                leftCpgO2SO2ActionPub.publish(cpgO2P);
	            cout << "leftCpgO2SO2Action: " << cpgO2 << " " << "... leftCpgO2SO2Action Published !" << endl;
            }
        }
    } 
}

// **********************************************************
// Service callback
// **********************************************************
bool ExvisParallelRealCPGROS::cpgService_CB(exvis_parallel_real_cpg_online::cpgService::Request& req, exvis_parallel_real_cpg_online::cpgService::Response& res) {
    cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
    
    if (req.cpgType == "AFDC") {
        if (req.cpgFunc == "initOnline") {
            initFreqRAFDC = req.initFreqR; // Internal freq unit
            initFreqLAFDC = req.initFreqL; // Internal freq unit
            flagStrAFDC = true;
        } else if (req.cpgFunc == "updateOnline") {
            updateFreqRAFDC = req.updateFreqR; // Internal freq unit
            updateFreqLAFDC = req.updateFreqL; // Internal freq unit
            flagUpdateAFDC = true;
        }
        res.success = true;
    } else if (req.cpgType == "SO2") {
        if (req.cpgFunc == "initLearn") {
            initFreqRSO2Learn = req.initFreqR; // Internal freq unit
            initFreqLSO2Learn = req.initFreqL; // Internal freq unit
            flagStrSO2Learn = true;
        } else if (req.cpgFunc == "updateLearn") {
            updateFreqRSO2Learn = req.updateFreqR; // Internal freq unit
            updateFreqLSO2Learn = req.updateFreqL; // Internal freq unit
            flagUpdateSO2Learn = true;
        } else if (req.cpgFunc == "initAction") {
            initFreqRSO2Action = req.initFreqR; // Internal freq unit
            initFreqLSO2Action = req.initFreqL; // Internal freq unit
            flagStrSO2Action = true;
        } else if (req.cpgFunc == "updateAction") {
            updateFreqRSO2Action = req.updateFreqR; // Internal freq unit
            updateFreqLSO2Action = req.updateFreqL; // Internal freq unit
            flagUpdateSO2Action = true;
        }
        res.success = true;
    } else {
        res.success = false;
        cout << "cpgService failed" << endl;
    }
    
    // while(cin.get() != '\n'){
    //     ;
    // }
    return res.success;
}

// **********************************************************
// rosSpinOnce
// **********************************************************
void ExvisParallelRealCPGROS::rosSpinOnce(){

	cout << "ExvisParallelRealCPGROS node is spinning" << endl;

	ros::spinOnce();
    bool rateMet = rate->sleep();

    if(!rateMet)
    {
        ROS_ERROR("Sleep rate not met");
    }

}

ExvisParallelRealCPGROS::~ExvisParallelRealCPGROS() {
    ROS_INFO("ExvisParallelRealCPGROS just terminated!");
    ros::shutdown();
}