/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "shiftregister.h"

//***************************************************************************

shift_register::shift_register(int delay) {
	delay_=delay;

		for(int i=0;i<delay_;i++)
			shiftRegister.push_back(0);
	// TODO Auto-generated constructor stub

}

shift_register::~shift_register() {
	// TODO Auto-generated destructor stub
}

vector<double> shift_register::getReg()
{
	return shiftRegister;
}



int shift_register::getRegSize()
{
return shiftRegister.size();
}
double shift_register::update(double value)
{
    if(delay_ == 0)
    	return value;
    else if(delay_ == 1)
	{
    	double temp=shiftRegister.at(0);
    	shiftRegister[0]=value;
    	return temp;

	}

    else
    {
	double temp=shiftRegister.at(0);
    for(int i=0;i<=shiftRegister.size()-2;i++)
		shiftRegister[i]=shiftRegister.at(i+1);
	shiftRegister[shiftRegister.size()-1] = value;

    return temp;
    }
}
