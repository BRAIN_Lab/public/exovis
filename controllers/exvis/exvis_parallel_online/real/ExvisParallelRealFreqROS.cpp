/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealFreqROS.h"

//***************************************************************************

ExvisParallelRealFreqROS::ExvisParallelRealFreqROS(int argc, char **argv) {
    // Create a ROS nodes
    int _argc = 0;
    char** _argv = NULL;
    ros::init(_argc,_argv,"ExvisParallelRealFreqAssem");

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("ExvisParallelRealFreqROS just started!");

    // Initialize Subscribers


    // Initialize Publishers
    onlineVsManualFreqPub = node.advertise<std_msgs::Int32>("/exvis/in/onlineVsManualFreq",1);
    freqManualPub = node.advertise<std_msgs::Float64>("/exvis/in/freqManual",1);
    upDownFreqValPub = node.advertise<std_msgs::Float64>("/exvis/in/upDownFreqVal",1);

    // Set Rate
    rate = new ros::Rate(1000);
//    rate = new ros::Rate(17*4); // 60hz
//    rate = new ros::Rate(100);
//    rate = new ros::Rate(11.1);
}

// **********************************************************
// sendOnlineVsManualFreq
// **********************************************************
void ExvisParallelRealFreqROS::sendOnlineVsManualFreq(int onlineVsManualFreq) {
    std_msgs::Int32 onlineVsManualFreqP;
    onlineVsManualFreqP.data = onlineVsManualFreq;
    onlineVsManualFreqPub.publish(onlineVsManualFreqP);
    cout << "onlineVsManualFreq: " << onlineVsManualFreqP.data << endl;
    cout << "... onlineVsManualFreq Published !" << endl;
}


// **********************************************************
// sendFreqManual
// **********************************************************
void ExvisParallelRealFreqROS::sendFreqManual(double freqManual) {
    std_msgs::Float64 freqManualP;
    freqManualP.data = freqManual;
    freqManualPub.publish(freqManualP);
    cout << "freqManual: " << freqManualP.data << endl;
    cout << "... freqManual Published !" << endl;
}

// **********************************************************
// sendUpDownFreqVal
// **********************************************************
void ExvisParallelRealFreqROS::sendUpDownFreqVal(double upDownFreqVal) {
    std_msgs::Float64 upDownFreqValP;
    upDownFreqValP.data = upDownFreqVal;
    upDownFreqValPub.publish(upDownFreqValP);
    cout << "upDownFreqVal: " << upDownFreqValP.data << endl;
    cout << "... upDownFreqVal Published !" << endl;
}

// **********************************************************
// rosSpinOnce
// **********************************************************
void ExvisParallelRealFreqROS::rosSpinOnce(){

	cout << "ExvisParallelRealFreqROS node is spinning" << endl;

	ros::spinOnce();
    bool rateMet = rate->sleep();

    if(!rateMet)
    {
        ROS_ERROR("Sleep rate not met");
    }

}

ExvisParallelRealFreqROS::~ExvisParallelRealFreqROS() {
    ROS_INFO("ExvisParallelRealFreqROS just terminated!");
    ros::shutdown();
}
