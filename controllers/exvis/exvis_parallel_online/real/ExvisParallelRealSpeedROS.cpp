/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealSpeedROS.h"

//***************************************************************************

ExvisParallelRealSpeedROS::ExvisParallelRealSpeedROS(int argc, char **argv) {
    // Create a ROS nodes
    int _argc = 0;
    char** _argv = NULL;
    ros::init(_argc,_argv,"ExvisParallelRealSpeedAssem");

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("ExvisParallelRealSpeedROS just started!");

    // Initialize Subscribers


    // Initialize Publishers
     speedPub = node.advertise<std_msgs::Int32>("/exvis/in/speed",1);

    // Set Rate
     rate = new ros::Rate(1000);
//    rate = new ros::Rate(17*4); // 60hz
//    rate = new ros::Rate(100);
//    rate = new ros::Rate(11.1);
}

// **********************************************************
// sendSpeed
// **********************************************************
void ExvisParallelRealSpeedROS::sendSpeed(int speed) {
    std_msgs::Int32 speedP;
    speedP.data = speed;
    speedPub.publish(speedP);
    cout << "speed: " << speedP.data << endl;
    cout << "... speed Published !" << endl;
}


// **********************************************************
// rosSpinOnce
// **********************************************************
void ExvisParallelRealSpeedROS::rosSpinOnce(){

	cout << "ExvisParallelRealSpeedROS node is spinning" << endl;

	ros::spinOnce();
    bool rateMet = rate->sleep();

    if(!rateMet)
    {
        ROS_ERROR("Sleep rate not met");
    }

}

ExvisParallelRealSpeedROS::~ExvisParallelRealSpeedROS() {
    ROS_INFO("ExvisParallelRealSpeedROS just terminated!");
    ros::shutdown();
}
