/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#ifndef DYNAMICCPG_H
#define DYNAMICCPG_H

#include <stdio.h> // standard input / output functions
#include <unistd.h> // standard function definitions
#include <stdlib.h> // variable types, macros, functions
#include <string.h> // string function definitions
#include <iostream> // C++ to use cout
#include <fstream> // To read/write on file
#include <vector>
#include <cmath>
#include <string>

#include "derivativeTransitionRegister.h"
#include "plastic.h"
#include "lowPassfilter.h"
#include "DynamicVector.h"
#include "shiftregister.h"

using namespace std;

//***************************************************************************

class DynamicCpg {
	public:

		DynamicCpg(double cpgInitialPhi, const string& name, bool oneCPG, int CPGphase, const string& CPGtype, const string& CPGfunc);


		virtual ~DynamicCpg();

		void setEnable(bool externalCondition,double motor0, double leftFoot, double rightFoot);
		int getEnable();
		vector<double> getGeneratedMotorsSingle();
		vector<double> getGeneratedMotorsDouble();
		plastic* getCpg();
		double getCpgO0();
		double getCpgO1();
		double getCpgO2();

		vector<double> getValueToShow();
		vector<double> getValueToShow2CPG();
		double generateOutputOneLeg(int feedback, int stepValue, double timeStep, int cpgPhaseValue, bool applyPer);
		void soTwoMode(double cpgInternalPhi, int stepValue, double timeStep);

		bool debugON = false;

	private:
		int countPerturbation;
		DynamicVector *dynRegister;
		vector<double> derHip;
		plastic *cpg;
		int delayValue;
		lowPass_filter *filterFeedback;
		derivativeTransitionRegister *checkSignal;
		shift_register *feedbackDelay;

		// + ChaicharnA ---------------
		ofstream cpgPlot;
		ofstream cpgPlot_soTwo;
		// - ChaicharnA ---------------


		double phaseDelay;

		vector<double> systemVectorFreq, der, derOut1, derOut2, shiftVector;
		double amplitudeMotor0, amplitudeMotor2, maxFeedback, minFeedback;
		double cpgSignal0, cpgSignal1, cpgSignal2, cpgSignal3;
		bool enableCpgController;
		int counter, step, countder, countDelay=0;
		bool countDelay1, countDelay2;
		int counterDelay1Value, counterDelay2Value;
		int enable;
		bool single, delayFiltered, delayFeedback;
		int counterDelayFeedback, counterDelayFiltered;
		vector<double> delay;


		double changeRange(double oldMin, double oldMax, double newMin, double newMarx, double value);
		void updateCpg(double perturbation);
		vector<double> generateCpgHipsMultiple(double signal, double derivative,double oscillation, double value);
		vector<double> generateCpgKneeMultiple(double signal, double derivative, double oscillation, double value);
		double generateHip(double signal, double derivative,double oscillation, double value);
		double generateKnee(double signal, double derivative,double oscillation, double valueHip, double valueKnee);
		vector<double> getMaxMinFeedback(double feedback, double max, double min);
		double getAmplitudeHips(double signal, double max);
		double getAmplitudeKnee(double signal_knee, double max);
		double getAbsol(double a, double b);
		double getShiftDelay(double out1, double out2, int step, double fequency);


		// + ChaicharnA -----------------------
		string InstName; // To keep name of DynamicCpg instance

		double GFeedback;
		double GFBfilter;
		double GFBScale;
		double GOsc;

		double GcounterDelayFeedback;
		double GcounterDelayFiltered;
		double GdelayValue;


		// 1 CPG ----------
		double cpgOut0;
		double cpgOut1;
		double cpgOut2;

		// 2 CPG ----------
		double cpgPhase;

		double twoCpgOut;
		double cpgO0;
		double cpgO1;
		double cpgO2;


		// - ChaicharnA -----------------------




};

#endif // DYNAMICCPG_H
