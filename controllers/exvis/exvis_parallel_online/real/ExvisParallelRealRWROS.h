/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#ifndef EXVISPARALLELREALRWROS_H
#define EXVISPARALLELREALRWROS_H

#include <stdio.h> // standard input / output functions
#include <unistd.h> // standard function definitions
#include <stdlib.h> // variable types, macros, functions
#include <string.h> // string function definitions
#include <iostream> // C++ to use cout
#include <fstream> // To read/write on file
#include <math.h>
#include <vector>
#include <pwd.h> // check directory
#include <sys/time.h>
#include <numeric> // for inner_product
#include <sys/stat.h> // check file existing

using namespace std;

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Int32MultiArray.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayLayout.h>


//***************************************************************************

class ExvisParallelRealRWROS {
	public:
		
		// ------------------------
		// Subscribers related variables
		// ------------------------
		double timeStep;

		// TIME:
		string timeStampCAN;
		// UI:
		string mode;
		int typeCon;
		int speedCon;
		int assistCon;
		int notiNo;
		// EXO:
			// Feedback signal
		vector<int> jointAngle = {0, 0, 0, 0, 0, 0};
		vector<int> jointTorque = {0, 0, 0, 0, 0, 0};
		vector<int> motorTorque = {0, 0, 0, 0, 0, 0};
		vector<int> footSwitch = {0, 0, 0, 0, 0, 0};
			// Control signal
		vector<int> jointAngleCon = {0, 0, 0, 0, 0, 0};
		vector<int> torqueCon = {0, 0, 0, 0, 0, 0};
		vector<int> stiffnessCon = {0, 0, 0, 0, 0, 0};
		// CPG:
			// AFDC
		bool flagStrAFDC; // true when starts AFDC
			// AFDC - Right
		double rightLegFreqAFDC; // Internal freq unit
		double rightCpgO0AFDC;
		double rightCpgO1AFDC;
		double rightCpgO2AFDC;
			// AFDC - Left
		double leftLegFreqAFDC; // Internal freq unit
		double leftCpgO0AFDC;
		double leftCpgO1AFDC;
		double leftCpgO2AFDC;
			// SO2 Action
		bool flagStrSO2Action; // true when starts SO2 Action
		bool flagStrSO2ActionPhaseZero; // true when SO2 Action has zero phase	
			// SO2 Action - Right
		double rightLegFreqSO2Action; // Internal freq unit
		double rightCpgO0SO2Action;
		double rightCpgO1SO2Action;
		double rightCpgO2SO2Action;
			// SO2 Action - Left
		double leftLegFreqSO2Action; // Internal freq unit
		double leftCpgO0SO2Action;
		double leftCpgO1SO2Action;
		double leftCpgO2SO2Action;

		// ------------------------
		// Publishers related variables
		// ------------------------


		// ------------------------
		// Public Methods
		// ------------------------
		ExvisParallelRealRWROS(int argc, char *argv[]);
		~ExvisParallelRealRWROS();

		void rosSpinOnce();



	private:
		ros::Rate* rate;

		// ------------------------
		// Subscribers 
		// ------------------------
		// TIME:
		ros::Subscriber timeStampCANSub;
		// UI:
		ros::Subscriber modeSub;
		ros::Subscriber typeConSub;
		ros::Subscriber speedConSub;
		ros::Subscriber assistConSub;
		ros::Subscriber notiNoSub;
		// EXO:
			// Feedback signal
		ros::Subscriber jointAngleSub;
		ros::Subscriber jointTorqueSub;
		ros::Subscriber motorTorqueSub;
		ros::Subscriber footSwitchSub;
			// Control signal
		ros::Subscriber jointAngleConSub;
		ros::Subscriber torqueConSub;
		ros::Subscriber stiffnessConSub;
		// CPG:
			// AFDC
		ros::Subscriber flagStrAFDCSub;
			// AFDC - Right
		ros::Subscriber rightLegFreqAFDCSub;
		ros::Subscriber rightCpgO0AFDCSub;
		ros::Subscriber rightCpgO1AFDCSub;
		ros::Subscriber rightCpgO2AFDCSub;
			// AFDC - Left
		ros::Subscriber leftLegFreqAFDCSub;
		ros::Subscriber leftCpgO0AFDCSub;
		ros::Subscriber leftCpgO1AFDCSub;
		ros::Subscriber leftCpgO2AFDCSub;
			// SO2 Action
		ros::Subscriber flagStrSO2ActionSub; 
		ros::Subscriber flagStrSO2ActionPhaseZeroSub;
			// SO2 Action - Right
		ros::Subscriber rightLegFreqSO2ActionSub;
		ros::Subscriber rightCpgO0SO2ActionSub;
		ros::Subscriber rightCpgO1SO2ActionSub;
		ros::Subscriber rightCpgO2SO2ActionSub;
			// SO2 Action - Left
		ros::Subscriber leftLegFreqSO2ActionSub;
		ros::Subscriber leftCpgO0SO2ActionSub;
		ros::Subscriber leftCpgO1SO2ActionSub;
		ros::Subscriber leftCpgO2SO2ActionSub;

		// ------------------------
		// Publishers
		// ------------------------


		// ------------------------
		// Private Methods
		// ------------------------
		// TIME:
		void timeStampCAN_CB(const std_msgs::String&);
		// UI:
		void mode_CB(const std_msgs::String&);
		void typeCon_CB(const std_msgs::Int32&);
		void speedCon_CB(const std_msgs::Int32&);
		void assistCon_CB(const std_msgs::Int32&);
		void notiNo_CB(const std_msgs::Int32&);
		// EXO:
			// Feedback signal
		void jointAngle_CB(const std_msgs::Int32MultiArray&);
		void jointTorque_CB(const std_msgs::Int32MultiArray&);
		void motorTorque_CB(const std_msgs::Int32MultiArray&);
		void footSwitch_CB(const std_msgs::Int32MultiArray&);
			// Control signal
		void jointAngleCon_CB(const std_msgs::Int32MultiArray&);
		void torqueCon_CB(const std_msgs::Int32MultiArray&);
		void stiffnessCon_CB(const std_msgs::Int32MultiArray&);
		// CPG:
			// AFDC
		void flagStrAFDC_CB(const std_msgs::Bool&);
			// AFDC - Right
		void rightLegFreqAFDC_CB(const std_msgs::Float64&);
		void rightCpgO0AFDC_CB(const std_msgs::Float64&);
		void rightCpgO1AFDC_CB(const std_msgs::Float64&);
		void rightCpgO2AFDC_CB(const std_msgs::Float64&);
			// AFDC - Left
		void leftLegFreqAFDC_CB(const std_msgs::Float64&);
		void leftCpgO0AFDC_CB(const std_msgs::Float64&);
		void leftCpgO1AFDC_CB(const std_msgs::Float64&);
		void leftCpgO2AFDC_CB(const std_msgs::Float64&);
			// SO2 Action
		void flagStrSO2Action_CB(const std_msgs::Bool& );
		void flagStrSO2ActionPhaseZero_CB(const std_msgs::Bool&);
			// SO2 Action - Right
		void rightLegFreqSO2Action_CB(const std_msgs::Float64&);
		void rightCpgO0SO2Action_CB(const std_msgs::Float64&);
		void rightCpgO1SO2Action_CB(const std_msgs::Float64&);
		void rightCpgO2SO2Action_CB(const std_msgs::Float64&);
			// SO2 Action - Left
		void leftLegFreqSO2Action_CB(const std_msgs::Float64&);
		void leftCpgO0SO2Action_CB(const std_msgs::Float64&);
		void leftCpgO1SO2Action_CB(const std_msgs::Float64&);
		void leftCpgO2SO2Action_CB(const std_msgs::Float64&);


};


#endif //EXVISPARALLELREALRWROS_H
