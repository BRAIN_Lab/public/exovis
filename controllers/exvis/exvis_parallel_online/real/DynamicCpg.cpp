/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "DynamicCpg.h"

//***************************************************************************

static double DEGTORAD = M_PI/180.0;


DynamicCpg::DynamicCpg(double cpgInitialPhi, const string& name, bool oneCPG, int CPGphase, const string& CPGtype, const string& CPGfunc) {

	enableCpgController = false;//reflexive controller starts
	dynRegister = new DynamicVector(4);

	systemVectorFreq.push_back(0);
	systemVectorFreq.push_back(0);

	delay.push_back(0);
	delay.push_back(0);

	der.push_back(0);
    der.push_back(0);

	//
    delayFeedback = false;
    delayFiltered = false;

//	cpg = new plastic(0.2, 0.2, 0.2, cpgInitialPhi, 1.01);//1.01//0.01
    if (CPGphase == 0){
    	cpg = new plastic(0, 0.2, 0.2, cpgInitialPhi, 1.01);
    } else if (CPGphase == 180) {
    	cpg = new plastic(0, -0.2, 0.2, cpgInitialPhi, 1.01);
    }

	checkSignal = new derivativeTransitionRegister(4,1); //the 4 has actually no effect, check instead the 1
	filterFeedback = new lowPass_filter(0.2);//0.2
	feedbackDelay = new shift_register(4);//3//56
	minFeedback = 1000; //needs to be a high number, just to allow the min function to work
	maxFeedback = 0;

	step = 0;
	countPerturbation = 0;


	// + ChaicharnA -------------------------
	InstName = name;
	string homepath = getenv("HOME");

	if (CPGtype == "AFDC") { // Save file when using as AFDC ------------------------

		string filename;
		if (oneCPG) {
			filename = homepath + "/Experiment/exvisResult/twoleg_AFDC.txt";
		} else {
			if (InstName == "DinLeft") {
				filename = homepath + "/Experiment/exvisResult/TwoCPG_leftleg_AFDC_" + CPGfunc + ".txt";
			} else if (InstName == "DinRight") {
				filename = homepath + "/Experiment/exvisResult/TwoCPG_rightleg_AFDC_" + CPGfunc + ".txt";
			}
		}

		cpgPlot.open(filename.c_str());

	} else if (CPGtype == "SO2") { // Save file when using as SO2 ------------------------

		string filename_so2;
		if (oneCPG) {
			filename_so2 = homepath + "/Experiment/exvisResult/twoleg_SO2.txt";
		} else {
			if (InstName == "DinLeft") {
				filename_so2 = homepath + "/Experiment/exvisResult/TwoCPG_leftleg_SO2_" + CPGfunc + ".txt";
			} else if (InstName == "DinRight") {
				filename_so2 = homepath + "/Experiment/exvisResult/TwoCPG_rightleg_SO2_" + CPGfunc + ".txt";
			}
		}

		cpgPlot_soTwo.open(filename_so2.c_str());

	}
	// - ChaicharnA -------------------------

	// 1 CPG ---------------------------------------


	// 2 CPG: Right+Left ---------------------------
	shiftVector.push_back(0);
	shiftVector.push_back(0);

    derOut1.push_back(0);
    derOut1.push_back(0);

    derOut2.push_back(0);
    derOut2.push_back(0);


}


double DynamicCpg::changeRange(double oldMin, double oldMax, double newMin, double newMax, double value)
{

	return ((value-oldMin)/(oldMax-oldMin)) * (newMax-newMin) + newMin;

}



double DynamicCpg::getAmplitudeHips(double signal, double max)
{
	if (signal > 0 && signal > max)
		max = signal;

	return max;

}


double  DynamicCpg::getAmplitudeKnee(double signal_knee, double max)
{
	if (signal_knee > 0 && signal_knee > max)
		max = signal_knee;

	return max;

}


plastic* DynamicCpg::getCpg()
{
	return cpg;
}

double DynamicCpg::getCpgO0(){
	return cpgO0;
}

double DynamicCpg::getCpgO1(){
	return cpgO1;
}

double DynamicCpg::getCpgO2(){
	return cpgO2;
}

double DynamicCpg::getAbsol(double a, double b)
{
	double result=a-b;
	if (result>0)
		return result;
	else return -result;

}


int DynamicCpg::getEnable()
{
	return enable;
}


void DynamicCpg::setEnable(bool externalCondition, double motor0, double leftFoot, double rightFoot)
{
	if(externalCondition == false)
	{
		enable=0;
		counter=0;
	}

	// cpgSignal0 = output to drive Hip's motor created from post-processing of our cpg
	// motor0 = real signal to drive Hip's motor selected from Reflex or Cpg method
	// If two signals above do not far from each other, it is ok.

	// leftFoot > 3000? 0:1, 0=In the air, 1=On ground
	// rightFoot > 3000? 0:1, 0=In the air, 1=On ground
	// If only one leg on the ground, it is ok.

	if (externalCondition == true && counter==0 && getAbsol(cpgSignal0,motor0) < 0.07 && leftFoot*rightFoot==0 )
	{
		enable=1;
		counter++;
	}
}


vector<double> DynamicCpg::getMaxMinFeedback(double feedback, double max, double min)
{
	vector<double> fin;
	if(feedback > max)
		max=feedback;
	if(feedback < min)
		min=feedback;

   fin.push_back(max);
   fin.push_back(min);
   return fin;
}


// + ChaicharnA ------------------
vector<double> DynamicCpg::getValueToShow(){

	vector<double> result;
	result.push_back(GFeedback);
	result.push_back(GFBfilter);
	result.push_back(GFBScale);
	result.push_back(GOsc);
	result.push_back(cpgOut1);

	return result;
}

vector<double> DynamicCpg::getValueToShow2CPG(){

	vector<double> result;
	result.push_back(GFeedback);
	result.push_back(GFBfilter);
	result.push_back(GFBScale);
	result.push_back(GOsc);
	result.push_back(twoCpgOut);

	return result;
}
// - ChaicharnA ------------------




// Common fn. for "2 CPG: Right/Left" and "1 CPG"  ##################################################################################################################################
vector<double> DynamicCpg::getGeneratedMotorsSingle()
{

	vector<double> temp;
	temp.push_back(cpgSignal0);
	temp.push_back(cpgSignal1);
	return temp;
}


vector<double> DynamicCpg::getGeneratedMotorsDouble()
{

	vector<double> temp;
	temp.push_back(cpgSignal0);
	temp.push_back(cpgSignal1);
	temp.push_back(cpgSignal2);
	temp.push_back(cpgSignal3);


	return temp;
}

// 2 CPG: Right/Left ##################################################################################################################################

double DynamicCpg::getShiftDelay(double out1, double out2, int step, double frequency)
{
	derOut1[0]=derOut1.at(1);
	derOut1[1]=tanh(20*out1);
	double derOut1Value=derOut1.at(1)-derOut1.at(0);

	derOut2[0]=derOut2.at(1);
	derOut2[1]=tanh(15000*out2);
	double derOut2Value=derOut2.at(1)-derOut2.at(0);

	if(tanh(20*out1)>0 && derOut1Value>0 && counterDelay1Value==0)
	{
		shiftVector[0]=step;
		countDelay1=true;
		counterDelay1Value++;
	}

	if(tanh(15000*out2)>0 && derOut2Value>0 && counterDelay2Value==0)
	{
		shiftVector[1]=step;
		countDelay2=true;
		counterDelay2Value++;
	}

	if(countDelay1==true && countDelay2==true)
	{
		phaseDelay=shiftVector.at(1)-shiftVector.at(0);
		countDelay1=false;

		countDelay2=false;

	}

	if(tanh(20*out1) == 0 && tanh(15000*out2)== 0)
	{
		counterDelay1Value=0;
		counterDelay2Value=0;

	}


	cout << "CountDelay" << countDelay << endl;;
	return phaseDelay;

}



// Use this ... ChaicharnA
double DynamicCpg::generateOutputOneLeg(int feedback, int stepValue, double timeStep, int cpgPhaseValue, bool applyPer)
{
	cout << "-----------" << InstName << "-----------" << endl;

	// Convert int to double
	feedback = (double)feedback;


	if(!isnan(abs(feedback))) {
		countPerturbation++;//counting how many steps the perturbation is present
	} else {
		countPerturbation = 0;
	}

	if (debugON) {
	cout << "Perturbation time " << countPerturbation << endl;
	}
	step = stepValue;


	double fbFilter; // signal after smoothing
	double fbScale = 0.0; // signal after scaling

	if(!isnan(abs(feedback))) { //if there is a perturbation then is processed, filtered and change range
		fbFilter = filterFeedback->update(feedback);
		fbScale = changeRange(minFeedback, maxFeedback, -0.2, 0.2, fbFilter);

		if (isnan(abs(fbScale)) || isinf(fbScale)) {
			fbScale = 0.2;
		}

	} else {
		fbScale = 0.0; // otherwise set perturbation to zero
	}

	if (debugON) {
	cout << "feedback: " << feedback << endl;
	cout << "fbFilter: " << fbFilter << endl;
	cout << "minFeedback: " << minFeedback << endl;
	cout << "maxFeedback: " << maxFeedback << endl;
	cout << "fbScale: " << fbScale << endl;
	string checkFBNaN = (isnan(abs(fbScale)))? "true" : "false";
	cout << "fbScale is NaN: " << checkFBNaN << endl;
	}

	int oscillationFeedback = (maxFeedback+minFeedback)/2; //get the oscillation point of the perturbation
	bool look = true;


    // + ChaicharnA ----------------------------------
    GFeedback = feedback;
    GFBfilter = fbFilter;
    GFBScale = fbScale;
    GOsc = (double)oscillationFeedback;
    // - ChaicharnA ----------------------------------


	// + compute delay between feedback and fbFilter ---------------------------------------
	if(feedback > oscillationFeedback && counterDelayFeedback == 0 && delayFeedback == false )
	{
		delay[0] = step;
		counterDelayFeedback++;
		delayFeedback = true;
	}

	if(fbFilter > oscillationFeedback && counterDelayFiltered == 0 && delayFiltered == false )
	{
		delay[1] = step;
		counterDelayFiltered++;
		delayFiltered = true;
	 }

	if(feedback < oscillationFeedback)
		counterDelayFeedback = 0;

	if(fbFilter < oscillationFeedback)
		counterDelayFiltered = 0;

	if(delayFeedback == true && delayFiltered == true )
	{
		delayValue = delay.at(1)-delay.at(0);
		delayFeedback = false;
		delayFiltered = false;
	}
	// - compute delay between feedback and fbFilter ---------------------------------------


    // + ChaicharnA ----------------------------------
    GcounterDelayFeedback = (double)counterDelayFeedback;
    GcounterDelayFiltered = (double)counterDelayFiltered;
    GdelayValue = (double) delayValue;

    // - ChaicharnA ----------------------------------


	//compute delay... the delay is in delayValue



	// + Input perturbation into CPG -----------
	double perturbation = 0.0;
//    double freq = 1;
//    double amp = 0.2;

	if(applyPer || step >= 400000)
	{
		perturbation = fbScale;

//		perturbation = amp*sin(2*M_PI*freq*step*1e-3);
	}
//	else if (step > 2000) // turn off perturbation after applying cpg control to robot
//	{
//		perturbation = 0;
//
//	}

	cpg->update(perturbation, step, timeStep);//apply perturbation to cpg
	// - Input perturbation into CPG -----------


	// + Find max/min of Feedback -------------
	vector<double> vec;
	if (!isnan(feedback))//check max of feedback, only if present
	{
		vec = getMaxMinFeedback(feedback, maxFeedback,minFeedback);

		maxFeedback = vec.at(0);
		minFeedback = vec.at(1);
	}
	// - Find max/min of Feedback -------------


	// + Choose CPG signal -----------
	cpgPhase = cpgPhaseValue;

	if (cpgPhase == 0) {
		twoCpgOut = cpg->getOut1();
		if (debugON) {
		cout << "cpgPhase: " << cpgPhase << endl;
		}
	} else if (cpgPhase == 180) {
		// Have to implement better technique to do any phase shift
		twoCpgOut = cpg->getOut1();
		if (debugON) {
		cout << "cpgPhase: " << cpgPhase << endl;
		}
	}
	// - Choose CPG signal -----------



    // *************************************************************************
    // * cpgPlot to file
    // *************************************************************************
	cpgO0 = cpg->getOut0();
	cpgO1 = cpg->getOut1();
	cpgO2 = cpg->getOut2();

	if (debugON) {
	cout << "Perturbation: " << perturbation << endl;
	cout << "cpgO0: " << cpgO0 << endl;
	cout << "cpgO1: " << cpgO1 << endl;
	cout << "cpgO2: " << cpgO2 << endl;
	cout << "cpgFreq: " << cpg->getFreq() << endl;
	}
	// + Write to files -----------------------------
    cpgPlot << step << " "
    		<< GFeedback << " "
			<< GFBfilter << " "
			<< GOsc << " "
			<< GcounterDelayFeedback << " "
			<< GcounterDelayFiltered << " "
			<< GdelayValue << " "
			<< perturbation << " "
			<< twoCpgOut << " "
			<< cpgO0 << " "
			<< cpgO1 << " "
			<< cpgO2 << " "
			<< cpg->getFreq() << endl;
	// - Write to files -----------------------------

	// *************************************************************************

	return cpg->getFreq();
}

void DynamicCpg::soTwoMode(double cpgInternalPhi, int step, double timeStep) {

    // *************************************************************************
    // * Change CPG to SO2
    // *************************************************************************
	cout << "SO2 mode active" << endl;
	cpg->updateSoTwo(cpgInternalPhi, step, timeStep);

    // *************************************************************************
    // * cpgPlot_soTwo to file
    // *************************************************************************
	cpgO0 = cpg->getOut0();
	cpgO1 = cpg->getOut1();
	cpgPlot_soTwo	<< step << " "
					<< cpgO0 << " "
					<< cpgO1 << " "
					<< cpg->getFreq() << endl;

}


// 1 CPG ##################################################################################################################################




// Destructor ##################################################################################################################################
DynamicCpg::~DynamicCpg() {
	// TODO Auto-generated destructor stub
}
