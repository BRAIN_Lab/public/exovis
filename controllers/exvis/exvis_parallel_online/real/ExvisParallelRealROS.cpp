/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealROS.h"

//***************************************************************************

ExvisParallelRealROS::ExvisParallelRealROS(int argc, char **argv) {
    // Create a ROS nodes
    int _argc = 0;
    char** _argv = NULL;
    ros::init(_argc,_argv,"ExvisParallelRealAssem");

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("ExvisParallelRealROS just started!");

    // --------------------------
    // Initialize Subscribers
    // --------------------------
    // TIME:
    timeStampCANSub = node.subscribe("/exvis/timeStampCAN", 1, &ExvisParallelRealROS::timeStampCAN_CB, this);
    // EXO:
    jointAngleSub = node.subscribe("/exvis/jointAngle", 1, &ExvisParallelRealROS::jointAngle_CB, this);
    jointTorqueSub = node.subscribe("/exvis/jointTorque", 1, &ExvisParallelRealROS::jointTorque_CB, this);
    motorTorqueSub = node.subscribe("/exvis/motorTorque", 1, &ExvisParallelRealROS::motorTorque_CB, this);
    footSwitchSub = node.subscribe("/exvis/footSwitch", 1, &ExvisParallelRealROS::footSwitch_CB, this);
    exoStatusSub = node.subscribe("/exvis/exoStatus", 1, &ExvisParallelRealROS::exoStatus_CB, this);
    // UI:
    modeSub = node.subscribe("/exvis/in/mode", 1, &ExvisParallelRealROS::mode_CB, this);
    speedSub = node.subscribe("/exvis/in/speed", 1, &ExvisParallelRealROS::speed_CB, this);
    assistSub = node.subscribe("/exvis/in/assist", 1, &ExvisParallelRealROS::assist_CB, this);
    onlineVsManualFreqSub = node.subscribe("/exvis/in/onlineVsManualFreq", 1, &ExvisParallelRealROS::onlineVsManualFreq_CB, this);
    freqManualSub = node.subscribe("/exvis/in/freqManual", 1, &ExvisParallelRealROS::freqManual_CB, this);
    upDownFreqValSub = node.subscribe("/exvis/in/upDownFreqVal", 1, &ExvisParallelRealROS::upDownFreqVal_CB, this);
    notiNo_1_DoneSub = node.subscribe("/exvis/noti/notiNo_1_Done", 1, &ExvisParallelRealROS::notiNo_1_Done_CB, this);
    setpointValueInSub = node.subscribe("/exvis/in/setpointValueIn", 1, &ExvisParallelRealROS::setpointValueIn_CB, this);
    switchFlagSub = node.subscribe("/exvis/in/switchFlag", 1, &ExvisParallelRealROS::switchFlag_CB, this);
    // CPG:
        // AFDC - Right
    rightLegFreqAFDCSub = node.subscribe("/exvis/cpg/rightLegFreqAFDC", 1, &ExvisParallelRealROS::rightLegFreqAFDC_CB, this);
    rightCpgO0AFDCSub = node.subscribe("/exvis/cpg/rightCpgO0AFDC", 1, &ExvisParallelRealROS::rightCpgO0AFDC_CB, this);
    rightCpgO1AFDCSub = node.subscribe("/exvis/cpg/rightCpgO1AFDC", 1, &ExvisParallelRealROS::rightCpgO1AFDC_CB, this);
    rightCpgO2AFDCSub = node.subscribe("/exvis/cpg/rightCpgO2AFDC", 1, &ExvisParallelRealROS::rightCpgO2AFDC_CB, this);
        // AFDC - Left
    leftLegFreqAFDCSub = node.subscribe("/exvis/cpg/leftLegFreqAFDC", 1, &ExvisParallelRealROS::leftLegFreqAFDC_CB, this);
    leftCpgO0AFDCSub = node.subscribe("/exvis/cpg/leftCpgO0AFDC", 1, &ExvisParallelRealROS::leftCpgO0AFDC_CB, this);
    leftCpgO1AFDCSub = node.subscribe("/exvis/cpg/leftCpgO1AFDC", 1, &ExvisParallelRealROS::leftCpgO1AFDC_CB, this);
    leftCpgO2AFDCSub = node.subscribe("/exvis/cpg/leftCpgO2AFDC", 1, &ExvisParallelRealROS::leftCpgO2AFDC_CB, this);
        // SO2 Learn - Right
    rightLegFreqSO2LearnSub = node.subscribe("/exvis/cpg/rightLegFreqSO2Learn", 1, &ExvisParallelRealROS::rightLegFreqSO2Learn_CB, this);
    rightCpgO0SO2LearnSub = node.subscribe("/exvis/cpg/rightCpgO0SO2Learn", 1, &ExvisParallelRealROS::rightCpgO0SO2Learn_CB, this);
    rightCpgO1SO2LearnSub = node.subscribe("/exvis/cpg/rightCpgO1SO2Learn", 1, &ExvisParallelRealROS::rightCpgO1SO2Learn_CB, this);
    rightCpgO2SO2LearnSub = node.subscribe("/exvis/cpg/rightCpgO2SO2Learn", 1, &ExvisParallelRealROS::rightCpgO2SO2Learn_CB, this);
        // SO2 Learn - Left
    leftLegFreqSO2LearnSub = node.subscribe("/exvis/cpg/leftLegFreqSO2Learn", 1, &ExvisParallelRealROS::leftLegFreqSO2Learn_CB, this);
    leftCpgO0SO2LearnSub = node.subscribe("/exvis/cpg/leftCpgO0SO2Learn", 1, &ExvisParallelRealROS::leftCpgO0SO2Learn_CB, this);
    leftCpgO1SO2LearnSub = node.subscribe("/exvis/cpg/leftCpgO1SO2Learn", 1, &ExvisParallelRealROS::leftCpgO1SO2Learn_CB, this);
    leftCpgO2SO2LearnSub = node.subscribe("/exvis/cpg/leftCpgO2SO2Learn", 1, &ExvisParallelRealROS::leftCpgO2SO2Learn_CB, this);
        // SO2 Action - Right
    rightLegFreqSO2ActionSub = node.subscribe("/exvis/cpg/rightLegFreqSO2Action", 1, &ExvisParallelRealROS::rightLegFreqSO2Action_CB, this);
    rightCpgO0SO2ActionSub = node.subscribe("/exvis/cpg/rightCpgO0SO2Action", 1, &ExvisParallelRealROS::rightCpgO0SO2Action_CB, this);
    rightCpgO1SO2ActionSub = node.subscribe("/exvis/cpg/rightCpgO1SO2Action", 1, &ExvisParallelRealROS::rightCpgO1SO2Action_CB, this);
    rightCpgO2SO2ActionSub = node.subscribe("/exvis/cpg/rightCpgO2SO2Action", 1, &ExvisParallelRealROS::rightCpgO2SO2Action_CB, this);
        // SO2 Action - Left
    leftLegFreqSO2ActionSub = node.subscribe("/exvis/cpg/leftLegFreqSO2Action", 1, &ExvisParallelRealROS::leftLegFreqSO2Action_CB, this);
    leftCpgO0SO2ActionSub = node.subscribe("/exvis/cpg/leftCpgO0SO2Action", 1, &ExvisParallelRealROS::leftCpgO0SO2Action_CB, this);
    leftCpgO1SO2ActionSub = node.subscribe("/exvis/cpg/leftCpgO1SO2Action", 1, &ExvisParallelRealROS::leftCpgO1SO2Action_CB, this);
    leftCpgO2SO2ActionSub = node.subscribe("/exvis/cpg/leftCpgO2SO2Action", 1, &ExvisParallelRealROS::leftCpgO2SO2Action_CB, this);

    // --------------------------
    // Initialize Publishers
    // --------------------------
    // UI:
    typeConPub = node.advertise<std_msgs::Int32>("/exvis/con/type",1);
    speedConPub = node.advertise<std_msgs::Int32>("/exvis/con/speed",1);
    dataPtsConPub = node.advertise<std_msgs::Int32>("/exvis/con/dataPts",1);
    assistConPub = node.advertise<std_msgs::Int32>("/exvis/con/assist",1);
    notiNoPub = node.advertise<std_msgs::Int32>("/exvis/noti/notiNo",1);
    // EXO:
        // Control signal
    jointAngleConPub = node.advertise<std_msgs::Int32MultiArray>("/exvis/con/jointAngle",1);
    torqueConPub = node.advertise<std_msgs::Int32MultiArray>("/exvis/con/torque",1);
    stiffnessConPub = node.advertise<std_msgs::Int32MultiArray>("/exvis/con/stiffness",1);
    // CPG:
        // AFDC
    flagStrAFDCPub = node.advertise<std_msgs::Bool>("/exvis/flag/strAFDC",1);
        // SO2 Action
    flagStrSO2ActionPub = node.advertise<std_msgs::Bool>("/exvis/flag/strSO2Action",1);
    flagStrSO2ActionPhaseZeroPub = node.advertise<std_msgs::Bool>("/exvis/flag/strSO2ActionPhaseZero",1);

    // --------------------------
    // Initialize Client
    // --------------------------
    cpgServiceCli = node.serviceClient<exvis_parallel_real_cpg_online::cpgService>("/ExvisParallelRealCPGAssem/cpgService");

    // -----------
    // Set Rate
    // -----------
    timeStep = 0.001; // sec (0.001 s = 1 ms)
    rate = new ros::Rate(1/timeStep);
//    rate = new ros::Rate(100);
//    rate = new ros::Rate(17*4); // 60hz
//    rate = new ros::Rate(50);
//    rate = new ros::Rate(29);
//    rate = new ros::Rate(11.1);
//    rate = new ros::Rate(10);
//    rate = new ros::Rate(1);
//    rate = new ros::Rate(0.1);
}

// **********************************************************
// Subscriber callback
// **********************************************************
// TIME:
void ExvisParallelRealROS::timeStampCAN_CB(const std_msgs::String& _timeStampCAN) {

    // Get the data from Topic into main node variable
	timeStampCAN = _timeStampCAN.data;
	cout << "timeStampCAN: " << timeStampCAN << endl;
}

// UI:
void ExvisParallelRealROS::mode_CB(const std_msgs::String& _mode) {

    // Get the data from Topic into main node variable
	mode = _mode.data; // 
	cout << "mode: " << mode << endl;
}

void ExvisParallelRealROS::speed_CB(const std_msgs::Int32& _speed) {

    // Get the data from Topic into main node variable
	speed = _speed.data; // C++ int = ROS Int32
	cout << "speed: " << speed << endl;
}

void ExvisParallelRealROS::assist_CB(const std_msgs::Int32& _assist) {

    // Get the data from Topic into main node variable
	assist = _assist.data; // C++ int = ROS Int32
	cout << "assist: " << assist << endl;
}

void ExvisParallelRealROS::onlineVsManualFreq_CB(const std_msgs::Int32& _onlineVsManualFreq) {

    // Get the data from Topic into main node variable
	onlineVsManualFreq = _onlineVsManualFreq.data; // 
	cout << "onlineVsManualFreq: " << onlineVsManualFreq << endl;
}

void ExvisParallelRealROS::freqManual_CB(const std_msgs::Float64& _freqManual) {

    // Get the data from Topic into main node variable
	freqManual = _freqManual.data; // C++ double = ROS Float64
	cout << "freqManual: " << freqManual << endl;
}

void ExvisParallelRealROS::upDownFreqVal_CB(const std_msgs::Float64& _upDownFreqVal) {

    // Get the data from Topic into main node variable
	upDownFreqVal = _upDownFreqVal.data; // C++ double = ROS Float64
	cout << "upDownFreqVal: " << upDownFreqVal << endl;
}

void ExvisParallelRealROS::notiNo_1_Done_CB(const std_msgs::Bool& _notiNo_1_Done) {

    // Get the data from Topic into main node variable
	notiNo_1_Done = _notiNo_1_Done.data;
	cout << "notiNo_1_Done: " << notiNo_1_Done << endl;
}

void ExvisParallelRealROS::setpointValueIn_CB(const std_msgs::Int32MultiArray& _setpointValueIn) {

    // Get the data from Topic into main node variable
	setpointValueIn = _setpointValueIn.data;
	cout << "setpointValueIn: " << setpointValueIn[0] << " " << setpointValueIn[1] << " " << setpointValueIn[2] << " " << setpointValueIn[3] << " " << setpointValueIn[4] << " " << setpointValueIn[5] << endl;
}

void ExvisParallelRealROS::switchFlag_CB(const std_msgs::Bool& _switchFlag) {

    // Get the data from Topic into main node variable
	switchFlag = _switchFlag.data;
	cout << "switchFlag: " << switchFlag << endl;
}

// EXO:
    // Feedback signal
void ExvisParallelRealROS::jointAngle_CB(const std_msgs::Int32MultiArray& _jointAngle) {

    // Get the data from Topic into main node variable
	jointAngle = _jointAngle.data; // C++ int = ROS Int32
	cout << "jointAngle: " << jointAngle[0] << " " << jointAngle[1] << " " << jointAngle[2] << " " << jointAngle[3] << " " << jointAngle[4] << " " << jointAngle[5] << endl;
}

void ExvisParallelRealROS::jointTorque_CB(const std_msgs::Int32MultiArray& _jointTorque) {

    // Get the data from Topic into main node variable
	jointTorque = _jointTorque.data; // C++ int = ROS Int32
	cout << "jointTorque: " << jointTorque[0] << " " << jointTorque[1] << " " << jointTorque[2] << " " << jointTorque[3] << " " << jointTorque[4] << " " << jointTorque[5] << endl;
}

void ExvisParallelRealROS::motorTorque_CB(const std_msgs::Int32MultiArray& _motorTorque) {

    // Get the data from Topic into main node variable
	motorTorque = _motorTorque.data; // C++ int = ROS Int32
	cout << "motorTorque: " << motorTorque[0] << " " << motorTorque[1] << " " << motorTorque[2] << " " << motorTorque[3] << " " << motorTorque[4] << " " << motorTorque[5] << endl;
}

void ExvisParallelRealROS::footSwitch_CB(const std_msgs::Int32MultiArray& _footSwitch) {

    // Get the data from Topic into main node variable
	footSwitch = _footSwitch.data; // C++ int = ROS Int32
	cout << "footSwitch: " << footSwitch[0] << " " << footSwitch[1] << " " << footSwitch[2] << " " << footSwitch[3] << " " << footSwitch[4] << " " << footSwitch[5] << endl;
}

void ExvisParallelRealROS::exoStatus_CB(const std_msgs::Int32MultiArray& _exoStatus) {

    // Get the data from Topic into main node variable
	exoStatus = _exoStatus.data; // C++ int = ROS Int32
	cout << "exoStatus: " << exoStatus[0] << " " << exoStatus[1] << " " << exoStatus[2] << " " << exoStatus[3] << " " << exoStatus[4] << " " << exoStatus[5] << endl;
}

// CPG:
    // AFDC - Right
void ExvisParallelRealROS::rightLegFreqAFDC_CB(const std_msgs::Float64& _rightLegFreqAFDC) {

    // Get the data from Topic into main node variable
	rightLegFreqAFDC = _rightLegFreqAFDC.data; // C++ double = ROS Float64
	cout << "rightLegFreqAFDC: " << rightLegFreqAFDC << endl;
}

void ExvisParallelRealROS::rightCpgO0AFDC_CB(const std_msgs::Float64& _rightCpgO0AFDC) {

    // Get the data from Topic into main node variable
	rightCpgO0AFDC = _rightCpgO0AFDC.data; // C++ double = ROS Float64
	cout << "rightCpgO0AFDC: " << rightCpgO0AFDC << endl;
}

void ExvisParallelRealROS::rightCpgO1AFDC_CB(const std_msgs::Float64& _rightCpgO1AFDC) {

    // Get the data from Topic into main node variable
	rightCpgO1AFDC = _rightCpgO1AFDC.data; // C++ double = ROS Float64
	cout << "rightCpgO1AFDC: " << rightCpgO1AFDC << endl;
}

void ExvisParallelRealROS::rightCpgO2AFDC_CB(const std_msgs::Float64& _rightCpgO2AFDC) {

    // Get the data from Topic into main node variable
	rightCpgO2AFDC = _rightCpgO2AFDC.data; // C++ double = ROS Float64
	cout << "rightCpgO2AFDC: " << rightCpgO2AFDC << endl;
}

    // AFDC - Left
void ExvisParallelRealROS::leftLegFreqAFDC_CB(const std_msgs::Float64& _leftLegFreqAFDC) {

    // Get the data from Topic into main node variable
	leftLegFreqAFDC = _leftLegFreqAFDC.data; // C++ double = ROS Float64
	cout << "leftLegFreqAFDC: " << leftLegFreqAFDC << endl;
}

void ExvisParallelRealROS::leftCpgO0AFDC_CB(const std_msgs::Float64& _leftCpgO0AFDC) {

    // Get the data from Topic into main node variable
	leftCpgO0AFDC = _leftCpgO0AFDC.data; // C++ double = ROS Float64
	cout << "leftCpgO0AFDC: " << leftCpgO0AFDC << endl;
}

void ExvisParallelRealROS::leftCpgO1AFDC_CB(const std_msgs::Float64& _leftCpgO1AFDC) {

    // Get the data from Topic into main node variable
	leftCpgO1AFDC = _leftCpgO1AFDC.data; // C++ double = ROS Float64
	cout << "leftCpgO1AFDC: " << leftCpgO1AFDC << endl;
}

void ExvisParallelRealROS::leftCpgO2AFDC_CB(const std_msgs::Float64& _leftCpgO2AFDC) {

    // Get the data from Topic into main node variable
	leftCpgO2AFDC = _leftCpgO2AFDC.data; // C++ double = ROS Float64
	cout << "leftCpgO2AFDC: " << leftCpgO2AFDC << endl;
}

    // SO2 Learn - Right
void ExvisParallelRealROS::rightLegFreqSO2Learn_CB(const std_msgs::Float64& _rightLegFreqSO2Learn) {

    // Get the data from Topic into main node variable
	rightLegFreqSO2Learn = _rightLegFreqSO2Learn.data; // C++ double = ROS Float64
	cout << "rightLegFreqSO2Learn: " << rightLegFreqSO2Learn << endl;
}

void ExvisParallelRealROS::rightCpgO0SO2Learn_CB(const std_msgs::Float64& _rightCpgO0SO2Learn) {

    // Get the data from Topic into main node variable
	rightCpgO0SO2Learn = _rightCpgO0SO2Learn.data; // C++ double = ROS Float64
	cout << "rightCpgO0SO2Learn: " << rightCpgO0SO2Learn << endl;
}

void ExvisParallelRealROS::rightCpgO1SO2Learn_CB(const std_msgs::Float64& _rightCpgO1SO2Learn) {

    // Get the data from Topic into main node variable
	rightCpgO1SO2Learn = _rightCpgO1SO2Learn.data; // C++ double = ROS Float64
	cout << "rightCpgO1SO2Learn: " << rightCpgO1SO2Learn << endl;
}

void ExvisParallelRealROS::rightCpgO2SO2Learn_CB(const std_msgs::Float64& _rightCpgO2SO2Learn) {

    // Get the data from Topic into main node variable
	rightCpgO2SO2Learn = _rightCpgO2SO2Learn.data; // C++ double = ROS Float64
	cout << "rightCpgO2SO2Learn: " << rightCpgO2SO2Learn << endl;
}

    // SO2 Learn - Left
void ExvisParallelRealROS::leftLegFreqSO2Learn_CB(const std_msgs::Float64& _leftLegFreqSO2Learn) {

    // Get the data from Topic into main node variable
	leftLegFreqSO2Learn = _leftLegFreqSO2Learn.data; // C++ double = ROS Float64
	cout << "leftLegFreqSO2Learn: " << leftLegFreqSO2Learn << endl;
}

void ExvisParallelRealROS::leftCpgO0SO2Learn_CB(const std_msgs::Float64& _leftCpgO0SO2Learn) {

    // Get the data from Topic into main node variable
	leftCpgO0SO2Learn = _leftCpgO0SO2Learn.data; // C++ double = ROS Float64
	cout << "leftCpgO0SO2Learn: " << leftCpgO0SO2Learn << endl;
}

void ExvisParallelRealROS::leftCpgO1SO2Learn_CB(const std_msgs::Float64& _leftCpgO1SO2Learn) {

    // Get the data from Topic into main node variable
	leftCpgO1SO2Learn = _leftCpgO1SO2Learn.data; // C++ double = ROS Float64
	cout << "leftCpgO1SO2Learn: " << leftCpgO1SO2Learn << endl;
}

void ExvisParallelRealROS::leftCpgO2SO2Learn_CB(const std_msgs::Float64& _leftCpgO2SO2Learn) {

    // Get the data from Topic into main node variable
	leftCpgO2SO2Learn = _leftCpgO2SO2Learn.data; // C++ double = ROS Float64
	cout << "leftCpgO2SO2Learn: " << leftCpgO2SO2Learn << endl;
}

    // SO2 Action - Right
void ExvisParallelRealROS::rightLegFreqSO2Action_CB(const std_msgs::Float64& _rightLegFreqSO2Action) {

    // Get the data from Topic into main node variable
	rightLegFreqSO2Action = _rightLegFreqSO2Action.data; // C++ double = ROS Float64
	cout << "rightLegFreqSO2Action: " << rightLegFreqSO2Action << endl;
}

void ExvisParallelRealROS::rightCpgO0SO2Action_CB(const std_msgs::Float64& _rightCpgO0SO2Action) {

    // Get the data from Topic into main node variable
	rightCpgO0SO2Action = _rightCpgO0SO2Action.data; // C++ double = ROS Float64
	cout << "rightCpgO0SO2Action: " << rightCpgO0SO2Action << endl;
}

void ExvisParallelRealROS::rightCpgO1SO2Action_CB(const std_msgs::Float64& _rightCpgO1SO2Action) {

    // Get the data from Topic into main node variable
	rightCpgO1SO2Action = _rightCpgO1SO2Action.data; // C++ double = ROS Float64
	cout << "rightCpgO1SO2Action: " << rightCpgO1SO2Action << endl;
}

void ExvisParallelRealROS::rightCpgO2SO2Action_CB(const std_msgs::Float64& _rightCpgO2SO2Action) {

    // Get the data from Topic into main node variable
	rightCpgO2SO2Action = _rightCpgO2SO2Action.data; // C++ double = ROS Float64
	cout << "rightCpgO2SO2Action: " << rightCpgO2SO2Action << endl;
}

    // SO2 Action - Left
void ExvisParallelRealROS::leftLegFreqSO2Action_CB(const std_msgs::Float64& _leftLegFreqSO2Action) {

    // Get the data from Topic into main node variable
	leftLegFreqSO2Action = _leftLegFreqSO2Action.data; // C++ double = ROS Float64
	cout << "leftLegFreqSO2Action: " << leftLegFreqSO2Action << endl;
}

void ExvisParallelRealROS::leftCpgO0SO2Action_CB(const std_msgs::Float64& _leftCpgO0SO2Action) {

    // Get the data from Topic into main node variable
	leftCpgO0SO2Action = _leftCpgO0SO2Action.data; // C++ double = ROS Float64
	cout << "leftCpgO0SO2Action: " << leftCpgO0SO2Action << endl;
}

void ExvisParallelRealROS::leftCpgO1SO2Action_CB(const std_msgs::Float64& _leftCpgO1SO2Action) {

    // Get the data from Topic into main node variable
	leftCpgO1SO2Action = _leftCpgO1SO2Action.data; // C++ double = ROS Float64
	cout << "leftCpgO1SO2Action: " << leftCpgO1SO2Action << endl;
}

void ExvisParallelRealROS::leftCpgO2SO2Action_CB(const std_msgs::Float64& _leftCpgO2SO2Action) {

    // Get the data from Topic into main node variable
	leftCpgO2SO2Action = _leftCpgO2SO2Action.data; // C++ double = ROS Float64
	cout << "leftCpgO2SO2Action: " << leftCpgO2SO2Action << endl;
}

// **********************************************************
// Publisher send function
// **********************************************************
// UI:
void ExvisParallelRealROS::sendTypeCon(int typeCon) {
	std_msgs::Int32 typeConP;
	typeConP.data = typeCon;
	typeConPub.publish(typeConP);
	cout << "typeCon: " << typeCon << " " << "... typeCon Published !" << endl;
}

void ExvisParallelRealROS::sendSpeedCon(int speedCon) {
	std_msgs::Int32 speedConP;
	speedConP.data = speedCon;
	speedConPub.publish(speedConP);
	cout << "speedCon: " << speedCon << " " << "... speedCon Published !" << endl;
}

void ExvisParallelRealROS::sendDataPtsCon(int dataPtsCon) {
	std_msgs::Int32 dataPtsConP;
	dataPtsConP.data = dataPtsCon;
	dataPtsConPub.publish(dataPtsConP);
	cout << "dataPtsCon: " << dataPtsCon << " " << "... dataPtsCon Published !" << endl;
}

void ExvisParallelRealROS::sendAssistCon(int assistCon) {
	std_msgs::Int32 assistConP;
	assistConP.data = assistCon;
	assistConPub.publish(assistConP);
	cout << "assistCon: " << assistCon << " " << "... assistCon Published !" << endl;
}

void ExvisParallelRealROS::sendNotiNo(int notiNumber) {
    // publish the notification
    std_msgs::Int32 notiNoP;
    notiNoP.data = notiNumber;
    notiNoPub.publish(notiNoP);
    cout << "notiNo: " << notiNumber << " " << "... notiNo Published !" << endl;
}

// EXO:
    // Control signal
void ExvisParallelRealROS::sendJointAngleCon(vector<int> jointAngleCon) {
    // publish the motor positions:
    std_msgs::Int32MultiArray array;
    array.data.clear();

    for (int vJointAngle : jointAngleCon) {
        array.data.push_back(vJointAngle);
    }

    jointAngleConPub.publish(array);
    cout << "jointAngleCon: " << jointAngleCon[0] << " " << jointAngleCon[1] << " " << jointAngleCon[2] << " " << jointAngleCon[3] << " " << jointAngleCon[4] << " " << jointAngleCon[5] << " " << "... jointAngleCon Published !" << endl;
}

void ExvisParallelRealROS::sendTorqueCon(vector<int> torqueCon) {
    // publish the motor torque:
    std_msgs::Int32MultiArray array;
    array.data.clear();

    for (int vTorque : torqueCon) {
        array.data.push_back(vTorque);
    }

    torqueConPub.publish(array);
    cout << "torqueCon: " << torqueCon[0] << " " << torqueCon[1] << " " << torqueCon[2] << " " << torqueCon[3] << " " << torqueCon[4] << " " << torqueCon[5] << " " << "... torqueCon Published !" << endl;
}

void ExvisParallelRealROS::sendStiffnessCon(vector<int> stiffnessCon) {
    // publish the motor stiffness:
    std_msgs::Int32MultiArray array;
    array.data.clear();

    for (int vStiffness : stiffnessCon) {
        array.data.push_back(vStiffness);
    }

    stiffnessConPub.publish(array);
    cout << "stiffnessCon: " << stiffnessCon[0] << " " << stiffnessCon[1] << " " << stiffnessCon[2] << " " << stiffnessCon[3] << " " << stiffnessCon[4] << " " << stiffnessCon[5] << " " << "... sitffnessCon Published !" << endl;
}

// CPG:
    // AFDC
void ExvisParallelRealROS::sendFlagStrAFDC(bool flagStrAFDCV) {
    std_msgs::Bool flagStrAFDCP;
    flagStrAFDCP.data = flagStrAFDCV;
    flagStrAFDCPub.publish(flagStrAFDCP);
    cout << "flagStrAFDCP: " << flagStrAFDCV << " " << "... flagStrAFDCP Published !" << endl;
}
    // SO2 Action
void ExvisParallelRealROS::sendFlagStrSO2Action(bool flagStrSO2ActionV) {
    std_msgs::Bool flagStrSO2ActionP;
    flagStrSO2ActionP.data = flagStrSO2ActionV;
    flagStrSO2ActionPub.publish(flagStrSO2ActionP);
    cout << "flagStrSO2Action: " << flagStrSO2ActionV << " " << "... flagStrSO2Action Published !" << endl;
}

void ExvisParallelRealROS::sendFlagStrSO2ActionPhaseZero(bool flagStrSO2ActionPhaseZeroV) {
    std_msgs::Bool flagStrSO2ActionPhaseZeroP;
    flagStrSO2ActionPhaseZeroP.data = flagStrSO2ActionPhaseZeroV;
    flagStrSO2ActionPhaseZeroPub.publish(flagStrSO2ActionPhaseZeroP);
    cout << "flagStrSO2ActionPhaseZero: " << flagStrSO2ActionPhaseZeroV << " " << "... flagStrSO2ActionPhaseZero Published !" << endl;
}

// **********************************************************
// Client send function
// **********************************************************
bool ExvisParallelRealROS::sendCpgService(const string& cpgTypeV, const string& cpgFuncV, double initFreqRV, double initFreqLV, double updateFreqRV, double updateFreqLV) { 

    exvis_parallel_real_cpg_online::cpgService srv;
    srv.request.cpgType = cpgTypeV;
    srv.request.cpgFunc = cpgFuncV;
    srv.request.initFreqR = initFreqRV; // Internal freq unit
    srv.request.initFreqL = initFreqLV; // Internal freq unit
    srv.request.updateFreqR = updateFreqRV; // Internal freq unit
    srv.request.updateFreqL = updateFreqLV; // Internal freq unit

    cout << "cpgServiceCli.call(srv): " << cpgServiceCli.call(srv) << endl;
    if (cpgServiceCli.call(srv)) {
        cout << "Success to call cpgService" << endl;
    } else {
        cout << "Failed to call cpgService" << endl;
    }

    return srv.response.success;

}


// **********************************************************
// rosSpinOnce
// **********************************************************
void ExvisParallelRealROS::rosSpinOnce(){

	cout << "ExvisParallelRealROS node is spinning" << endl;

	ros::spinOnce();
    bool rateMet = rate->sleep();

    if(!rateMet)
    {
        ROS_ERROR("Sleep rate not met");
    }

}

ExvisParallelRealROS::~ExvisParallelRealROS() {
    ROS_INFO("ExvisParallelRealROS just terminated!");
    ros::shutdown();
}
