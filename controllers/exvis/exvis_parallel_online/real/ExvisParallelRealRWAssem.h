/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#ifndef EXVISPARALLELREALRWASSEM_H
#define EXVISPARALLELREALRWASSEM_H

#include <stdio.h> // standard input / output functions
#include <unistd.h> // standard function definitions
#include <stdlib.h> // variable types, macros, functions
#include <string.h> // string function definitions
#include <iostream> // C++ to use cout
#include <fstream> // To read/write on file
#include <vector>
#include <map>
#include <queue>

using namespace std;

#include "ExvisParallelRealRWROS.h"
#include "jsoncpp/json/json.h"
//***************************************************************************
class ExvisParallelRealRWROS;

class ExvisParallelRealRWAssem {
	public:
		

		// ----------------------------------------------
		// Public Methods
		// ----------------------------------------------
		ExvisParallelRealRWAssem(int argc,char* argv[]);
		~ExvisParallelRealRWAssem();
		bool runAssem();


	private:

		// Class obj ====================================
		ExvisParallelRealRWROS * realRos;
		// ==============================================

		int nodeCounter = 0;
		timespec tNode;

		int modeMain;
		int modeSub;
		int modeSubSub;

		int typeCon;

		bool flagStrAFDC = false;
		bool flagStrSO2ActionFristTime = false;
		bool flagStrSO2Action = false;
		bool flagStrSO2ActionPhaseZero = false;
		
		bool flagRecodSO2ActionFristTime = false;
		bool flagLogRecordStrAFDC = false;
		bool flagLogRecordStrSO2Action = false;
		bool flagLogStrSO2ActionPhaseZero = false;
		
		bool openJsonWriteSignal = false;
		bool enterModeBasicAdapFirstTime = false;
		bool openJsonWriteZeroTorque = false;
		bool enterModeBaicZeroTorque = false;

		// Record file ============================
		const char *homedir;


		// + To record exvisJSONSignal.json ----------------------------
		// + JSON Writer -------------------
		char jsonWriteSignal[100]; // To store path for write
		ofstream ofsjWS;
		// - JSON Writer -------------------

		bool existJsonWriteSignal = false;
		bool clearJArray = false;

		// json obj/record 
		// Ex. 
		//	objjWs["ID1"] = [x1, x2]; // array
		//	objjWs["ID2"] = "xxx"
		// 	objjWS["ID3"] = {y1, y2}; // vector
		// gives objjWS = {"ID1": [[x1, x2]], "ID2": ["xxx"], "ID3": [{y1, y2}]} 
		//
		// In this work,
		// [{ 
		//	"EXO":
		//		[{
		//			"timeStamp": [...],
		//			"nodeCounter": [...],
		//			"mode": [...],
		//			"typeCon": [...],
		//			"speed": [...],
		//			"assist": [...],
		//			"notiNo": [...],
		//			"hip":
		//				[{
		//					"right":
		//						[{
		//							"feedback":
		//								[{
		//									"joint": [...],
		//									"torque": [...],
		//									"motor": [...]
		//								}],
		//							"control":
		//								[{
		//									"position": [...],
		//									"torque": [...],
		//									"stiffness": [...]
		//								}]
		//						}],
		//					"left":
		//						[{...}]					
		//				}],
		//			"knee":
		//				[{...}],
		//			"ankle"
		//				[{...}],
		//			"foot":
		//				[{
		//					"right":
		//						[{
		//							"heel": [...],
		//							"toe": [...]
		//						}],
		//					"left":
		//						[{
		//							"heel": [...],
		//							"toe": [...]
		//						}]
		//				}]
		//		}]
		//	"CPG":
		//		[{
		//			"AFDC":
		//				[{
		//					"right":
		//						[{
		//							"timeStampStr": [x],
		//							"nodeCounterStr": [x],
		//							"freq": [...],
		//							"CPG0": [...],
		//							"CPG1": [...],
		//							"CPG2": [...]
		//						}],
		//					"left":
		//						[{...}]
		//				}],
		//			"SO2Action":
		//				[{
		//					"right":
		//						[{
		//							"timeStampStr": [x],
		//							"nodeCounterStr": [x],
		//							"timeStampPhaseZero": [x],
		//							"nodeCounterPhaseZero": [x],
		//							"freq": [...],
		//							"CPG0": [...],
		//							"CPG1": [...],
		//							"CPG2": [...]
		//						}],
		//					"left":
		//						[{...}]
		//				}]
		//		}]
		// }]
		// objjWS[0] = [{...}]
		// objjWS[0]["EXO"] = [{"EXO": ...}]	
		Json::Value objjWS;
		// Hip ---
		Json::Value objjWSExoHip;
		// Knee ---
		Json::Value objjWSExoKnee;
		// Ankle ---
		Json::Value objjWSExoAnkle;
		// Foot ---
		Json::Value objjWSExoFoot;
		// CPG AFDC ---
		Json::Value objjWSCPGAFDC;
		// CPG SO2Learn ---
		Json::Value objjWSCPGSO2Action;
		
		// Json::Value stores as array 
		// Ex.
		//	jWS_nodeCounter_Out.append(x); x can be any datatype
		// 	For each prog. loop, each var. stores only one data at a time.
		// TIME:
		Json::Value jWS_nodeCounter_Out; // "nodeCounter"
		Json::Value jWS_timeStampCAN_Out; // "timeStamp"		
		// UI:
		Json::Value jWS_mode_Out; // "mode"
		Json::Value jWS_typeCon_Out; // "typeCon"
		Json::Value jWS_speedCon_Out; // "speed"
		Json::Value jWS_assistCon_Out; // "assist"
		Json::Value jWS_notiNo_Out; // "notiNo"
		// EXO:
			// Feedback signal - Right
		Json::Value jWS_rHipJ_FB_Out; // Right Hip joint angle
		Json::Value jWS_rKneeJ_FB_Out; // Right Knee joint angle
		Json::Value jWS_rAnkleJ_FB_Out; // Right Ankle joint angle
		Json::Value jWS_rHipT_FB_Out; // Right Hip torque
		Json::Value jWS_rKneeT_FB_Out; // Right Knee torque
		Json::Value jWS_rAnkleT_FB_Out; // Right Ankle torque
		Json::Value jWS_rHipM_FB_Out; // Right Hip motor torque
		Json::Value jWS_rKneeM_FB_Out; // Right Knee motor torque
		Json::Value jWS_rAnkleM_FB_Out; // Right Ankle motor torque
		Json::Value jWS_rFHeel_FB_Out; // Right Foot heel
		Json::Value jWS_rFToe_FB_Out; // Right Foot toe
			// Feedback signal - Left
		Json::Value jWS_lHipJ_FB_Out; 
		Json::Value jWS_lKneeJ_FB_Out; 
		Json::Value jWS_lAnkleJ_FB_Out; 
		Json::Value jWS_lHipT_FB_Out; 
		Json::Value jWS_lKneeT_FB_Out; 
		Json::Value jWS_lAnkleT_FB_Out; 
		Json::Value jWS_lHipM_FB_Out; 
		Json::Value jWS_lKneeM_FB_Out; 
		Json::Value jWS_lAnkleM_FB_Out;
		Json::Value jWS_lFHeel_FB_Out; 
		Json::Value jWS_lFToe_FB_Out; 
			// Control signal - Right
		Json::Value jWS_rHipP_Ctrl_Out; // Right Hip pos control
		Json::Value jWS_rKneeP_Ctrl_Out; // Right Knee pos control
		Json::Value jWS_rAnkleP_Ctrl_Out; // Right Ankle pos control
		Json::Value jWS_rHipT_Ctrl_Out; // Right Hip torque control
		Json::Value jWS_rKneeT_Ctrl_Out; // Right Knee torque control
		Json::Value jWS_rAnkleT_Ctrl_Out; // Right Ankle torque control
		Json::Value jWS_rHipS_Ctrl_Out; // Right Hip stiffness control
		Json::Value jWS_rKneeS_Ctrl_Out; // Right Knee stiffness control
		Json::Value jWS_rAnkleS_Ctrl_Out; // Right Ankle stiffness control		
			// Control signal - Left
		Json::Value jWS_lHipP_Ctrl_Out;
		Json::Value jWS_lKneeP_Ctrl_Out; 
		Json::Value jWS_lAnkleP_Ctrl_Out; 
		Json::Value jWS_lHipT_Ctrl_Out; 
		Json::Value jWS_lKneeT_Ctrl_Out; 
		Json::Value jWS_lAnkleT_Ctrl_Out; 
		Json::Value jWS_lHipS_Ctrl_Out; 
		Json::Value jWS_lKneeS_Ctrl_Out; 
		Json::Value jWS_lAnkleS_Ctrl_Out;
		// CPG:
			// AFDC
		Json::Value jWS_nodeAFDCGen_Out; // Start time step
		Json::Value jWS_timeAFDCGen_Out; // Start time	stamp
			// AFDC - Right
		Json::Value jWS_rCPG0AFDC_Out;
		Json::Value jWS_rCPG1AFDC_Out;
		Json::Value jWS_rCPG2AFDC_Out;
		Json::Value jWS_rCPGFreqAFDC_Out;
			// AFDC - Left
		Json::Value jWS_lCPG0AFDC_Out;
		Json::Value jWS_lCPG1AFDC_Out;
		Json::Value jWS_lCPG2AFDC_Out;
		Json::Value jWS_lCPGFreqAFDC_Out;
			// SO2 
		Json::Value jWS_nodeSO2ActionGen_Out; // Start time step	
		Json::Value jWS_timeSO2ActionGen_Out; // Start time stamp
		Json::Value jWS_nodeSO2ActionPhaseZero_Out; // Start time step at phase zero
		Json::Value jWS_timeSO2ActionPhaseZero_Out; // Start time stamp at phase zero
			// SO2 Action - Right
		Json::Value jWS_rCPG0SO2Action_Out;
		Json::Value jWS_rCPG1SO2Action_Out;
		Json::Value jWS_rCPG2SO2Action_Out;
		Json::Value jWS_rCPGFreqSO2Action_Out;
			// SO2 Action - Left
		Json::Value jWS_lCPG0SO2Action_Out;
		Json::Value jWS_lCPG1SO2Action_Out;
		Json::Value jWS_lCPG2SO2Action_Out;
		Json::Value jWS_lCPGFreqSO2Action_Out;


		// Variable and vectors to be used together with json ------
		// TIME:
		vector<int> nodeCounter_Out;
		vector<string> timeStampCAN_Out;
		// UI:
		vector<string> mode_Out;
		vector<int> typeCon_Out;
		vector<int> speedCon_Out;
		vector<int> assistCon_Out;
		vector<int> notiNo_Out;
		// EXO:
			// Feedback signal - Right
		vector<int> rHipJ_FB_Out;
		vector<int> rKneeJ_FB_Out;
		vector<int> rAnkleJ_FB_Out;
		vector<int> rHipT_FB_Out;
		vector<int> rKneeT_FB_Out;
		vector<int> rAnkleT_FB_Out;
		vector<int> rHipM_FB_Out;
		vector<int> rKneeM_FB_Out;
		vector<int> rAnkleM_FB_Out;
		vector<int> rFHeel_FB_Out;
		vector<int> rFToe_FB_Out;
			// Feedback signal - Left
		vector<int> lHipJ_FB_Out;
		vector<int> lKneeJ_FB_Out;
		vector<int> lAnkleJ_FB_Out;
		vector<int> lHipT_FB_Out;
		vector<int> lKneeT_FB_Out;
		vector<int> lAnkleT_FB_Out;
		vector<int> lHipM_FB_Out;
		vector<int> lKneeM_FB_Out;
		vector<int> lAnkleM_FB_Out;
		vector<int> lFHeel_FB_Out;
		vector<int> lFToe_FB_Out;
			// Control signal - Right
		vector<int> rHipP_Ctrl_Out;
		vector<int> rKneeP_Ctrl_Out;
		vector<int> rAnkleP_Ctrl_Out;
		vector<int> rHipT_Ctrl_Out;
		vector<int> rKneeT_Ctrl_Out;
		vector<int> rAnkleT_Ctrl_Out;
		vector<int> rHipS_Ctrl_Out;
		vector<int> rKneeS_Ctrl_Out;
		vector<int> rAnkleS_Ctrl_Out;
		vector<int> rFHeel_Ctrl_Out;
		vector<int> rFToe_Ctrl_Out;
			// Control signal - Left
		vector<int> lHipP_Ctrl_Out;
		vector<int> lKneeP_Ctrl_Out;
		vector<int> lAnkleP_Ctrl_Out;
		vector<int> lHipT_Ctrl_Out;
		vector<int> lKneeT_Ctrl_Out;
		vector<int> lAnkleT_Ctrl_Out;
		vector<int> lHipS_Ctrl_Out;
		vector<int> lKneeS_Ctrl_Out;
		vector<int> lAnkleS_Ctrl_Out;
		vector<int> lFHeel_Ctrl_Out;
		vector<int> lFToe_Ctrl_Out;
		// CPG:
			// AFDC
		int nodeAFDCGen_Out;
		string timeAFDCGen_Out;	
			// AFDC - Right
		vector<double> rCPG0AFDC_Out;
		vector<double> rCPG1AFDC_Out;
		vector<double> rCPG2AFDC_Out;
		vector<double> rCPGFreqAFDC_Out;
			// AFDC - Left
		vector<double> lCPG0AFDC_Out;
		vector<double> lCPG1AFDC_Out;
		vector<double> lCPG2AFDC_Out;
		vector<double> lCPGFreqAFDC_Out;
			// SO2
		int nodeSO2ActionGen_Out;
		string timeSO2ActionGen_Out;
		int nodeSO2ActionPhaseZero_Out;
		string timeSO2ActionPhaseZero_Out;
			// SO2 Action - Right
		vector<double> rCPG0SO2Action_Out;
		vector<double> rCPG1SO2Action_Out;
		vector<double> rCPG2SO2Action_Out;
		vector<double> rCPGFreqSO2Action_Out;
			// SO2 Action - Left
		vector<double> lCPG0SO2Action_Out;
		vector<double> lCPG1SO2Action_Out;
		vector<double> lCPG2SO2Action_Out;
		vector<double> lCPGFreqSO2Action_Out;
		// - To record exvisJSONSignal.json ----------------------------



		// + To record signals during Basic > Zero torque (Mode 131) in JSON ----------------------------
		// exvisJSONZeroTorque.json
	    // + JSON Writer -------------------
		char jsonWriteZeroTorque[100]; // To store path for write
		ofstream ofsjWZT;
		// - JSON Writer -------------------
		
		bool existJsonWriteZeroTorque = false;
		bool clearJArray_ZT = false;

		Json::Value objjWZT;
		// Hip ---
		Json::Value objjWZTExoHip;
		// Knee ---
		Json::Value objjWZTExoKnee;
		// Ankle ---
		Json::Value objjWZTExoAnkle;
		// Foot ---
		Json::Value objjWZTExoFoot;
		// CPG AFDC ---
		Json::Value objjWZTCPGAFDC;
		// CPG SO2Learn ---
		Json::Value objjWZTCPGSO2Action;
		
		// Json::Value stores as array 
		// Ex.
		//	jWZT_nodeCounter_Out.append(x); x can be any datatype
		// 	For each prog. loop, each var. stores only one data at a time.
		// TIME:
		Json::Value jWZT_nodeCounter_Out; // "nodeCounter"
		Json::Value jWZT_timeStampCAN_Out; // "timeStamp"		
		// UI:
		Json::Value jWZT_mode_Out; // "mode"
		Json::Value jWZT_typeCon_Out; // "typeCon"
		Json::Value jWZT_speedCon_Out; // "speed"
		Json::Value jWZT_assistCon_Out; // "assist"
		Json::Value jWZT_notiNo_Out; // "notiNo"
		// EXO:
			// Feedback signal - Right
		Json::Value jWZT_rHipJ_FB_Out; // Right Hip joint angle
		Json::Value jWZT_rKneeJ_FB_Out; // Right Knee joint angle
		Json::Value jWZT_rAnkleJ_FB_Out; // Right Ankle joint angle
		Json::Value jWZT_rHipT_FB_Out; // Right Hip torque
		Json::Value jWZT_rKneeT_FB_Out; // Right Knee torque
		Json::Value jWZT_rAnkleT_FB_Out; // Right Ankle torque
		Json::Value jWZT_rHipM_FB_Out; // Right Hip motor torque
		Json::Value jWZT_rKneeM_FB_Out; // Right Knee motor torque
		Json::Value jWZT_rAnkleM_FB_Out; // Right Ankle motor torque
		Json::Value jWZT_rFHeel_FB_Out; // Right Foot heel
		Json::Value jWZT_rFToe_FB_Out; // Right Foot toe
			// Feedback signal - Left
		Json::Value jWZT_lHipJ_FB_Out; 
		Json::Value jWZT_lKneeJ_FB_Out; 
		Json::Value jWZT_lAnkleJ_FB_Out; 
		Json::Value jWZT_lHipT_FB_Out; 
		Json::Value jWZT_lKneeT_FB_Out; 
		Json::Value jWZT_lAnkleT_FB_Out; 
		Json::Value jWZT_lHipM_FB_Out; 
		Json::Value jWZT_lKneeM_FB_Out; 
		Json::Value jWZT_lAnkleM_FB_Out;
		Json::Value jWZT_lFHeel_FB_Out; 
		Json::Value jWZT_lFToe_FB_Out; 
			// Control signal - Right
		Json::Value jWZT_rHipP_Ctrl_Out; // Right Hip pos control
		Json::Value jWZT_rKneeP_Ctrl_Out; // Right Knee pos control
		Json::Value jWZT_rAnkleP_Ctrl_Out; // Right Ankle pos control
		Json::Value jWZT_rHipT_Ctrl_Out; // Right Hip torque control
		Json::Value jWZT_rKneeT_Ctrl_Out; // Right Knee torque control
		Json::Value jWZT_rAnkleT_Ctrl_Out; // Right Ankle torque control
		Json::Value jWZT_rHipS_Ctrl_Out; // Right Hip stiffness control
		Json::Value jWZT_rKneeS_Ctrl_Out; // Right Knee stiffness control
		Json::Value jWZT_rAnkleS_Ctrl_Out; // Right Ankle stiffness control		
			// Control signal - Left
		Json::Value jWZT_lHipP_Ctrl_Out;
		Json::Value jWZT_lKneeP_Ctrl_Out; 
		Json::Value jWZT_lAnkleP_Ctrl_Out; 
		Json::Value jWZT_lHipT_Ctrl_Out; 
		Json::Value jWZT_lKneeT_Ctrl_Out; 
		Json::Value jWZT_lAnkleT_Ctrl_Out; 
		Json::Value jWZT_lHipS_Ctrl_Out; 
		Json::Value jWZT_lKneeS_Ctrl_Out; 
		Json::Value jWZT_lAnkleS_Ctrl_Out;
		// CPG:
			// AFDC
		Json::Value jWZT_nodeAFDCGen_Out; // Start time step
		Json::Value jWZT_timeAFDCGen_Out; // Start time	stamp
			// AFDC - Right
		Json::Value jWZT_rCPG0AFDC_Out;
		Json::Value jWZT_rCPG1AFDC_Out;
		Json::Value jWZT_rCPG2AFDC_Out;
		Json::Value jWZT_rCPGFreqAFDC_Out;
			// AFDC - Left
		Json::Value jWZT_lCPG0AFDC_Out;
		Json::Value jWZT_lCPG1AFDC_Out;
		Json::Value jWZT_lCPG2AFDC_Out;
		Json::Value jWZT_lCPGFreqAFDC_Out;
			// SO2 
		Json::Value jWZT_nodeSO2ActionGen_Out; // Start time step	
		Json::Value jWZT_timeSO2ActionGen_Out; // Start time stamp
		Json::Value jWZT_nodeSO2ActionPhaseZero_Out; // Start time step at phase zero
		Json::Value jWZT_timeSO2ActionPhaseZero_Out; // Start time stamp at phase zero
			// SO2 Action - Right
		Json::Value jWZT_rCPG0SO2Action_Out;
		Json::Value jWZT_rCPG1SO2Action_Out;
		Json::Value jWZT_rCPG2SO2Action_Out;
		Json::Value jWZT_rCPGFreqSO2Action_Out;
			// SO2 Action - Left
		Json::Value jWZT_lCPG0SO2Action_Out;
		Json::Value jWZT_lCPG1SO2Action_Out;
		Json::Value jWZT_lCPG2SO2Action_Out;
		Json::Value jWZT_lCPGFreqSO2Action_Out;

				// Variable and vectors to be used together with json ------
		// TIME:
		vector<int> nodeCounter_ZT_Out;
		vector<string> timeStampCAN_ZT_Out;
		// UI:
		vector<string> mode_ZT_Out;
		vector<int> typeCon_ZT_Out;
		vector<int> speedCon_ZT_Out;
		vector<int> assistCon_ZT_Out;
		vector<int> notiNo_ZT_Out;
		// EXO:
			// Feedback signal - Right
		vector<int> rHipJ_FB_ZT_Out;
		vector<int> rKneeJ_FB_ZT_Out;
		vector<int> rAnkleJ_FB_ZT_Out;
		vector<int> rHipT_FB_ZT_Out;
		vector<int> rKneeT_FB_ZT_Out;
		vector<int> rAnkleT_FB_ZT_Out;
		vector<int> rHipM_FB_ZT_Out;
		vector<int> rKneeM_FB_ZT_Out;
		vector<int> rAnkleM_FB_ZT_Out;
		vector<int> rFHeel_FB_ZT_Out;
		vector<int> rFToe_FB_ZT_Out;
			// Feedback signal - Left
		vector<int> lHipJ_FB_ZT_Out;
		vector<int> lKneeJ_FB_ZT_Out;
		vector<int> lAnkleJ_FB_ZT_Out;
		vector<int> lHipT_FB_ZT_Out;
		vector<int> lKneeT_FB_ZT_Out;
		vector<int> lAnkleT_FB_ZT_Out;
		vector<int> lHipM_FB_ZT_Out;
		vector<int> lKneeM_FB_ZT_Out;
		vector<int> lAnkleM_FB_ZT_Out;
		vector<int> lFHeel_FB_ZT_Out;
		vector<int> lFToe_FB_ZT_Out;
			// Control signal - Right
		vector<int> rHipP_Ctrl_ZT_Out;
		vector<int> rKneeP_Ctrl_ZT_Out;
		vector<int> rAnkleP_Ctrl_ZT_Out;
		vector<int> rHipT_Ctrl_ZT_Out;
		vector<int> rKneeT_Ctrl_ZT_Out;
		vector<int> rAnkleT_Ctrl_ZT_Out;
		vector<int> rHipS_Ctrl_ZT_Out;
		vector<int> rKneeS_Ctrl_ZT_Out;
		vector<int> rAnkleS_Ctrl_ZT_Out;
		vector<int> rFHeel_Ctrl_ZT_Out;
		vector<int> rFToe_Ctrl_ZT_Out;
			// Control signal - Left
		vector<int> lHipP_Ctrl_ZT_Out;
		vector<int> lKneeP_Ctrl_ZT_Out;
		vector<int> lAnkleP_Ctrl_ZT_Out;
		vector<int> lHipT_Ctrl_ZT_Out;
		vector<int> lKneeT_Ctrl_ZT_Out;
		vector<int> lAnkleT_Ctrl_ZT_Out;
		vector<int> lHipS_Ctrl_ZT_Out;
		vector<int> lKneeS_Ctrl_ZT_Out;
		vector<int> lAnkleS_Ctrl_ZT_Out;
		vector<int> lFHeel_Ctrl_ZT_Out;
		vector<int> lFToe_Ctrl_ZT_Out;
		// CPG:
			// AFDC
		int nodeAFDCGen_ZT_Out;
		string timeAFDCGen_ZT_Out;	
			// AFDC - Right
		vector<double> rCPG0AFDC_ZT_Out;
		vector<double> rCPG1AFDC_ZT_Out;
		vector<double> rCPG2AFDC_ZT_Out;
		vector<double> rCPGFreqAFDC_ZT_Out;
			// AFDC - Left
		vector<double> lCPG0AFDC_ZT_Out;
		vector<double> lCPG1AFDC_ZT_Out;
		vector<double> lCPG2AFDC_ZT_Out;
		vector<double> lCPGFreqAFDC_ZT_Out;
			// SO2
		int nodeSO2ActionGen_ZT_Out;
		string timeSO2ActionGen_ZT_Out;
		int nodeSO2ActionPhaseZero_ZT_Out;
		string timeSO2ActionPhaseZero_ZT_Out;
			// SO2 Action - Right
		vector<double> rCPG0SO2Action_ZT_Out;
		vector<double> rCPG1SO2Action_ZT_Out;
		vector<double> rCPG2SO2Action_ZT_Out;
		vector<double> rCPGFreqSO2Action_ZT_Out;
			// SO2 Action - Left
		vector<double> lCPG0SO2Action_ZT_Out;
		vector<double> lCPG1SO2Action_ZT_Out;
		vector<double> lCPG2SO2Action_ZT_Out;
		vector<double> lCPGFreqSO2Action_ZT_Out;
		// - To record signals during Basic > Zero torque (Mode 131) in JSON ----------------------------




		// + To record signals during Adaptive > Pattern Pos (Mode 23) in JSON ----------------------------
	    // + JSON Writer -------------------
		// char jsonWriteAdapPatPosAction[100]; // To store path for write
		// ofstream ofsjAPPA;
		// // - JSON Writer -------------------

		// bool existJsonWriteAdapPatPosAction = false;
		// bool clearOfsjAPPA = false;

		// Json::Value objjAPPA; // json obj/record
		


		// - To record signals during Adaptive > Pattern Pos (Mode 23) in JSON ----------------------------




		// ----------------------------------------------
		// Private Methods
		// ----------------------------------------------
		void jsonWriteSignalFn(); // put into json format
		void logJsonWriteSignalFn(); // record to vector
		void jsonWriteZeroTorqueFn();
		void logJsonWriteZeroTorqueFn();
		void clearVarZeroTorqueFn();
		bool exists_File(const std::string & name);
		
};

#endif //EXVISPARALLELREALRWASSEM_H
