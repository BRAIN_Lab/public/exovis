/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#include "ExvisParallelRealNotiAssem.h"

//***************************************************************************

//--------------------------------------------------------------------------
// initializer
//--------------------------------------------------------------------------
ExvisParallelRealNotiAssem::ExvisParallelRealNotiAssem(int argc,char* argv[]){

	// ROS related
    realRos = new ExvisParallelRealNotiROS(argc, argv);



	if(ros::ok()) {
    	cout << "Initialize ExvisParallelRealNotiAssem" << endl;
    }

}

//--------------------------------------------------------------------------
// runAssem
//--------------------------------------------------------------------------
bool ExvisParallelRealNotiAssem::runAssem() {

	if(ros::ok()) {
		// cout << "********************************************************" << endl;
		// cout << "ExvisParallelRealNotiAssem is running" << endl;
		// cout << "********************************************************" << endl;
		cout << nodeCounter << endl;


		// + Pub/Sub area --------------------------------
		// cout << "--------------- Published ---------------" << endl;
		// clock_gettime(CLOCK_REALTIME, &tNode);
		// cout << "tPublished: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		

        switch (realRos->notiNo) {
            case 1 : 
                if (!showOnce1) {
                    cout << "! FINISH LEARNING READY TO ACTION WHEN SAFE" << endl;
                    cout << "PRESS ENTER TO PROCESS FURTHER ..." << endl;
                    showOnce1 = true;
                }
                if(cin.get() == '\n'){
					showOnce1 = false;
					notiNo_1_done = true;
					realRos->sendNotiDone(notiNo_1_done, realRos->notiNo);
			    } else {
					notiNo_1_done = false;
					realRos->sendNotiDone(notiNo_1_done, realRos->notiNo);
				}
                
                break;
			
			default : // i.e., notiNo = 0
				cout << "The program is running normally." << endl;
        }


		// - Pub/Sub area --------------------------------
		
		

		// + Node spin to have sub value --------------------------------
		// cout << "--------------- Subscribed ---------------" << endl;
		clock_gettime(CLOCK_REALTIME, &tNode);
		// cout << "tSubscribed: " << to_string(tNode.tv_sec + tNode.tv_nsec/1e9) << endl;
		realRos->rosSpinOnce();
		// - Node spin to have sub value --------------------------------

		nodeCounter++;
		cout << "********************************************************" << endl;
		return true;

	} else {
		cout << "Shutting down the node" << endl;
		return false;
	}
}





ExvisParallelRealNotiAssem::~ExvisParallelRealNotiAssem(){

}
