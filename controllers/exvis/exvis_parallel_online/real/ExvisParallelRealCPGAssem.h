/***************************************************************************
 *   Copyright (C) 2019 by BRAIN, VISTEC	                               *
 *                                    									   *
 *    chaicharn.a_s17@vistec.ac.th                                         *
 *    																	   *
 *   LICENSE:                                                              *
 *   This work is licensed under the Creative Commons                      *
 *   Attribution-NonCommercial-ShareAlike 2.5 License. To view a copy of   *
 *   this license, visit http://creativecommons.org/licenses/by-nc-sa/2.5/ *
 *   or send a letter to Creative Commons, 543 Howard Street, 5th Floor,   *
 *   San Francisco, California, 94105, USA.                                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                         *
 ***************************************************************************/

//***************************************************************************
// LIBRARY
//***************************************************************************

#ifndef EXVISPARALLELREALCPGASSEM_H
#define EXVISPARALLELREALCPGASSEM_H

#include <stdio.h> // standard input / output functions
#include <unistd.h> // standard function definitions
#include <stdlib.h> // variable types, macros, functions
#include <string.h> // string function definitions
#include <iostream> // C++ to use cout
#include <fstream> // To read/write on file
#include <vector>
#include <map>
#include <queue>

using namespace std;

#include "ExvisParallelRealCPGROS.h"
#include "DynamicCpg.h"

//***************************************************************************
class ExvisParallelRealCPGROS;

class ExvisParallelRealCPGAssem {
	public:
		ExvisParallelRealCPGAssem(int argc,char* argv[]);
		~ExvisParallelRealCPGAssem();
		bool runAssem();




	private:
		int nodeCounter = 0;
		timespec tNode;

		ExvisParallelRealCPGROS * realRos;

		// Right
		double meanrCPGFreqRW = 0;
		double meanrCPGTimeRW = 0;
		// Left
		double meanlCPGFreqRW = 0;
		double meanlCPGTimeRW = 0;

		double avgTimeBothLegs;


		// ***************************************
		// * Please change here
		// ***************************************
//		bool oneCPG = true; // one CPG
		bool oneCPG = false; // two CPG
		// ***************************************

		// Common -----------------------
		int CPGphase;
//		double cpgFreqScale = 10; // This number is to convert cpgFreq = phi/2*pi to real world freq.
		double cpgFreqScale = 100;
		double internalrFreqCPG = 0.01; // Right; To set internal freq. when using as SO2
		double internallFreqCPG = 0.01; // Left; To set internal freq. when using as SO2

		// AFDC -------------------------
		bool flagAssemStrAFDC = false;
		bool flagAssemUpdateAFDC = false;
		DynamicCpg *dinRightAFDC, *dinLeftAFDC, *dinLeft180AFDC;
		bool checkStartPer = true;
		double fbDerVal = 0;
		double fbOld = 0;
		double fbNew = 0;
		double fbDerValCounter = 0;
		bool applyPer = false;

		// SO2 Learn ------
		bool flagAssemStrSO2Learn = false;
		bool flagAssemUpdateSO2Learn = false;
		DynamicCpg *dinRightSO2Learn, *dinLeftSO2Learn, *dinLeft180SO2Learn;

		// SO2 Action -----
		bool flagAssemStrSO2Action = false;
		bool flagAssemUpdateSO2Action = false;
		DynamicCpg *dinRightSO2Action, *dinLeftSO2Action, *dinLeft180SO2Action;

		// For adjusting freq of CPG ----
		bool setCurrentFreqFirstTime = false;
		double rCurrentFreq = 0.01; // Current freq. internal
		double lCurrentFreq = 0.01; // Current freq. internal
		double rOnlineFreq = 0.01; // Online freq. internal
		double lOnlineFreq = 0.01; // Online freq. internal
		double learningRateFreq = 1; // Learning rate to combine current and online freq
		int cnt_incFreq = 0; // To count timestep to increase CPG freq
		bool stopIncFreq = false;
		double freqManual ; // // Manual vs. Online freq; e.g., 0.5 Hz


		// CPG Signal ---------
		// AFDC
		double rightLegFreqAFDC; // Internal freq unit
		double rightCpgO0AFDC;
		double rightCpgO1AFDC;
		double rightCpgO2AFDC;
		double leftLegFreqAFDC; // Internal freq unit
		double leftCpgO0AFDC;
		double leftCpgO1AFDC;
		double leftCpgO2AFDC;
		// SO2 Learn
		double rightLegFreqSO2Learn; // Internal freq unit
		double rightCpgO0SO2Learn;
		double rightCpgO1SO2Learn;
		double rightCpgO2SO2Learn;
		double leftLegFreqSO2Learn; // Internal freq unit
		double leftCpgO0SO2Learn;
		double leftCpgO1SO2Learn;
		double leftCpgO2SO2Learn;
		// SO2 Action
		double rightLegFreqSO2Action; // Internal freq unit
		double rightCpgO0SO2Action;
		double rightCpgO1SO2Action;
		double rightCpgO2SO2Action;
		double leftLegFreqSO2Action; // Internal freq unit
		double leftCpgO0SO2Action;
		double leftCpgO1SO2Action;
		double leftCpgO2SO2Action;


		// Method ----------------------------------------------
		bool updateAFDC(int rightHip, int leftHip, int nodeCounter, double timeStep);
		bool updateSO2Learn(double intrFreqCPG, double intlFreqCPG, int nodeCounter, double timeStep, int CPGphase);
		bool updateSO2Action(double intrFreqCPG, double intlFreqCPG, int nodeCounter, double timeStep, int CPGphase);

};

#endif //EXVISPARALLELREALCPGASSEM_H
